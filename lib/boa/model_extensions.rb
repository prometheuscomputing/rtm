require_relative 'umm_extensions'
module UmlMetamodel
  class Classifier
    alias_attribute(:description, :documentation)
    # derived_attribute :description, String
    # def description
    #   documentation
    # end
    
    def base_uml
      self
    end
    
    def all_properties
      parents.collect { |parent| parent.all_properties }.flatten + properties
    end
  end
end

# Does a property restriction mean it must have it and can't have any other properties?  Or that it must have it and other properties are allowed?  Or what?  We probably need some means of identifying the different cases / possibilities and also need to decide what the default behavior is.
module Boa
  DEVICE_SPECIFICATION_ID = Boa::SpecificationType.where(:value => 'Device Specification').first.id
  
  class ClassifierRestriction
    def to_xml(xml, card = 1)
      args = {:refid => refids_string}
      args[:card] = card unless card.to_i == 1 || card == '1..1'
      args[:e] = enum_refids if enums.any?
      args[:u] = unit_refids if units.any?
      xml.send(xml_tag, args) {
        included_profiles.each do |inclusion|
          unless Rosetta.built_modules.include?(inclusion)
            inclusion.to_inclusion_xml(xml)
            Rosetta.built_modules << inclusion
          end
          xml.include(:module => inclusion.short_name)
        end
        associations.each { |prop| prop.to_xml(xml) }
      }
    end
    
    def associations
      properties.select { |prop| prop.is_a?(Boa::AssociationRestriction) }
    end
    
    def enum_refids
      enums.map do |enum|
        if enum.is_a?(RTM::Token)
          enum.value
        else
          enum.refid.value
        end
      end.join(' ')
    end
    
    def unit_refids
      units.map { |unit| unit.refid.value }.join(' ')
    end
    
    def xml_tag
      tag = base_uml.name.downcase.to_sym
      if tag == :metric || tag == :facet
        metric_type_tag = metric_type_property&.constraints&.first&.expression&.downcase&.to_sym
        tag = metric_type_tag if metric_type_tag
      end
      tag
    end
    
    def metric_type_property
      properties.find { |prop| prop.base == DeviceProfiles::RCP_Builder::METRIC_TYPE_PROPERTY }
    end
    
    def refids_string
      terms.map(&:refids).join(' ')
    end
    
    derived_attribute :info, String    
    def info
      "#{base_uml.name.demodulize}: #{refids_string}"
    end
    
    def to_title
      return 'New Profile' if new?
      rs = refids_string
      if base
        "Profiled #{base_uml.name}: #{rs.empty? ? '<no term(s) set' : rs}"
      else
        "Profiled <please set profiled classifier!>: #{rs.empty? ? '<no term(s) set' : rs}"
      end
    end
      
    # def to_xml(indent = 0)
    #   tag = base_uml.name
    #   if description && !description.empty?
    #     xml = ["#{'  '*indent}<#{tag} description=#{description.inspect}>"]
    #   else
    #     xml = ["#{'  '*indent}<#{tag}>"]
    #   end
    #   attrs, assocs = properties.partition { |prop| prop.is_a?(Boa::AttributeRestriction) }
    #   attrs.each  { |a| xml << a.to_xml(indent + 1) }
    #   assocs.each { |a| xml << a.to_xml(indent + 1) }
    #   xml << "#{'  '*indent}</#{tag}>"
    #   xml.join("\n")
    # end
    
    def after_change_tracker_create
      self.author = Ramaze::Current.session[:user] if defined?(Ramaze)
      save
      super
    end
    
    derived_attribute :qualified_name, String
    def qualified_name
      # Use description as name?
      # ^^ No. The description information is available / displayable elsewhere.
      # Only show UML base? Then base_uml needed to differentiate (CR can restrict another CR without base)
      if base_uml
        "Restriction on #{_qualified_base_name}"
      else
        'NO UML BASE'
      end
    end
    
    derived_attribute :restriction_base_display_name, String
    def restriction_base_display_name
      ret = qualified_name
      ret << ": #{description}" if description&.[](0)
      ret
    end
    
    def _qualified_base_name
      return unless base
      base.is_a?(UmlMetamodel::Classifier) ? base.restriction_base_display_name : base._qualified_base_name
    end
    
    def base_uml
      return unless base
      base.base_uml
    end
    
    def available_properties
      base_uml.all_properties
    end
    
    def available_associations
      assocs = available_properties.select { |prop| prop.association_count == 1 }
      # Reject inverse associations (no part of original BICEPS)
      assocs.reject { |prop| prop.name =~ /\Ainv_/ }
    end
    
    def available_attributes
      available_properties.select { |prop| prop.association_count == 0 }
    end
    
    # FIXME not everything is guaranteed to have a rolename...I think.  Hence the #compact below.
    derived_attribute :available_property_names, String
    def available_property_names
      available_properties.collect(&:name).compact.sort.join(', ')
    end
    
    # FIXME not everything is guaranteed to have a rolename...I think.  Hence the #compact below.
    derived_attribute :available_association_names, String
    def available_association_names
      '<strong>Restrictable Base Properties:</strong> ' + available_associations.collect(&:name).compact.sort.join(', ')
    end
    
    derived_attribute :available_attribute_names, String
    def available_attribute_names
      '<strong>Restrictable Base Properties:</strong> ' + available_attributes.collect(&:name).collect(&:to_s).sort.join(', ')
    end
    
    derived_attribute :available_attribute_names, String
    def available_attribute_names
      restricts.available_attribute_names
    end
    
    # Define co_owners derived association ------------------------------------
    base_methods = [
     :base_type,
     :type_of_base,
     :remove_base,
     :remove_from_base,
     :base_remove,
     :remove_all_base,
     :base_remove_all,
     :base,
     # :base_unassociated,
     :base_count,
     # :base_unassociated_count,
     # :base_unassociated_query,
     # :unassoc_get_many_query_base,
     :base_add,
     :base=,
     :base_is_polymorphic?,
     :base_info,
     :base_opp_info,
     # :base_query,
     # :get_many_query_base
    ]
    derived_association :allowed_base, 'Boa::RestrictionBase', :type => :many_to_one
    base_methods.each { |m| alias_method("allowed_#{m}".to_sym, m) }
    def allowed_base_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      puts Rainbow(filter.inspect).cyan
      caller[0..4].each {|x| puts x}; puts
      ba = base_unassociated(filter, limit, offset, order)#.reject{ |b| base_chain_ids(b).include?(self.id)}
      # limit ? ba[offset..offset+limit-1] : ba
      puts "Count #{ba.count}"
      ba
    end
    def allowed_base_unassociated_count(*args)    
      allowed_base_unassociated(*args).count
    end
    def base_chain_ids(base)
      return [] unless base
      (base.respond_to?(:base) && base.base) ? [base.id] + base_chain_ids(base.base) : [base.id] 
    end
    
    derived_attribute(:set_tree_view_root, ::NilClass)
    def set_tree_view_root
      puts "CALLED SET TREEVIEWROOT for #{self.class.name}"
      Gui::Controller.session[:tree_view_roots] ||= []
      Gui::Controller.session[:tree_view_roots].unshift(self)
      Gui::Controller.session[:tree_view_roots].uniq!
    end
    
    # This updates the tree view root if changes are saved (it isn't updated correctly otherwise)
    # TODO set_tree_view_root is sometimes called twice. why?
    def after_change_tracker_save
      if defined?(Gui::Controller)
        set_tree_view_root if Gui::Controller.session[:tree_view_roots]&.first == self
      end
      super
    end
    
  end
  
  class PropertyRestriction
    derived_attribute :qualified_name, String
    def qualified_name
      return unless base
      base.qualified_name
    end
    
    derived_attribute :property_restriction_display_name, String
    def property_restriction_display_name
      qualified_name
    end
    
    derived_attribute :property_restriction_info, String
    
    derived_attribute :multiplicity_string, String
    def multiplicity_string
      if upper
        "#{lower || 0}..#{upper}"
      else
        "#{lower || 1}"
      end
    end
  end
  
  class AttributeRestriction
    # FIXME
    def to_xml(xml)
      raise 'AttributeRestriction#to_xml is not implemented!'
    end
    
    def property_restriction_info
      qualified_name
    end
    #
    # def to_xml(indent = 0)
    #   tag = base.name
    #   if description && !description.empty?
    #     xml = ["#{'  '*indent}<#{tag} min=#{lower || 1} max=#{upper || 1} description=#{description.inspect}>"]
    #   else
    #     xml = ["#{'  '*indent}<#{tag} min=#{lower || 1} max=#{upper || 1}>"]
    #   end
    #   xml << "#{'  '*(indent+1)}<constraints>"
    #   constraints.each { |c| xml << c.to_xml(indent + 2) }
    #   xml << "#{'  '*(indent+1)}</constraints>"
    #   xml << "#{'  '*indent}</#{tag}>"
    #   xml.join("\n")
    # end
    #
    derived_attribute :available_attribute_names, String
    def available_attribute_names
      restricts.available_attribute_names
    end
    
    # Define co_owners derived association ------------------------------------
    base_methods = [
     :base_type,
     :type_of_base,
     :remove_base,
     :remove_from_base,
     :base_remove,
     :remove_all_base,
     :base_remove_all,
     :base,
     # :base_unassociated,
     :base_count,
     # :base_unassociated_count,
     # :base_unassociated_query,
     # :unassoc_get_many_query_base,
     :base_add,
     :base=,
     :base_is_polymorphic?,
     :base_info,
     :base_opp_info,
     # :base_query,
     # :get_many_query_base
    ]
    derived_association :allowed_base, 'UmlMetamodel::Property', :type => :many_to_one
    base_methods.each { |m| alias_method("allowed_#{m}".to_sym, m) }
    def allowed_base_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      # TODO If newly created restriction, restricts is not defined and self.id is nil -> allowed_base can't be determined 
      return [] unless restricts&.base_uml # TODO Advise user to first choose a base for the associated ClassifierRestriction - how to display?
      
      aa = restricts.available_attributes
      limit ? aa[offset..offset+limit-1] : aa
    end
    def allowed_base_unassociated_count(*args)    
      restricts&.base_uml ? restricts.available_attributes.count : 0
    end
  end
  
  class AssociationRestriction
    def to_xml(xml)
      if type_restrictions_count < 1
        xml.send(base.name.downcase.to_sym, :card => multiplicity_string)
      elsif type_restrictions_count == 1
        type_restrictions.first.to_xml(xml, multiplicity_string)
      else
        # TODO implement some sort of choice mechanism
      end
    end
    
    def to_title
      # "#{base&.qualified_name}<br>#{type_restrictions.map(&:to_title).join('; ')}"
      "#{type_restrictions.map(&:to_title).join('; ')}"
    end
    
    def property_restriction_info
      to_title
    end
    
    # def to_xml(indent = 0)
    #   tag = base.name
    #   if description && !description.empty?
    #     xml = ["#{'  '*indent}<#{tag} min=#{lower || 1} max=#{upper || 1} description=#{description.inspect}>"]
    #   else
    #     xml = ["#{'  '*indent}<#{tag} min=#{lower || 1} max=#{upper || 1}>"]
    #   end
    #   type_restrictions.each { |tr| xml << tr.to_xml(indent + 1) }
    #   xml << "#{'  '*indent}</#{tag}>"
    #   xml.join("\n")
    # end
    #
    derived_attribute :available_association_names, String
    def available_association_names
      restricts.available_association_names
    end
    
    # Define co_owners derived association ------------------------------------
    base_methods = [
     :base_type,
     :type_of_base,
     :remove_base,
     :remove_from_base,
     :base_remove,
     :remove_all_base,
     :base_remove_all,
     :base,
     # :base_unassociated,
     :base_count,
     # :base_unassociated_count,
     # :base_unassociated_query,
     # :unassoc_get_many_query_base,
     :base_add,
     :base=,
     :base_is_polymorphic?,
     :base_info,
     :base_opp_info,
     # :base_query,
     # :get_many_query_base
    ]
    derived_association :allowed_base, 'UmlMetamodel::Property', :type => :many_to_one
    base_methods.each { |m| alias_method("allowed_#{m}".to_sym, m) }
    def allowed_base_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      # TODO If newly created restriction, restricts is not defined and self.id is nil -> allowed_base can't be determined 
      return [] unless restricts&.base_uml # TODO Advise user to first choose a base for the associated ClassifierRestriction - how to display?
      
      aa = restricts.available_associations
      limit ? aa[offset..offset+limit-1] : aa
    end
    def allowed_base_unassociated_count(*args)    
      restricts&.base_uml ? restricts.available_associations.count : 0
    end
    
    def type_restrictions_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      return [] unless base
      base_type = base.type
      puts base_type.inspect
      type_filters = []
      if filter.is_a?(Array)
        filter.each {|f| (type_filters << f.delete('type')) if f.is_a?(Hash) }
        filter.delete({})
      else
        (type_filters << filter.delete('type')) if filter.is_a?(Hash)
      end
      
      all_people = (People::Person.filter(filter).all +
       Automotive::Mechanic.filter(filter).all +
       People::Clown::Clown.filter(filter).all -
       co_owners(filter) - [self]
      )
      limit ? all_people[offset..offset+limit-1] : all_people
    end
  end
  
  class ValueConstraint
    def to_xml(indent)
      tag = 'constraint'
      if expression_kind&.value
        xml = ["#{'  '*indent}<#{tag} type=#{expression_kind.value.inspect} value=#{expression.inspect}>"]
      else
        xml = ["#{'  '*indent}<#{tag} value=#{expression.inspect}>"]
      end
      xml
    end
  end
end
