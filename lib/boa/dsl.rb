require 'cleanroom'
require_relative 'find'
require_relative 'umm'
load File.join(__dir__, 'find.rb')
load File.join(__dir__, 'umm_extensions.rb')

# require_relative 'elements'
module Boa
  
  # ----- Ruby DSL deserialization code -----
  def self.from_dsl(dsl, file_path = nil, options = {})
    Boa::DSL.parse(dsl, file_path, options)
  end

  def self.from_dsl_file(file_path, options = {})
    path = File.expand_path(file_path)
    self.from_dsl(File.read(path), path, options)
  end
  
  # Create them if they aren't there already.
  # There is also a way to snag the primitives that are already 
  def self.uml_primitives
    prims = uml_package.contents.select { |c| c.is_a?(UmlMetamodel::Primitive) }
    names = ['Boolean', 'String', 'Integer', 'Float', 'Real', 'Date', 'ByteString']
    additional_xml_primitives= ['Uri', 'Datetime']
    ret = {}
    (names + additional_xml_primitives).each do |name|
      sequel_model = prims.find { |prim| prim.name == name }
      unless sequel_model
        ChangeTracker.start
        sequel_model = UmlMetamodel::Primitive.create(:name => name)
        ChangeTracker.commit
      end
      ret["UML::#{name}"] = sequel_model
    end
    ret
  end
  
  def self.uml_package
    pkg = UmlMetamodel::Package.where(:name => 'UML').first
    unless pkg
      ChangeTracker.start
      pkg = UmlMetamodel::Package.create(:name => 'UML')
      ChangeTracker.commit
    end
    pkg
  end
  
  class DSL
    include Cleanroom
    attr_accessor :top_level_elements
    attr_accessor :guid_registry
    
    def self.parse(dsl, file_path = nil, options = {})
      file_path ||= 'UmlMetamodel::DSL' # file_path is passed to evaluate so that it can be referenced in stack traces...
      dsl_instance = self.new(options)
      dsl_instance.evaluate(dsl, file_path, 1)
      dsl_instance.set_deferred_reference_properties
      top_level_elements = dsl_instance.top_level_elements
      # Return singular top level element if there is only a single project in the array
      if top_level_elements.length == 1 && top_level_elements.first.is_a?(UmlMetamodel::Project)
        top_level_elements.first 
      else
        top_level_elements
      end
    end
    
    def initialize(options = {})
      # An array of the top level elements described by the DSL
      @top_level_elements = []
      @registry = Hash.new.merge(Boa.uml_primitives)
      @elements = []
      # A 2-dimensional array of objects, a property, a value (or values) to dereference and apply, and
      #   optionally, a type that the reference must match
      # e.g. [<UmlMetamodel::Class Foo>, :parents, ["Bar"], UmlMetamodel::Classifier]
      @properties_to_dereference = []
      # An array representing the context stack
      @context = [@top_level_elements]
      # DSL parsing options
      #   :suppress_warnings => Wether or not to suppress warnings created during DSL parsing
      @options = options
    end
    
    def find_element_by_qualified_name(qname)
      @elements.find do |e|
        e.qualified_name == qname
      end
    end
    
    # Resolve reference properties that could not be resolved earlier.
    # This method is run after all DSL entities have been processed, so it can be assumed
    # that all references should be resolvable.
    def set_deferred_reference_properties
      @properties_to_dereference.dup.each do |element, property, value, type|
        
        val = dereference_value(value, type)
        if val.nil? || (value.is_a?(Array) && val.count != value.count) 
          name = element.respond_to?(:qualified_name) ? element.qualified_name : element.respond_to?(:name) ? element.name : element.instance_of&.name
          puts Rainbow("Warning: could not dereference: #{name}(#{element.class}) :#{property} => #{value.inspect}.  val was #{val.inspect}").magenta
          next
        end
        ChangeTracker.start
        element.send("#{property}=", val)
        ChangeTracker.commit
      end
    end
    
    def project(name, opts = {}, &block)
      element(UmlMetamodel::Project, name, nil, nil, {}, opts, &block)
    end
    expose :project
    
    def _package(type, name, opts, &block)
      references = {
        :imports => nil,
        :imported_by => nil
      }
      element(type, name, :add_content, nil, references, opts, &block)
    end
    
    def package(name, opts = {}, &block)
      _package(UmlMetamodel::Package, name, opts, &block)
    end
    expose :package
    
    def model(name, opts = {}, &block)
      _package(UmlMetamodel::Model, name, opts, &block)
    end
    expose :model
        
    def profile(name, opts = {}, &block)
      _package(UmlMetamodel::Profile, name, opts, &block)
    end
    expose :profile
    
    # Concrete classifier types
    def klass(name, opts = {}, &block)
      # defaults
      options = {:is_singleton => false}
      options.merge!(opts)
      classifier(UmlMetamodel::Class, name, options, &block)
    end
    expose :klass
    
    def interface(name, opts = {}, &block)
      classifier(UmlMetamodel::Interface, name, opts, &block)
    end
    expose :interface
    
    def datatype(name, opts = {}, &block)
      classifier(UmlMetamodel::Datatype, name, opts, &block)
    end
    expose :datatype
    
    def primitive(name, opts = {}, &block)
      classifier(UmlMetamodel::Primitive, name, opts, &block)
    end
    expose :primitive
    
    def enumeration(name, opts = {}, &block)
      # Set value_type to nil to prevent automatic creation of value property, 
      # which will be described in the DSL
      classifier(UmlMetamodel::Enumeration, name, opts, &block)
    end
    expose :enumeration
    
    def property(name, opts = {}, &block)
      # defaults
      options = {:is_navigable => true, :is_unique => false, :is_derived => false, :is_ordered => false, :lower => 0, :upper => 1, :aggregation => 'none', :visibility => 'private'}
      options.merge!(opts)
      references = {:type => UmlMetamodel::Classifier}
      element(UmlMetamodel::Property, name, :add_property, UmlMetamodel::Classifier, references, options, &block)
    end
    expose :property
    
    def literal(name, opts = {}, &block)
      element(UmlMetamodel::EnumerationLiteral, name, :add_literal, UmlMetamodel::Enumeration, {}, opts, &block)
    end
    expose :literal
    
    def stereotype(name, opts = {}, &block)
      element(UmlMetamodel::Stereotype, name, :add_content, nil, {}, opts, &block)
    end
    expose :stereotype
    
    def tag(name, opts = {}, &block)
      references = {:type => UmlMetamodel::Classifier}
      element(UmlMetamodel::Tag, name, :add_tag, UmlMetamodel::Stereotype, references, opts, &block)
    end
    expose :tag
    
    def association(opts = {}, &block)
      references = {
        :properties => UmlMetamodel::Property,
        :association_class => UmlMetamodel::Class
      }
      element(UmlMetamodel::Association, nil, :add_content, nil, references, opts.merge(:defer_setting_properties => true), &block)
    end
    expose :association
    
    def applied_stereotype(opts = {}, &block)
      references = {:instance_of => UmlMetamodel::Stereotype}
      # NOTE: applied_stereotype is not actually an element
      element(UmlMetamodel::AppliedStereotype, nil, :add_applied_stereotype, UmlMetamodel::Element, references, opts, &block)
    end
    expose :applied_stereotype
    
    def applied_tag(opts = {}, &block)
      references = {:instance_of => UmlMetamodel::Tag}
      # NOTE: applied_tag is not actually an element
      element(UmlMetamodel::AppliedTag, nil, :add_applied_tag, UmlMetamodel::AppliedStereotype, references, opts, &block)
    end
    expose :applied_tag
    
    private
    
    # Interpret a DSL element into a UmlMetamodel element
    def element(type, name, context_add_method, expected_context_type, references = {}, opts = {}, &block)
      references.merge!(:applied_stereotypes => nil)
      # Split opts into reference options (those with values that are references) and normal options that can be set immediately
      ref_opts, creation_opts = partition_reference_options(opts, references.keys)
      # Initialize the element
      creation_opts[:name] = name if name
      creation_opts[:guid] = creation_opts.delete(:id)
      creation_opts.delete(:additional_info) # not in model
      creation_opts.delete(:defer_setting_properties) # not in model
      creation_opts[:upper] = nil if creation_opts[:upper] == Float::INFINITY # Sequel doesn't like Infinity...
      creation_opts[:aggregation] = creation_opts[:aggregation].to_s if creation_opts[:aggregation]
      if type.to_s =~ /Applied(Stereotype|Tag)/
        creation_opts.delete(:guid) # these types of elements don't have GUIDs
        creation_opts.delete(:name) # these types of elements don't have names
      end
      ChangeTracker.start
      element = type.create(creation_opts)
      ChangeTracker.commit
      @elements << element
      if opts[:id] # and here is where it gets interesting....
        @registry[opts[:id]] = element
      end
      # Add the element to the parent context
      add_to_parent_context(element, context_add_method, expected_context_type) # unless type.to_s =~ /Applied(Stereotype|Tag)/
      # Set reference options if possible. Those that can't yet be dereferenced will be stored for later dereferencing in @properties_to_dereference
      set_reference_properties(element, ref_opts, references)
      # Evaluate the passed block in the context of this element
      evaluate_context_for(element, &block) if block
    end
    
    # This method is not invoked directly, but instead by the concrete classifier type methods (class, interface, etc)
    def classifier(type, name, opts = {}, &block)
      # defaults
      options = {:is_abstract => false}
      options.merge!(opts)
      references = {
        :parents    => type,
        :children   => type,
        :implements => UmlMetamodel::Interface
      }
      element(type, name, :add_content, nil, references, options, &block)
    end
    
    # Helper methods, not intended for direct DSL usage
    
    # Evaluate the passed block in the context of the passed element
    def evaluate_context_for(element, &block)
      @context.push(element)
      # Invoke Cleanroom DSL evaluation for passed block
      self.evaluate(&block)
      @context.pop
    end
    
    # Add an element to the current context
    # Prints a warning if adding a duplicate qualified_name for a non-association element
    def add_to_parent_context(element, method = nil, expected_context_type = nil)
      return if element.is_a?(UmlMetamodel::Project)
      # Get current context
      context = @context.last
      
      # Determine add method and check for validity of context type
      raise "Should never get here!" if context.is_a?(Array) # @top_level_elements
        
      raise "A #{element.class} may only be defined as a top level element" unless method

      if expected_context_type && !context.is_a?(expected_context_type)
        raise "You may only define a #{element.class} within a #{expected_context_type}. Attempted to add #{element.name} to a #{@context.class}"
      end
      
      # NOTE - There was a bunch of checking to make sure that there were no duplicate qualified names.  We are going to rely on the fact that this will have all passed through the Ruby UmlMetamodel pipeline beforehand and that problems would have been discovered prior to this point.
      
      # Add the element to the context using the specified method
      ChangeTracker.start
      context.send(method, element)
      ChangeTracker.commit
      element
    end
    
    def partition_reference_options(opts, reference_keys)
      ref_opts, normal_opts = opts.partition{|k,v| reference_keys.include?(k) }
      [Hash[ref_opts], Hash[normal_opts]]
    end
    
    def set_reference_properties(element, ref_opts, references = {})
      ref_opts.each do |property, value|
        set_reference_property(element, property, value, references[property])
      end
    end
    
    def set_reference_property(element, property, value, type = nil)
      @properties_to_dereference << [element, property, value, type]
      return
      val = dereference_value(value, type)
      if !val.nil? && (!value.is_a?(Array) || val.count == value.count)
        ChangeTracker.start
        element.send("#{property}=", val)
        element.save
        ChangeTracker.commit
      else
        puts Rainbow("#{element.class} -- #{property} -- #{value} -- #{type}") if value.is_a?(Array)
        @properties_to_dereference << [element, property, value, type]
      end
    end
    
    # Given a type reference or array of type references, convert them into the referenced types.
    # If type is passed, only allow references to that UML Metamodel type
    def dereference_value(value, type = nil, opts = {})
      if value.is_a?(Array)
        missing_value = false
        deref_values = []
        value.each do |v|
          deref_v = dereference_value(v, type)
          unless deref_v
            missing_value = true
            break
          end
          deref_values << deref_v
        end
        missing_value ? nil : deref_values
      else
        @registry[value] || find_element_by_qualified_name(value) || Boa.find_uml_in_db(value, type)
      end
    end
  end
end
