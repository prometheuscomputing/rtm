load File.join(__dir__, 'dsl.rb')
load File.join(__dir__, 'clear_uml.rb')
module Boa
  module_function
  def import_rtm_model
    t = Time.now
    clear_uml
    dsl_file = File.join(__dir__, '../../test_data/RTM-1.6.0.rb')
    Boa.from_dsl_file(dsl_file)
    remove_extraneous_uml
    puts "Loading model took #{Time.now - t} seconds."
  end
  
  def add_missing_role_names
    UmlMetamodel::Property.each do |prop|
      next if prop.name
      begin
        ChangeTracker.start
        prop.name = Appellation.role(prop)
        prop.save
        ChangeTracker.commit
      rescue
      end
    end
  end
  
  def remove_extraneous_uml
    packages = UmlMetamodel::Package.all
    ChangeTracker.start
    packages.each do |pkg|
      if pkg.name == 'Gui_Builder_Profile' || pkg.name == 'SDoc'
        pkg.contents.each { |x| x.destroy }
        pkg.destroy
      end
      # if pkg.name == 'RTM'
      #   pkg.contents.each do |x|
      #     if x.qualified_name == "RTM::Term"
      #       next
      #     end
      #     x.destroy
      #   end
      #   pkg.destroy if pkg.contents.empty?
      # end
      # if pkg.name == 'Rosetta'
      #   # pkg.contents.each { |x| x.destroy unless x.is_a?(UmlMetamodel::Classifier) }
      #   # pkg.contents.each { |x| x.destroy if x.is_abstract }
      # end
    end
    ChangeTracker.commit
    puts UmlMetamodel::Class.count
  end
end
