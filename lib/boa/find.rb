module Boa
  def self.name_searchable_umm_classes(type = nil)
    @name_searchable_umm_type_hash ||= {}
    if type
      return @name_searchable_umm_type_hash[type] if @name_searchable_umm_type_hash[type]
      @name_searchable_umm_type_hash[type] = (type.children << type).select { |k| k.properties.keys.include?(:name) }.reject { |k| k.abstract? }
      @name_searchable_umm_type_hash[type]
    else
      return @umm_classes if @umm_classes
      @umm_classes = UmlMetamodel.classes(:no_imports => true).select { |k| k.properties.keys.include?(:name) }.reject { |k| k.abstract? }
    end
  end
  
  def self.find_uml_in_db(qualified_name, type = nil)
    found = []
    names = qualified_name.split('::')
    name  = names.pop
    klasses = name_searchable_umm_classes(type)
    klasses.each do |klass|
      found += klass.where(:name => name).all
    end
    if found.count == 1
      puts Rainbow("Found:#{qualified_name} -- #{type.inspect}").green
      return found.first
    end
    # # FIXME Big, ugly hack!  Do something better than this.
    # case type
    # when UmlMetamodel::Property
    # when UmlMetamodel::Classifier
    # when UmlMetamodel::Package
    # else
    # end

    puts Rainbow(found.collect(&:name).inspect).orange
    puts Rainbow("Not Found: #{qualified_name} -- #{type.inspect}").yellow
  end
  
  def self.find_uml_in_db_step2
  end
  
  def self.find_in_elements(value, elements, options = {})
    find_by_id_in_elements(value, elements, options) || find_by_qualified_name_in_elements(value, elements, options)
  end
  
  # Given a relative(!) qualified_name, and the containing context object(s), returns the referenced element
  # Options are:
  #   :search_imports - whether or not to search any imported packages in top level packages only
  #   :type - A specific UML Metamodel type to look for (e.g. UmlMetamodel::Association)
  #   :search_uml - Wether or not to search the default UML package
  #   :require_uml_namespace - Whether or not to require fully qualified names for UML elements (e.g. UML Primitives)
  #   :search_containing - Whether or not to search containing namespaces
  # Examples:
  #   UmlMetamodel.find('Automotive::Car', [<Project CarExample>]) # => <Class Car>
  #   UmlMetamodel.find('Car', [<Package Automotive>]) # => <Class Car>
  def self.find_by_qualified_name(qualified_name, contexts, options = {})
    default_options = {
      :search_imports => true,
      :search_uml => true,
      # NOTE: Since versions of UML Metamodel prior to v1.9 did not output a UML:: namespace for UML primitives,
      #       the :require_uml_namespace must be set to false in order to interpret DSLs produced with earlier versions
      #       This option will default to true in later versions, and will eventually be removed.
      :require_uml_namespace => true,
      # Whether or not to search containing contexts
      :search_containing => false
    }
    options = default_options.merge(options)
    found = nil
    
    contexts = Array(contexts)
    # Account for containing contexts
    # e.g. if a context is People::Clown, also search ::People and ::
    if options[:search_containing]
      contexts.dup.each do |context|
        contexts += UmlMetamodel.containing_contexts_for(context)
      end
      contexts.uniq!
    end
    
    # Search available contexts for qualified_name
    contexts.each do |context|
      elements = UmlMetamodel.searchable_contents_for(context, options)
      found = UmlMetamodel.find_by_qualified_name_in_elements(qualified_name, elements, options)
      break if found
    end
    
    # Search in UML package if not found
    if !found && options[:search_uml]
      uml_elements = [UmlMetamodel.uml_package]
      uml_elements += UmlMetamodel.uml_package.contents unless options[:require_uml_namespace]
      found = UmlMetamodel.find_by_qualified_name_in_elements(qualified_name, uml_elements, options)
    end
    found
  end
  
  # Given a qualified_name, and the elements to search in, returns the referenced element
  # Options are:
  #   :search_imports - whether or not to search any imported packages in top level packages only
  #   :type - A specific UML Metamodel type to look for (e.g. UmlMetamodel::Association)
  # Examples:
  #   UmlMetamodel.find_in_elements('Automotive::Car', [<Package Automotive>, ...]) # => <Class Car>
  #   UmlMetamodel.find_in_elements('Car', [<Class Car>, ...]) # => <Class Car>
  def self.find_by_qualified_name_in_elements(qualified_name_to_find, elements, options = {})
    options = {
      :type => nil
    }.merge(options)
    
    # Replace any Projects in elements with their contents, since Projects do not affect the namespace of their contents.
    projects, elements = elements.partition{|e| e.is_a?(UmlMetamodel::Project)}
    elements += projects.collect{|p| p.contents}.flatten
    
    components = qualified_name_to_find.to_s.split('::')
    # Since String#split does not add an empty string for a trailing delimiter, add one manually
    components << '' if qualified_name_to_find.to_s =~ /::$/
    found = nil
    elements.each do |element|
      # return element if element.qualified_name == qualified_name_to_find
      next unless element.name == components.first
      # If a type is specified, and this is the last qname component, check that the context matches
      next unless (options[:type].nil? || (components.length > 1) || element.is_a?(options[:type]))
      if components.length <= 1
        found = element
      else
        found = find_by_qualified_name_in_elements(components[1..-1].join('::'), searchable_contents_for(element), options)
      end
      break if found
    end
    found
  end
  
  # Return searchable contents of an element for the purpose of qualified name resolution
  def self.searchable_contents_for(element, options = {})
    case element
    when UmlMetamodel::Project
      element.contents
    when UmlMetamodel::Package
      element.contents + (options[:search_imports] ? element.imports : [])
    when UmlMetamodel::Enumeration
      # NOTE: there is potential for ambiguity here if a literal has the same name as a property
      (element.properties + [element.literals].flatten).compact
    when UmlMetamodel::Classifier
      element.properties
    when UmlMetamodel::Stereotype
      element.tags
    else # Name matched this element, but no contents to search within
      []
    end
  end
  
  # Return containing (aka parent) contexts for an element
  def self.containing_contexts_for(element, options = {})
    contexts = case element
    when UmlMetamodel::Project
      []
    when UmlMetamodel::Property
      [element.owner]
    when UmlMetamodel::AppliedStereotype
      [element.element]
    else
      [element.package, element.project]
    end.compact
    contexts.dup.each do |context|
      contexts += UmlMetamodel.containing_contexts_for(context)
    end
    contexts.uniq
  end
  
  def self.find_by_id(id, contexts, options = {})
    default_options = {
      :search_imports => true,
      :search_uml => true,
      # # NOTE: Since versions of UML Metamodel prior to v1.9 did not output a UML:: namespace for UML primitives,
      # #       the :require_uml_namespace must be set to false in order to interpret DSLs produced with earlier versions
      # #       This option will default to true in later versions, and will eventually be removed.
      # :require_uml_namespace => true,
      # # Whether or not to search containing contexts
      :search_containing => false
    }
    options = default_options.merge(options)
    found = nil
    
    contexts = Array(contexts)
    # Account for containing contexts
    # e.g. if a context is People::Clown, also search ::People and ::
    if options[:search_containing]
      contexts.dup.each do |context|
        contexts += UmlMetamodel.containing_contexts_for(context)
      end
      contexts.uniq!
    end
    
    # Search available contexts for id
    contexts.each do |context|
      elements = UmlMetamodel.searchable_contents_for(context, options)
      found = UmlMetamodel.find_by_id_in_elements(id, elements, options)
      break if found
    end
    
    # Search in UML package if not found
    if !found && options[:search_uml]
      uml_elements = [UmlMetamodel.uml_package]
      uml_elements += UmlMetamodel.uml_package.contents unless options[:require_uml_namespace]
      found = UmlMetamodel.find_by_id_in_elements(id, uml_elements, options)
    end
    found
  end
 
  def self.find_by_id_in_elements(id_to_find, elements, options = {})
    return unless id_to_find
    
    # Replace any Projects in elements with their contents, since Projects do not affect the namespace of their contents.
    projects, elements = elements.partition { |e| e.is_a?(UmlMetamodel::Project) }
    elements += projects.collect(&:contents).flatten
    
    found = nil
    elements.each do |element|
      found = element if element.id == id_to_find
      break if found
      found = find_by_id_in_elements(id_to_find, searchable_contents_for(element), options)
      break if found
    end
    found
  end
end