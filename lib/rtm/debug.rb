module RTM_Debug
  def mdc_regex
    /MDCX?_([A-Z]+([a-z0-9]+)?_)*[A-Z]+([a-z0-9]+)?/
  end
  
  def _stat(base_term = nil)
    (terms - [base_term]).map(&:_stat)
  end

  def _stat_anounce
    "#{self.class.name.demodulize}[#{id}] #{value}"
  end
  
  # def stat
  #   puts Rainbow(_stat_anounce).cyan
  #   _stat.each { |s| puts "  #{s}" }
  #   nil
  # end

  def statplus
    out = []
    out << _stat_anounce
    terms.each do |term|
      term._statplus(self).each { |line| out << ('  ' + line) }
    end
    this_obj  = out[0]
    this_term = out[1]
    out.each do |line|
      # if same_stat?(line, this_obj)
        stat_puts(line, this_obj, this_term)
      # elsif same_stat?(line, this_term)
      #   stat_puts(line, this_obj)
      # else
      #   stat_puts(line, value)
      # end
    end
    nil
  end
  alias_method :stats, :statplus
  
  def stat_puts(str, obj, terminfo)
    # puts str.inspect
    refid_match = terminfo.strip.split(/\s/).find { |x| x =~ mdc_regex }
    code_match  = terminfo.strip.split(/\s/).find { |x| x =~ /\d+::\d+/ }
    out = str
    if str.strip == obj.strip
      # match_str = str.strip.split(/\s/).last
      if obj =~ /RefID/
        out = Rainbow(str).orange
        # match_out = Rainbow(match_str).orange
      else
        out = Rainbow(str).cyan
        # match_out = Rainbow(match_str).cyan
      end
    end
    if str.strip == terminfo.strip
      leading_space = str[/\A */].size
      if terminfo =~ /Published/
        out = ' ' * leading_space + Rainbow(str.strip).green
      elsif terminfo =~ /Deprecated|Proposed|Provisional/
        out = ' ' * leading_space + Rainbow(str.strip).yellow
      elsif terminfo =~ /Withdrawn/
        out = ' ' * leading_space + Rainbow(str.strip).red
      else
        ' ' * leading_space + out.strip
      end 
    end
    # puts out.inspect
    parts = out.split(/(?<=\S)\s+(?=\S)/)
    out_colored = parts.map do |part|
      begin
        if part.strip == code_match.strip # part =~ /#{code_match}/
          # puts Rainbow("Code match with #{part}").red
          Rainbow(part).cyan
        elsif part.strip == refid_match.strip # part =~ /#{refid_match}/
          # puts Rainbow("RefID match with #{part}").red
          Rainbow(part).orange
        else
          part
        end
      rescue
        puts "terminfo.strip.split: #{terminfo.strip.split(/\s/).inspect}"
        puts "code_match: #{code_match.inspect}"
        puts "refid_match: #{refid_match.inspect}"
        raise
      end
    end
    # puts out_colored.inspect
    puts out_colored.join(' ')
  end
  
  def same_stat?(a,b)
    aa = a.strip.slice(/.*(?=\])/)
    bb = b.strip.slice(/.*(?=\])/)
    aa == bb
  end
end

module RTM
  class Term
    def _stat
      str = "#{self.class.name.demodulize}[#{id}] "
      if status
        sv = status.value
        case
        when sv =~ /Published/
          str << Rainbow(status.value).underline.green + ' '
        when sv == 'Deprecated' || sv == 'Provisional' || sv == 'Proposed'
          str << Rainbow(status.value).underline.yellow + ' '
        when sv == 'Withdrawn'
          str << Rainbow(status.value).underline.red + ' '
        else
          str << Rainbow(status.value).underline + ' '
        end
      end
      str << " #{refid_stat} #{code_stat}"
      if discriminators.any?
        dd = discriminators.map { |disc| " #{disc.discriminator_set.name}(#{disc.offset})" }
        if dd.count == 2 && dd[0].sub(/LEAD[1|2]/, 'LEAD') == dd[0].sub(/LEAD[1|2]/, 'LEAD')
          dd = [dd[0].sub(/LEAD[1|2]/, 'LEAD')]
        end
        dd.each {|d| str << d}
      end
      str
    end
    
    def stat
      puts _stat
    end
  
    def _statplus(origin_obj = self)
      out = [_stat]
      # line1 = "#{self.class.name.demodulize}[#{id}] "
      # line1 << Rainbow(status.value).underline + ' ' if status
      # line1 << "#{refid_stat} #{code_stat}"
      # out << line1
      rs = refid._stat(self) unless refid == origin_obj
      if rs&.any?
        out << "  RefID[#{refid.id}] #{refid.value}"
        rs.each do |s|
          out << "    #{s}"
        end
      end
      tcs = term_code._stat(self) unless term_code == origin_obj
      if tcs&.any?
        out << "  TermCode[#{term_code.id}] #{term_code.part_code}"
        tcs.each do |s|
          out << "    #{s}"
        end
      end
      out
    end
    
    def statplus
      puts _statplus.join("\n")
    end
  end

  class RefID
    include RTM_Debug
    def stat
      puts Rainbow(_stat_anounce).orange
      _stat.each { |s| puts "  #{s}" }
      nil
    end
  end

  class TermCode
    include RTM_Debug
    def stat
      puts Rainbow(_stat_anounce).cyan
      _stat.each { |s| puts "  #{s}" }
      nil
    end
  end
end