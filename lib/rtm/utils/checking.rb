module RTM
  module_function
  def mdc_regex
    /MDCX?_([A-Z]+([a-z0-9]+)?_)*[A-Z]+([a-z0-9]+)?/
  end
  
  # This was originally used while checking terms sent by Paul Schluter
  def base_term_expansion_values(refid, code, discriminator_set_name)
    # puts Rainbow("Expanding #{refid} #{code} with #{discriminator_set_name}").orange
    refids = []
    part_codes = []
    discriminator_set_name = 'LEAD2' if discriminator_set_name == 'LEAD'
    ds = RTM::DiscriminatorSet.where(:name => discriminator_set_name).first
    raise "#{discriminator_set} not found." unless ds
    ds.discriminators.each do |disc|
       ex_refid, ex_part_code = disc.expansion_values(refid, code)
       # puts "  #{ex_refid} #{ex_part_code}"
       refids << ex_refid
       part_codes << ex_part_code
    end
    [refids, part_codes]
  end
  
  def new_code_is_unique?(part_code)
    ptn_number, code = part_code.split(/::?/).map(&:to_i)
    return "Malformed partition::code -- #{part_code}." unless code
    ptn = RTM::Partition.where(:number => ptn_number).first
    return "No partition numbered #{ptn_number} for #{part_code}" unless ptn
    RTM::TermCode.where(:code => code, :partition_id => ptn.id).count > 0 ? false : true
  end
  
  def new_refid_is_unique?(refid_val)
    RTM::RefID.where(:value => refid_val).count > 0 ? false : true
  end
  
  # FIXME make it mandatory that a header row is included
  def check_terms_from_file(file)
    partition_size = (2**16)
    raw  = File.read(file)
    rows = CSV.new(raw, liberal_parsing: true).read
    header = rows.first
    refid_index     = header.index { |cell| cell =~ /refid|reference/i }
    partition_index = header.index { |cell| cell =~ /part/i && !(cell =~ /partcode/i) }
    code10_index    = header.index { |cell| cell =~ /code/i && !(cell =~ /partcode/i) }
    disc_index      = header.index { |cell| cell =~ /disc/i }
    rows.shift unless header.any? { |cell| cell =~ RTM.mdc_regex }
    problems = []
    rows.each do |row|
      refid = row[refid_index]
      ptn   = row[partition_index] if partition_index
      code  = row[code10_index].to_i
      disc  = row[disc_index] if disc_index

      # if code is a cfcode this should work
      if code.to_i > partition_size + 1
        # str = "Calculating partition for cfcode #{code}."
        ptn  = (code - (code % partition_size)) / partition_size
        code = code - (ptn * partition_size)
        # str << "  Partition is #{ptn}.  Code is #{code}."
        # puts str
      end
      # puts "RefID: #{row[refid_index]}, PartCode: #{partition_index ? row[partition_index] : 0}::#{row[code10_index]}, Discriminator: #{row[disc_index]}"
      unless ptn.to_i > 0
        row_str = row.map { |cell| cell.inspect }.join(', ')
        puts row_str
        puts ptn.inspect
        problems << "Could not determine the partition for #{row_str}"
        next
      end
      unless new_refid_is_unique?(refid)
        existing_refid = RTM.find_refid(refid)
        existing_terms = existing_refid.terms.map { |t| "#{t.refid&.value} #{t.part_code} #{t.status&.value}" }.join("; ")
        problems << "Submitted term #{refid} #{ptn}::#{code} includes a RefID that already exists. #{existing_terms}"
      end
      code_unique = new_code_is_unique?("#{ptn}::#{code}")
      if code_unique.is_a?(String) # FIXME we should not be checking for problems by checking for a String.  Refactor this crap!
        problems << code_unique
      end
      unless code_unique
        existing_code = RTM.find_code("#{ptn}::#{code}")
        existing_terms = existing_code.terms.map { |t| "#{t.refid&.value} #{t.part_code} #{t.status&.value}" }.join("; ")
        problems << "Submitted term #{refid} #{ptn}::#{code} includes a term code that already exists. #{existing_terms}"
      end
    end
    answer = {}
    if problems.any?
      answer[:error] = problems
    else
      answer[:success] = "These terms look fine to me."
    end
    answer
  end
  
end
