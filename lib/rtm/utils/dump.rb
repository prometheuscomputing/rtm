# load File.join(__dir__, '../lib/rtm/model_extensions.rb')

def _csv_headers_front
  [
    'Status',
    'Auto-expanded',
    'Preferred RefIDs',
    'Secondary RefIDs',
    'Deprecated RefIDs',
    'Withdrawn RefIDs',

    'Part::Code',
    'Secondary Part::Codes',
    'CF_CODE10',
    'Discriminators',

    'Systematic Name',
    'Common Term',
    'Acronym',
    'Description'
  ]
end
def _csv_headers_back
  [
    # 'Discriminator Sets',
    # 'Discriminator Offsets',
    # 'Discriminator Values',
    # 'Discriminators'
  ]
end
def csv_headers
  _csv_headers_front + 
  [
    'DIM',
    'UOM_MDC',
    'UOM_UCUM',
    'UCODE10',
    'CF_UCODE10',
    'UNIT_GROUPS',
    'Enum_Values',
    'Enum_Descriptions',
    'Enumeration_Groups',
    'Body Sites',
    'Body Sites Descriptions',
    'Body Site Groups'
  ] +
  _csv_headers_back
end
def csv_units_headers
  _csv_headers_front + 
  [
    'Dim',
    'Dim_c',
    'Symbol',
    'UOM_UCUM'
  ] +
  _csv_headers_back
end

require 'csv'
module RTM
  class DumpedTerm
    attr_accessor :status, :auto_expanded, :preferred_refids, :secondary_refids, :deprecated_refids, :withdrawn_refids, :preferred_part_code, :other_part_codes, :cf_term_code, :discriminators_display, :systematic_name, :common_term, :acronym, :descriptions_string, :term
    attr_accessor :dim, :dim_c, :symbol, :ucum_units # for units 
    attr_accessor :dims, :urefids, :ucums, :ucodes, :ucfcodes, :ugroups_refids, :erefids, :edescriptions, :egroups_refids, :bsrefids, :bsdescriptions, :bsgroups_refids # for terms that aren't units
    def initialize(args = {})
      args.each { |getter, value| send((getter.to_s + '=').to_sym, value) }
    end
    
    def to_csv
      csv_row = [status, auto_expanded, preferred_refids, secondary_refids, deprecated_refids, withdrawn_refids, preferred_part_code, other_part_codes, cf_term_code, discriminators_display, systematic_name, common_term, acronym, descriptions_string]
      if term.is_a?(RTM::Unit)
        csv_row.concat([dim, dim_c, symbol, ucum_units])
      else
        csv_row.concat([dims, urefids, ucums, ucfcodes, ugroups_refids, erefids, edescriptions, egroups_refids, bsrefids, bsdescriptions, bsgroups_refids])
      end
      csv_row
    end
  end
    
    
  module_function
  def the_terms
    @all_terms ||= RTM::Term.all.reject { |term| term.is_a?(RTM::Token) }
  end
  
  def serialize_dump(parts, filename = '')
    path = File.expand_path(File.join(__dir__, "../dump#{filename}.json"))
    puts path
    File.open(path, 'w+') { |f| f.puts parts.to_json }
  end

  def deserialize_dump(filename = '')
    path = File.expand_path(File.join(__dir__, "../dump#{filename}.json"))
    json = File.read(path)
    parsed = JSON.parse(json)
    dump = {}
    parsed.each { |k,v| dump[k.to_i] = v }
    dump
  end

  def lead_terms
    lead1 = RTM::DiscriminatorSet.where(:name => 'LEAD1').first
    lead2 = RTM::DiscriminatorSet.where(:name => 'LEAD2').first
    terms = (lead1.discriminators + lead2.discriminators).map(&:terms).flatten.uniq
  end

  def dump_term(term)
    row = []
    args = {:term => term}
    args[:status] = term.status&.value
    args[:auto_expanded] = term.auto_expanded ? 'true' : ''
    args[:preferred_refids] = term.preferred_refids.join(' ')
    args[:secondary_refids] = term.secondary_refids.join(' ')
    args[:deprecated_refids] = term.deprecated_refids.join(' ')
    args[:withdrawn_refids] = term.withdrawn_refids.join(' ')
  
    args[:preferred_part_code] = term.preferred_part_code
    args[:other_part_codes] = term.other_part_codes.join(' ')
    args[:cf_term_code] = term.cf_term_code
    args[:discriminators_display] = term.discriminators_display

    args[:systematic_name] = term.systematic_name
    args[:common_term] = term.common_term
    args[:acronym] = term.acronym
    args[:descriptions_string] = term.descriptions_string
  
    if term.is_a?(RTM::Unit)
      args[:dim] = term.dim
      args[:dim_c] = term.dim_c
      args[:symbol] = term.symbol
      args[:ucum_units] = term.ucum_units.map(&:value).join(' ')
    else
      umatrix = term.units_matrix_for_csv
      args[:dims] = umatrix.map { |row| row[0] }.join("\n")
      args[:urefids] = umatrix.map { |row| row[1] }.join("\n")
      args[:ucums] = umatrix.map { |row| row[2] }.join("\n")
      args[:ucodes] = umatrix.map { |row| row[3] }.join("\n")
      args[:ucfcodes] = umatrix.map { |row| row[4] }.join("\n")

      args[:ugroups_refids] = term.unit_groups_refids.join("\n")
    
      ematrix = term.enums_matrix_for_csv
      args[:erefids] = ematrix.map { |row| row[0] }.join("\n")
      args[:edescriptions] = ematrix.map { |row| row[1] }.join("\n")
    
      args[:egroups_refids] = term.enum_groups_refids.join("\n")
    
      bsmatrix = term.body_sites_matrix_for_csv
      args[:bsrefids] = bsmatrix.map { |row| row[0] }.join("\n")
      args[:bsdescriptions] = bsmatrix.map { |row| row[1] }.join("\n")
    
      args[:bsgroups_refids] = term.body_site_groups_refids.join("\n")
    end
    RTM::DumpedTerm.new(args)
  end

  def dump(terms, include_expanded = false)
    t = Time.now
    count = 0
    parts = {}
    refids_seen = []
    terms = terms.reject { |t| t.auto_expanded == true } unless include_expanded
    puts "There are #{terms.count}"
    terms.each do |term0|
      # break if count > 100
      count +=1
      print "#{count} " if count % 100 == 0
      next if term0.is_a?(RTM::Token)
      next if refids_seen.include?(term0.refid)
      # puts term0.refid.value if term0.part_code == '1::1'
      term = term0.primary_term
      puts term0.statplus unless term
      
      if term.term_code
        term.term_code.terms.map(&:refid).each { |r| refids_seen << r }
        preferred_partition = term.preferred_part_code.to_s.slice(/\d+(?=::)/).to_i
        # puts "#{preferred_partition} -- #{term.preferred_part_code} -- #{term.part_code}"
      else
        refids_seen << term.refid
        preferred_partition = 0
        # puts Rainbow("#{preferred_partition} -- #{term.preferred_part_code} -- #{term.part_code}").red
      end
      
      if parts[preferred_partition]
        # if parts[preferred_partition].count < 2
        #   puts "Adding #{term._stat} to partition #{preferred_partition}"
        # end
      else
        # puts "Starting partition #{preferred_partition}"
        parts[preferred_partition] = []
        # parts[preferred_partition] << (preferred_partition == 4 ? csv_units_headers : csv_headers)
      end
        
      # next unless preferred_partition == 2

      dt = dump_term(term)
      # row << term.discriminator_sets_with_infix.uniq.join(' ')
      # row << term.discriminator_offsets.join(' ')
      # row << term.discriminator_values.join(' ')

      parts[preferred_partition] << dt
    end
    puts "That took #{(Time.now - t)/60} minutes."
    parts
  end

  def dump_p2(terms = nil)
    if terms.nil?
      partition = RTM::Partition.where(:number => 2).first
      terms = partition.term_codes.map(&:terms).flatten.uniq
    end
    dump(terms)
  end

  def dump_lead_test
    terms = RTM.find('MDC_ECG_LEAD_C').terms
    x = dump(terms)
    dump_to_csv(x)
  end
  
  def dump_all(terms = nil, expanded = false)
    t = Time.now
    terms ||= the_terms
    @parts ||= dump(terms)
    dump_to_views(@parts, expanded)
    dump_to_csv(@parts)
    dump_to_csv(@parts, expanded) if expanded
    dump_more(terms, expanded)
    puts "That took #{(Time.now - t)/60} minutes."
  end
  
  def dump_expanded(terms = nil)
    t = Time.now
    terms ||= the_terms
    @parts ||= dump(terms)
    dump_to_views_expanded(@parts)
    dump_to_csv(@parts, true)
    dump_more(terms, true)
    puts "That took #{(Time.now - t)/60} minutes."
  end
  

  def dump_to_views(parts, expanded = false)
    t = Time.now
    ChangeTracker.start
    RTM::TermView.all.each(&:destroy)
    RTM::UnitView.all.each(&:destroy)
    ChangeTracker.commit
    parts.each do |num, dumped_terms|
      dumped_terms.each do |dt|
        next if (!expanded && dt.auto_expanded == true)
        # next if e[0] == 'Status' && e[1] == 'Auto-expanded'
        dt.term.class == RTM::Unit ? create_unit_view(dt) : create_term_view(dt)
      end
    end
    puts "That took #{(Time.now - t)/60} minutes."
  end
  
  
  def dump_to_views_expanded(parts)
    t = Time.now
    parts.each do |num, dumped_terms|
      dumped_terms.each do |dt|
        next if dt.auto_expanded == false
        dt.term.class == RTM::Unit ? create_unit_view(dt) : create_term_view(dt)
      end
    end
    puts "That took #{(Time.now - t)/60} minutes."
  end
  

  # FIXME change to use DumpedTerm objects
  def dump_to_csv(parts, include_expanded = false)
    keys = parts.keys.sort
    puts keys.inspect
    puts "Generating CSV files."
    keys.each do |num|
      entries = parts[num]
      # parts[num].each do |entries|
      puts Rainbow(num).orange
      fn = "partition_#{num}_terms"
      fn << '_expanded' if include_expanded
      foldername = 'csv'
      foldername << '_expanded' if include_expanded
      csv_file = File.expand_path(File.join(__dir__, "../../../#{foldername}/#{fn}.csv"))
      puts "Partition #{num} had #{entries.count}.  #{csv_file}"
      csv = CSV.open(csv_file, "wb")
      sorted_entries = entries.sort_by do |e|
        begin
          e.preferred_part_code.to_s.split(/::?/).last.to_i 
        rescue
          puts '*'*22
          puts e.inspect
          raise
        end
      end
      if num == 4
        csv << csv_units_headers
      else
        csv << csv_headers
      end
      sorted_entries.each { |e| csv << e.to_csv }
      csv.close
      # end
    end
  end

  def create_unit_view(dt)
    ChangeTracker.start
    view = create_collated_view(dt, RTM::UnitView)
    view.dim        = dt.dim
    view.dim_c      = dt.dim_c
    view.symbol     = dt.symbol
    view.ucum_units = dt.ucum_units
    view.save
    ChangeTracker.commit
  end

  def create_term_view(dt)
    ChangeTracker.start
    view = create_collated_view(dt, RTM::TermView)
    view.units_table      = [dt.dims, dt.urefids, dt.ucums, dt.ucodes, dt.ucfcodes].to_json
    view.unit_groups      = dt.ugroups_refids
    view.enums_table      = [dt.erefids, dt.edescriptions].to_json
    view.enum_groups      = dt.egroups_refids
    view.body_sites_table = [dt.bsrefids, dt.bsdescriptions].to_json
    view.body_site_groups = dt.bsgroups_refids
    view.save
    ChangeTracker.commit
  end

  def create_collated_view(dt, type)
    trm = dt.term
    view = nil
    if trm.is_a?(RTM::Unit) || type == RTM::UnitView
      view = RTM::UnitView.new
    elsif trm.is_a?(RTM::Term)
      view = RTM::TermView.new
    else
      raise
    end
    view.add_term(trm)
    view.status            = dt.status
    view.refids            = dt.preferred_refids.split(/\s/).uniq.join(' ')
    view.secondary_refids  = dt.secondary_refids
    view.deprecated_refids = dt.deprecated_refids
    view.withdrawn_refids  = dt.withdrawn_refids
    view.term_codes        = dt.preferred_part_code
    # view.secondary_term_codes = entry[7]
    view.cf_term_codes      = dt.cf_term_code
    view.secondary_cf_term_codes = dt.other_part_codes.split(/\s+/).map { |pc| RTM.calculate_cf_code(pc).to_s }.join(' ')
    view.discriminators     = dt.discriminators_display
    view.systematic_name    = dt.systematic_name
    view.common_term        = dt.common_term
    view.acronym            = dt.acronym
    view.description        = dt.descriptions_string
    view
  end

  def dump_withdrawn(include_expanded = false)
    # withdrawn
    withdrawn_terms = [['Withdrawn Term', 'Notes', 'Other Terms']]
    wt = RTM::CurrentStatus.where(:value => 'Withdrawn').first.terms_with_status
    wt.reject! { |t| t.auto_expanded == true } unless include_expanded
    wt.each do |term|
      other_terms = term.synonyms(false)
      report = ["#{term.refid&.value} #{term.part_code}", term.notes_text.gsub(/Status note:\s*/i, '')]
      other_terms.sort_by! { |t| t.sort_val }
      stats = other_terms.map do |t|
        stat = "#{t.refid&.value} #{t.part_code}"
        stat << " (#{t.status&.value})" if t.status&.value
        stat
      end
      report << stats.join('; ')
      withdrawn_terms << report
    end
  
    fn = 'withdrawn_terms'
    fn = fn + '_expanded' if include_expanded
    csv_file = File.expand_path(File.join(__dir__, "../../../csv/#{fn}.csv"))
    puts "#{fn}: #{withdrawn_terms.count}.  #{csv_file}"
    csv = CSV.open(csv_file, "wb")
    withdrawn_terms.each { |cs| csv << cs }
    csv.close
  end

  def dump_deprecated(include_expanded = false)
    # deprecated
    deprecated_terms = [['Deprecated Term', 'Notes', 'Other Terms']]
    dt = RTM::CurrentStatus.where(:value => 'Deprecated').first.terms_with_status
    dt.reject! { |t| t.auto_expanded == true } unless include_expanded
    dt.each do |term|
      other_terms = term.synonyms(false)
      report = ["#{term.refid&.value} #{term.part_code}", term.notes_text.gsub(/Status note:\s*/i, '')]
      puts term.stat
      puts other_terms.inspect
      other_terms.sort_by! { |t| t.sort_val }
      stats = other_terms.map do |t|
        stat = "#{t.refid&.value} #{t.part_code}"
        stat << " (#{t.status&.value})" if t.status&.value
        stat
      end
      puts stats.join('; ')
      report << stats.join('; ')
      deprecated_terms << report
    end
  
    fn = 'deprecated_terms'
    fn = fn + '_expanded' if include_expanded
    csv_file = File.expand_path(File.join(__dir__, "../../../csv/#{fn}.csv"))
    puts "#{fn}: #{deprecated_terms.count}.  #{csv_file}"
    csv = CSV.open(csv_file, "wb")
    deprecated_terms.each { |cs| csv << cs }
    csv.close
  end

  def dump_more(terms, include_expanded = false)
    dump_withdrawn(include_expanded)
    dump_deprecated(include_expanded)
  
    terms = terms.reject { |t| t.is_a?(RTM::Token) }
    terms = terms.reject { |t| t.auto_expanded == true } unless include_expanded

    # synonyms
    term_code_synonyms            = [['RefID', 'Codes']]
    term_code_synonyms_ecg        = [['RefID', 'Codes']]
    term_code_synonyms_events     = [['RefID', 'Codes']]
    term_code_synonyms_deprecated = [['RefID', 'Codes']]
    term_code_synonyms_withdrawn  = [['RefID', 'Codes']]
    term_code_synonyms_other      = [['RefID', 'Codes']]
    refid_synonyms            = [['Term Code', 'RefIDs']]
    refid_synonyms_deprecated = [['Term Code', 'RefIDs']]
    refid_synonyms_withdrawn  = [['Term Code', 'RefIDs']]
    refid_synonyms_units      = [['Term Code', 'RefIDs']]
    refid_synonyms_ecg        = [['Term Code', 'RefIDs']]
    refid_synonyms_other      = [['Term Code', 'RefIDs']]
    
    ss = RTM::SynonymSet.all
    rss, css = ss.partition { |s| s.term_code_synonym }
    rss.each do |set|
      terms = set.terms.sort_by { |t| t.sort_val }
      report = [terms.first.part_code]
      refids = terms.collect do |t|
        stat = "#{t.refid_stat}"
        stat << " (#{t.status&.value})" if t.status&.value
        stat
      end
      report << refids.join('; ')
      refid_synonyms << report
      
      specifically_reported = false
      
      if terms.find { |term| term.deprecated? }
        refid_synonyms_deprecated << report
        specifically_reported = true
      end
      if terms.find { |term| term.withdrawn? }
        refid_synonyms_withdrawn << report
        specifically_reported = true
      end
      
      if terms.any? { |term| term.partition.number == 4 }
        refid_synonyms_units << report
        specifically_reported = true
      end
      
      if terms.find { |term| r = term.refid; r && r.value =~ /MDC_ECG/ }
        refid_synonyms_ecg << report
        specifically_reported = true
      end
      
      refid_synonyms_other << report unless specifically_reported
    end
    
    css.each do |set|
      terms = set.terms.sort_by { |t| t.sort_val }
      report = [terms.first.refid.value]
      codes = terms.map do |term|
        term.part_code.to_s + ' ' + "#{term.status&.value}"
      end
      report << codes.join('; ')
      term_code_synonyms << report
      
      code_specifically_reported = false
      
      if terms.find { |term| term.deprecated? }
        term_code_synonyms_deprecated << report
        code_specifically_reported = true
      end
      if terms.find { |term| term.withdrawn? }
        term_code_synonyms_withdrawn << report
        code_specifically_reported = true
      end
      if terms.find { |term| term.partition_number == 3}
        term_code_synonyms_events << report
        code_specifically_reported = true
      end
      
      if terms.find { |term| r = term.refid; r && r.value =~ /MDC_ECG/ }
        term_code_synonyms_ecg << report
        code_specifically_reported = true
      end
      
      term_code_synonyms_other << report unless code_specifically_reported
    end
  
    vars = %w(refid_synonyms refid_synonyms_deprecated refid_synonyms_withdrawn refid_synonyms_ecg refid_synonyms_units refid_synonyms_other term_code_synonyms term_code_synonyms_ecg term_code_synonyms_events term_code_synonyms_other term_code_synonyms_withdrawn term_code_synonyms_deprecated)
    vars.each do |var|
      fn = var.dup
      fn << '_expanded' if include_expanded
      foldername = 'csv'
      foldername << '_expanded' if include_expanded
      csv_file = File.expand_path(File.join(__dir__, "../../../#{foldername}/#{fn}.csv"))
      vals = eval(var)
      vals.uniq!
      puts "#{var}: #{vals.count}.  #{csv_file}"
      csv = CSV.open(csv_file, "wb")
      vals.each { |val| csv << val }
      csv.close
    end
  
    # 
  end

  # Apparently not used
  def dump2(terms, include_expanded = false)
    t = Time.now
    count = 0
    parts = {}
    refids_seen = []
    terms = terms.reject { |t| t.auto_expanded == true } unless include_expanded
    puts "There are #{terms.count}"
    terms.each do |term0|
      # break if count > 100
      count +=1
      print "#{count} " if count % 100 == 0
      next if term0.is_a?(RTM::Token)
      next if refids_seen.include?(term0.refid)
      # puts term0.refid.value if term0.part_code == '1::1'
      term = term0.primary_term
      puts term0.statplus unless term
      if term.term_code
        term.term_code.terms.map(&:refid).each { |r| refids_seen << r }
        preferred_partition = term.preferred_part_code.to_s.slice(/\d+(?=::)/).to_i
        # puts "#{preferred_partition} -- #{term.preferred_part_code} -- #{term.part_code}"
      else
        refids_seen << term.refid
        preferred_partition = 0
        # puts Rainbow("#{preferred_partition} -- #{term.preferred_part_code} -- #{term.part_code}").red
      end
      unless parts[preferred_partition]
        # puts "Starting partition #{preferred_partition}"
        parts[preferred_partition] = []
        parts[preferred_partition] << (preferred_partition == 4 ? csv_units_headers : csv_headers)
      end
        
      # next unless preferred_partition == 2

      row = dump_term(term)
      # row << term.discriminator_sets_with_infix.uniq.join(' ')
      # row << term.discriminator_offsets.join(' ')
      # row << term.discriminator_values.join(' ')

      parts[preferred_partition] << row
    end
    puts "That took #{(Time.now - t)/60} minutes."
    parts
  end
  
  def views_for_secondary_terms(terms = nil)
    t = Time.now
    # terms ||= RTM::Term.all.reject { |term| term.is_a?(RTM::Token) || term.collated_view }
    the_terms.reject { |t| t.collated_view }.each do |term|
      views = ([term.refid&.terms&.map(&:collated_view)] + [term.term_code&.terms&.map(&:collated_view)]).flatten.compact.uniq
      selected_views = views.select do |vu|
        begin
          vu.refids.to_s.split(/\s/).include?(term.refid.value) && vu.term_codes.to_s.split(/\s/).include?(term.part_code)
        rescue
          pp vu
          raise
        end
      end
      views = selected_views if selected_views.any?
      if views.count == 1
        # ChangeTracker.start
        # term.collated_view = views.first
        # ChangeTracker.commit
        # puts "#{term.stat} will map to #{views.first.refids} #{views.first.term_codes}"
      elsif views.count > 1
        views.select { |vu| vu.refids.strip == term.refid.value.strip && vu.term_codes.strip == term.part_code }
        if views.count > 1
          puts Rainbow("#{term.refid&.value} #{term.part_code} mapped to multiple views!").red
          views.each { |vu| puts "TV[#{vu.id}]: #{vu.refids} #{vu.term_codes}"}
          puts 
        else
          # ChangeTracker.start
          # term.collated_view = views.first
          # ChangeTracker.commit
          # puts "#{term.stat} will map to #{views.first.refids} #{views.first.term_codes}"
        end
      else
        puts Rainbow("#{term.class}  -- #{term.refid&.value} #{term.part_code} had no views!").orange
        termz = []       
        if term.refid&.terms_count.to_i > 1
          # puts "multiple refid terms"
          term.refid.terms.each { |trm| termz << trm; }# puts trm.stat }
        end
        if term.term_code&.terms_count.to_i > 1
          # puts "multiple term_code terms"
          term.term_code.terms.each { |trm| termz << trm }#; puts trm.stat }
        end
        termz.uniq!
        pterm = term.primary_term
        raise unless termz.include?(pterm)
        puts "Primary term: #{term.primary_term.stat}"
        puts
        if term.class == RTM::Term
          # entry = [term.status&.value,
          #          nil,
          #          term.refid&.value
          #          term
          #        ]
          #
        end
        if term.class == RTM::Unit
        end
        # create_collated_view(term.class, entry)
        # view.status = entry[0]
        # view.refids = entry[2].split(/\s/).uniq.join(' ')
        # view.secondary_refids = entry[3]
        # view.deprecated_refids = entry[4]
        # view.withdrawn_refids = entry[5]
        # view.term_codes = entry[6]
        # # view.secondary_term_codes = entry[7]
        # view.cf_term_codes = entry[8]
        # view.secondary_cf_term_codes = entry[7].split(/\s+/).map { |pc| RTM.calculate_cf_code(pc).to_s }.join(' ')
        # view.discriminators = entry[9]
        # view.systematic_name = entry[10]
        # view.common_term = entry[11]
        # view.acronym = entry[12]
        # view.description = entry[13]
        # view.term = entry.last
      end
    end
    puts "That took #{(Time.now - t)/60} minutes."
  end
end

