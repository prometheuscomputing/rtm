module RTM
  module_function
  def expand_discriminators
    sets = RTM::DiscriminatorSet.all.reject { |ds| ds.name =~ /RTMMSv1/ }
    sets.each do |dset|
      next if dset.name == 'LEAD' || dset.name == '1' || dset.name == 'VER'
      puts Rainbow("Expanding #{dset.name}.").cyan
      discriminators = dset.discriminators
      base_discriminator = discriminators.find { |d| d.offset == 0 }
      base_terms = base_discriminator.terms
      published_base_terms = base_terms.reject do |this_base_term|
        !this_base_term.from_10101R? && 
        base_terms.find do |other_base_term|
          other_base_term.from_10101R? && 
          other_base_term.term_code == this_base_term.term_code
        end
      end
      published_base_terms = published_base_terms.reject { |term| term.refid.value == 'MDC_ECG_LEAD_CONFIG' }
      published_base_terms.each do |base_term|
        base_term.stat unless base_term.from_10101R?
        # next if base_term.status =~ /RTMMSv1/
        next if ['MDC_DIM_BREATHS_PER_MIN_PER_L', 'MDC_DIM_OHM', 'MDC_DIM_PER_G'].include?(base_term.refid.value)
        bc  = base_term.term_code
        ptn = bc.partition
        discriminators.each { |disc| expand_discriminator(disc, base_term, bc, ptn) }
      end
    end
  end
  
  def expand_term(term, discriminator_set)
    if discriminator_set.is_a?(String)
      dset = RTM::DiscriminatorSet.where(:name => discriminator_set).first
    else
      dset = discriminator_set
    end
    if dset.name == 'LEAD'
      return expand_term(term, 'LEAD1') + expand_term(term, 'LEAD2')
    end
    expanded_terms = []
    dset.discriminators.each { |disc| expanded_terms << expand_discriminator(disc, term) }
    expanded_terms.compact
  end
    
  
  def expand_discriminator(disc, base_term, bc = nil, ptn = nil)
    bc  ||= base_term.term_code
    ptn ||= base_term.partition
    if disc.offset != 0 && disc.value.nil?
      return if disc.discriminator_set.name == 'UoM'
      return if disc.discriminator_set.name == 'LEAD'
      puts Rainbow("#{disc.discriminator_set.name} #{disc.offset}").red
    end
    if disc.offset == 0 && disc.discriminator_set.name == 'UoM'
      if base_term.refid.value =~ /_X_/ 
        xless_refid = base_term.refid.value.sub('_X_', '_')
        # puts Rainbow(xless_refid).green
        _expand_discriminator(xless_refid, disc, base_term, bc, ptn)
      else
        puts Rainbow("Skipping #{base_term.stat}").green
        return
      end
    end
    infix = base_term.get_infix_depth(disc)
    expanded_refid_val = expand_refid_val(disc, base_term.refid.value, infix, base_term)
    _expand_discriminator(expanded_refid_val, disc, base_term, bc, ptn)
  end
  
  def _expand_discriminator(expanded_refid_val, disc, base_term, bc, ptn)
    expanded_term = nil
    offset_code = disc.offset + base_term.term_code.code
    offset_part_code = "#{ptn.number}::#{offset_code}"
    expanded_terms = RTM.find_terms(offset_part_code, expanded_refid_val)
    RTM.error("Multiple expanded terms with the same RefID and TermCode: #{terms.map(&:stat)}", :red) if expanded_terms.count > 1
    expanded_term = expanded_terms.first
    unless expanded_term
      # expanded_refid = RTM.find_refid(expanded_refid_val)
      # unless expanded_refid
        ChangeTracker.start
        expanded_refid = RTM.find_or_create_refid(expanded_refid_val)
        # ChangeTracker.commit
      # end
      # offset_tc = RTM.find_code(offset_part_code)
      # unless offset_tc
        # ChangeTracker.start
        offset_tc = RTM.find_or_create_code_in_partition(offset_part_code)
        # ChangeTracker.commit
      # end
      type = ptn.number == 4 ? RTM::Unit : RTM::Term
      # ChangeTracker.start
      expanded_term = type.create
      expanded_term.auto_expanded = true
      expanded_term.definition = base_term.definition
      expanded_term.term_code = offset_tc
      expanded_term.refid = expanded_refid
      expanded_term.save
      ChangeTracker.commit
    end

    ChangeTracker.start
    sval = expanded_term.status&.value
    if sval && sval != base_term.status&.value
      puts "Expanded Term STATUS -- > #{expanded_term.refid&.value} #{expanded_term.part_code} has status #{sval}" unless sval =~ /Published/
    end
    expanded_term.status = base_term.status unless sval == 'Published' || sval == 'Deprecated' || sval == 'Withdrawn'
    expanded_term.add_discriminator(disc) unless expanded_term.discriminators.include?(disc)
    ChangeTracker.commit
    if disc.offset != 0
      multis = expanded_term.term_code.terms.reject { |t| t.status&.value =~ /Withdrawn/i }
      if multis.count > 1
        unless multis.all? { |m| m.refid.value =~ /MDC_ECG/ } 
          report = multis.map { |m| "#{m.refid.value} #{m.status&.value} "}.join
          puts Rainbow("Expanded Synonyms: #{expanded_term.part_code} -> #{report}").orange
        end
      end
    end
    expanded_term
  end

  def expand_refid_val(disc, base_refid, infix, base_term = nil)
    if base_refid =~ /_X_/ && disc.discriminator_set.name == 'UoM'
      expanded_refid = base_refid.gsub('_X_', disc.value.to_s + '_').gsub('__', '_').strip
      # puts "#{expanded_refid} " + Rainbow("#{disc.offset} #{disc.value.inspect}").yellow
    else
      # puts Rainbow(base_refid).blue
      # base_term.statplus if base_term
      infix_set = disc.discriminator_set.type&.value =~ /infix/i
      infix = 1 if infix_set && (infix.nil? || infix == 0)
      infix ||= 0
      refid_parts = base_refid.split(/_+/)
      insertion_index = -1 - infix
      refid_parts[insertion_index] = refid_parts[insertion_index] + disc.value.to_s
      expanded_refid = refid_parts.join('_').gsub('__', '_').strip
      # puts "#{expanded_refid} " + Rainbow("#{disc.offset} #{disc.value.inspect}").magenta
    end
    expanded_refid
  end
  
  def check_discriminators
    RTM::DiscriminatorSet.all.each do |dset|
      next if dset.name =~ /RTMMS/
      puts
      puts dset.name
      dset.discriminators.each do |d|
        puts "    #{d.offset}  #{d.value}  #{d.description}"
      end
    end
  end
end