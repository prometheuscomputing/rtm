module RTM
  module_function
  
  def propose_by_refid(refid)
    return nil if refid.nil? || refid.strip.empty?
    raise "RefID #{refid} already exists!" if find_refid(refid)
    ChangeTracker.start
    new_refid = RTM::RefID.create(:value => refid)
    new_term  = RTM::term.create
    new_term.refid = new_refid
    new_term.status = 'Proposed'
    new_term.save
    ChangeTracker.commit
    new_refid
  end
  
  # def clear_tables
  #   klasses = RTM.classes(:ignore_class_namespaces => true)
  #   klasses.each do |klass|
  #   # RTM.classes(:no_imports => true).each do |klass|
  #     if DB.table_exists? klass.table_name
  #       DB[klass.table_name].delete
  #     else
  #       puts "#{klass} with table_name #{klass.table_name} was not found in the DB!" unless klass.abstract?
  #     end
  #     DB[(klass.table_name.to_s + "_deleted").to_sym].delete if DB.table_exists? (klass.table_name.to_s + "_deleted").to_sym
  #   end
  # end  
end
