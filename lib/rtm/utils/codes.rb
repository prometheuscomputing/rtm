module RTM
  module_function
  
  def code_exists_in_partition(code, partition_num)
    all_codes_in_partition(partition_num).include?(code)
  end
  
  def all_codes_in_partition(partition_num)
    part = RTM::Partition.where(:number => partition_num.to_i).first
    part.term_codes.map(&:code).sort
  end
  
  def calculate_cf_code(part_code)
    ptn, code = part_code.split(/::?/)
    puts part_code unless ptn && code
    (ptn.to_i * (2**16)) + code.to_i
  end
  
  def all_cfcodes
    RTM::Partition.all.map do |part|
      part.term_codes.map do |tc|
        part.number * (2**16) + tc.code
      end
    end.flatten.sort
  end
  
  def all_codes
    RTM::Partition.map do |part|
      part.term_codes.map do |tc|
        "#{part.number}:#{tc.code}"
      end
    end.flatten.sort
  end
  
  def cfcode_exists?(cfcode)
    all_cfcodes.include?(cfcode.to_i)
  end
  
  def cfcodes_exist?(codes)
    cfcodes = [cfcodes].flatten
    good = []
    bad  = []
    ac = all_cfcodes
    cfcodes.each do |c|
      if ac.include?(c.to_i)
        bad << c.to_i
      else
        good << c.to_i
      end
    end
    puts "The following cfcodes already exist in RTMMS: #{bad.sort.inspect}"
    puts "The following cfcodes do not already exist in RTMMS: #{good.sort.inspect}"
  end
    
  def cf_codes_for_refid(refid)
    refid.cfcodes
  end
  
end
