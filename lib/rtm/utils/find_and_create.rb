module RTM
  module_function
  
  def _find(klass, args)
    klass.where(args).first
  end
  
  def _create(klass, args)
    klass.create(args)
  end

  def _find_or_create(klass, args)
    _find(klass, args) || _create(klass, args)
  end

  def find_or_create_description(input, wrap_in_paragraph = false)
    str = input.dup
    return nil if str.strip.empty? # on second thought, we just want nil if the string is empty
    # return RTM::Description.new if str.strip.empty? # if the description is empty then we probably want empty placeholders rather than having a lot of terms share an empty description (which would be problematic if the description were to change from empty to non-empty)
    # FIXME: also check to see if there is a non-HTML-formatted version of the description.  If there is, replace its contents with str.  Alternatively, write a script that goes through all definitions and merges them.
    # TODO factor out the wrapping in <p> and the checking to see if it is already wrapped in <p>
    desc = RTM::Description.where(:value_content => str.strip).first
    if wrap_in_paragraph
      str = '<p>' + str + '</p>' unless str =~ /^<p>.*<\/p>$/
    end
    desc ||= RTM::Description.where(:value_content => str.strip).first
    unless desc
      desc = RTM::Description.new
      rt = Gui_Builder_Profile::RichText.new
      rt.content = str.strip
      rt.markup_language = 'HTML'
      desc.value = rt
      desc.save
    end
    desc
  end
  
  def find_or_create_note(str, wrap_in_paragraph = false)
    return nil if str.strip.empty?
    if wrap_in_paragraph
      str = '<p>' + str + '</p>'
    end
    note = RTM::Note.where(:value_content => str.strip).first
    unless note
      note = RTM::Note.new
      rt = Gui_Builder_Profile::RichText.new
      rt.content = str.strip
      rt.markup_language = 'HTML'
      note.value = rt
      note.save
    end
    note
  end
  
  def create_definition(args = {})
    _create(RTM::Definition, args)
  end
  
  def find_definition(args)
    _find(RTM::Definition, args)
  end
  
  def find_or_create_tag(tag_text)
    _find_or_create(RTM::Tag, :name => tag_text.strip)
  end
  
  def find_or_create_token(value)
    _find_or_create(RTM::Token, :value => value.strip)
  end
  
  def find_or_create_definition(args)
    _find_or_create(RTM::Definition, args)
  end
  
  def find_or_create_dimension(str)
    _find_or_create(RTM::Dimension, :value => str.strip)
  end
  
  def find_or_create_ucum(str)
    _find_or_create(RTM::UCUM_Unit, :value => str.strip)
  end
  
  def find_or_create_uom(str)
    _find_or_create(RTM::UnitsOfMeasure, :value => str.strip)
  end
  
  def find_or_create_refid(str)
    _find_or_create(RTM::RefID, :value => str.strip)
  end
    
  def find_code_in_block(code, block)
    Kernel.warn 'RTM.find_code_in_block is deprecated'
    found_codes = RTM::TermCode.where(:code => code.to_i).all
    found_codes.find { |tc| tc.block.id == block.id}
  end
  
  def find_or_create_code_in_partition(code, ptn = nil)
    if ptn.nil? && code.to_s =~ /\d+::?\d+/
      ptn, code = code.split(/::?/).map(&:to_i)
    end
    unless ptn.is_a?(RTM::Partition)
      ptn = RTM::Partition.where(:number => ptn.to_i).first
    end
    tc  = find_code_in_partition(code, ptn)
    unless tc
      tc = RTM::TermCode.new(:code => code.to_i)
      ptn.add_term_code(tc)
      # note that there is no block for this code!
    end
    tc
  end
  
  def find_code_in_partition(code, ptn = nil)
    if ptn.nil? && code.to_s =~ /\d+::?\d+/
      ptn, code = code.split(/::?/).map(&:to_i)
    end
    unless ptn.is_a?(RTM::Partition)
      ptn = RTM::Partition.where(:number => ptn.to_i).first
    end
    found_codes = RTM::TermCode.where(:code => code.to_i).all
    found_codes.find { |tc| tc.partition.id == ptn.id}
  end

  def find_or_create_partition(number)
    _find_or_create(RTM::Partition, :number => number.strip.to_i)
  end
  
  def find_or_create_block(str)
    _find_or_create(RTM::Block, :name => str.strip)
  end
  
  def create_range_in_block(lower, upper, block)
    existing_range = find_range_in_block(lower, upper, block)
    raise "Range #{lower} to #{upper} already exists in partition #{block.partition.number}, block #{block.name}." if existing_range
    range = _create(RTM::Range, :lower => lower, :upper => upper)
    range.block = block
    range
  end

  def find_range_in_block(lower, upper, block)
    ranges = block.ranges
    ranges.each do |r|
      return r if r.upper == upper && r.lower == lower
      if lower.between?(r.lower, r.upper) or upper.between?(r.lower, r.upper)
        raise "Tried to create range that collides in partition #{block.partition.number}, block #{block.name}.  #{lower} to #{upper} conflicts with the existing range that has #{r.lower} to #{r.upper}."
      end
    end
    nil
  end
  
  def create_term(refid, part_code)
    type = part_code =~ /4::/ ? RTM::Unit : RTM::Term
    # ChangeTracker.start
    term = type.new
    term.refid     = RTM.find_or_create_refid(refid)
    term.term_code = RTM.find_or_create_code_in_partition(part_code)
    term.save
    term
    # ChangeTracker.commit
  end
      
  def find(val1, val2 = nil)
    if val2
      find_term("#{val1} #{val2}")
    else
      if val1 =~ /\d+::\d+\s+[A-Za-z_0-9]+/ || val1 =~ /[A-Za-z_0-9]+\s+\d+::\d+/
        find_term(val1)
      elsif val1 =~ /\d+::\d+/
        get_code_obj(val1)
      elsif val1 =~ /^[A-Za-z_0-9]+$/
        find_refid(val1)
      else
        puts "Can't process #{val1}"
      end
    end
  end
  
  # This might be a tad brittle
  def find_term_views(entry)
    # puts Rainbow(entry).magenta
    entries = entry.split(/[\s,;\|]/)
    refids, codes = entries.partition { |e| e =~ /[A-Z]+_/ }
    q = RTM::TermView
    refids.each { |r| q = q.where(Sequel.ilike(:refids, "%#{r}%")) }
    codes.each { |tc| q = q.where(Sequel.ilike(:term_codes, "%#{tc}%")) }
    tvs = q.all
    if refids.any?
      # puts Rainbow(refids).yellow
      tvs.select! do |tv|
        tvr = tv.refids
        # puts tvr
        (tv.refids.split(/\s/) & refids).any?
      end
    end
    if codes.any?
      tvs.select! { |tv| (tv.term_codes.split(/\s/) & codes).any? }
    end
    # puts "#{tvs.count}: #{ tvs.map { |tv| tv.refids }.join(' || ') }"
    tvs.any? ? tvs : nil
  end
  
  def find_term(str)
    terms = get_terms(*(str.split(/\s+/)))
    raise "Multiple terms for #{refid} #{part_code}" if terms.count > 1
    terms.first  
  end
  
  def find_codeless_terms(str)
    refid = RTM.find_refid(str)
    return [] unless refid
    terms = refid.terms.select { |t| t.term_code.nil? }
  end
  
  def find_refid(refid)
    return nil if refid.nil? || refid.strip.empty?
    RTM::RefID.where(:value => refid.strip).first
  end
  
  def find_unit(input)
    return nil if input.nil? || input.strip.empty?
    refid = RTM::RefID.where(:value => input.strip).first
    return nil unless refid
    terms = refid.terms
    return terms if terms.any?
    nil
  end
  
  def find_enum(input)
    return nil if input.nil? || input.strip.empty?
    refid = RTM::RefID.where(:value => input.strip).first
    return refid.terms if refid
    # find a token.  note that a Token is a Term but it doesn't have a RefID or TermCode
    tokens = RTM::Token.where(:value => input.strip).all
    return tokens if tokens
    nil
  end

  def find_unit_group(input)
    return nil if input.nil? || input.strip.empty?
    refid = RTM::RefID.where(:value => input.strip).first
    refid&.unit_group
  end
  
  def find_enum_group(input)
    return nil if input.nil? || input.strip.empty?
    refid = RTM::RefID.where(:value => input.strip).first
    refid&.enumeration_group
  end
  
  def find_terms_by_cf_code(cf_code)
    return nil unless cf_code && !cf_code.to_s.empty?
    partition_number = cf_code.to_i/(2**16)
    code = cf_code.to_i - (partition_number * (2**16))
    partition = RTM::Partition.where(:number => partition_number).first
    raise "There is no partition number #{partition_number} for context free code #{cf_code}" unless partition
    tc = RTM::TermCode.where(:code => code).first
    return nil unless tc
    tc.terms
  end
  
  def find_term_by_cf_code(cf_code)
    find_terms_by_cf_code.first
  end
  
  def find_refids_by_cf_code(cf_code)
    terms = find_terms_by_cf_code
    return nil if terms.nil?
    terms.map(&:refid)
  end
  
  # This is a bad method to use
  def find_refid_by_cf_code(cf_code)
    Kernel.warn('RTM.find_refid_by_cf_code is not a reliable method!')
    find_term_by_cf_code.refid
  end
  
  def find_token(name)
    RTM::Token.where(:name => name.strip).first
  end
    
end
