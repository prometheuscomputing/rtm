module RTM
  module_function
  
  # expects part_code to be in the form of '<num>::<num>'
  def get_code_objs(part_code)
    ptn, code = part_code.split(/::?/).map(&:to_i)
    partition = RTM::Partition.where(:number => ptn).first
    term_code = RTM::TermCode.where(:partition_id => partition.id, :code => code).all
  end
  
  # expects part_code to be in the form of '<num>::<num>'
  def get_code_obj(part_code)
    raise unless part_code.to_s[0]
    ptn, code = part_code.split(/::?/).map(&:to_i)
    partition = RTM::Partition.where(:number => ptn).first
    term_code = RTM::TermCode.where(:partition_id => partition.id, :code => code).first
  end
  self.singleton_class.send(:alias_method, :find_code, :get_code_obj)
  
  def get_terms(val1, val2)
    if val1 =~ /:/
      part_code, refid_val = val1, val2
    else
      part_code, refid_val = val2, val1
    end
    refid = find_refid(refid_val)
    # puts "No Refid #{refid_val}" unless refid
    return [] unless refid
    tcode = find_code(part_code)
    # puts "No TermCode #{part_code}" unless tcode
    return [] unless tcode
    tcode.terms & refid.terms
  end
  self.singleton_class.send(:alias_method, :find_terms, :get_terms)
  
end
