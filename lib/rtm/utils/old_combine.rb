module RTM
  module_function

  # # this is an additive merge for associations.  still not sure on attributes
  # def combine_definitions(obj1, obj2, force = false)
  #   raise "@combine_definitions is not longer something you should do, baby!"
  #   raise unless obj1.class == obj2.class
  #   raise "obj1 == obj2 YOU BIG DUMMY" if obj1.id == obj2.id && obj1.class == obj2.class
  #   # puts "Definition 1 status: #{obj1.status&.value}"
  #   # puts "Definition 2 status: #{obj2.status&.value}"
  #   attrs  = obj1.class.attributes
  #   assocs = obj1.class.associations
  #   ChangeTracker.start
  #   attr_results  = attrs.collect  { |a| merge_attribute(a, obj1, obj2, force) }
  #   assoc_results = assocs.collect { |a| merge_association(a, obj1, obj2, force) }
  #   results = (attr_results + assoc_results).flatten.compact
  #   if results.include?(:error)
  #     ChangeTracker.cancel
  #     RTM.error("You must complete the merger of definitions for #{obj1.refids_status} and #{obj2.refids_status} manually!", :red)
  #     return nil
  #   else
  #     obj2.destroy
  #     ChangeTracker.commit
  #     return obj1
  #   end
  # end
  #
  # def merge_attribute(data, obj1, obj2, force = false)
  #   return if data.last[:derived]
  #   attribute = data.first
  #   val1 = obj1.send(attribute)
  #   val2 = obj2.send(attribute)
  #   return if val1 == val2
  #   return if val2.nil?
  #   if val1.nil?
  #     set_merged_property(attribute, obj1, val2)
  #     return
  #   end
  #   if val1.is_a?(String)
  #     return if val2.empty?
  #     if val1.empty?
  #       set_merged_property(attribute, obj1, val2)
  #       return
  #     end
  #     combined_value = val1 + ' <&|> ' + val2
  #     RTM.warn("While combining #{obj1.refids_status} with #{obj2.refids_status}, the values of #{attribute} were different.  The merged value is '#{combined_value}'.", :none)
  #     set_merged_property(attribute, obj1, combined_value)
  #     return
  #   end
  #   if force
  #     RTM.warn("Can't combine #{attribute} for 1:#{obj1.refids_status} with 2:#{obj2.refids_status} because the values of #{attribute}. Keeping value 1: #{val1}.  Discarding value 2: #{val2}", :none)
  #     return
  #   else
  #     # Hopefully just integers and booleans.
  #     RTM.error("Could not combine the #{attribute} from #{obj1.refids_status} and #{obj2.refids_status} into a single definition. Fix manually! Inequal #{attribute}: #{val1.inspect}  != #{val2.inspect}.", :red)
  #     return :error
  #   end
  # end
  #
  # def set_merged_property(attribute, obj, val)
  #   obj.send("#{attribute}=".to_sym, val)
  # end
  #
  # def merge_association(data, obj1, obj2, force = false)
  #   return if data.last[:derived]
  #   assoc = data.first
  #   val1 = obj1.send(assoc)
  #   val2 = obj2.send(assoc)
  #   return if val1 == val2
  #
  #   if data.first == :status
  #     if obj1.status&.value == 'Published' || obj2.status&.value == 'Published'
  #       obj1.status = 'Published'
  #     end
  #     return
  #   end
  #
  #   if data.last[:type].to_s =~ /_to_one/
  #     return if val2.nil?
  #     if val1.nil?
  #       set_merged_property(assoc, obj1, val2)
  #       return
  #     end
  #     if force
  #       RTM.warn("Can't combine #{assoc} from 1:#{obj1.refids_status} and 2:#{obj2.refids_status} into a single definition because only one value is allowed.  Keeping #{assoc} from #1.", :none)
  #       return
  #     else
  #       RTM.error("Could not combine the #{assoc} from #{obj1.refids_status} and #{obj2.refids_status} into a single definition. Fix manually!\n  #{val1.inspect}\n  #{val2.inspect}", :red)
  #       return :error
  #     end
  #   end
  #   RTM.warn("Combining the #{assoc} from #{obj1.refids_status} and #{obj2.refids_status} into a single definition.", :none)
  #   set_merged_property(assoc, obj1, (val1 + val2).uniq)
  #   nil
  # end
  #
end
