module RTM
  module_function
    
  def deprecate_refid(term, note = nil)
    set_refid_status(term, 'Deprecated', note)
  end

  def deprecate_code(term, code, note = nil)
    set_code_status(term, 'Deprecated', note)
  end

  def deprecate_term(term, note = nil)
    # puts "Deprecating #{term.class}[#{term.id}] #{term.refid.value} #{term.part_code}"
    set_term_status(term, 'Deprecated', note)
  end

  def withdraw_term(term, note = nil)
    set_term_status(term, 'Withdrawn', note)
  end

  def set_term_status(term, status, note = nil)
    ChangeTracker.start
    term.status = status
    term.add_note(RTM.find_or_create_note('Status note: ' + note)) if note
    term.save
    ChangeTracker.commit
    true
  end

  def withdraw_refid(term, note = nil)
    set_refid_status(term, 'Withdrawn', note)
  end

  def withdraw_code(term, note = nil)
    set_code_status(term, 'Withdrawn', note)
  end
  
end
