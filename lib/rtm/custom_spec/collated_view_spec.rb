# RTM::CollatedView ============================================================

summary_view(RTM::CollatedView) {
  string('refids', :label => 'Refids')
  string('secondary_refids', :label => 'Secondary Refids')
  string('deprecated_refids', :label => 'Deprecated Refids')
  string('withdrawn_refids', :label => 'Withdrawn Refids')
  string('term_codes', :label => 'Term Codes')
  string('secondary_term_codes', :label => 'Secondary Term Codes')
  string('cf_term_codes', :label => 'Cf Term Codes')
  string('secondary_cf_term_codes', :label => 'Secondary Cf Term Codes')
  string('discriminators', :label => 'Discriminators')
  string('description', :label => 'Description')
  string('systematic_name', :label => 'Systematic Name')
  string('common_term', :label => 'Common Term')
  string('acronym', :label => 'Acronym')
  string('status', :label => 'Status')
}
detail_view(RTM::CollatedView) {
  message('summary_html', :label => '')
  hide('refids', 'secondary_refids', 'deprecated_refids', 'withdrawn_refids', 'term_codes', 'secondary_term_codes','cf_term_codes', 'secondary_cf_term_codes', 'discriminators', 'systematic_name', 'common_term', 'acronym', 'status')
  # attributes_from(RTM::CollatedView)
  view_ref(:Summary, 'terms', RTM::Term, :label => 'Term')
}

detail_view(RTM::UnitView) {
  message('summary_html', :label => 'Unit Summary')
  hide('refids', 'secondary_refids', 'deprecated_refids', 'withdrawn_refids', 'term_codes', 'secondary_term_codes','cf_term_codes', 'secondary_cf_term_codes', 'discriminators', 'systematic_name', 'common_term', 'acronym', 'status', 'description', 'dim', 'dim_c', 'symbol', 'ucum_units')
  # attributes_from(RTM::CollatedView)
  view_ref(:Summary, 'terms', RTM::Term, :label => 'Term')
}

detail_view(RTM::TermView) {
  message('summary_html', :label => 'Term Summary')
  relabel('classifier_restrictions', 'Usage in Profile Elements')
  ug_blank = proc { |arg| arg.unit_groups.nil? || arg.unit_groups.empty? }
  eg_blank = proc { |arg| arg.enum_groups.nil? || arg.enum_groups.empty? }
  bsg_blank = proc { |arg| arg.body_site_groups.nil? || arg.body_site_groups.empty? }
  hide_if(ug_blank, 'unit_groups')
  hide_if(eg_blank, 'enum_groups')
  hide_if(bsg_blank, 'body_site_groups')
  hide('refids', 'secondary_refids', 'deprecated_refids', 'withdrawn_refids', 'term_codes', 'secondary_term_codes','cf_term_codes', 'secondary_cf_term_codes', 'discriminators', 'systematic_name', 'common_term', 'acronym', 'status', 'description', 'units_table', 'enums_table', 'body_sites_table')
  # attributes_from(RTM::CollatedView)
  view_ref(:Summary, 'terms', RTM::Term, :label => 'Term')
}