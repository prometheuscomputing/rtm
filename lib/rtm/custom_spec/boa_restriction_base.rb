require_relative 'label_procs'

collection(:Summary, Boa::RestrictionBase) do
  string('restriction_base_display_name', :search_filter => :disabled)
  string('description')
  reorder('restriction_base_display_name', 'description', :to_beginning => true)
end

organizer(:Summary, Boa::RestrictionBase) do
  string('restriction_base_display_name', :label => 'Name')
  string('description')
  disable('restriction_base_display_name', 'description')
  reorder('restriction_base_display_name', 'description', :to_beginning => true)
end

organizer(:Details, Boa::ClassifierRestriction) do
  view_ref(:Author, 'author', :label => 'Author')
  disable('author')
  relabel('properties', 'Contained Elements')
  hide('association_restriction')
  view_ref(:Summary,'allowed_base', :label => 'Constraint On')
  view_ref(:Summary,'restrictions', :label => 'Derivative Restrictions') # this is hacked in because for some reason the viewref wasn't created in the generated gem spec
  hide('base')
  # relabel('restrictions', 'Derivative Restrictions')
  button('set_tree_view_root', label: 'Tree View:', button_text: 'Set as Root', save_page: true)
  html(:label => 'meta_section', :html => Boa::META_SECTION)
  reorder('description', 'allowed_base', 'properties', 'restrictions', 'set_tree_view_root', 'meta_section', 'specification_type', :to_beginning => true)
  
  # Include update & creation datetime, not editable
  timestamp('created_at', :disabled => true)
  timestamp('updated_at', :disabled => true)
end

collection(:Summary, Boa::ClassifierRestriction) do
  hide('term_group_type', 'specification_type')
  string('info', :label => 'Summary')
  reorder('info', 'description', :to_beginning => true)  
  # Include update & creation datetime, not editable
  timestamp('created_at', :disabled => true)
  timestamp('updated_at', :disabled => true)
end
