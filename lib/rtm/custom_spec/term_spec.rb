# RTM::Term ====================================================================
organizer(:Details, RTM::Term) {
  message('summary_html', :label => '')
  # view_ref(:Summary, 'enums', RTM::AssignableEnum, :label => 'Enums')
  # view_ref(:Summary, 'notes', RTM::Note, :label => 'Notes', :orderable => true)
  # view_ref(:Summary, 'rosetta_entries', RTM::RosettaEntry, :label => 'Rosetta Entries')
  # view_ref(:Summary, 'tags', RTM::Tag, :label => 'Tags')
  # view_ref(:Summary, 'units', RTM::AssignableUnit, :label => 'Units')
  # view_ref(:Summary, 'definition', RTM::Definition, :label => 'Definition')
  # view_ref(:Summary, 'discriminators', RTM::Discriminator, :label => 'Discriminators', :orderable => true)
  relabel('loincs', 'LOINC')
  # view_ref(:Summary, 'ref_id', RTM::RefID, :label => 'Ref ID')
  # view_ref(:Summary, 'term_code', RTM::TermCode, :label => 'Term Code')
  disable('status')
  hide('wildcard', 'refid_status', 'code_status', 'auto_expanded', 'infix_depths', 'refid_preferred', 'code_preferred', 'termed_nodes', 'table_rows', 'event_pairs', 'uses', 'used_by', 'sources', 'specializations', 'generalization', 'source_pairs', 'type', 'status')
  order('summary_html', :to_beginning => true)
}

