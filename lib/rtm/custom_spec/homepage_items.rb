homepage_items_to_create = [
  ['RTM::CollatedView', 'collated_views'],
  ['RTM::TermView', 'term_views'],
  ['RTM::UnitView', 'unit_views'],
  ['RTM::SynonymSet', 'synonym_sets'],
  ['RTM::Partition',  'partitions'],
  ['RTM::Block',      'blocks'],
  ['RTM::RefID',      'refids'],
  ['RTM::TermCode',   'term_codes'],
  # ['RTM::Discriminator', 'discriminators'],
  ['RTM::DiscriminatorSet', 'discriminator_sets'],
  ['RTM::RosettaEntry', 'rosettas'],
  ['RTM::Vendor', 'vendors'],
  ['RTM::Loinc', 'loincs'],
  ['RTM::UCUM_Unit', 'ucums'],
  ['RTM::UnitGroup', 'unit_groups'],
  ['RTM::Unit', 'units'],
  ['RTM::EnumerationGroup', 'enum_groups'],
  ['RTM::EnumGrpMember', 'enum_vals'],
  ['RTM::Token', 'tokens'],
  ['RTM::TermSource', 'term_sources'],
  ['Boa::AssociationRestriction', 'association_restrictions'],
  ['Boa::AttributeRestriction', 'attribute_restrictions'],
  ['Boa::ClassifierRestriction', 'classifier_restrictions'],
  # ['Boa::ClassifierRestriction', 'device_profiles'],
  ['Boa::Condition', 'conditions'],
  ['Boa::ValueConstraint', 'value_constraints'],
  ['UmlMetamodel::Primitive' ,'uml_primitives'],
  ['UmlMetamodel::Stereotype' ,'uml_stereotypes'],
  ['UmlMetamodel::AppliedStereotype' ,'uml_applied_stereotypes'],
  ['UmlMetamodel::Package' ,'uml_packages'],
  ['UmlMetamodel::Model' ,'uml_models'],
  ['UmlMetamodel::Interface' ,'uml_interfaces'],
  ['UmlMetamodel::AppliedTag' ,'uml_applied_tags'],
  # ['UmlMetamodel::ElementInstance' ,'element_instances'],
  # ['UmlMetamodel::AnyOfType' ,'any_of_types'],
  ['UmlMetamodel::EnumerationLiteral', 'uml_enumeration_literals'],
  ['UmlMetamodel::Class', 'uml_classes'],
  ['UmlMetamodel::Project', 'uml_projects'],
  ['UmlMetamodel::Profile', 'uml_profiles'],
  ['UmlMetamodel::Datatype', 'uml_datatypes'],
  ['UmlMetamodel::Association', 'uml_associations'],
  ['UmlMetamodel::Property', 'uml_properties'],
  ['UmlMetamodel::Enumeration', 'uml_enumerations'],
  ['UmlMetamodel::Tag', 'uml_tags'],
  ['Rosetta::DeviceProfile', 'device_profiles'],
  ['Rosetta::ModuleProfile', 'module_profiles']
]
homepage_items_to_create.each do |klass, getter|
  homepage_item(klass.to_sym, :getter => getter)
end

module GuiSpec
  class Home
    
    def all_term_codes
    end
    # May not be an instance method -- caught by method_missing and sent to class?
    # puts methods.sort
    # old_device_profiles = instance_method(:device_profiles)
    # define_method(:device_profiles) do |*optional_args|
    #   pp optional_args
    #   optional_args.merge({:filter => {:specification_type_id => Boa::DEVICE_SPECIFICATION_ID }})
    #   old_device_profiles.bind(self).(args)
    # end
  end
end
