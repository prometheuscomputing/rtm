collection(:Summary, Boa::PropertyRestriction) do
  string('property_restriction_info', :label => 'Type and Term', :disabled => true, :search_filter => :disabled)
  string('multiplicity_string', :label => 'Multiplicity')
  hide('lower', 'upper')
  reorder('property_restriction_info', 'description', 'multiplicity_string', :to_beginning => true)
end

organizer(:Details, Boa::AttributeRestriction) do
  # message('available_attribute_names')
  view_ref(:Summary,'allowed_base', :label => 'Base Property')
  hide('base')
  reorder('description', 'lower','upper', 'available_association_names', 'allowed_base', :to_beginning => true)
end

organizer(:Details, Boa::AssociationRestriction) do
  # message('available_association_names')
  view_ref(:Summary,'allowed_base', :label => 'Base Property')
  hide('base')
  reorder('description', 'lower','upper', 'available_association_names', 'allowed_base', :to_beginning => true)
end

# Not sure when there is ever a collection of only AssociationRestriction
collection(:Summary, Boa::AssociationRestriction) do
  string('qualified_name')
  reorder('qualified_name', :to_beginning => true)
end
