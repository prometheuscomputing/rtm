collection(:Summary, UmlMetamodel::Property) do
  string('type_name', :search_disabled => true)
  order('name', 'type_name', :to_beginning => true)
  hide('guid', 'visibility')
end

collection(:Summary, UmlMetamodel::Class) do
  hide('guid', 'is_singleton')
  order('name', 'is_abstract', :to_beginning => true)
end
