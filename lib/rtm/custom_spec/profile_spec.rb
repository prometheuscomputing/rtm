organizer(:Details, Rosetta::Profile) {
  #   string('name', :label => 'Name')
  #   date('date', :label => 'Date')
  #   integer('version', :label => 'Version')
  disable('date', 'version', 'authors')
  hide('head') # may not want to do this...
  relabel('source_xml', 'Source XML')
  relabel('date', 'System Entry Timestamp')
  relabel('mds', 'MDS Profile Root')
  #   view_ref(:Summary, 'authors', Gui_Builder_Profile::User, :label => 'Authors')
  #   view_ref(:Summary, 'mds', Rosetta::MDS, :label => 'MDS')
  #   view_ref(:Summary, 'previous', Rosetta::Profile, :label => 'Previous')
  #   view_ref(:Summary, 'next', Rosetta::Profile, :label => 'Next')
  # button('set_tree_view_root', label: 'Set Tree Root', button_text: 'Root', save_page: false, display_result: :none)
  button('download_rch_html', label: 'Rosetta Containment HTML', button_text: 'RCH HTML', save_page: true, display_result: :file)
  # button('download_summary_hierarchy', label: 'Rosetta Containment XML', button_text: 'Download XML', save_page: true, display_result: :file)
  # button('download_summary_hierarchy_with_codes', label: 'Rosetta Containment XML with codes', button_text: 'Download XML', save_page: true, display_result: :file)
  reorder('set_tree_view_root', 'name', 'version', 'date', 'download_rch_html', 'root', 'authors', 'previous', 'next', 'source_xml', :to_beginning => true)
}
organizer(:Details, Rosetta::DeviceProfile) {
  inherits_from_spec(:Details, Rosetta::Profile, :Organizer, true)
  reorder('set_tree_view_root', 'name', 'version', 'date', 'download_rch_html', 'root', 'authors', 'previous', 'next', 'source_xml', :to_beginning => true)
}
organizer(:Details, Rosetta::ModuleProfile) {
  inherits_from_spec(:Details, Rosetta::Profile, :Organizer, true)
  reorder('set_tree_view_root', 'name', 'version', 'date', 'download_rch_html', 'root', 'authors', 'previous', 'next', 'source_xml', :to_beginning => true)
}
