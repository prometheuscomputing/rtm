global_spec_modifier { |view|
  kontent = view.content
  view_classifier = view.data_classifier

  # Disabling creation of Association- and AttributeRestriction#base
  view.disable_creation('allowed_base') if view_classifier.parents.include?(Boa::PropertyRestriction)
  
  # disable all fields in organizer summary widgets
  if view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Summary)
    getters = kontent.collect { |w| w.getter.to_s }
    view.disable(*getters)
  end
  
  # Disabling all editing of any elements of the UML model through the UI.  The import process should have initialized everything as it should be so no need to edit in application.
  if view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Details) && view_classifier.to_s =~ /UmlMetamodel::/
    attr_getters = []
    assoc_getters = []
    kontent.collect do |w|
      next unless w.getter
      if w.is_a?(Gui::Widgets::ViewRef)
        assoc_getters << w.getter.to_s
      else
        attr_getters << w.getter.to_s
      end
    end
    view.disable(*attr_getters)
    view.disable_creation(*assoc_getters)
    view.disable_deletion(*assoc_getters)
    view.disable_addition(*assoc_getters)
    view.disable_removal(*assoc_getters)
  end
  
  if view.is_a?(Gui::Widgets::Collection) && view_classifier.to_s =~ /UmlMetamodel::/
    getters = kontent.collect { |w| w.getter.to_s }
    view.disable(*getters)
  end
#
#   # kontent.each do |w|
#   # end
}
