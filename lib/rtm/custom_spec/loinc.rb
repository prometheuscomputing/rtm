# RTM::Loinc ===================================================================
summary_view(RTM::Loinc) {
  string('number', :label => 'Number')
  string('description', :label => 'Description')
  string('common_name', :label => 'Common Name')
  string('version', :label => 'Version')
}
detail_view(RTM::Loinc) {
  hide_all
  message('summary_html')
}