organizer(:Details, RTM::Unit) {
  message('summary_html', :label => '')
  relabel('loincs', 'LOINC')
  # view_ref(:Summary, 'ref_id', RTM::RefID, :label => 'Ref ID')
  # view_ref(:Summary, 'term_code', RTM::TermCode, :label => 'Term Code')
  disable('status')
  hide('wildcard', 'refid_status', 'code_status', 'auto_expanded', 'infix_depths', 'refid_preferred', 'code_preferred', 'termed_nodes', 'table_rows', 'event_pairs', 'uses', 'used_by', 'sources', 'specializations', 'generalization', 'source_pairs', 'type', 'enums', 'units', 'dim', 'dim_c', 'symbol', 'definition', 'dimensions', 'terms', 'ucum_units', 'status')
  order('summary_html', :to_beginning => true)
}