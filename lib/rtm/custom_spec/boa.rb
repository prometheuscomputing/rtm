require_relative 'label_procs'

organizer(:Author, Gui_Builder_Profile::Person) do
  string('first_name', :label => 'First Name')
  string('last_name', :label => 'Last Name')
  string('email', :label => 'Email')
end
