organizer(:Details, Home) {
  view_ref(:HomeViews, 'collated_views', :label => 'Search RTMMS')#, :expanded => true)
  view_ref(:HomeViews, 'unit_views', :label => 'Search Units')#, :expanded => true)
  # view_ref(:Summary, 'synonym_sets', :label => 'Synonym Sets')
  view_ref(:Summary, 'unit_groups', :label => 'Unit Groups')
  view_ref(:Summary, 'ucums', :label => 'UCUM Units')
  view_ref(:Summary, 'enum_groups', :label => 'Enum Groups')
  view_ref(:Summary, 'loincs', :label => 'Mapped LOINC')
  view_ref(:Summary, 'tokens', :label => 'Tokens')
  # view_ref(:Blocks,  'blocks',         :label => 'Blocks')
  # view_ref(:Partitions, 'partitions',  :label => 'Partitions')
  # view_ref(:SearchByRefid, 'refids',   :label => 'RefIDs')
  # view_ref(:SearchByTermCode, 'term_codes', :label => 'Term Codes')
  # view_ref(:Summary, 'units', :label => 'Units')
  # view_ref(:Summary, 'documents', :label => 'Sources')
  # view_ref(:Summary, 'tables', :label => 'Tables')
  view_ref(:Summary, 'device_profiles', :label => 'Device Profiles')
  view_ref(:Summary, 'module_profiles', :label => 'Module Profiles')
  # view_ref(:DeviceProfiles, 'classifier_restrictions', :label => 'Device Profiles')
  # view_ref(:ModuleProfiles, 'classifier_restrictions', :label => 'Module Profiles')
  # view_ref(:Summary, 'uml_classes', :label => 'Profilable Classifiers')
  
  
  order('collated_views', 'unit_views', 'refids', 'term_codes', 'units', :to_beginning => true)
  disabled_widgets = ['uml_projects', 'uml_packages', 'uml_classes', 'uml_interfaces', 'uml_datatypes', 'uml_primitives', 'uml_enumerations', 'enum_groups', 'collated_views', 'unit_views', 'unit_groups', 'ucums', 'enum_groups', 'loincs', 'tokens']
  disable_creation(*disabled_widgets)
  disable_deletion(*disabled_widgets)
}

collection(:HomeViews, RTM::CollatedView, :page_size => 10) {
  inherits_from_spec(:Summary, RTM::CollatedView, :Collection, false)
  hide('secondary_term_codes')
}

collection(:HomeViews, RTM::UnitView, :page_size => 10) {
  inherits_from_spec(:HomeViews, RTM::CollatedView, :Collection, false)
  hide('secondary_term_codes')
}

collection(:DeviceProfiles, Boa::ClassifierRestriction, :page_size => 10, :filter_value => { :integer =>  {:specification_type_id => Boa::DEVICE_SPECIFICATION_ID} }) {
  inherits_from_spec(:Summary, Boa::ClassifierRestriction, :Collection, false)
}

collection(:ModuleProfiles, Boa::ClassifierRestriction, :page_size => 10, 
  :filter_value => { 
    :not => { 
      :integer =>  { :specification_type_id => Boa::DEVICE_SPECIFICATION_ID } 
    } 
  }) {
  inherits_from_spec(:Summary, Boa::ClassifierRestriction, :Collection, false)
}

collection(:Summary, RTM::SynonymSet, :page_size => 10) {
  string('summary')
  hide('term_code_synonym')
}

# collection(:Summary, RTM::Loinc, :page_size => 20) {
#   disable
# }

collection(:Blocks, RTM::Block, :page_size => 100) {
  inherits_from_spec(:Summary, RTM::Block, :Collection, false)
  string('refid', :label => 'Name')
  reorder('number', 'refid', :to_beginning => true)
}

collection(:Partitions, RTM::Partition, :page_size => 100) {
  inherits_from_spec(:Summary, RTM::Partition, :Collection, false)
}

collection(:SearchByRefid, RTM::RefID, :page_size => 15) {
  string('value', :label => 'RefID')
  string('term_summary', :label => 'Term Data', :search_filter => :disabled)
}

collection(:SearchByTermCode, RTM::TermCode, :page_size => 15) {
  integer('code', :label => 'Term Code')
  string('term_summary', :label => 'Term Data', :search_filter => :disabled)
}

collection(:SearchByTermData, RTM::Definition, :page_size => 15) {
  inherits_from_spec(:Summary, RTM::Definition, :Collection, false)
  # string('refids_string', :label => 'RefID', :search_filter => :disabled)
  # string('term_codes_string', :label => 'Term Code', :search_filter => :disabled)
  string('refids_and_codes', :label => 'RefID and Term Code', :search_filter => :disabled)
}