require 'rainbow'
boa_ext_dir = File.expand_path(File.join(__dir__, '../boa'))
rtm_ext_dir = File.expand_path(File.join(__dir__, 'model_extensions'))
profiles_dir = File.expand_path(File.join(__dir__, 'profiles'))
Dir.glob(File.join(boa_ext_dir, '**', '*.rb'), &method(:load))
Dir.glob(File.join(rtm_ext_dir, '**', '*.rb'), &method(:load))
Dir.glob(File.join(profiles_dir, '**', '*.rb'), &method(:load))
load File.expand_path(File.join(__dir__, 'utils.rb'))
# require_relative 'profiles/profiles'
# load File.expand_path(File.join(__dir__, 'profiles/profile_builder.rb'))
# load File.expand_path(File.join(__dir__, 'profiles/profile_builder_rcp.rb'))

require_relative 'debug'
require_relative 'utils'
require_relative 'reports'
require_relative 'tree_view'
require_relative '../../migrate/migrate'
# RCP = Rosetta # because I'm lazy
# HACK to fix bad singularization by inflector
Rosetta::Profile.properties[:next][:singular_opp_getter] = :previous
# Rosetta::MDS.properties[:profile][:singular_opp_getter] = :mds
Rosetta::MDS.properties[:vmds][:singular_opp_getter] = :mds
Rosetta::MDS.properties[:metrics][:singular_opp_getter] = :mds
