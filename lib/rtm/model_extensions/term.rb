module RTM
  class Term
    alias_method :refid, :ref_id
    alias_method :refid=, :ref_id=
    
    def acronym; definition&.acronym; end
    def common_term; definition&.common_term; end
    def systematic_name; definition&.systematic_name; end
    def aux; definition&.aux; end

    def deprecated?
      status&.value&.downcase == 'deprecated'
    end
    
    def withdrawn?
      status&.value&.downcase == 'withdrawn'
    end
    
    def published2019?
      s = status&.value
      s == 'Published' || s == 'Published (10101:2019 Annex C)'
    end
    alias_method :from_10101R?, :published2019?
    
    def published2019a? # published in 10101:2019 Annex A
      s = status&.value
      s == 'Published'
    end
    
    def published2019c?
      s = status&.value
      s == 'Published (10101:2019 Annex C)'
    end
    
    # This includes all terms that are at least Provisional and includes Deprecated terms
    def released?
      s = status&.value
      s =~ /Published|Provisional|Approved|Deprecated/i
    end
    
    def sort_val
      s = status&.value
      if refid_preferred
        return -900 if s == 'Published'
        return -800 if s == 'Published (10101:2019 Annex C)'
        return -700 if s =~ /Published/i
      end
      return -600 if s == 'Published'
      return -500 if s == 'Published (10101:2019 Annex C)'
      return -400 if s =~ /Published/i
      return 100 if s == 'Deprecated'
      return 200 if s == 'Provisional'
      return 500 if s == 'Proposed'
      return 900 if s == 'Withdrawn'
      return 0
    end
    
    def synonyms(include_self = true)
      # return [] unless synonyms?
      syns = synonym_sets.map(&:terms).flatten.uniq
      syns.reject! { |term| term == self } unless include_self
      syns
    end
    
    def synonyms?
      synonym_sets_count > 0
    end
    
    def primary_term
      return self unless synonyms?
      return synonyms.sort_by { |t| t.sort_val }.first # #sort_val be used instead of doing the stuff below here?
      # return self if terms.count == 1
      # preferred = terms.find { |t| t.refid_preferred }
      # return preferred if preferred
      # bad, good = terms.partition { |t| t.deprecated? || t.withdrawn? }
      # if good.any?
      #   pubA = good.find { |t| t.published2019a? }
      #   pubC = good.find { |t| t.published2019c? }
      #   return pubA || pubC || good.first
      # else
      #   return bad.find { |t| t.deprecated? } || bad.find { |t| t.withdrawn? }
      # end
    end
    
    def related_terms
      ((term_code&.terms || []) + (refid&.terms || [])).uniq
    end
    
    def deconflate_lead_terms(terms)
      return terms if terms.count < 2
      return terms if discriminators_count == 0
      # stat
      # puts discriminator_set_names
      if discriminator_set_names.count == 1
        dsname = discriminator_set_names.first
        return terms unless dsname == 'LEAD1' || dsname == 'LEAD2'
        other_dsname = dsname == 'LEAD1' ? 'LEAD2' : 'LEAD1'
        terms = terms.reject do |term|
          term.discriminator_set_names.include?(other_dsname) &&
          refid != term.refid
        end
        return terms        
      else
        return terms if discriminator_set_names.sort == ['LEAD1', 'LEAD2']
        raise "#{refid.value} #{part_code} had multiple discriminators: #{discriminators_report}.\n#{discriminators.pretty_inspect}\n#{discriminator_sets.pretty_inspect}"
      end
    end
    
    def preferred_refids
      # return [refid&.value].compact unless term_code
      return [refid&.value].compact unless synonyms?
      terms = synonyms
      terms.reject! { |t| t.deprecated? || t.withdrawn? }
      terms = deconflate_lead_terms(terms)
      preferred = terms.select { |t| t.refid_preferred }
      preferred = terms unless preferred.any?
      if preferred.any?
        published_2019, published_rtmms = preferred.partition { |t| t.published2019? }
        preferred = published_2019 if published_2019.any?
      end
      _refids(preferred)
    end
     
    def secondary_refids # without withdrawn refids
      # return [] unless term_code
      return [] unless synonyms?
      terms = synonyms
      terms = term_code.terms.reject { |t| t.deprecated? || t.withdrawn? }
      preferred = terms.select { |t| t.refid_preferred }
      if preferred.any?
        _refids(terms - preferred)
      else
        []
      end
    end

    def deprecated_refids
      if synonyms?
        terms = synonyms.select { |t| t.deprecated? }
        _refids(terms)
      else
        return deprecated? ? _refids(self) : []
      end
    end
    
    def withdrawn_refids
      if synonyms?
        terms = synonyms.select { |t| t.withdrawn? }
        _refids(terms)
      else
        return withdrawn? ? _refids(self) : []
      end
    end
    
    def _refids(terms)
      terms = [terms].flatten.compact
      return terms if terms.empty?
      terms.map(&:refid).compact.map(&:value)
    end

    
    def warning_id
      "#{self.class.name.demodulize} #{refid.value} #{part_code}"
    end
    
    derived_attribute(:summary_html, String)
    def summary_html
      lines = []
      lines << "Status: #{status&.value || '-'}"
      lines << "RefID: <b>#{refid.value}</b>"
      lines << "Term Code: <b>#{part_code}</b>"
      lines += definition&.summary_lines || ['No Definition']
      if rosetta_entries.any?
        lines << "Rosetta Entries:"
        lines += rosetta_entries.map(&:summary_lines).flatten
      end
      lines.join('<br>')
    end
    
    def summary(html = true)
      lines = []
      first_line = "#{self.class.name.demodulize}"
      first_line << "[#{self.id}]" unless html
      first_line << " #{status&.value}" if status
      lines << first_line
      lines << "RefID: #{refid.value}"
      lines << "Term Code: #{part_code}"
      lines += definition&.summary_lines || ['No Definition']
      if rosetta_entries.any?
        lines << "Rosetta:"
        lines += rosetta_entries.map(&:summary_lines).flatten
      end
      if html
        lines.join('<br>')
      else
        lines.join("\n")
      end
    end
    
    # def update_definition(desc, sysname, cmnterm, acrnym, ox, warnings = false)
    def update_definition(opts, warnings = false)
      desc    = opts[:description]
      sysname = opts[:sys_name]
      cmnterm = opts[:common_term]
      acrnym  = opts[:acronym]
      ox      = opts[:aux]
              
      unless definition
        ChangeTracker.start unless opts[:no_ct]
        self.definition = RTM::Definition.create
        self.save
        ChangeTracker.commit unless opts[:no_ct]
      end
      definition.update(desc, sysname, cmnterm, acrnym, ox, opts[:no_ct], warnings = false)
    end
        
    def partition
      term_code&.partition
    end
    
    def partition_number
      partition&.number || 0
    end
    
    alias_method :code, :term_code
    
    def cf_code
      term_code&.cf_code
    end
    alias_method :cf_term_code, :cf_code
    alias_method :cfcode, :cf_code
    
    def part_code
      term_code&.part_code
    end
        
    def preferred_part_code
      if synonyms?
        terms = synonyms
        bad, good = terms.partition { |t| t.deprecated? || t.withdrawn? }
        if good.any?
          (good.find { |t| t.code_preferred } || good.find { |t| t.code_preferred } || good.first).part_code
        else
          (bad.find { |t| t.deprecated? } || bad.first).part_code
        end
      else
        part_code
      end
    end
    
    def other_part_codes
      if synonyms?
        synonyms.map(&:part_code).uniq - [preferred_part_code]
      else
        []
      end
    end
    
    def part_code_status
      pcs = part_code
      pcs << " (Preferred)" if code_preferred
      pcs
    end
    alias_method :code_stat, :part_code_status
    
    def refid_stat
      rs = refid&.value
      rs << " (Preferred)" if refid_preferred
      rs
    end
            
    def discriminator_offset
      discriminators.map(&:offset).reduce(&:+)
    end
    
    def discriminator_values
      discriminators.map(&:value).join(" ")
    end
    
    def discriminators_report
      rep = []
      ifixd = _infix_depths
      discriminators.each_with_index do |d, i|
        str = "#{d.discriminator_set.name}#{',' * ifixd[i].to_i} (#{d.offset})"
        # str << " #{d.value}" unless d.value.to_s.strip.empty?
        rep << str
      end
      if rep.count == 2 && rep[0].sub(/LEAD[1|2]/, 'LEAD') == rep[0].sub(/LEAD[1|2]/, 'LEAD')
        rep = [rep[0].sub(/LEAD[1|2]/, 'LEAD')]
      end
      rep.join('; ')
    end
    alias_method :discriminators_display, :discriminators_report
    
    def discriminator_sets
      discriminators.map(&:discriminator_set).uniq
    end
    
    def discriminator_set_names
      discriminator_sets.map(&:name)
    end
    
    def discriminator_sets_with_infix
      discs = discriminators
      return '' if discs.empty?
      discs.map(&:discriminator_set).map(&:name).zip(_infix_depths).map { |ds, infix| "#{ds}#{','*infix.to_i}"}.join(' ')
    end
    
    def _infix_depths
      infix_depths.to_s.split(/\s|,/)
    end
    
    def x_report_self
      "RefID: #{refid_stat}; Code: #{code_stat}; DiscriminatorSets: #{discriminator_sets_with_infix}"
    end
    
    # FIXME -- this can probably be removed...
    def report_discriminator_issue
      puts Rainbow("There is a problem with the discriminators for #{self.class}[#{self.id}] -- #{x_report_self}").orange
      term_codes.each { |tc| puts "#{tc.part_code} -- #{tc.discriminators_report}" }
      other_defs_refid = refids.map(&:definitions).flatten.reject { |d| d.id == id }
      if other_defs_refid.any?
        puts "There are #{other_defs_refid.count} definitions sharing the same refid:"
        other_defs_refid.each { |od| puts "  #{od.x_report_self}"}
      end
      other_defs_tc = term_codes.map(&:definitions).flatten.reject { |d| d.id == id }
      if other_defs_tc.any?
        puts "There are #{other_defs_refid.count} definitions sharing the same term_code:"
        other_defs_tc.each { |od| puts "  #{od.x_report_self}"}
      end
      puts
    end
    
    def valid_units_refids(include_groups = true)
      valid_units(include_groups).map(&:refid)
    end
    
    def valid_units_refid_values(include_groups = true)
      valid_units_refids(include_groups).map(&:value)
    end
    
    # NOTE - this includes all proposed stuff too so maybe not appropriate in all use cases?
    def valid_units(include_groups = true)
      uu = include_groups ? expanded_units : units_without_groups
      uu.reject { |u| u.status&.value.to_s =~ /Withdraw/i }
    end
    
    def expanded_units
      units.map { |u| u.is_a?(RTM::Unit) ? u : u.members }.flatten.uniq
    end
    
    def units_without_groups
      units.select { |u| u.is_a?(RTM::Unit) }
    end
    
    def unit_groups_refids
      _refids(units.select { |u| u.is_a?(RTM::UnitGroup) })
    end
    
    # NOTE - this includes all proposed stuff too so maybe not appropriate in all use cases?
    def valid_enums(include_groups = true)
      ee = include_groups ? expanded_enums : enums_without_groups
      ee.reject { |e| e.status&.value.to_s =~ /Withdraw/i }
    end
    
    def expanded_enums
      enums.map { |e| e.is_a?(RTM::EnumerationGroup) ? e.members : e }.flatten.uniq
    end
    
    def enums_without_groups
      enums.reject { |e| e.is_a?(RTM::EnumerationGroup) }
    end
    
    # This also includes Tokens.  It returns a collection of RefID and Token
    def valid_enums_refids(include_groups = true)
      valid_enums(include_groups).map { |e| e.is_a?(RTM::Token) ? e : e.refid }
    end
    
    def valid_enums_refid_values(include_groups = true)
      valid_enums_refids(include_groups).map { |x| x.is_a?(RTM::Token) ? "#{x.value}(#{x.asn1_bit})" : x.value }.uniq
    end
            
    def enum_groups_refids
      _refids(enums.select { |e| e.is_a?(RTM::EnumerationGroup) })
    end
    
    def body_site_groups_refids
      _refids(body_sites.select { |e| e.is_a?(RTM::EnumerationGroup) })
    end
    
    def units_matrix_for_csv
      expanded_units = units.collect { |u| u.is_a?(RTM::UnitGroup) ? u.members : u }.flatten.uniq
      expanded_units.collect { |u| u.units_matrix_data }
    end
    
    def enums_matrix_for_csv
      expanded_enums = enums.collect { |e| e.is_a?(RTM::EnumerationGroup) ? e.members : e }.flatten.uniq
      expanded_enums.collect { |e| e.enums_matrix_data }
    end
    
    def body_sites_matrix_for_csv
      expanded_body_sites = body_sites.collect { |e| e.is_a?(RTM::EnumerationGroup) ? e.members : e }.flatten.uniq
      expanded_body_sites.collect { |e| e.enums_matrix_data }
    end

    def enums_matrix_data
      output = [preferred_refids.join(' '), descriptions_string]
    end
    
    def descriptions_string
      return '' unless definition
      dvcs = definition.descriptions.map(&:value_content)
      if dvcs.count > 1
        output = []
        dvcs.each_with_index do |vc, i|
          if vc =~ /^<p>/
            output << vc.sub(/^<p>/, "<p>#{i + 1}. ")
          else
            output << "#{i + 1}. #{vc}"
          end
        end
        output.join("\n")
      else
        dvcs.first.to_s
      end
    end
    
    def get_infix_depth(discriminator)
      set_name = discriminator.discriminator_set.name
      infixes  = self.infix_depths.to_s.split(/\s+/)
      return 0 unless infixes.any?
      depth = infixes.find { |i| i.split(/:/).first == set_name }&.split(/:/).last.to_i
    end
    
    def notes_text
      notes.map(&:value_content).join("\n")
    end
    
  end
end