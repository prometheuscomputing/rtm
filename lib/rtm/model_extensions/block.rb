module RTM
  class Block
    derived_attribute :refid, String
    def refid
      name
    end
  end
end
