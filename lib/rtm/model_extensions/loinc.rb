module RTM
  class Loinc
    def to_title
      # "LOINC TERM <a href='https://loinc.org/#{number}/' target='_blank'>#{number}</a>"
      "LOINC TERM #{number}"
    end
    
    derived_attribute(:summary_html, String)
    def summary_html
      lines = []
      lines << "LOINC Code: #{number}"
      lines << "Common Name: #{common_name}"
      lines << "Description: #{description}"
      lines << "version: #{version}"
      lines << "UCUM Units: #{ucum_units.map(&:value).join(' ')}" if ucum_units.any?
      lines << "Visit LOINC page: <a href='https://loinc.org/#{number}/' target='_blank'>https://loinc.org/#{number}/</a>"
      # So far, all the terms are type RTM::Term...no RTM::Unit
      if terms.any?
        lines << '<br><br>'
        lines << "<strong>Mapped 11073 Terms:</strong>"
        terms.each do |term|
          if term.collated_view
            lines << "<a href='/RTM/TermView/#{term.collated_view.id}' target='_blank'><b>#{term.refid.value} #{term.part_code}</b></a> (Status: #{term.status&.value || '-'})<br>"
          else
            lines << "<a href='/RTM/Term/#{term.id}' target='_blank'><b>#{term.refid.value} #{term.part_code}</b></a> (Status: #{term.status&.value || '-'})<br>"
          end
          # tlines = []
          # tlines << "Status: #{term.status&.value || '-'}"
          # tlines << "RefID: "
          # tlines << "Term Code: <b>#{term.part_code}</b>"
          lines += term.definition&.summary_lines || ['No Definition<br>']
          lines << '<br>'
          # lines << tlines.join('<br>')
          # lines += term.
        end
      end
      lines = lines.join('<br>')
    end
    
  end
end
