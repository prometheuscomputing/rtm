module RTM
  class Description
    def matches?(str)
      value_content.strip == str.strip
    end
  end
end
