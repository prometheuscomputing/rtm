require 'nokogiri'
module Rosetta
  def self.built_modules
    @built_modules ||= []
    @built_modules
  end
  
  def self.reset_built_modules
    @built_modules = []
  end

  class Profile

  end
  
  class DeviceProfile
    def to_xml_file(path=nil)
      path ||= File.expand_path(File.join(__dir__, '../../../test_data', name.gsub(/\s/,'_') + '_output.xml'))
      File.open(path, 'w') { |f| f.puts to_xml }
      puts path
    end
    
    def to_xml
      Rosetta.reset_built_modules
      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') { |xml|
        xml.RosettaProfile(:name => name, :owner => authors.map { |a| "#{a.first_name} #{a.last_name}"}.join('; ')) {
          root.to_xml(xml)
        }
      }
      Rosetta.reset_built_modules
      builder.to_xml
    end
    
    # is_unique :name
    def refids_string
      root ? root.refids_string : ''
    end
    
    def to_title
      rs = refids_string
      "Device Profile on #{root ? root.base_uml.name : '<unset>'}: #{rs.empty? ? '<no term(s) set>' : rs}"
    end
  end
  
  class ModuleProfile
    def to_inclusion_xml(xml)
      xml.module(:name => short_name, :version => version) {
        roots.each { |root| root.to_xml(xml) }
      }
    end
    
    def short_name
      name.sub(/_module_for_.*/, '')
    end
    
    is_unique :name
    def to_title
      "Module Profile: #{name}"
    end
  end
end

