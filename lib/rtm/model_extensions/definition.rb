module RTM
  class Definition
    def summary_lines
      lines = []
      lines << "Description: #{descriptions.map(&:value_content).join('|')}" if descriptions and !descriptions.empty?
      lines << "Acronym: #{acronym}" if acronym and !acronym.empty?
      lines << "Common Term: #{common_term}" if common_term and !common_term.empty?
      lines << "Systematic Name: #{systematic_name}" if systematic_name and !systematic_name.empty?
      lines
    end
    
    def update(desc, sysname, cmnterm, acrnym, ox, no_change_track, warnings = false)
      if warnings
        warning_id
        if systematic_name && sysname && systematic_name != sysname
          RTM.warn("System name for #{term.warning_id} has changed from: '#{systematic_name}' to: '#{sysname}'", :none)
        end
        if acronym && acrnym && acronym != acrnym
          RTM.warn("Acronym for #{term.warning_id} has changed from: '#{acronym}' to: '#{acrnym}'", :none)
        end
        if common_term && cmnterm && common_term != cmnterm
          RTM.warn("Common Term for #{term.warning_id} has changed from: '#{common_term}' to: '#{cmnterm}'", :none)
        end
      end
      ChangeTracker.start unless no_change_track
      systematic_name = sysname if sysname
      acronym = acrnym if acrnym
      common_term = cmnterm if cmnterm
    
      update_aux(ox)
      update_descriptions(desc) if desc
    
      self.save
      ChangeTracker.commit unless no_change_track
    end
    
    def update_aux(ox)
      if aux && ox
        # We go round-trip through parse/to_json before merging to avoid conflicts b/w string keys and symbol keys in the incoming ox
        new_aux = ox.is_a?(Hash) ? JSON.parse(ox.to_json) : JSON.parse(ox) # if it isn't a hash then it needs to be a JSON string already
        aux = (JSON.parse(aux).merge(new_aux)).to_json
      elsif ox
        aux = ox.to_json
      end
    end
    
    def description_exists?(str, warn = false)
      extant = descriptions.find { |desc| desc.matches?(str) }
      RTM.info("Existing term #{term.stat} has a different description than 10101R term.", :none) if warn && !extant
      extant
    end
    
    def update_descriptions(str)
      return if description_exists?(str)
      desc = RTM.find_or_create_description(str, true)
      add_description(desc)
    end
    
    def description_matches?(existing_def, description, refid)
      existing_descs = existing_def.descriptions.map(&:value)
      return false if existing_descs.empty?
      if existing_descs.include?(description)
        true
      else

        false
      end
    end
  end
end
