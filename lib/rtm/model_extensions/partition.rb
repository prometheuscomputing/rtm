module RTM
  class Partition
    def terms
      term_codes.map { |tc| tc.terms }.flatten.uniq
    end
  end
end
