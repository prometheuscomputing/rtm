module RTM
  class RefID
    is_unique :value
  
    # FIXME what if there are multiple terms?
    derived_attribute :term_summary, String
    def term_summary
      tt = terms
      tt.any? ? tt.first.summary : "No term data"
    end
  
    def cfcodes
      terms.map(&:term_code).map(&:cfcode)
    end
    alias_method :cf_codes, :cfcodes
  
  end
end
