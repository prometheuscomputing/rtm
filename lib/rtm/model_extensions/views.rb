module RTM
  def self.find_view(str)
    views = nil
    if str =~ /^\d+$/
      views = get_view_by_attrs(str, :cf_term_codes, :secondary_cf_term_codes)
      # views = RTM::TermView.where(Sequel.ilike(:cf_term_codes, "%#{str}%")).all
      # views = RTM::UnitView.where(Sequel.ilike(:cf_term_codes, "%#{str}%")).all if views.empty?
      # views = RTM::TermView.where(Sequel.ilike(:secondary_cf_term_codes, "%#{str}%")).all if views.empty?
      # views = RTM::UnitView.where(Sequel.ilike(:secondary_cf_term_codes, "%#{str}%")).all if views.empty?
    elsif str =~ /^\d+::\d+$/
      puts "Searching by part code"
      views = get_view_by_attrs(str, :term_codes, :secondary_term_codes)
      # views = RTM::TermView.where(Sequel.ilike(:term_codes, "%#{str}%")).all
      # views = RTM::UnitView.where(Sequel.ilike(:term_codes, "%#{str}%")).all if views.empty?
      # views = RTM::TermView.where(Sequel.ilike(:secondary_term_codes, "%#{str}%")).all if views.empty?
      # views = RTM::UnitView.where(Sequel.ilike(:secondary_term_codes, "%#{str}%")).all if views.empty?
    else
      views = get_view_by_attrs(str, :refids, :secondary_refids, :deprecated_refids, :withdrawn_refids)
      # views = RTM::TermView.where(Sequel.ilike(:refids, "%#{str}%")).all
      # views = RTM::UnitView.where(Sequel.ilike(:refids, "%#{str}%")).all if views.empty?
      # views = RTM::TermView.where(Sequel.ilike(:secondary_refids, "%#{str}%")).all if views.empty?
      # views = RTM::UnitView.where(Sequel.ilike(:secondary_refids, "%#{str}%")).all if views.empty?
      # views = RTM::TermView.where(Sequel.ilike(:deprecated_refids, "%#{str}%")).all if views.empty?
      # views = RTM::UnitView.where(Sequel.ilike(:deprecated_refids, "%#{str}%")).all if views.empty?
      # views = RTM::TermView.where(Sequel.ilike(:withdrawn_refids, "%#{str}%")).all if views.empty?
      # views = RTM::UnitView.where(Sequel.ilike(:withdrawn_refids, "%#{str}%")).all if views.empty?
    end
    if views
      puts Rainbow("Multiple views found for #{str}.").red unless views.count == 1
      # TODO what can we do here when there is more than one view?
      return views.first 
    end
    return nil
  end
  
  def self.get_view_by_attrs(str, *attrs)
    attrs.each do |attr|
      views = get_view_by_attr(str, attr)
      return views if views
    end
    nil
  end
  
  def self.get_view_by_attr(str, attr)
    views = RTM::TermView.where(Sequel.ilike(attr, "%#{str}%")).all
    # puts "views for TermView.#{attr} = #{str} -- #{views}"
    if views.empty?
      views = RTM::UnitView.where(Sequel.ilike(attr, "%#{str}%")).all
      # puts "views for UnitView.#{attr} = #{str} -- #{views}"
    end
    return views if views.any?
    nil
  end
  
  class CollatedView
    def to_title
      "#{refids} #{term_codes}"
    end
    
    derived_attribute(:summary_html, String)
    def summary_html
      html = []
      html << "Status: #{status || 'None'}"
      refids_string = "RefID: <b>#{refids}</b>"
      if secondary_refids.to_s[0]
        refids_string << " #{secondary_refids}"
      end
      html << refids_string
      
      html << "Deprecated RefID(s): #{deprecated_refids}" if deprecated_refids.to_s[0]
      html << "Withdrawn RefID(s): <span style='color:red;'>#{withdrawn_refids}</span>" if withdrawn_refids.to_s[0]
      
      tc_string = "Term Code: <b>#{term_codes}</b>"
      if secondary_term_codes.to_s[0]
        tc_string << " #{secondary_term_codes}"
      end
      html << tc_string
      
      html << "Discriminator: #{discriminators}" if discriminators.to_s[0]
      
      cf_string = "Context Free Term Code: <b>#{cf_term_codes}</b>"
      if secondary_cf_term_codes.to_s[0]
        cf_string << " #{secondary_cf_term_codes}"
      end
      html << cf_string
      
      html << "Description: #{description}"
      html << "Systematic Name: #{systematic_name}"
      html << "Common Term: #{common_term}"
      html << "Acronym: #{acronym}"
      html.join('<br>')
    end
  end
  
  class TermView
    derived_attribute(:summary_html, String)
    def summary_html
      html = super
      # puts html.pretty_inspect
      
      # columns: DIM, UOM_MDC, UOM_UCUM, UCODE10, CF_UCODE10
      ut = JSON.parse(units_table)
      pp units_table
      if ut&.first[0]
        html << '<br><br>'
        num_rows = ut.map { |col| col.split("\n").count }.max
        rows = Array.new(num_rows) { [] }
        ut.each_with_index do |col, i|
          # puts i
          tds = col.split("\n")
          # pp tds
          # tds.each { rows << [] } if i == 0 # initialize rows
          tds.each_with_index do |td, row_num|
            # puts td.inspect
            case i
            when 1, 3, 4
              view = RTM.find_view(td)
              if view
                url = Gui.create_url_from_object(view)
                rows[row_num] << '<a href="' + url + '">' + td + '</a>'
              else
                rows[row_num] << td
              end
            else
              rows[row_num] << td
            end
          end
        end
        engine = Haml::Engine.new(File.read(relative('units_table.haml')))
        html_table = engine.render(Object.new, {:rows => rows})
        html << html_table
      end
      et = JSON.parse(enums_table)
      if et&.first.to_s[0]
        html << '<br><br>'
        rows = []
        et.each_with_index do |col, i|
          tds = col.split("\n")
          tds.each { rows << [] } if i == 0 # initialize rows
          tds.each_with_index { |td, row_num| rows[row_num] << td }
        end
        engine = Haml::Engine.new(File.read(relative('enums_table.haml')))
        html_table = engine.render(Object.new, {:rows => rows})
        # puts html_table
        html << html_table
      end
      html.gsub!("\n", '')
      html << '<br><br>'
      html
    end
  end
  
  class UnitView
    derived_attribute(:summary_html, String)
    def summary_html
      html = [super]
      html << "Dimension: #{dim}"
      html << "Symbol: #{symbol}"
      html << "UCUM Units: #{ucum_units}"
      html.join('<br>')
    end
  end
end
