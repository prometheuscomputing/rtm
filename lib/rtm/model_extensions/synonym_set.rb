module RTM
  class SynonymSet
    def summary
      terms.sort_by { |term| term.sort_val }.map { |term| "#{term.refid_stat} #{term.code_stat}"}.join(' | ')
    end
    
    def id_text
      codes  = terms.map(&:term_code).map(&:part_code).uniq
      refids = terms.map(&:refid).map(&:value).uniq
      if codes.count == 1 && refids.count > 1
        return codes
      end
      if codes.count > 1 && refids.count == 1
        return refids
      end
      if codes.count > 1 && refids.count > 1
        return codes.sort + refids.sort
      end
      puts Rainbow("Warning! SynonymSet[#{self.id}] doesn't seem well formed!").red
      return nil
    end
  end
end
