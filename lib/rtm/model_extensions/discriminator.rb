module RTM
  class Discriminator
    # Note that infix depth is only available if we know what definition the set is applied to
    def representation
      "#{discriminator_set.name} (#{offset})"
    end
    
    # TODO this currently assumes that the discriminator value is a suffix, i.e. that the infix value is 0.  Make this cover all cases (will need knowledge of the Definition obj in order to do this)
    def expansion_values(refid, code, infix = 0)
      # TODO also cover the _X_ case
      output_refid = refid.is_a?(RTM::RefID) ? refid.value + value.to_s : refid + value.to_s
      output_refid.gsub!(/_+/, '_')
      if code =~ /::?/ # if sent a partition::term_code, then return in same format
        ptn, tcode = code.split(/::?/)
        output_code = ptn + '::' + (tcode.to_i + offset).to_s
        [output_refid, output_code]
      else # return just the term_code value (no partition)
        [output_refid, code.to_i + offset]
      end
    end
  end
end
