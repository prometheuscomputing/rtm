module RTM
  class Unit
    def uom
      units_of_measure&.value
    end
    
    derived_attribute(:summary_html, String)
    def summary_html
      lines = []
      lines << "Status: #{status&.value || '-'}"
      lines << "RefID: <b>#{refid&.value}</b>"
      lines << "Term Code: <b>#{part_code}</b>"
      lines += definition&.summary_lines || ['No Definition']
      lines << "UoM: #{uom}" if uom
      lines << "dim: #{dim}" if dim
      lines << "dim_c: #{dim_c}" if dim_c
      lines << "symbol: #{symbol}" if symbol
      lines << "dimensions: " + dimensions.map(&:value).flatten.join(' ') if dimensions.any?
      lines << "ucum_units: " + ucum_units.map(&:value).flatten.join(' ') if ucum_units.any?
      if rosetta_entries.any?
        lines << "<br>"
        lines << "Rosetta Entries:"
        lines += rosetta_entries.map(&:summary_lines).flatten
      end
      lines.join('<br>')
    end
    
    def summary(html = true)
      lines = []
      first_line = "#{self.class.name.demodulize}"
      first_line << "[#{self.id}]" unless html
      first_line << " #{status&.value}" if status
      lines << first_line
      lines << "RefID: #{refid.value}"
      lines << "Term Code: #{part_code}"
      lines += definition&.summary_lines || ['No Definition']
      lines << "UoM: #{uom}" if uom
      lines << "dim: #{dim}" if dim
      lines << "dim_c: #{dim_c}" if dim_c
      lines << "symbol: #{symbol}" if symbol
      lines << "dimensions: " + dimensions.map(&:value).flatten.join(' ') if dimensions.any?
      lines << "ucum_units: " + ucum_units.map(&:value).flatten.join(' ') if ucum_units.any?
      if rosetta_entries.any?
        lines << "Rosetta:"
        lines += rosetta_entries.map(&:summary_lines).flatten
      end
      if html
        lines.join('<br>')
      else
        lines.join("\n")
      end
    end
    
    def set_unit_properties(opts)      
      ChangeTracker.start
      if self.dim && opts[:dimensionality] && self.dim != opts[:dimensionality].strip
        RTM.warn("dimensionality for #{opts[:refid]} has changed from: '#{self.dim}' to: '#{opts[:dimensionality].strip}'", :none)
      end
      self.dim = opts[:dimensionality].strip if opts[:dimensionality]
      
      if self.dim_c && opts[:dimensionality_c] && self.dim_c != opts[:dimensionality_c].strip
        RTM.warn("dimensionality_c for #{opts[:refid]} has changed from: '#{self.dim_c}' to: '#{opts[:dimensionality_c].strip}'", :none)
      end
      self.dim_c = opts[:dimensionality_c].strip if opts[:dimensionality_c]
  
      if self.symbol && opts[:symbol] && self.symbol != opts[:symbol].strip
        RTM.warn("symbol for #{opts[:refid]} has changed from: '#{self.symbol}' to: '#{opts[:symbol].strip}'", :none)
      end
      self.symbol = opts[:symbol].strip if opts[:symbol]
      
      # do UCUM units and Dimension and UnitOfMeasure
      dimension = opts[:dimension]&.strip
      unless dimension.to_s.empty?
        dim_obj = RTM.find_or_create_dimension(dimension)
        self.add_dimension(dim_obj) unless self.dimensions.find { |dmn| dmn == dim_obj }
      end
      
      uom = opts[:uom]&.strip
      unless uom.to_s.empty?
        uom_obj = RTM.find_or_create_uom(uom)
        self.units_of_measure = uom_obj
      end
      
      ucum = opts[:ucum]&.strip
      unless ucum.to_s.empty?
        uus = ucum.split(/\s+/)
        uus.each do |uu|
          uu  = RTM.find_or_create_ucum(ucum)
          self.add_ucum_unit(uu) unless self.ucum_units.find { |ucu| ucu == uu }
        end
      end
      ChangeTracker.commit
    end
    
    def units_matrix_data
      uu = ucum_units.map(&:value).join(' ')
      output = [dim, preferred_refids.join(' '), uu, part_code, cf_term_code]
    end
  end
end
