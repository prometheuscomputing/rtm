module RTM
  class EnumerationGroup
    
    derived_attribute(:refid_string, String)
    def refid_string
      refid&.value
    end
    
    
    derived_attribute(:summary_html, String)
    def summary_html
    end
    
    def summary(html = true)
    end
    
  end
end
