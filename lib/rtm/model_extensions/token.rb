module RTM
  class Token
    def warning_id
      "#{self.class.name.demodulize} #{value || refid.value}"
    end    
  end
end
