module RTM
  class RosettaEntry
    def summary_lines
      lines = []
      lines << "  harmonized: #{harmonized}" unless harmonized.nil?
      lines << "  status: #{status.value}" if status
      lines << "  comment: #{comment}" if comment and !comment.empty?
      lines << "  description: #{description.value_content}" if description
    end
  end
end
