module RTM
  class TermCode
    def cfcode
      ptn = partition
      return nil unless ptn
      (ptn.number * (2**16)) + code
    end
    alias_method :cf_code, :cfcode
    
    def part_code
      "#{partition.number}::#{code}"
    end
    alias_method :value, :part_code
    
    # FIXME what if there are multiple terms?
    derived_attribute :term_summary, String
    def term_summary
      terms.first&.summary
    end
  
  end
end
