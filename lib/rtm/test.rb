load File.join(__dir__, 'migration/migration_logger.rb')

require 'rainbow'

module RTM
  module_function
  def testlog
    place = File.join(__dir__, '../../scripts/testlog')
    file = File.new(place, File::WRONLY | File::APPEND)
    file.sync = true
    mylog = Logger.new(file)
    mylog.error Time.now.utc
    # RTM.reset_log('Migration_log_RTMMSv1')
  end  
end
