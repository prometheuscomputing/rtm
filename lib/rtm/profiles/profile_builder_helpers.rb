module DeviceProfiles
  class Builder
  
  end

  class RCP_Builder < Builder
    METRIC_UML = UmlMetamodel::Class.where(:name => 'Metric').first unless defined?(METRIC_UML)
    FACET_UML  = UmlMetamodel::Class.where(:name => 'Facet').first unless defined?(FACET_UML)
    METRIC_TYPE_PROPERTY = METRIC_UML.properties.find { |prop| prop.name == 'metric_type' } unless defined?(METRIC_TYPE_PROPERTY)
    FACET_TYPE_PROPERTY  = FACET_UML.properties.find { |prop| prop.name == 'metric_type' } unless defined?(FACET_TYPE_PROPERTY)

    def _set_metric_type(cr,type)
      attr_restriction = Boa::AttributeRestriction.new
      if cr.base_uml == METRIC_UML
        attr_restriction.base = METRIC_TYPE_PROPERTY
      elsif cr.base_uml == FACET_UML
        attr_restriction.base = FACET_TYPE_PROPERTY
      else
        raise "Should not have anything other than Metric or Facet"
      end
      constraint = Boa::ValueConstraint.create(:expression => type)
      constraint.expression_kind = 'String'
      constraint.save
      attr_restriction.add_constraint(constraint)
      cr.add_property(attr_restriction)
    end
    
    ALIASES = {
      Rosetta::MDS => [],
      Rosetta::VMD => [],
      Rosetta::Channel => ['chan'],
      Rosetta::Metric => ['num', 'numeric', 'enum', 'enumeration'],
      Rosetta::Facet => [],
      Rosetta::ModuleProfile => ['module', 'mod'],
      Boa::IncludeableProfile => ['include']
    } unless defined?(ALIASES)
    VALID_CLASSES = [Rosetta::MDS, Rosetta::VMD, Rosetta::Channel, Rosetta::Metric, Rosetta::Facet, Rosetta::ModuleProfile, Boa::IncludeableProfile] unless defined?(VALID_CLASSES)
    LOOKUP_TAG_BY_CLASS = {} unless defined?(LOOKUP_TAG_BY_CLASS)
    VALID_CLASSES.each { |c| LOOKUP_TAG_BY_CLASS[c] = ([c.name.demodulize.downcase] + Array(ALIASES[c]).uniq) }
    LOOKUP_CLASS_BY_TAG = {} unless defined?(LOOKUP_CLASS_BY_TAG)
    LOOKUP_TAG_BY_CLASS.each do |klass, tags|
      tags.each { |tag| LOOKUP_CLASS_BY_TAG[tag] = klass }
    end
    PROFILE_CLASS = Rosetta::DeviceProfile unless defined?(PROFILE_CLASS)
    IGNORED_KEYS = ['xmlns', 'xmlns:xsi', 'xsi:type', 'profile_root'] unless defined?(IGNORED_KEYS)
        
    def _ignored_keys
      super + IGNORED_KEYS
    end
    
    def role_for_tag(tag)
      case tag
      when 'chan', 'channel'
        'channels'
      when 'vmd'
        'vmds'
      when 'metric', 'enumeration', 'numeric'
        'metrics'
      when 'mds'
        'mds'
      when 'facet'
        'facets'
      else
        raise "I don't know what a #{tag} is."
      end
    end
  end
end


#  TODO move this elsewhere
module Boa
  class ClassifierRestriction
    def property_set
      [base_properties, property_restrictions]
    end
    
    def base_properties
      # NOTE should this really be base_uml.all_properties ???
      base_uml.properties
    end
    
    def restricted_properties
      if base.is_a?(UmlMetamodel::Classifier)
        properties
      else
        base.restricted_properties + properties
      end
    end
  end
end

