require_relative 'profile_builder'
module DeviceProfiles
  def self.process_rosetta_profile_upload(file)
    builder = DeviceProfiles::RCP_Builder.new(file, :submitted_name => name, :xml_format => :rcp)
    profile = builder.build
    puts profile.inspect
  end

  class RCP_Builder < Builder

    
    def build_profile_from_rcp(container_node, contained_element_collections)
      # puts "\nContainer Node: #{container_node.inspect}"
      # FIXME this needs generalized error handling so that the page doesn't just go sour.  Example, if there is garbage in the xml then contained_elements can be a String which, of course, does not respond to #each.  Maybe there should be a specific error if contained_elements is a String as well as general/broad scope error handling.
      puts
      puts Rainbow(container_node.to_title).orange      
      new_objects = []
      contained_element_collections.each do |tag,contained_element_collection|
        element_klass = get_element_klass(tag)
        if errors.any?
          return errors
          # FIXME exactly how to handle bad XML is a bit up in the air...once it is hammered out this whole file probably needs to be cleaned up
          warnings << "Could not parse unknown tag &lt#{tag}&gt"
          next
        end
        [contained_element_collection].flatten.each do |contained_element|
          refids = contained_element.delete('refid')
          
          if element_klass == Boa::IncludeableProfile
            # puts Rainbow(contained_element.pretty_inspect).magenta
            mod = find_included_module(contained_element)
            return errors if errors.any?
            ChangeTracker.start
            container_node.add_included_profile(mod)
            ChangeTracker.commit
            next
          end
          
          if element_klass == Rosetta::ModuleProfile
            # puts Rainbow(contained_element.pretty_inspect).cyan
            mod = process_module_profile(contained_element)
            return errors if errors.any?
            next
          end

          unless refids
            puts Rainbow('NO RefID').red
            puts Rainbow(contained_element.pretty_inspect).red
          end
          puts Rainbow("#{tag}: Adding a #{element_klass.name.demodulize} -- #{refids}").green          
          # puts Rainbow(contained_element.pretty_inspect).yellow
          
          # TODO need to check and see if this CR already exists.  Use existing CR if it does exist.
          existing_cr = find_existing_cr(element_klass, contained_element) # TODO
          if existing_cr
            cr = existing_cr
            # TODO check everything to see if the one we found is the same as the one that is given here. Lots of work...
          else
            ChangeTracker.start
            cr = Boa::ClassifierRestriction.new
            # puts "Element klass is #{element_klass.inspect}"
            base = UmlMetamodel::Class.where(:name => element_klass.name.demodulize).first
            # puts base.inspect
            raise "Could not find #{element_klass} to use as base" unless base
            cr.base = base
            add_refids(cr,refids)
            new_objects << cr
            ChangeTracker.commit
          end
        
          if container_node.is_a?(Rosetta::DeviceProfile)
            ChangeTracker.start
            container_node.root = cr
            ChangeTracker.commit
          elsif container_node.is_a?(Rosetta::ModuleProfile)
            ChangeTracker.start
            container_node.add_root(cr)
            ChangeTracker.commit
          else
            ChangeTracker.start
            ar = Boa::AssociationRestriction.new
            ar.add_type_restriction(cr)
            # find the appropriate_getter.  we really hope there is only one!!
            base_properties = container_node.base_properties
            # restricted_properties = cr.restricted_properties
            tag_role = role_for_tag(tag)
            correct_property = base_properties.find do |property|
              property.name == tag_role
            end
            
            if correct_property
              container_node.add_property(ar)
              puts Rainbow("Adding to #{tag_role} to #{container_node.to_title}, which now has #{container_node.properties_count} properties.").magenta
            else
              raise "#{container_node.base_uml.name} doesn't have a property for #{tag_role} among #{base_properties.map(&:name).sort}"
            end
            
            cardinality = contained_element.delete("card") || contained_element.delete("cardinality")
            if cardinality
              lower, upper = parse_cardinality(cardinality)
              ar.lower = lower
              ar.upper = upper
              # ar.term_group_type = 'one of' # FIXME what to do here??
            else
              # TODO make sure this isn't going to f things up if this was a pre-existing CR
              ar.lower = 1
              ar.upper = 1
              # ar.term_group_type = 'one of' # FIXME term_group_type is not a property of PropertyRestriction TODO this is an assumption.  We need a way of actually indicating this
            end
            
            ChangeTracker.commit
          end

          ChangeTracker.start
          u = contained_element.delete('u')
          assign_units(cr,u) if u
          e = contained_element.delete('e')
          assign_enums(cr,e) if e
          set_metric_type(cr,tag,e,u) if element_klass == Rosetta::Metric
          ChangeTracker.commit
          build_profile_from_rcp(cr, contained_element) if contained_element.any?
          return errors if errors.any? # recursively exits. i.e. climbs back up the stack with the error.
        end
      end
    end
    
    def add_refids(cr,refids)
      refids.split(/[\s,;\|]/).each { |refid| add_refid(cr, refid) }
    end
    
    def add_refid(cr,refid)
      # TODO add support for the inclusion of multiple refids
      if refid
        term_views = RTM.find_term_views(refid)
        if term_views&.any?
          # Question TODO -- add the RefID or add the Term? Discuss with Paul
          if term_views.count == 1
            cr.add_term(term_views.first)
            return true
          elsif term_views.count > 1
            # Ambiguous term, how handle this TODO
            # TODO pick term_view with preferred term
            puts Rainbow("Multiple TermViews for #{refid}").red
            pp term_views.map { |tv| "#{tv.refids} -- #{tv.term_codes}"}
            return false
          end
        else
          if refid =~ /MDCX/
            success = add_refid(cr, refid.sub('MDCX_', 'MDC_'))
            if success
              message = "Replaced #{refid} with #{refid.sub('MDCX_', 'MDC_')}"
              puts Rainbow(message).orange
              warnings << message
              return true
            end
          end         
          # TODO make a new proposed refid, term, and term_view
          message = "No TermView for #{refid}"
          puts Rainbow(message).red
          warnings << message
          # complain that there is no RefiD?? TODO
        end
      end
    end
    
    def set_metric_type(cr,tag,enums,units)
      case tag
      when 'numeric', 'num'
        _set_metric_type(cr,'Numeric')
      when 'enumeration', 'enum'
        _set_metric_type(cr,'Enumeration')
      when 'metric'
        if units
          _set_metric_type(cr,'Numeric')
        elsif enums
          _set_metric_type(cr,'Enumeration')
        end
      else
        # Not really sure what to do with the other tag types yet TODO
      end
    end
    
    def assign_units(cr,str)
      refids = str.split(/[\s,;\|]/)
      refids.each do |refid|
        assignable_units = [RTM.find_unit_group(refid) || RTM.find_unit(refid)].flatten
        if assignable_units.count == 1
          cr.add_unit(assignable_units.first)
        elsif assignable_units.count > 1
          assignable_units.each { |au| cr.add_unit(au) }
          message = "Multiple unit terms matched #{refid}!"
          warnings << message
          puts Rainbow(message).yellow
        else
          # TODO create new ones instead of warning
          if refid[0] == '_'
            warnings << "UnitGroup #{refid} is not in the RTMMS system."
          else
            warnings << "Unit #{refid} is not in the RTMMS system."
          end
        end
      end
    end
    
    def assign_enums(cr,str)
      refids = str.split(/[\s,;\|]/)
      refids.each do |refid|
        assignable_enums = [RTM.find_enum_group(refid) || RTM.find_enum(refid)].flatten
        if assignable_enums.count == 1
          cr.add_enum(assignable_enums.first)
        elsif assignable_enums.count > 1
          assignable_enums.each { |ae| cr.add_enum(ae) }
          message = "Multiple enum terms matched #{refid}!"
          warnings << message
          puts Rainbow(message).yellow
        else
          # TODO create new ones instead of warning
          if refid[0] == '_'
            warnings << "EnumerationGroup #{refid} is not in the RTMMS system."
          else
            warnings << "Enumeration value #{refid} is not in the RTMMS system."
          end
        end
      end
    end

    def find_included_module(inclusion)
      mod = _find_module(inclusion['module'], inclusion['version'])
      errors << "Could not find a module definition for included module #{inclusion['name']}" unless mod
      mod
    end
    
    def _mod_name(name)
      name.split('_module_for_').first + '_module_for_' + profile.name
    end
    
    def _find_module(name, version)
      args = {:name => _mod_name(name)}
      version ? args[:version] = version.to_i : args[:head] = true
      mod = Rosetta::ModuleProfile.where(args).first
    end
    
    def process_module_profile(hash)
      mod_name = hash.delete('name')
      unless mod_name
        errors << "Every module definition must have a name!"
        return
      end
      mod = _find_module(mod_name, hash.delete('version'))
      # TODO if this is an existing module then we need to check and see if anything has changed about it and handle any changes (i.e. new module w/ incremented version)
      unless mod
        ChangeTracker.start
        mod = Rosetta::ModuleProfile.create(:name => _mod_name(mod_name))
        mod.head = true
        mod.date = Time.now
        mod.version = 1
        # TODO make this find just the module declaration within @file
        # mod.source_xml = Gui_Builder_Profile::File.instantiate(:filename => "#{_mod_name(mod_name)}.xml", :data => ::File.binread(file), :mime_type => 'text/xml')
        ChangeTracker.commit
        # puts "Sending #{hash}"
        build_profile_from_rcp(mod, hash)
      end
    end
    
    def find_or_create_classifier_restriction(element_klass, value)
    end
    
    def find_existing_cr(element_klass, value)
    end
      
  end # RCP_Builder
end # DeviceProfiles
