module DeviceProfiles  
  class Builder
    attr_reader :file, :submitted_name, :xml_format, :json, :profile_root_keys, :profile, :warnings, :errors
    def initialize(file, opts)
      @file           = file
      @submitted_name = opts[:submitted_name]
      # @submitted_type = opts[:submitted_type]
      # @device_type    = opts[:device_type]
      @xml_format     = opts[:xml_format]
      # begin
        # puts "Hash#from_xml is from: " + Hash.method(:from_xml).source_location.to_s # Hash#from_xml is from active_support
        @json = Hash.from_xml(file.read)
        # puts Rainbow('json:' + "\n" + @json.pretty_inspect).green
      # rescue REXML::ParseException => e
      #   # The line number and position that comes back in the message does not seem to correspond well to the original XML so we remove it
      #   useful_message = e.message.gsub(/Line: \d+\nPosition: \d+\nLast 80 unconsumed characters:/, 'near: ')
      #   return {:error => useful_message}
      # end
      @warnings = []
      @errors   = []
    end

    # def json
    #   @json
    # end
    
    # FIXME this belongs in a more generalized place
    def find_first(klass, params = {})
      if klass.interface?
        klasses = klass.implementors
      else
        klasses = klass.children.unshift(klass)
      end
      klasses.reject{|k| k.abstract?}.each do |k|
        found = k.where(params).first
        return found if found
      end
      nil
    end
    
    def get_type_from_node(node, prefix = 'DIM')
      if xsi_type = node["xsi:type"]
        str = xsi_type.split(/:+/).unshift(prefix).join('::')
        # puts str
        str.to_const
      end
    end
    
    def get_device_with_metadata
      case xml_format
      # when :full
      #   wrapper = json["PCDProfile"] || json["PHDProfile"]
      when :rch
        device = json["RCH"]# || @json["IHE"]
        errors << "XML problem: The device must be contained within the <RCH/> element." unless device
        device# pp json;puts "%"*30
      when :rcp
        device = json["Rosetta"] || json["RosettaProfile"] || json["RCP"]
        unless device
          errors << "XML problem: The device must be contained within the <Rosetta> element."
          return nil
        end
        device# pp json;puts "%"*30
      else
        raise 'xml format specified is unknown or unspecified'
      end
    end
    
    def get_device
      {@profile_root_key => get_device_with_metadata[@profile_root_key]}
    end
    
    def get_element_klass(tag, contents = nil)
      case xml_format
      when :rch, :rcp
        klass = lookup_class_by_tag(tag)
        error_message = "Can't find a class for tag: #{tag}"
      # when :full
      #   klass = get_type_from_node(contents, 'DIM')
      #   error_message = "Can't find a class associated with tag: #{tag}. It should have a valid value for the 'xsi:type' attribute."
      else
        raise 'unknown xml format'
      end
      unless klass
        errors << error_message
        return
        # FIXME exactly how to handle bad XML is a bit up in the air...once it is hammered out this whole file probably needs to be cleaned up
        warnings << "Could not parse unknown tag &lt#{tag}&gt"
      end
      klass
    end
    
    def _ignored_keys
      []
    end
    
    def set_profile_root_keys
      case xml_format
      when :rch, :rcp
        profile_root_keys = get_device_with_metadata.keys.select { |tag| lookup_class_by_tag(tag) }
      # when :full
      #   # pp device;puts "%"*30
      #   profile_root = get_device_with_metadata.select do |tag, contents|
      #     get_type_from_node(contents, 'DIM')
      #   end
      #   profile_root_keys = profile_root.keys
      else
        raise 'This was unexpected'
      end
      # puts "profile_root_keys are #{profile_root_keys.inspect}"
      return {:error => "Too many tags left to have just one profile_root -- (#{profile_root_keys})"} if profile_root_keys.count > 1
      return {:error => "No valid tag for profile root"} if profile_root_keys.count < 1
      @profile_root_key = profile_root_keys.first
    end
    
    def _start_profile(device)
      ChangeTracker.start
      profile      = self.class::PROFILE_CLASS.new
      profile.name = json.first.last['name']#submitted_name
      profile.date = Time.now
      profile.head = true # TODO is this really necessary?
      
      # profile.intended_use = submitted_type
      # case xml_format.to_s
      # when :full
      #   data = json[profile.class.name.demodulize] || {}
      # when "rch"
      #   data = json["RCH"] || {}
      # when "rcp"
      #   data = json["Rosetta"] || json["RCP"] || {}
      #   puts "JSON is:"
      #   pp data
      # end
      profile.save
      ChangeTracker.commit
      @profile = profile
      # name        = device.delete('name')
      # keys_to_delete = []
      
      # FIXME What is this?
      # data.each do |k,v|
     #    next if k == @profile_root_key
     #    next if _ignored_keys.include?(k)
     #    # puts k;puts v;puts
     #    # keys_to_delete << k
     #  end
      profile
      # keys_to_delete.each{|k| device.delete(k)}
    end
    
    def build
      set_profile_root_keys
      device = get_device

      profile  = _start_profile(device)
      puts Rainbow('Empty Profile Created').red
      begin
        result = build_profile_from_rcp(profile, device)
        puts Rainbow('Build Profile FINISHED').red
        # result = case xml_format.to_s
        #          when "rch"
        #            build_profile_from_rch(profile, device)
        #          when "rch"
        #            puts "YAY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        #            build_profile_from_rcp(profile, device)
        #          when "full"
        #            build_profile_from_full_xml(profile, device['profile_root'], 'profile_root')
        #          end
      rescue Exception => e
        result = {:error => "Unhandled error in XML parsing.\nPlease contact developer with this information:\n#{e.message}\n#{e.backtrace[0..4].pretty_inspect}"}
        puts e.message
        puts e.backtrace
      end
      if errors.any?
        ChangeTracker.start
        profile.destroy
        ChangeTracker.commit
        puts errors
        return {:error => errors}
      end
      ChangeTracker.start unless ChangeTracker.started?
      profile.source_xml = Gui_Builder_Profile::File.instantiate(:filename => "#{submitted_name}.xml", :data => ::File.binread(file), :mime_type => 'text/xml')
      profile.save
      ChangeTracker.commit if ChangeTracker.started?
      ChangeTracker.start
      previous_profiles = profile.class.where(:name => profile.name).all.reject { |pr| pr == profile }
      previous_heads = previous_profiles.select { |pr| pr.head == true }
      warnings.unshift("Unable to determine which profile was the last version of #{profile.name}") if previous_heads.count > 1
      previous_head = previous_heads.first
      if previous_head
        profile.previous = previous_head
        previous_version = previous_head.version || '1'
        profile.version = next_version(previous_version)
      else
        profile.version = '1'
      end
      previous_profiles.each { |pr| pr.head = false; pr.save }
      ChangeTracker.commit
      puts warnings
      {:profile => profile, :warnings => warnings.uniq}
    end
    
    def parse_cardinality(val)
      val = val.strip
      return ['0', '*'] if val == '*' || val == '0..*'
      return ['1', '1'] if val == '1'
      lower, upper = val.split(/\.+/)
      return [lower, upper]
    end
     
    def deconstruct_cf_code10(cf_code10)
      binary_string = "%b" % cf_code10.to_i
      # this returns nil if the cfcode10 results in a binary string of less than 16 chars
      least_significant_16 = binary_string[-16..-1] || ""
      # Oddly enough, this will return empty string even if there are only 16 or fewer chars in the string
      most_significant_16  = binary_string[0..-17]
      {:partition => most_significant_16.to_i(2), :oid => least_significant_16.to_i(2)}
    end

    def get_partition(cf_code10)
      deconstruct_cf_code10(cf_code10)[:partition]
      # binary_string = "%b" % cf_code10.to_i
      # most_significant_16  = binary_string[0..-17]
      # most_significant_16.to_i(2)
    end

    def get_code10(cf_code10)
      deconstruct_cf_code10(cf_code10)[:oid]
      # binary_string = "%b" % cf_code10.to_i
      # least_significant_16 = binary_string[-16..-1]
      # least_significant_16.to_i(2)
    end
    
    def lookup_class_by_tag(tag)
      self.class::LOOKUP_CLASS_BY_TAG[tag]
    end

    def next_version(last)
      # puts Rainbow("last is #{last.inspect}").orange
      # puts Rainbow("last[-1] is #{last[-1].inspect}").orange
      # puts Rainbow("last is a #{last.class}").orange
      if last.to_s[-1] =~ /\d/
        last.to_s.succ
      else
        last.to_s + '1'
      end
    end
  end


end # DeviceProfiles
