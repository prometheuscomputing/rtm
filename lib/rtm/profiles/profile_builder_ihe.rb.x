module DeviceProfiles
  def self.process_ihe_profile_upload(file)
    profile = DeviceProfiles::IHE_Builder.new(file, :submitted_name => name, :device_type => :ihe, :xml_format => :rcp).build
    # TODO finish this if ever necessary
  end

  class IHE_Builder < Builder
    ALIASES = {
      Rosetta::MDS => [],
      Rosetta::VMD => [],
      Rosetta::Channel => ['chan'],
      Rosetta::Metric => ['num', 'numeric', 'enum', 'enumeration']
    }
    VALID_CLASSES = [Rosetta::MDS, Rosetta::VMD, Rosetta::Channel, Rosetta::Metric]
    LOOKUP_TAG_BY_CLASS = {}
    VALID_CLASSES.each { |c| LOOKUP_TAG_BY_CLASS[c] = ([c.name.demodulize.downcase] + Array(ALIASES[c]).uniq) }
    LOOKUP_CLASS_BY_TAG = {}
    LOOKUP_TAG_BY_CLASS.each do |klass, tags|
      tags.each { |tag| LOOKUP_CLASS_BY_TAG[tag] = klass }
    end
    PROFILE_CLASS = Rosetta::Profile
    # TYPE_CLASS         = DIM::CommonDataTypes::ASN1::TYPE
    # NomPartition_CLASS = DIM::CommonDataTypes::ASN1::NomPartition
    IGNORED_KEYS = ['xmlns', 'xmlns:xsi', 'xsi:type', 'profile_root']
        
    def _ignored_keys
      super + IGNORED_KEYS
    end
    
    def build_profile_from_rch(container_node, contained_elements, results = {:warnings => []})
      # puts "\nContainer Node: #{container_node.inspect}"
      # FIXME this needs generalized error handling so that the page doesn't just go sour.  Example, if there is garbage in the xml then contained_elements can be a String which, of course, does not respond to #each.  Maybe there should be a specific error if contained_elements is a String as well as general/broad scope error handling.
      contained_elements.each do |tag,value|
        # puts "  <#{tag}>"
        # puts "    #{value.inspect}"
        # puts
        # FIXME this is a quick patch and needs to be replaced with generalized capability for more attributes
        if tag == 'card' || tag == 'cardinality'
          lower, upper = parse_cardinality(value)
          container_node.cardinality_min = lower
          container_node.cardinality_max = upper
          next
        end
        
        element_klass_result = get_element_klass(tag, value)
        if element_klass_result[:error]
          return element_klass_result
          # FIXME exactly how to handle bad XML is a bit up in the air...once it is hammered out this whole file probably needs to be cleaned up
          results[:warnings] << "Could not parse unknown tag &lt#{tag}&gt"
          next
        end
        element_klass = element_klass_result[:klass]
        # find the appropriate_getter.  we really hope there is only one!!
        correct_property = container_node.class.properties.find do |getter, property_info|
          # FIXME and another one... calling #derived_attribute(should also instantiate the <attribute>_type method.  It does not.  Also check to see if there are any other methods that should get created in order to facilitate iteration of a classes properties.)
          # FIXME the check for :complex_attribute is a workaround for a weird thing in SSA.  Revise this once that is fixed.
          next if property_info[:derived] || (property_info[:type] == :complex_attribute) # assuming that this does not 'find' anything
          type_getter = (getter.to_s + "_type").to_sym
          # puts "type_getter: #{type_getter}"
          # FIXME we call Array() because some type_getters, e.g. #cardinality_type, are not returning an array but instead just a single class.  WHY IS THAT HAPPENING?  I don't think it should be happening.
          types = Array(container_node.send(type_getter))
          puts "Checking #{container_node.class}.#{getter}_type for #{element_klass} -- #{types.inspect}"
          # puts types.inspect
          # puts_red property_info if types.include?(element_klass)
          types.include?(element_klass)
        end
    
        if correct_property
          adder = (correct_property.first.to_s + "_add").to_sym
        else
          puts "Can't find a property/getter to add a #{element_klass} to a #{container_node.class}"
          message = "The DIM does not allow you to add a #{element_klass.to_s.demodulize} to a #{container_node.class.to_s.demodulize}.  Please review your device structure."
          return {:error => message}
          next # FIXME need to do something more useful than just skipping this.  raise or log error or message.
        end
    
        values = []
        if value.is_a?(Hash)
          values << value
        elsif value.is_a?(Array)
          values += value
        else
          raise "value was a #{value.class} -- #{value.inspect}"
        end
        values.each do |value|
          # puts element_klass
          # pp value
          ChangeTracker.start
          element_node = element_klass.new
          element_node.save
          ChangeTracker.commit
          type_getter = (element_node.respond_to?(:system_type) ? "system_type" : "type").to_sym
          type_setter = (type_getter.to_s + "=").to_sym
          term_setter = (type_getter.to_s + "_term=").to_sym
          term        = nil
          given_refid = value.delete("refid")
          given_code  = value.delete("code")
          puts Rainbow(given_refid.inspect).orange
          if given_refid && !given_refid.strip.empty?
            refid = RTM.find_refid(given_refid)
          elsif given_code
            refid = RTM.find_refid_by_cf_code(given_code)
            puts "Code: #{given_code.inspect} -- #{refid.inspect}"
          else
            results[:warnings] << "No refid or code provided for &lt#{tag}&gt #{value}"
          end
          # CLEAR_KEYS.each{|key| value.delete key}
          # TODO if the refid was bogus but the code was good then we should let the user know!
          if refid
            ChangeTracker.start
            element_node.add_ref_id(refid)
            ChangeTracker.commit
          else
            refid = RTM.propose_by_refid(given_refid)
            if refid # method above returns nil if given_refid is empty
              results[:warnings] << "#{given_refid} was not found in the Nomenclature. A new Proposed Term was created."
              ChangeTracker.start
              element_node.add_ref_id(refid)
              ChangeTracker.commit
            end
          end

            
          # Populate Allowed Units / Enums
          # rtmms_ok means legal according to RTMMS
          # proposed means the refid does not map to any term in RTMMS
          if enums.any?
            rtmms_ok_contraints  = term.respond_to?(:collected_literals) ? term.collected_literals : []
            contraints           = []
            proposed_constraints = []
            if e = value.delete("e")
              e.split(/ +/).each do |refid_or_token|
                if found = RTM.find_term_by_ref_id(refid_or_token) || RTM.find_token(refid_or_token)
                  contraints << found
                elsif found = RTM::ProposedLiteral.where(:reference_id => refid_or_token).first
                  results[:warnings] << "#{refid_or_token} is not available in the Device Profiling Tool. An existing Proposed Enumeration Value was used for this #{refid}."
                  proposed_constraints << found
                else
                  ChangeTracker.start
                  proposed_literal = RTM::ProposedLiteral.new(:reference_id => refid_or_token, :proposed_on => Date.today.to_s, :proposed_by => 'fixme')
                  term.note_creation(proposed_literal)
                  proposed_literal.save
                  results[:warnings] << "#{refid_or_token} is not available in the Device Profiling Tool. A new Proposed Enumeration Value was created for this #{refid}."
                  ChangeTracker.commit
                  proposed_constraints << proposed_literal
                end
              end
            else
              contraints = rtmms_ok_contraints
            end
            if contraints.any? || proposed_constraints.any?
              ChangeTracker.start
              element_node.allowed_enumerations = contraints           if contraints.any?
              element_node.proposed_enums       = proposed_constraints if proposed_constraints.any?
              element_node.save
              if term.is_a?(RTM::ProposedType)
                term.enums          = contraints           if contraints.any?
                term.proposed_enums = proposed_constraints if proposed_constraints.any?
                term.save
              end
              ChangeTracker.commit
            end
          else
            results[:error] = "You can not use the @e attribute for an #{element_node.class.to_s.demodulize}" if value.delete("e")
          end

          if element_node.respond_to?(:allowed_units)
            rtmms_ok_contraints  = term.respond_to?(:collected_units) ? term.collected_units : []
            contraints           = []
            proposed_constraints = []
            if u = value.delete("u")
              u.split(/ +/).each do |unit_refid|
                if found = RTM.find_term_by_ref_id(unit_refid)
                  contraints << found
                elsif found = RTM::ProposedUnit.where(:reference_id => unit_refid).first
                  proposed_constraints << found
                  results[:warnings] << "#{unit_refid} is not available in the Device Profiling Tool. An existing Proposed Unit was used for this #{refid}."
                else
                  ChangeTracker.start
                  proposed_unit = RTM::ProposedUnit.new(:reference_id => unit_refid, :proposed_on => Date.today.to_s, :proposed_by => 'fixme')
                  # FIXME the commented out line was obviously wrong.  I think the line below is what it should have been.  Just make sure that that is correct.
                  # term.note_creation(proposed_unit)
                  proposed_unit.note_creation
                  proposed_unit.save
                  results[:warnings] << "#{unit_refid} is not available in the Device Profiling Tool. A new Proposed Unit was created for this #{refid}."
                  ChangeTracker.commit
                  proposed_constraints << proposed_unit
                end
              end
            else
              contraints = rtmms_ok_contraints
            end
            if contraints.any? || proposed_constraints.any?
              ChangeTracker.start
              element_node.allowed_units  = contraints          if contraints.any?
              element_node.proposed_units = proposed_constraints if proposed_constraints.any?
              element_node.save
              if term.is_a?(RTM::ProposedType)
                term.units          = contraints           if contraints.any?
                term.proposed_units = proposed_constraints if proposed_constraints.any?
                term.save
              end
              ChangeTracker.commit
            end
          else
            results[:error] = "You can not use the @u attribute for an #{element_node.class.to_s.demodulize}" if value.delete("u")
          end

          # puts "NO TYPE" unless element_node.send(type_getter)
          ChangeTracker.start
          # puts "Adding a #{element_node.class} to a #{container_node.class} with #{adder}"
          container_node.send(adder, element_node)
          container_node.save
          ChangeTracker.commit
          # puts "*"*5 + " #{element_klass} -- #{refid}"
          results = build_profile_from_rch(element_node, value, results)
          return results if results[:error] # recursively exits. i.e. climbs back up the stack with the error.
        end
      end
      unless container_node.is_a?(Rosetta::Profile)
        ChangeTracker.start
        container_node.cardinality_min = '1' unless container_node.cardinality_min
        container_node.cardinality_max = '1' unless container_node.cardinality_max
        container_node.term_group_type = 'one of' unless container_node.term_group_type
        container_node.save
        ChangeTracker.commit
      end
      results
    end
    
  end # IHE_Builder
end # DeviceProfiles
