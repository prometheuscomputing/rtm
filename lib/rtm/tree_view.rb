require_relative 'reports'
module Rosetta
  module Display
    def html_summary
      'FIX this summary'
    end
    
    def cardinality
      "#{cardinality_min}..#{cardinality_max}"
    end
    
    def refid_display_tree_display
      # TODO - More colors for more statuses
      if ref_ids.any?
        refid = ref_ids.first
        rid = refid.value
        color = case refid.definitions.first&.status&.value
        when 'Approved', 'Published'
          'chartreuse'
        when 'Proposed'
          'yellow'
        when 'Provisional', 'Accepted', 
          'blue'
        when 'Deprecated',
          rid = rid + ' (DEPRECATED)'
          'orange'
        when 'Withdrawn'
          rid = rid + ' (WITHDRAWN)'
          'red'
        when 'Rejected'
          rid = rid + ' (REJECTED)'
          'red'
        else
          'violet'
        end
      else
        color = 'red'
        rid   = 'refid missing'
      end
      "<font style='color: #{color}'>\u2B24</font> #{rid} "
    end

    def display_name
      text = "#{refid_display_tree_display}#{self.class.name.demodulize} (#{cardinality})"
      "<font style='background-color: #{tree_view_color}'>#{text}</font>"
    end
  end
  
  class MDS
    include Display
    def tree_view_color
      Rosetta::Reports::MDS_BACKGROUND_COLOR
    end
  end
  class VMD
    include Display
    def tree_view_color
      Rosetta::Reports::VMD_BACKGROUND_COLOR
    end
  end
  class Channel
    include Display
    def tree_view_color
      Rosetta::Reports::CHANNEL_BACKGROUND_COLOR
    end
  end
  class Metric
    include Display
    def tree_view_color
      Rosetta::Reports::METRIC_BACKGROUND_COLOR
    end
  end
  class Facet
    include Display
    def tree_view_color
      Rosetta::Reports::FACET_BACKGROUND_COLOR
    end
  end
end
