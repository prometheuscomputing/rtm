module Rosetta
  class Profile
    
    derived_attribute(:download_rch_html, ::Hash)
    def download_rch_html
      fname = sanitize_filename(display_name)
      if version && !version.empty?
        fname << "_#{version}"
      end
      fname << '_rch.html' 
      {:content => rch_html, :filename => fname, :type => 'html'}
    end # download_rch_html
    
    def rch_values_report
      return unless mds
      mds.rch_values_report
    end # def rch_values_report
    
    def rch_html(file_path = nil)
      html = Rosetta::Reports::RCH_HTML.new.build_html_table(rch_values_report)
      if file_path
        File.open(file_path, 'w+') { |f| f.write html }
      else
        html
      end
    end
  end # class Profile

  class TermedNode
    def term
      if respond_to?(:system_type)
        system_type_term
      else
        type_term
      end
    end
    def rch_values_report
      report_hash = {:type => nil, :refid => nil, :cfcode => nil, :unit_values => [], :uom_ucum => [], :ucode => [], :enum_values => [], :children => []}
      report_hash[:description] = refid.definition&.description
      report_hash[:refid]   = [refid, :allowed]
      tcs = refid.definitions.collect(&:cf_term_codes).flatten.uniq.collect(&:to_s).join(', ')
      report_hash[:cfcode]  = [tcs, :allowed]
      # puts "#{report_hash[:refid]} -- #{term.cf_term_code}"
      report_hash
    end # def rch_values_report
  end # class Top

  class MDS
    # FIXME to handle nested MDS instances
    def rch_values_report
      report_hash = super
      report_hash[:type] = :mds
      metrics.each { |m| report_hash[:children] << m.rch_values_report }
      vmds.each { |v| report_hash[:children] << v.rch_values_report }
      report_hash
    end # def rch_values_report
  end # class MDS
  
  class VMD
    def rch_values_report
      report_hash = super
      report_hash[:type] = :vmd
      metrics.each { |m| report_hash[:children] << m.rch_values_report }
      channels.each { |c| report_hash[:children] << c.rch_values_report }
      report_hash
    end # def rch_values_report
  end # class VMD
  class Channel
    def rch_values_report
      report_hash = super
      report_hash[:type] = :channel
      metrics.each {|m| report_hash[:children] << m.rch_values_report}
      report_hash
    end # def rch_values_report
  end # class Channel
  class Metric
    def rch_values_report
      report_hash = super
      dfn = refid&.definition
      au  = allowed_units
      if dfn && term.respond_to?(:collected_units)
        defn.collected_units.each do |unit|
          if unit.is_a?(RTM::Unit) # FIXME what if it is a UnitGroup? Depends on whether or not the list should include UnitGroups or not.  Waiting on Paul Schluter for guidance here.
            allowed = au.include?(unit) || au.empty? ? :allowed : :restricted
            report_hash[:unit_values] << [unit.reference_id, allowed]
            report_hash[:ucode]       << [unit.cf_term_code.to_s, allowed]
            report_hash[:uom_ucum]    << [unit.ucum_units.collect{|uu| uu.code}.join(' '), allowed]
          end
        end
        term.proposed_units.each do |pu|
          in_rtmms = pu.found_in_rtmms ? :needs_update : :proposed
          report_hash[:unit_values] << [pu.reference_id, in_rtmms]
          report_hash[:ucode]       << ['', in_rtmms]
          report_hash[:uom_ucum]    << ['', in_rtmms]
        end
      elsif pt
        au.each do |unit|
          if unit.is_a?(Nomenclature::Unit) # FIXME what if it is a UnitGroup? Depends on whether or not the list should include UnitGroups or not.  Waiting on Paul Schluter for guidance here.
            report_hash[:unit_values] << [unit.reference_id, :allowed]
            report_hash[:ucode]       << [unit.cf_term_code.to_s, :allowed]
            report_hash[:uom_ucum]    << [unit.ucum_units.collect{|uu| uu.code}.join(' '), :allowed]
          end
        end
        pt.proposed_units.each do |pu|
          in_rtmms = pu.found_in_rtmms ? :needs_update : :proposed
          report_hash[:unit_values] << [pu.reference_id, in_rtmms]
          report_hash[:ucode]       << ['', in_rtmms]
          report_hash[:uom_ucum]    << ['', in_rtmms]
        end
      end
      report_hash
    end # def rch_values_report
  # end # class Numeric
  # class Enumeration
    def rch_values_report
      report_hash = super
      pt = proposed_type  
      ae = allowed_enumerations
      if term && term.respond_to?(:collected_literals)
        term.collected_literals.each do |literal|
          allowed = ae.include?(literal) ? :allowed : :restricted
          report_hash[:enum_values] << [literal.name, allowed] # #name is an alias for #reference_id so that you can iterate over both tokens (which don't have reference_ids) and literals
        end
        term.proposed_enums.each do |pe|
          in_rtmms = pe.found_in_rtmms ? :needs_update : :proposed
          report_hash[:enum_values] << [pe.reference_id, in_rtmms]
          report_hash[:enum_values] << [pe.reference_id, in_rtmms]
        end
      elsif pt
        ae.each do |enum_value|
          report_hash[:enum_values] << [enum_value.name, :allowed] # #name is an alias for #reference_id so that you can interate over both tokens (which don't have reference_ids) and literals
        end
        pt.proposed_enums.each do |pe|
          in_rtmms = pe.found_in_rtmms ? :needs_update : :proposed
          report_hash[:enum_values] << [pe.reference_id, in_rtmms]
          report_hash[:enum_values] << [pe.reference_id, in_rtmms]
        end
      end
      report_hash
    end # rch_values_report
  end # class Metric
end # module Rosetta

module RTM
  class Definition
    def collected_literals
      ee = enums.collect do |e|
        if e.is_a?(RTM::EnumerationLiteral) || e.is_a?(RTM::Token)
          e
        elsif e.is_a?(RTM::EnumerationGroup)
          e.collected_literals
        else
          nil
        end
      end
      ee.flatten.uniq.compact
    end # def collected_literals
    
    def collected_units
      uu = units.collect do |u|
        if u.is_a?(RTM::Unit)
          u
        elsif u.is_a?(RTM::UnitGroup)
          u.collected_units
        else
          nil
        end
      end
      uu.flatten.uniq.compact
    end # def collected_units
  end # class Enumeration
  
  class EnumerationGroup
    def collected_literals
      ee = members.collect do |e|
        if e.is_a?(RTM::EnumerationLiteral) || e.is_a?(RTM::Token)
          e
        elsif e.is_a?(RTM::EnumerationGroup)
          e.collected_literals
        else
          nil
        end
      end
      ee.flatten.uniq.compact
    end # def collected_literals
  end # class EnumerationGroup
  
  class UnitGroup
    def collected_units
      uu = members.collect do |u|
        if u.is_a?(RTM::Unit)
          u
        elsif u.is_a?(RTM::UnitGroup)
          u.collected_units
        else
          nil
        end
      end
      uu.flatten.uniq.compact
    end # def collected_units
  end # class UnitGroup
end # module RTM

module Rosetta
  module Reports
    MDS_BACKGROUND_COLOR           = "CCFFFF"
    VMD_BACKGROUND_COLOR           = "CCFFCC"
    CHANNEL_BACKGROUND_COLOR       = "FFFFCC"
    METRIC_BACKGROUND_COLOR        = "FFFFFF" # yes, that is white
    FACET_BACKGROUND_COLOR         = "FFFFFF" # yes, that is white
    class RCH_HTML
      TABLE_HEADING_BACKGROUND_COLOR = "F0F0F0"
      RFID_HEADING                   = "REFID"
      CF_CODE_HEADING                = "CF_CODE10"
      UOM_MDC_HEADING                = "UOM_MDC"
      UOM_UCUM_HEADING               = "UOM_UCUM"
      CF_UCODE_HEADING               = "CF_UCODE10"
      ENUM_VALUES_HEADING            = "Enum_Values"
      DESCRIPTION_HEADING            = "Description"
      PROPOSED_VALUE_COLOR           = 'blue'
      PROPOSED_VALUE_COLOR_2         = 'magenta'
      DISALLOWED_VALUE_COLOR         = 'red'
      
      def build_html_table_rows(data_hash, depth=0)
        #Column Cells
        return '' unless data_hash
        type        = data_hash[:type]
        refid       = data_hash[:refid]
        cfcode      = data_hash[:cfcode]
        uom_mdc     = data_hash[:unit_values]
        uom_ucum    = data_hash[:uom_ucum]
        ucode       = data_hash[:ucode]
        enum_values = data_hash[:enum_values]
        description = data_hash[:description]
        html_row    = create_html_data_row(type, refid, cfcode, uom_mdc, uom_ucum, ucode, enum_values, description, depth)
        #Children of the subsequent rows with increasing depth
        children    = data_hash[:children]
        children.each do |child|
         html_row << build_html_table_rows(child, depth + 1)
        end
        html_row
      end

      def build_html_table(data_hash)
        html_table = "<table border='1' cellpadding='2' cellspacing='0'>"
        html_table << create_html_heading_row
        html_table << "<tbody>"
        html_table << build_html_table_rows(data_hash)
        html_table << "</tbody></table>" << legend
        html_table
      end
      
      def legend
        Haml::Engine.new(File.read(relative 'pages/view/rch_report_legend.haml')).render
      end
  
      def create_html_heading_row headings=nil
        headings ||= [RFID_HEADING, CF_CODE_HEADING, UOM_MDC_HEADING, UOM_UCUM_HEADING, CF_UCODE_HEADING, ENUM_VALUES_HEADING, DESCRIPTION_HEADING]
        table_heading = "<thead>" 
        heading_row = "<tr bgcolor='#{TABLE_HEADING_BACKGROUND_COLOR}' align='left'>"
        headings.each do |heading|
          heading_row << "<th>" << heading << "</th>"
        end
        heading_row << "</tr>"
        table_heading << heading_row
        table_heading << "</thead>"
        table_heading
      end
  
      def create_html_data_row(type, refid, cfcode, uom_mdc, uom_ucum, ucode, enum_values, description, depth)
        #Background color of the current row
        background_color = determine_background_color type
  
        table_row = "<tr bgcolor='#{background_color}'>"
        table_row << "<td>" << format_ref_id_cell(refid, type) << "</td>"
        table_row << "<td>" << format_cf_code_cell(cfcode) << "</td>"
        table_row << "<td>" << format_multi_line_cell(uom_mdc) << "</td>"
        table_row << "<td>" << format_multi_line_cell(uom_ucum) << "</td>"
        table_row << "<td>" << format_multi_line_cell(ucode) << "</td>"
        table_row << "<td>" << format_multi_line_cell(enum_values) << "</td>"
        table_row << "<td>" << description.to_s << "</td>"
        table_row << "</tr>"
        table_row
      end
  
      def format_cf_code_cell cfcode
        cfcode_val  = cfcode && cfcode[0]
        restriction = cfcode && cfcode[1]
        return '' unless cfcode_val
        case restriction
        when :allowed
          cfcode_string = "<pre>" << cfcode_val << "</pre>"
        when :proposed
          cfcode_string = "<pre>" << "<span style=\"color: #{PROPOSED_VALUE_COLOR};\" >" + cfcode_val + "</span>" << "</pre>"
        # when :needs_update
        #   cfcode_string = "<pre>" << "<span style=\"color: #{PROPOSED_VALUE_COLOR_2};\" >" + cfcode_val + "</span>" << "</pre>"
        end
        cfcode_string
      end

      # def format_ref_id_cell refid, depth
      def format_ref_id_cell refid, type
        refid_val   = refid && refid[0]
        restriction = refid && refid[1]
        depth = case type
                when :mds; 0
                when :vmd; 1
                when :channel; 2
                when :metric; 3
                when :facet; 4
                end
        dot_string = ". " * depth || ""
        return (dot_string || '') unless refid_val
        case restriction
        when :allowed
          refid_string = "<pre>" << dot_string + refid_val << "</pre>"
        when :proposed
          puts "#{refid_val} is proposed"
          refid_string = "<pre>" << dot_string + "<span style=\"color: #{PROPOSED_VALUE_COLOR};\" >" + refid_val + "</span>" << "</pre>"
        when :needs_update
          puts "#{refid_val} needs_update"
          refid_string = "<pre>" << dot_string + "<span style=\"color: #{PROPOSED_VALUE_COLOR_2};\" >" + refid_val + "</span>" << "</pre>"
        else
          raise "refid was #{refid}"
        end
        refid_string
      end

      def format_multi_line_cell cell_value, opts = {}
        table_cell = "<pre>"
        cell_text = ""
        if !cell_value || cell_value.empty? 
          # cell_text = "."
        else
          cell_value.each do |value|
            restricted = false
            if value.is_a?(Array)
              restriction = value[1] # because the value is actually true whether it is an allowed unit / enumeration_literal
              value = value[0]
            end
            if value
              case restriction
              when :proposed
                cell_text << "<span style=\"color: #{PROPOSED_VALUE_COLOR};\" >"
                cell_text << value + "\n"
                cell_text << "</span>"
              when :needs_update
                cell_text << "<span style=\"color: #{PROPOSED_VALUE_COLOR_2};\" >"
                cell_text << value + "\n"
                cell_text << "</span>"
              when :allowed
                cell_text << "<span>"
                cell_text << value + "\n"
                cell_text << "</span>"
              when :restricted
                value = "##{value}"
                cell_text << "<span style=\"color: #{DISALLOWED_VALUE_COLOR};\" >"
                cell_text << value + "\n"
                cell_text << "</span>"
              end
            end
          end
        end
        table_cell << cell_text
        table_cell << "</pre>"
        table_cell
      end

      def determine_background_color type
        case type
        when :mds
          background_color = MDS_BACKGROUND_COLOR
        when :vmd
          background_color = VMD_BACKGROUND_COLOR
        when :channel
          background_color = CHANNEL_BACKGROUND_COLOR
        end
        background_color
      end
    end # RCH_HTML
  end # module Reports
end # module Rosetta