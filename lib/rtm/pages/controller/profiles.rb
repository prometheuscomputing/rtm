class UploadProfile < Gui::Controller
  layout :default
  map '/upload_profile'
  
  def public_page?; true; end
  
  def ihe_rch
    @klass_tags = DeviceProfiles::IHE_Builder::LOOKUP_TAG_BY_CLASS
    params = request.safe_params
    if request.post?
      # pp params
      # puts "*"*30
      upload   = params['rch_upload']
      name     = params['profile_name'] || upload[:filename].delete(".xml")
      type     = params['profile_type']
      # TODO convention to distinguish from other profiles with same name
      file     = upload[:tempfile]
      # RubyProf.start
      result   = DeviceProfiles::IHE_Builder.new(file, :submitted_name => name, :device_type => :ihe, :xml_format => :rch).build
      # performance = RubyProf.stop
      # printer = RubyProf::MultiPrinter.new(performance)
      # printer.print(:path => ".", :profile => "performance")
      profile  = result[:profile]
      error    = result[:error]
      warnings = result[:warnings]
      if error
        flash[:rch_parse_error] = error
        raw_redirect "/upload_profile/ihe_rch/"
      elsif profile
        flash[:error_messages] = warnings if warnings.any?
        raw_redirect "/IHE/Profile/#{profile.id}/"
      else
        raise 'No result from IHE Profile Builder'
      end
    end
  end
end