module Gui
  class ProfilesController < Gui::Controller
    map '/profiles'
    layout('default') { |path, wish| !request.xhr? }
    
    def index
      params = request.safe_params
      if request.post?
        upload   = params['upload']
        file     = upload[:tempfile]
        result = DeviceProfiles.process_rosetta_profile_upload(file)
      end
    end
    
  end
end # Gui
