module Gui
  class CheckTermsController < Gui::Controller
    map '/check_terms'
    layout('default') { |path, wish| !request.xhr? }
    
    def index
      params = request.safe_params
      if request.post?
        upload   = params['terms_to_check_file']
        file     = upload[:tempfile]
        result   = RTM.check_terms_from_file(file)
        flash[:error]   = result[:error]
        flash[:success] = result[:success]

        # profile  = result[:profile]
        # error    = result[:error]
        # warnings = result[:warnings]
        # if error
        #   flash[:xml_parse_error] = error
        #   raw_redirect "/upload_profile/pcd_xml/"
        # elsif profile
        #   flash[:error_messages] = warnings if warnings.any?
        #   raw_redirect "/MyDevice/PCDProfile/#{profile.id}/"
        # else
        #   raise 'No result from PCD Profile Builder'
        # end
      end
    end
    
  end
end # Gui
