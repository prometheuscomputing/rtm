ucums = RTM::UCUM_Unit.all
vals  = ucums.map(&:value).map(&:strip)

# find duplicate ucum units entries and unify them
dup_vals = vals.select { |e| vals.count(e) > 1 }.uniq
dup_vals.each do |dval|
  # puts dval
  ducums = ucums.select { |u| u.value == dval }
  how_many = ducums.count
  keeper_ucum = ducums.pop
  puts Rainbow("Keeping 1 of #{how_many}: #{keeper_ucum.value}").green
  ducums.each_with_index do |ducum, i|
    ChangeTracker.start
    ducum.units.each { |unit| keeper_ucum.add_unit(unit) }
    ducum.remove_all_units
    ChangeTracker.commit
    ChangeTracker.start
    puts "Detroying #{ducum.value} - #{i}"
    ducum.destroy
    ChangeTracker.commit
  end
end
puts '---------------------------'

# separate entries that are for multiple units
if true
ucums = RTM::UCUM_Unit.all
vals  = ucums.map(&:value).map(&:strip)
dup_vals = vals.select { |e| vals.count(e) > 1 }.uniq
puts dup_vals.count
puts dup_vals
ucums.each do |ucum|
  v = ucum.value
  if v =~ /\s/
    correct_values = v.split(/\s+/)
    puts "#{v} --> #{correct_values}"
    units_for_ucum_entry = ucum.units
    correct_values.each do |cv|
      correct_ucum = ucums.find { |u| u.value == cv }
      if correct_ucum
        ChangeTracker.start
        units_for_ucum_entry.each { |ufue| correct_ucum.add_unit(ufue) }
        ChangeTracker.commit
      else
        ChangeTracker.start
        correct_ucum = RTM::UCUM_Unit.new(:value => cv)
        correct_ucum.save
        units_for_ucum_entry.each { |ufue| correct_ucum.add_unit(ufue) }
        ChangeTracker.commit        
      end
    end
    ChangeTracker.start
    ucum.destroy
    ChangeTracker.commit
  end
end
end




