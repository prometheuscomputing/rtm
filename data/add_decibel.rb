load File.join(__dir__, '../migrate/term_migrator.rb')
# TODO add discriminator_set UOM1
decibel = RTM.find('MDC_DIM_DECIBEL_SPL', '4::7904')
unless decibel
  args = {
    :refid            => 'MDC_DIM_DECIBEL_SPL',
    :part_code        => '4::7904',
    :status           => 'Provisional',
    :dimensionality   => '[Bel]',
    :dimensionality_c => '[Bel]',
    :dimension        => 'decibel (sound pressure level)',
    :symbol           => 'dB(SPL) dB',
    :uom              => 'decibel (20 uPa)',
    :ucum             => 'dB[SPL]',
    :note => "added 2021-01-19  decibel Sound Pressure Level, reference pressure = 20 micropascals (uPa)  Reference:  Ross Roeser, Michael Valente, Audiology: Diagnosis (Thieme 2007), p. 240.  https://en.wikipedia.org/wiki/Sound_pressure  See also UCUM  B[SPL].  [This is the first of four available codes 4::7904, 4::7936, 4::7968 and 4::8000 that are unused in 10101-2009, and should be considered 'reserved' for future decibel definitions that use specific reference levels.]"
  }
  decibel = TermMigrator.process_term(args)
  uom1  = RTM::DiscriminatorSet.where(:name => 'UoM1').first
  uom1d = uom1.discriminators.first
  ChangeTracker.start
  decibel.add_discriminator(uom1d)
  ChangeTracker.commit
end