require_relative 'add_decibel.rb'
csv = CSV.read(File.expand_path(File.join(__dir__, 'CheckMDCUnits.4j.x2d.2021-01-21T11.added-scaled-and-dbSPL.units.trimmed.csv')), :liberal_parsing => true)
csv.shift
# CF_UCODE10,UCODE10,UOM_MDC,Unit_of_Measure,UOM_UCUM

# Fix capitalization
if true
  uom = RTM::UnitsOfMeasure.where(:value => 'milliliter per hectoPascal created').first
  if uom
    ChangeTracker.start
    uom.value = 'milliliter per hectopascal created'
    uom.save
    ChangeTracker.commit
  end
  uom = RTM::UnitsOfMeasure.where(:value => '«magnitude» Siemens per cm created').first
  if uom
    ChangeTracker.start
    uom.value = '«magnitude» siemens per cm created'
    uom.save
    ChangeTracker.commit
  end  
end
# Going to do this is two passes.  

# First create missing UCUM units and units of measure
csv.each do |row|
  # next
  break if row.compact.empty?
  ucums = row[4]&.strip&.split(/\s+/)
  # puts Rainbow(row.inspect).yellow unless ucums

  if ucums && !ucums.empty?
    ucums.each do |ucum|
      ucum_obj = RTM::UCUM_Unit.where(:value => ucum).first
      unless ucum_obj
        ChangeTracker.start
        ucum_obj = RTM::UCUM_Unit.new(:value => ucum)
        ucum_obj.save
        ChangeTracker.commit
        puts "UCUM: #{ucum} created"
      end
    end
  end
  
  uom = row[3]&.strip
  if uom && !uom.empty?
    uom_obj = RTM::UnitsOfMeasure.where(:value => uom).first
    unless uom_obj
      ChangeTracker.start
      uom_obj = RTM::UnitsOfMeasure.new(:value => uom)
      uom_obj.save
      ChangeTracker.commit
      puts "UnitsOfMeasure: #{uom} created"
    end
  end
  
end


# 
csv.each do |row|
  # next
  break if row.compact.empty?
  refid = row[2]&.strip
  puts Rainbow(row.inspect).orange unless refid
  
  code  = '4::' + row[1].strip
  code10 = row[0].strip.to_i
  raise row.inspect unless (code10 - 262144) == row[1].strip.to_i
  uom = row[3]&.strip
  ucums = row[4]&.strip&.split(/\s+/)
  # puts Rainbow(row.inspect).yellow unless ucums

  existing_mdc = RTM.find(refid,code)
  unless existing_mdc
    puts "Term: #{refid} #{code} not found"
  end  

  if ucums&.any?
    eucums = existing_mdc.ucum_units.map(&:value)
    only_existing = eucums - ucums
    only_new      = ucums - eucums

    if only_existing.any?
      puts Rainbow("#{refid} does not include existing UCUMs #{only_existing}").coral
      puts "Existing:"
      eucums.each do |x|
        if only_existing.include?(x)
          puts Rainbow("  #{x}").orange
        else
          puts Rainbow("  #{x}")
        end
      end
      if ['MDC_DIM_PARTS_PER_10_TO_9', 'MDC_DIM_PARTS_PER_10_TO_12', 'MDC_DIM_PARTS_PER_10_TO_15', 'MDC_DIM_PARTS_PER_10_TO_18', 'MDC_DIM_CUBIC_X_M_PER_M_CUBE', 'MDC_DIM_CUBIC_X_M_PER_CM_CUBE', 'MDC_DIM_PER_X_OHM', 'MDC_DIM_X_AMPS'].include?(refid)
        only_existing.each do |oe_val|
          ucum_to_remove = RTM::UCUM_Unit.where(:value => oe_val).first
          ChangeTracker.start
          ucum_to_remove.remove_unit(existing_mdc)
          ChangeTracker.commit
        end
      end
    end
    if only_new.any?
      puts Rainbow("#{refid} does not include new UCUMs #{only_new}").cadetblue
      puts "Existing:"
      eucums.each do |x|
        if only_new.include?(x)
          puts Rainbow("  #{x}").orange
        else
          puts Rainbow("  #{x}")
        end
      end
    end
    ChangeTracker.start
    ucums.each do |ucum|
      next if eucums.include?(ucum)
      ucum_obj = RTM::UCUM_Unit.where(:value => ucum).first
      existing_mdc.add_ucum_unit(ucum_obj)
    end
    ChangeTracker.commit
  end
  
  if uom && !uom.empty?
    euom = existing_mdc.units_of_measure
    if euom
      unless euom.value == uom
        ChangeTracker.start
        euom.value = uom
        euom.save
        ChangeTracker.commit
        # puts "#{refid} Units of Measure changed! "+Rainbow("#{euom.value}").magenta + ' --> ' + Rainbow("#{uom}").orange
      end
    else
      ChangeTracker.start
      uom_obj = RTM::UnitsOfMeasure.where(:value => uom).first
      existing_mdc.units_of_measure = uom_obj
      ChangeTracker.commit
    end
  end
  
end
