# load File.join(__dir__, '../rtm/model_extensions.rb')
load File.join(__dir__, '../../migrate/term_migrator.rb')
path = File.expand_path(File.join(__dir__, '2020-02-24.DMC.1h.2020-10-19T18.cf_code10.trimmed.csv'))
file = File.read(path)
rows = CSV.new(file, liberal_parsing: true).read
header = rows.shift
# header.each_with_index { |r, i| puts Rainbow("#{i} - #{r}") }
# 0 - ﻿REFID
# 1 - gp
# 2 - CommonTerm
# 3 - Acronym
# 4 - Description
# 5 - Disc
# 6 - PART
# 7 - CODE10
# 8 - CF_CODE10
# 9 - UOM_UCUM
# 10 - Enum_Values
# 11 - Enum_Descriptions
# 12 - AlertType

NewTerm = Struct.new(*(%i{refid common_term acronym description disc_set part_code alert_type enums uom_ucum}))

terms = []
this_term = nil
enums = []
rows.each do |row|
  if row[1]&.strip == 'P'
    this_term = nil
    next
  end
  refid_str   = row[0]&.slice(/[A-Z].*/)&.strip # remove leading dots
  enum_value  = row[10]&.strip
  enum_description  = row[11]&.strip
  
  if row[1]
    next unless row[1]&.strip == '2' || row[1]&.strip == 'R2'

    # if refid_str && !refid_str.empty?
      this_term = NewTerm.new
      terms << this_term
      this_term.refid = refid_str
      this_term.common_term = row[2]&.strip
      this_term.acronym     = row[3]&.strip
      this_term.description = row[4]&.strip
      this_term.disc_set    = row[5]&.strip
      this_term.part_code   = "#{row[6]}::#{row[7]}"
      this_term.uom_ucum    = row[9]&.strip
      if enum_value && !enum_value.empty?
        # puts refid_str
        this_term.enums = enum_value.strip.split(/\n|\r/).zip(enum_description.to_s.split(/\n|\r/))
        # puts Rainbow(this_term.enums.inspect).green
      end
      this_term.alert_type  = row[12]&.strip
    # end
  elsif this_term && enum_value && !enum_value.empty? && refid_str.nil?
    # puts this_term.inspect
    # puts [enum_value.strip, enum_description&.to_s.strip].inspect
    this_term.enums << [enum_value.strip, enum_decfbxfgbscription&.to_s.strip] if enum_value
  end
end

terms.each do |nt|
  if nt.enums
    puts "#{nt.refid} #{nt.part_code}"
    puts nt.enums.inspect
  end
  next
  existing_term = RTM.find("#{refid_str} #{part_code}")
  
  if enum_values && !enum_values.empty?
    puts "#{refid_str} #{part_code}"
    ev =  enum_values.split(/\n/)
    ed =  enum_descriptions.to_s.split(/\n/)
    puts ev.zip(ed).inspect
  end
    
  if existing_term
    puts "Existing Term: #{refid_str} #{part_code} -- #{existing_term._stat}"
    # TODO
  else
    next # FIXME
    # TODO if this is reused...we already know there are no existing refids or codes so we're just going to proceed but if this is used with other term sets then you really need to check for and handle the use of existing refids and codes in the term set
=begin
    existing_refid = RTM.find(refid_str)
    if existing_refid
      rterms = existing_refid.terms
      puts "Existing refid: #{refid_str}"
      rterms.each(&:stat)
    end
    existing_code  = RTM.find(part_code)
    if existing_code
      cterms = existing_code.terms
      puts "Existing code: #{part_code}"
      cterms.each(&:stat)
    end
=end
    term = TermMigrator.process_term(:refid => refid_str, :part_code => part_code, :status => 'Provisional', :description => description, :common_term => common_term, :acronym => acronym, :aux => {:alert_type => alert_type}.to_json)
    if disc_set && !disc_set.empty?
      # TODO if reused with terms that have offsets other than 0 then this has to be fixed
      TermMigrator.add_discriminator_to_term(term, disc_set, 0, :provisional => true)
    end
  end
  
end
