# load File.join(__dir__, '../lib/rtm/model_extensions.rb')
path = File.expand_path(File.join(__dir__, '../new_terms/MEM_DMC/2020-02-24.DMC.1h.2020-10-19T18.cf_code10.csv'))
file = File.read(path)
rows = CSV.new(file, liberal_parsing: true).read
header = rows.shift
# codes  = []
# refids = []
terms = []
rows.each do |row|
  next unless row[1]&.strip == '2' || row[1]&.strip == 'R2'
  refid = row[0].slice(/[A-Z].*/).strip
  part_code = "#{row[6]}::#{row[7]}"
  if row[5] != '1'
    vals = RTM.base_term_expansion_values(refid, part_code, row[5])
    vals.first.zip(vals.last).each { |x| terms << x }
  else
    terms << [refid, part_code]
  end
end
terms.each do |refid, part_code|
  existing_term = RTM.find("#{refid} #{part_code}")
  puts "Found #{refid} #{part_code} -- #{existing_term._stat}" if existing_term
  existing_refid = RTM.find(refid)
  if existing_refid
    rterms = existing_refid.terms
    puts "Existing refid: #{refid}"
    rterms.each(&:stat)
  end
  existing_code  = RTM.find(part_code)
  if existing_code
    cterms = existing_code.terms
    puts "Existing code: #{part_code}"
    cterms.each(&:stat)
  end
end
puts terms.count
  

# csv.each_with_index do |row, i|
#   refid = row[0].strip
#   puts Rainbow("#{i+2} #{refid}").cyan
#   disc_name = row[1].strip
#   ptn  = row[2].strip.to_i
#   code = row[3].strip.to_i
#   ptn_code = "#{ptn}::#{code}"
#   next if refids.flatten.include?(refid) && codes.flatten.include?(ptn_code)
#   vals = RTM.base_term_expansion_values(refid, ptn_code, disc_name)
#   refids << vals.first
#   codes  << vals.last
#   break if refid == 'MDC_SAT_O2_REGIONAL_AREA_UNDER_CURVE_CUMULATIVE'
# end
# refids.flatten!
# codes.flatten!
# refid_dupes = refids.select { |e| refids.count(e) > 1 }.uniq
# code_dupes  = codes.select  { |e| codes.count(e)  > 1 }.uniq
# if refid_dupes.any?
#   puts "The following refids appeared multiple times:"
#   refid_dupes.each {|rd| puts "  #{rd}"}
# end
# if code_dupes.any?
#   puts "The following codes appeared multiple times:"
#   code_dupes.each {|cd| puts "  #{cd}"}
# end
# problem_refids = []
# problem_codes  = []
# refids.each do |r|
#   problem_refids << r unless RTM.new_refid_is_unique?(r)
# end
# codes.each do |c|
#   problem_codes << c unless RTM.new_code_is_unique?(c)
# end
# if problem_refids.any?
#   puts "The following RefIDs already existed: #{problem_refids}"
# end
# if problem_codes.any?
#   puts "The following codes already existed: #{problem_codes}"
# end
