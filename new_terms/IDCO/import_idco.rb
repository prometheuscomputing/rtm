require 'csv'
require 'json'
require 'xmlsimple'
module RTM
  PARTITION_SIZE = (2**16)
  module_function
  def import_idco
    xml_terms, xml_enums = get_xml_idco
    terms = get_idco_expanded_terms
    enum_terms = {}
    enum_groups = {} # not used later during creation....would remove but not hurting anything
    get_idco_normative_enums(terms, enum_terms, enum_groups)
    get_idco_enums(terms, enum_terms, enum_groups)
    discsets = get_idco_discriminators
    get_idco_ieee(terms, discsets)
    get_idco_idc(terms, discsets)
    # pp terms
    # egkeys = enum_groups.keys.map { |x| '_' + x }
    # enum_ids = terms.map { |t,v| v[:enums] }.uniq.compact.map { |x| x[0] == '_' ? x : '_' + x }
    # create_idco_discriminators(discsets) # NOTE Already done
    # enum_terms.each { |refid, data| create_enum_term(refid, data) }
    # terms.each { |refid, data| create_idco_term(refid, data) }
    # enums_report(terms)
    xml_report(terms, xml_terms, enum_terms, xml_enums)
  end
  
  def enums_report(terms)
    report = terms.collect { |refid, data| check_idco_term(refid, data) }.compact
    report_file = File.join(File.expand_path(__dir__), "enum_groups_report.csv")
    CSV.open(report_file, "w") do |csv|
      csv << ['Base Term', 'Enum Group', 'Expanded Term', 'Enum Group']
      report.each { |r| csv << r }
    end
  end
  
  def xml_report(terms, xml_terms, enum_terms, xml_enums)
    idc_refids = terms.map { |_,data| data[:idc] }.uniq.compact
    ieee_refids = terms.map { |_,data| data[:ieeedoc] }.uniq.compact
    xml_keys = xml_terms.keys
    xml_enum_keys = xml_enums.keys
    # puts idc_refids
    # puts idc_refids.count
    # puts
    # puts ieee_refids
    # puts ieee_refids.count
    # puts xml_keys - idc_refids
    # puts
    # puts idc_refids - xml_keys
    report = []
    idc_terms = terms.select { |refid,data| refid == data[:idc] }
    idc_terms.each do |idcrefid, idcdata|
      next if idcrefid == 'MDC_IDC_SET_BRADY_RV_PACE_AVOIDANCE_STATUS' # FIXME when sheet it fixed
      xml_term = xml_terms[idcrefid]
      puts idcrefid unless xml_term
      same = xml_term[:sysname] == idcdata[:sysname] &&
        xml_term[:description]&.gsub(/\r|\n/, ' ') == idcdata[:description]&.gsub(/\r|\n/, ' ') &&
        xml_term[:display_name] == idcdata[:aux][:display_name] &&
        xml_term[:cfcode] == idcdata[:cfcode] &&
        xml_term[:uom] == idcdata[:uom]
      unless same
        # xml_term[:sysname]
        # idcdata[:sysname]
        # xml_term[:description]
        # idcdata[:description]
        # xml_term[:display_name]
        # idcdata[:aux][:display_name]
        # xml_term[:cfcode]
        # idcdata[:cfcode]
        # xml_term[:uom]
        # idcdata[:uom]
        # puts
        # pp idcdata
        # puts
        message = "#{idcrefid} differs:\n"
        message << "  SystemName #{xml_term[:sysname].inspect} VS #{idcdata[:sysname].inspect}\n" unless xml_term[:sysname] == idcdata[:sysname] 
        message << "  Description\n    XML:#{xml_term[:description].inspect}\n    Excel:#{idcdata[:description].inspect}\n" unless xml_term[:description] == idcdata[:description] 
        message << "  DisplayName #{xml_term[:display_name].inspect} VS #{idcdata[:aux][:display_name].inspect}\n" unless xml_term[:display_name] == idcdata[:aux][:display_name] 
        message << "  Code #{xml_term[:cfcode].inspect} VS #{idcdata[:cfcode].inspect}\n" unless xml_term[:cfcode] == idcdata[:cfcode] 
        message << "  UOM #{xml_term[:uom].inspect} VS #{idcdata[:uom].inspect}\n" unless xml_term[:uom] == idcdata[:uom]
        # report << message
      end
    end
    xml_enums.each do |xmlrefid, xml_term|
      idcdata = enum_terms[xmlrefid]
      puts xmlrefid unless idcdata
      idcgroup = idcdata[:root].sub(/^_/, '')
      xgroup = xml_term[:group].sub(/^_/, '') 
      same = xml_term[:token] == idcdata[:mnemonic] &&
        xml_term[:description]&.gsub(/\r|\n/, ' ') == idcdata[:description]&.gsub(/\r|\n/, ' ') &&
        xml_term[:display_name]&.strip == idcdata[:display_name]&.strip &&
        xml_term[:cfcode] == idcdata[:cfcode] &&
        xgroup == idcgroup
      unless same
        pp xml_term
        puts
        pp idcdata
        puts "*"*8
        # xml_term[:sysname]
        # idcdata[:sysname]
        # xml_term[:description]
        # idcdata[:description]
        # xml_term[:display_name]
        # idcdata[:aux][:display_name]
        # xml_term[:cfcode]
        # idcdata[:cfcode]
        # xml_term[:uom]
        # idcdata[:uom]
        # puts
        # pp idcdata
        # puts
        message = "#{xmlrefid} differs:\n"
        message << "  Token/Mnemonic #{xml_term[:token].inspect} VS #{idcdata[:mnemonic].inspect}\n" unless xml_term[:token] == idcdata[:mnemonic] 
        message << "  Description\n    XML:#{xml_term[:description].inspect}\n    Excel:#{idcdata[:description].inspect}\n" unless xml_term[:description] == idcdata[:description] 
        message << "  DisplayName #{xml_term[:display_name].inspect} VS #{idcdata[:display_name].inspect}\n" unless xml_term[:display_name] == idcdata[:display_name] 
        message << "  Code #{xml_term[:cfcode].inspect} VS #{idcdata[:cfcode].inspect}\n" unless xml_term[:cfcode] == idcdata[:cfcode] 
        message << "  Group #{xgroup.inspect} VS #{idcgroup.inspect}\n" unless xgroup == idcgroup
        report << message
      end
    end
    report_file = File.join(File.expand_path(__dir__), "xml_report.txt")
    File.open(report_file, 'w') { |f| report.each { |line| f.puts(line) }}
  end
  
  def get_xml_idco
    terms = {}
    xml = XmlSimple.xml_in(File.read relative('terms.8g.xml'))
    tt = xml['Terms'].first['term']
    tt.each_with_index do |t, i|
      # puts i;pp t
      refid = t['REFID'].first
      if terms[refid]
        pp t
        raise "#{i} - #{refid} already seen.\n#{terms[refid].pretty_inspect}"
      end
      # puts t['UOM_MDC'].inspect
      uom = t['UOM_MDC']&.first.is_a?(String) ? t['UOM_MDC'].first.strip : nil
      terms[refid] = {
        index:i,
        refid:refid,
        sysname:t['SysName'].first.to_s.strip,
        description:t['Description'].first.to_s.strip,
        display_name:t['DisplayName'].first.to_s.strip,
        cfcode:t['ECODE10'].first.to_i,
        uom:uom,
        enum_xml:t['Enum_Values'] ? t['Enum_Values'].first : nil
      }
    end
    enums = {}
    tt = xml['Enums'].first['term']
    tt.each_with_index do |t, i|
      # puts i;pp t
      refid = t['REFID'].first
      if enums[refid]
        pp t
        raise "#{i} - #{refid} already seen.\n#{terms[refid].pretty_inspect}"
      end
      enums[refid] = {
        index:i,
        refid:refid,
        token:t['TOKEN'].first.to_s.strip,
        description:t['Description'].first.to_s.strip,
        display_name:t['DisplayName'].first.to_s.strip,
        cfcode:t['ECODE10'].first.to_i,
        group:t['_ENUM_GROUPS'] ? t['_ENUM_GROUPS'].first.to_s.strip : nil
      }
    end
    [terms, enums]
  end
  
  def create_idco_discriminators(discsets)
    discsets.each do |name, data|
      next if name == 'MMM'
      # puts name; pp data;puts
      ChangeTracker.start
      set = RTM::DiscriminatorSet.where(:name => name).first
      if set
        # puts "Found #{set.name}"
      else
        set = RTM::DiscriminatorSet.create(:name => name)
        # puts "Created #{set.name}"
      end
      set.allocation  = data[:allocation].to_i
      set.description = data[:description]
      # set.type = # FIXME ? 'infix' : 'suffix' # This will have to be deduced later...sighn
      set.save
      data[:discs].each do |value, offset, description|
        discriminator = RTM::Discriminator.create(:offset => offset, :value => value, :description => description)
        set.add_discriminator(discriminator)
      end
      ChangeTracker.commit
    end
  end
  
  def create_enum_term(refid, data)
    term = RTM.find_term("#{refid} #{data[:ptn]}::#{data[:code]}")
    if term
      # None of them do so this won't ever show.
      puts "#{refid} #{data[:ptn]}::#{data[:code]} ALREADY EXISTS"
    else
      ChangeTracker.start
      term = create_term(refid, "#{data[:ptn]}::#{data[:code]}")
      aux = {
        :mnemonic => data[:mnemonic],
        :mnemonic_synonym => data[:mnemonic_synonym],
        :display_name => data[:display_name],
        :version => data[:version],
        :vendor_section => data[:vendor_section],
        :vendor_offset_lookup => data[:vendor_offset_lookup],
        :id_in_sub_table => data[:id_in_sub_table]
      }
      term.update_definition({:description => data[:description], :aux => aux, :no_ct => true })
      term.status = 'Provisional'
      enum_group_refid = data[:root]
      enum_group_refid = '_' + enum_group_refid unless enum_group_refid[0] == '_'
      egrefid = RTM.find_or_create_refid(enum_group_refid)
      eg = egrefid.enumeration_group
      unless eg
        eg = RTM::EnumerationGroup.create(:name => enum_group_refid)
        eg.status = 'Provisional'
        egrefid.enumeration_group = eg
      end
      eg.add_member(term)
      ChangeTracker.commit
    end
  end

  def check_idco_term(refid, data)
    if data[:enums] != data[:enum_id] && data[:enums] != ('_' + data[:enum_id].to_s)
      return [data[:ieeedoc],data[:enum_id],refid,data[:enums]]
      # return "#{data[:abstract_refid]} - #{data[:enum_id]} // #{refid} - #{data[:enums]}"
      raise
    else
      return nil
    end
  end

  def create_idco_term(refid, data)
    if (data[:enums] && data[:enum_id]) && data[:enums] != data[:enum_id] && data[:enums] != ('_' + data[:enum_id])
      puts Rainbow(refid).red
      pp data
      raise
    end
    term = RTM.find_term("#{refid} #{data[:ptn]}::#{data[:code]}")
    if term
      # puts Rainbow("#{refid} #{data[:ptn]}::#{data[:code]} IDCO Term ALREADY EXISTS -- #{term._stat}")
    else
      ChangeTracker.start
      term = create_term(refid, "#{data[:ptn]}::#{data[:code]}")
      ChangeTracker.commit
    end
    ChangeTracker.start
    term.update_definition({ :description => data[:description], :aux => data[:aux], :sys_name => data[:sysname], :no_ct => true })
    term.status = 'Provisional' unless term.status # FIXME
    
    enum_group_refid = data[:enums] || data[:enum_id]
    if enum_group_refid
      enum_group_refid = '_' + enum_group_refid unless enum_group_refid[0] == '_'
      egrefid = RTM.find_refid(enum_group_refid)
      if egrefid
        eg = egrefid.enumeration_group
        term.add_enum(eg)
      else
        puts Rainbow("Enumeration Group #{enum_group_refid} was not found for #{term._stat}").red
      end
    end
    term.save
    ChangeTracker.commit
  end
      
  def get_idco_normative_enums(terms, enum_terms, enum_groups)
    path = relative('MDC_IDC_Nomenclature_v1.01.00_20200709_normative_enums.csv')
    header, rows = get_csv(path)
    puts Rainbow(header.inspect).orange
    rows.each do |row|
      next if row.uniq.compact.empty?
      refid = row[1]
      cfcode = row[6].to_i
      kode = row[9].to_i
      ptn, code = code_from_cfcode(cfcode)
      raise "#{insert} -- #{row.inspect}" unless code == kode
      raise "#{row.inspect} from #{insert} already exists" if enum_terms[refid]
      raise "#{refid} already exists in terms" if terms[refid]
      data = {
        :refid => refid,
        :ptn => ptn,
        :code => code,
        :root => row[0],
        :mnemonic => row[2],
        :mnemonic_synonym => nil,
        :display_name => row[3],
        :description => row[4].gsub("\r", "\n"),
        :version => row[5],
        :sub_table_id => row[7],
        :id_in_sub_table => row[8],
        :cfcode => cfcode
      }
      enum_terms[refid] = data
      enum_groups[row[0].strip] ||= []
      enum_groups[row[0].strip] << data
    end

  end

  def get_idco_enums(terms, enum_terms, enum_groups)
    inserts = ['BIO', 'BSX', 'MDT', 'ABT_STJ', 'MPT_SOR']
    inserts.each do |insert|
      path = relative("MDC_IDC_Nomenclature_v1.01.00_20200709_#{insert}_enums.csv")
      header, rows = get_csv(path)
      # puts Rainbow(header.inspect).orange
      rows.each do |row|
        next if row.uniq.compact.empty?
        refid = row[1]
        cfcode = row[7].to_i
        kode = row[11].to_i
        ptn, code = code_from_cfcode(cfcode)
        raise "#{insert} -- #{row.inspect}" unless code == kode
        raise "#{row.inspect} from #{insert} already exists" if enum_terms[refid]
        raise "#{refid} already exists in terms" if terms[refid]
        data = {
          :refid => refid,
          :ptn => ptn,
          :code => code,
          :root => row[0],
          :mnemonic => row[2],
          :mnemonic_synonym => row[3],
          :display_name => row[4],
          :description => row[5].gsub("\r", "\n"),
          :version => row[6],
          :vendor_section => row[8],
          :vendor_offset_lookup => row[9],
          :id_in_sub_table => row[10]
        }
        enum_terms[refid] = data
        enum_groups[row[0].strip] ||= []
        enum_groups[row[0].strip] << data
      end
    end
    [enum_terms, enum_groups]
  end

  def get_idco_ieee(terms, discsets)
    path = relative('MDC_IDC_Nomenclature_v1.01.00_20200709_ieeedoc_1.1.csv')
    header, rows = get_csv(path)
    # puts Rainbow(header.inspect).orange
    rows.each do |row|
      next if row[0] =~ /#REF!/
      puts Rainbow(row.inspect).yellow unless row[3]
      next if row[3].strip =~ /GROUP/ # FIXME do something about this
      refid = row[0].strip
      if refid =~ /\[.+\]/
        placeholders = refid.scan(/\[.+?\]/).map { |f| f.delete('[]') }
        refids = expand_idco_refid(refid, placeholders, discsets)
      else
        refids = [refid]
      end
      refids.each do |refid|
        t = terms[refid]
        if t
          terms[refid][:description] = row[2].strip if row[2]
          aux = {:display_name => row[1].strip, :idco_type => row[3].strip, :format => row[4].strip, :key => 'Y'}
          aux[:key] = 'Y' unless row[7] == '-'
          aux[:optionality]  = row[8].strip  unless row[8]  == '-'
          aux[:cardinality]  = row[9].strip  unless row[9]  == '-'
          aux[:coconstraint] = row[10].strip unless row[10] == '-'
          terms[refid][:aux] = aux 
          terms[refid][:enum_id] = row[6].strip unless row[6] == '-'
          terms[refid][:uom] = row[5].strip unless row[5] == '-'
          terms[refid][:ieeedoc] = row[0].strip
        else
          # puts "No entry in expanded terms for #{refid} from IEEDoc #{row[0]}"
          terms[refid]= {:ieeedoc => row[0].strip }
        end
      end
    end
  end
  
  def get_idco_idc(terms, discsets)
    path = relative('MDC_IDC_Nomenclature_v1.01.00_20200709_idc_nomenclature.csv')
    header, rows = get_csv(path)
    # puts Rainbow(header.inspect).orange
    rows.each_with_index do |row, rownum|
      next if row[0] !~ /\d+|tbd/
      refid = row[1].strip
      
      next unless row[2].to_s[0]
      # next if refid =~ /BLANKING_REFRACTORY/
      if refid =~ /\[.+\]/
        placeholders = refid.scan(/\[.+?\]/).map { |f| f.delete('[]') }
        refids = expand_idco_refid(refid, placeholders, discsets)
      else
        refids = [refid]
      end

      refids.each do |expanded_refid|
        t = terms[expanded_refid]
        if t
          # FIXME flesh this out with all of the stuff in the worksheet
          terms[expanded_refid][:idc] = refid
          if row[2].nil?
            puts Rainbow("Hey, why are we doing #{row[1].strip}?").orange
          end
          # puts
          # row.each_with_index { |c, i| puts Rainbow("  #{i} - #{c}").green }
          # puts
          
        else
          if row[2].nil?
            # puts "IDC Nomenclature entry '#{refid}', based on '#{row[1].strip}' on row #{rownum + 1} not found in IEEEDoc or Expanded Terms"
          else
            puts Rainbow("IDC Nomenclature entry '#{expanded_refid}', based on '#{refid}' on row #{rownum + 1} not found in IEEEDoc or Expanded Terms").orange unless expanded_refid =~ /_RVP_/
          end
          # row.each_with_index { |c, i| puts Rainbow("  #{i} - #{c}").yellow }
          # puts
          if refid =~ /^\d+|tbd$/
            puts "#{rownum}"
            pp row
            raise
          end
          terms[expanded_refid] = {:idc => refid }
        end
      end
    end
  end
  
  def expand_idco_refid(refid, placeholders, discsets)
    refids = []
    if placeholders.count == 1 || placeholders.count == 2
      discs0 = discsets[placeholders[0]][:discs]
      discs0.each do |disc0|
        if disc0.first.to_s.strip.empty?
          refid1 = refid.sub("_[#{placeholders[0]}]", '')
        else
          refid1 = refid.sub("[#{placeholders[0]}]", disc0.first)
        end
        if placeholders.count == 1
          refids << refid1
        elsif placeholders.count < 1
          raise "How did this happen?? #{refid}"
        else
          # puts "Expanding #{refid} --> #{refid1} -- > #{placeholders.inspect}"
          discs1 = discsets[placeholders[1]][:discs]
          discs1.each do |disc1|
            if disc1.first.to_s.strip.empty?
              refid2 = refid1.sub("_[#{placeholders[1]}]", '')
              refids << refid2
            else
              refid2 = refid1.sub("[#{placeholders[1]}]", disc1.first)
              refids << refid2
            end
          end
        end
      end  
    else
      raise "Whaattttt #{refid}"
    end
    refids
  end
  
  def get_idco_expanded_terms
    path = relative('MDC_IDC_Nomenclature_v1.01.00_20200709_expanded_terms1.1.csv')
    header, rows = get_csv(path)
    terms = {}
    enumgroups = []
    # puts Rainbow(header.inspect).orange
    rows.each do |row|
      ptn, code = code_from_cfcode(row[2])
      raise row.inspect unless ptn == 11
      data = { :ptn => ptn, :code => code, :sysname => row[1], :expanded => true, :cfcode => row[2].strip.to_i }
      data[:enums] = row[3] if row[3] && !row[3].strip.empty?
      terms[row.first.strip] = data
    end
    terms
  end
  
  def get_csv(path)
    raw  = File.read(path)
    rows = CSV.new(raw, liberal_parsing: true).read
    header = rows.shift
    [header, rows]
  end
  
  def code_from_cfcode(cfcode)
    cfcode = cfcode.to_i
    ptn  = (cfcode - (cfcode % PARTITION_SIZE)) / PARTITION_SIZE
    code = cfcode - (ptn * PARTITION_SIZE)
    [ptn, code]
  end
  
  def get_idco_discriminators
    path = relative('MDC_IDC_Nomenclature_v1.01.00_20200709_discriminators.csv')
    header, rows = get_csv(path)
    sets = {}
    # puts Rainbow(header.inspect).orange
    rows.each do |row|
      name = row[1].strip
      sets[name] ||= { :discs => [] }
      sets[name][:description] ||= row[0].strip
      sets[name][:allocation]  ||= 2**(row[2].strip.to_i)
      # [value, offset, description]
      sets[name][:discs] << [row[4].to_s.strip, row[6].to_s.strip.to_i, row[5].to_s.strip]
    end
    sets
  end
  
end

def boop
  unless new_refid_is_unique?(refid)
    existing_refid = RTM.find_refid(refid)
    existing_terms = existing_refid.terms.map { |t| "#{t.refid&.value} #{t.part_code} #{t.status&.value}" }.join("; ")
    problems << "Submitted term #{refid} #{ptn}::#{code} includes a RefID that already exists. #{existing_terms}"
  end
  code_unique = new_code_is_unique?("#{ptn}::#{code}")
  if code_unique.is_a?(String) # FIXME we should not be checking for problems by checking for a String.  Refactor this crap!
    problems << code_unique
  end
  unless code_unique
    existing_code = RTM.find_code("#{ptn}::#{code}")
    existing_terms = existing_code.terms.map { |t| "#{t.refid&.value} #{t.part_code} #{t.status&.value}" }.join("; ")
    problems << "Submitted term #{refid} #{ptn}::#{code} includes a term code that already exists. #{existing_terms}"
  end
  answer = {}
  if problems.any?
    answer[:error] = problems
  else
    answer[:success] = "These terms look fine to me."
  end
  answer
end
