undeprecated_obj = RTM.find('MDC_EVT_DELAY_IMPOSSIBLE 3::742')
RTM.set_term_status(undeprecated_obj, 'Provisional') if undeprecated_obj

rejects = ['MDC_EVT_SYRINGE_PATIENT_PRESSURE 3::604',
'MDC_EVT_SYRINGE_BARREL_CAPTURE 3::604',
'MDC_EVT_FLUID_LINE_OCCL_PROXIMAL 3::750',
'MDC_EVT_FLUID_LINE_OCCL_DISTAL 3::752',
'MDC_EVT_FLUID_LINE_AIR_PROXIMAL 3::754',
'MDC_EVT_FLUID_LINE_AIR_DISTAL 3::756']
rejects.concat(['MDC_DIM_DECIBEL_X_NV 4::7904', 'MDC_DIM_DECIBEL_DECA_NV 4::7936', 'MDC_DIM_X_BEL_MV 4::7968', 'MDC_DIM_X_BEL 4::8000'])
rejects.each do |reject|
  term_obj = RTM.find(reject)
  RTM.set_term_status(term_obj, 'Rejected') if term_obj
end

secondaries = ['MDC_EVT_STAT_VENT_TIME_RESP_VOL_LIMITED 3::6202 ', 'MDC_EVT_STAT_VENT_PRESS_RESP_VOL_LIMITED 3::6206']
secondaries.each do |s|
  term_obj = RTM.find(s)
  ChangeTracker.start
  term_obj.refid_preferred = false
  term_obj.save
  ChangeTracker.commit
end
