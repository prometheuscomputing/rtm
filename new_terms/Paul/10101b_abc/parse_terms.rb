# load File.join(__dir__, '../rtm/model_extensions.rb')
load File.join(__dir__, '../../../migrate/term_migrator.rb')
load File.join(__dir__, 'add_term.rb')
# load File.join(__dir__, 'parse_units.rb')
require 'nokogiri'

require_relative 'ais.rb'
require_relative 'aux_10101b.rb'

terms_path = File.expand_path(File.join(__dir__, 'terms.html'))
terms_file = File.read(terms_path)
document = Nokogiri::HTML(terms_file)
table  = document.at('table')
trs    = table.css('tr')
header = trs.shift
terms_rows = trs.map { |tr| tr.css('td').map { |td| td.inner_html.strip } }
# header.css('th').each_with_index { |td,i| puts "#{i} - #{td.text.strip}" }
# 0 - REFID
# 1 - CommonTerm
# 2 - Acronym
# 3 - Description
# 4 - Disc
# 5 - PartCode
# 6 - CF_CODE10
# 7 - fgn
# 8 - gp
# 9 - rx
# 10 - UOM_MDC
# 11 - UOM_UCUM
# 12 - Enum_Values
# 13 - Enum_Descriptions

terms_rows.each { |row| add_10101b_term(row) }
