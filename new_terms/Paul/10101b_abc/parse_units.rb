# load File.join(__dir__, '../rtm/model_extensions.rb')
load File.join(__dir__, '../../../migrate/term_migrator.rb')
load File.join(__dir__, 'add_term.rb')
require 'nokogiri'

require_relative 'ais.rb'
require_relative 'aux_10101b.rb'

units_path = File.expand_path(File.join(__dir__, 'units.html'))
units_file = File.read(units_path)
document = Nokogiri::HTML(units_file)
table  = document.at('table')
trs    = table.css('tr')
# RTM.check_discriminators
units_rows = trs.map { |tr| tr.css('td').map { |td| td.inner_html.strip } }
# header.css('th').each_with_index { |td,i| puts "#{i} - #{td.text.strip}" }
# 0 - REFID
# 1 - CommonTerm
# 2 - Acronym
# 3 - Description
# 4 - Disc
# 5 - PartCode
# 6 - CF_CODE10
# 7 - fgn
# 8 - gp
# 9 - rx
# 10 - UOM_MDC
# 11 - UOM_UCUM
# 12 - Enum_Values
# 13 - Enum_Descriptions

units_rows.each { |row| add_10101b_term(row) }


