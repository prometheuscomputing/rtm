discriminators = [[0, '_ALARM_OFF', 'State of indefinite duration in which an ALARM SYSTEM or part of an ALARM SYSTEM does not generate ALARM SIGNALS'],
[2, '_ALARM_PAUSED', 'State of limited duration in which the ALARM SYSTEM or part of the ALARM SYSTEM does not generate ALARM SIGNALS'],
[4, '_AUDIO_OFF', 'State of indefinite duration in which the ALARM SYSTEM or part of the ALARM SYSTEM does not generate an auditory ALARM SIGNAL'],
[6, '_AUDIO_PAUSED', 'State of limited duration in which the ALARM SYSTEM or part of the ALARM SYSTEM does not generate an auditory ALARM SIGNAL'],
[8, '_AUDIO_AND_HAPTIC_OFF', 'State of indefinite duration in which an ALARM SYSTEM or part of an ALARM SYSTEM does not generate auditory and haptic ALARM SIGNALS'],
[10, '_AUDIO_AND_HAPTIC_PAUSED', 'State of limited duration in which the ALARM SYSTEM or part of the ALARM SYSTEM does not generate auditory and haptic ALARM SIGNALS'],
[12, '_ENABLED', 'State of indefinite duration in which an ALARM SYSTEM or part of an ALARM SYSTEM can generate ALARM SIGNALS'],
[14, '_ALARM_OFF_OR_PAUSED', 'State comprised of indefinite and limited durations in which an ALARM SYSTEM or part of an ALARM SYSTEM does not generate ALARM SIGNALS'],
[16, '_AUDIO_OFF_OR_PAUSED', 'State comprised of indefinite and limited durations in which an ALARM SYSTEM or part of an ALARM SYSTEM does not generate auditory ALARM SIGNALS'],
[18, '_INDEFINITE_ACKNOWLEDGED', 'State of an ALARM SYSTEM initiated by OPERATOR action, where the auditory ALARM SIGNAL associated with a currently active ALARM CONDITION is inactivated until the ALARM CONDITION no longer exists.  NOTE: INDEFINITE_ACKNOWLEDGED only affects ALARM SIGNALS that are active at the time of the OPERATOR action.'],
[20, '_TIMED_ACKNOWLEDGED', 'State of an ALARM SYSTEM initiated by OPERATOR action, where the auditory ALARM SIGNAL associated with a currently active ALARM CONDITION is inactivated until the ALARM CONDITION no longer exists or until a predetermined time interval has elapsed.  NOTE: TIMED_ACKNOWLEDGED only affects ALARM SIGNALS that are active at the time of the OPERATOR action.']]

# create new AIS Discriminator Set....
ais = RTM::DiscriminatorSet.where(:name => 'AIS').first
unless ais
  ChangeTracker.start
  ais = RTM::DiscriminatorSet.new(:name => 'AIS')
  ais.save
  discriminators.each do |offset, value, description|
    d = RTM::Discriminator.new(:offset => offset, :value => value, :description => 'Refers to metric. ' + description)
    d.save
    ais.add_discriminator(d)
    d = RTM::Discriminator.new(:offset => offset + 1, :value => value, :description => 'Refers to object. ' + description)
    d.save
    ais.add_discriminator(d)
  end
  ChangeTracker.commit
end