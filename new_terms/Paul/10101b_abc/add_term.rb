load File.expand_path(File.join(__dir__, '../../../lib/rtm/utils/expand_discriminators.rb'))
NewTerm = Struct.new(*(%i{refid common_term acronym description disc_set disc_offset part_code alert_type enums uom_ucum uom_mdc gp value_constraint}))

def add_10101b_term(row)
  refid_str         = row[0]&.slice(/[A-Z].*/)&.strip # remove leading dots
  enum_values       = row[12]&.strip
  enum_descriptions = row[13]&.strip
  nt = NewTerm.new
  nt.refid       = refid_str
  nt.common_term = row[1]
  nt.acronym     = row[2]
  nt.description = row[3].gsub(/\r\n\s*/, ' ').gsub('❖', '').gsub(/\s+/, ' ')
  nt.disc_set    = row[4].sub(/\s*\(\d+\)/, '').strip
  nt.disc_offset = row[4].scan(/\(\d+\)/).first.to_s.delete('()').strip.to_i
  nt.part_code   = row[5]
  nt.gp          = row[8]
  if row[10][0] && row[10] != '--'
    nt.uom_mdc     = row[10].split('<br>')
    # puts Rainbow(nt.uom_mdc.inspect).orange
  end
  if row[11][0] && row[11] != '--' && row[11] !~ /^_/ # Paul put unit group refids in the ucum units column...sigh
    nt.uom_ucum    = row[11].split('<br>')
    # puts Rainbow(nt.uom_ucum.inspect).magenta
  end
  if enum_values && !enum_values.empty? && !(enum_values == '--' || enum_values == '-')
        # puts refid_str
        # nt.enums = enum_values.strip.split(/\n|\r/).zip(enum_descriptions.to_s.split(/\n|\r/))
        vals  = enum_values.split('<br>')
        descs = enum_descriptions.split('<br>').map { |ed| ed.gsub(/\r\n\s*/, ' ').gsub('❖', '').gsub(/\s+/, ' ') }
        # if descs.any? && vals.count != descs.count
          # puts Rainbow(refid_str).orange
          # pp vals
          # pp descs
        # end
        nt.enums = vals.zip(descs)
        # puts Rainbow(nt.enums.inspect).green
      # end
    # end
  end

  term = RTM.find("#{nt.refid} #{nt.part_code}")
    
  # TODO, we need to know about these terms and potentially handle them in some way but their existence doesn't mean we shouldn't create new terms
=begin
  existing_refid = RTM.find_refid(nt.refid)
  existing_code  = RTM.get_code_objs(nt.part_code)
  puts nt if (existing_tc.any? && !existing_refid) || (existing_tc.empty? && existing_refid)
  if existing_refid
    rterms = existing_refid.terms
    puts "Existing refid: #{refid_str}"
    rterms.each(&:stat)
  end
  if existing_code
    cterms = existing_code.terms
    puts "Existing code: #{part_code}"
    cterms.each(&:stat)
  end
=end
  
  # pp nt;puts
  
  # TODO if this is reused...we already know there are no existing refids or codes so we're just going to proceed but if this is used with other term sets then you really need to check for and handle the use of existing refids and codes in the term set
  aux = {:group_number => nt.gp}
  aux[:value_constraint] = nt.value_constraint if nt.value_constraint
  
  opts = {:refid => nt.refid, :part_code => nt.part_code, :status => 'Provisional', :description => nt.description, :common_term => nt.common_term, :acronym => nt.acronym, :aux => aux.to_json}
  if term
    term.update_definition(opts, true)
  else
    term = TermMigrator.process_term(opts)
  end
  unless nt.disc_set.empty?
    # TODO if reused with terms that have offsets other than 0 then this has to be fixed
    TermMigrator.add_discriminator_to_term(term, nt.disc_set, nt.disc_offset, :provisional => true)
    if nt.disc_offset == 0
      dset = RTM::DiscriminatorSet.where(:name => nt.disc_set).first
      expanded_terms = RTM.expand_term(term, dset)
      # pp expanded_terms.compact.map { |et| et.refid.value }; puts
    end
  end

  if nt.enums
    if nt.enums.count == 1 && nt.enums.first.first =~ /^_/
      group = RTM::EnumerationGroup.where(:name => nt.enums.first.first).first
      if group
        ChangeTracker.start
        term.add_enum(group)
        ChangeTracker.commit
      else
        puts Rainbow("No existing enum group for #{nt.enums.first.first}")
      end
    else
      # TODO this set of 10101b terms doesn't have any enum values that are MDC terms.  If it did then we'd have to handle that.
      nt.enums.each do |val, desc|
        raise if val =~ /MDC_/
        token = TermMigrator.process_token(:value => val, :status => 'Provisional', :description => desc)
        ChangeTracker.start
        term.add_enum(token)
        ChangeTracker.commit
      end
    end
    # puts Rainbow("#{nt.refid} #{nt.part_code}").orange
    # pp nt.enums;puts
  end
  
  if nt.uom_mdc
    ucums = []
    mdcs  = []
    nt.uom_mdc.each do |mdc|
      refid = RTM.find_refid(mdc)
      unless refid
        puts "Can't find #{mdc} units for #{nt.refid}" + Rainbow(' FIXME').red
        next
      end
      if refid.terms_count == 1
        u = refid.terms.first
        ChangeTracker.start
        term.add_unit(u)
        ChangeTracker.commit
        mdcs << u
        ucums = ucums + u.ucum_units.map(&:value)
      elsif refid.unit_group
        ChangeTracker.start
        term.add_unit(refid.unit_group)
        ChangeTracker.commit
      else
        puts Rainbow("#{mdc} maps to multiple terms").red
      end
      # check matching b/w uom_mdc and uom_ucum
      ucums.uniq
      if nt.uom_ucum && (ucums - nt.uom_ucum).any?
        puts Rainbow(nt.refid).orange
        puts "The following ucum units weren't listed: #{(ucums - nt.uom_ucum)}"
        puts "HTML UCUMS: #{nt.uom_ucum}"
        puts mdcs.map { |mdc| mdc.ucum_units.map(&:value)}
      end
      if nt.uom_ucum && (nt.uom_ucum - ucums).any?
        puts Rainbow(nt.refid).orange
        puts "The following ucum units weren't associated with any of the mdc units: #{(nt.uom_ucum - ucums)}"
        puts "HTML UCUMS: #{nt.uom_ucum}"
        puts mdcs.map { |mdc| mdc.ucum_units.map(&:value)}
      end
    end
  end
  
  # TODO this applies only to this list from Paul for 10101b
  if nt.uom_ucum && nt.uom_mdc.nil?
    nt.value_constraint = nt.uom_ucum
    nt.uom_ucum = nil
    # puts Rainbow("#{nt.refid} #{nt.part_code}").orange
    # raise "UCUM without MDC units"
  end
end