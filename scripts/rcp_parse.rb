require 'rainbow'
require 'csv'
require 'nokogiri'
require_relative 'rcp_parse_types'

module RCPParse
  module_function
  
  def parse(path, opts = {})
    raw  = File.read(path)
    rows = CSV.new(raw, liberal_parsing: true).read

    modules = create_modules(rows)
    device  = create_device(rows, modules)
    xml = create_rcp_xml(device, modules, opts)
    expansion_type = ''
    expansion_type = '_expanded' if opts[:expand]
    expansion_type = '_inline'   if opts[:inline]
    out_path = File.join(File.dirname(path), "pump#{expansion_type}.rcp.xml")
    File.open(out_path, 'w+') { |f| f.puts xml }
  end
  
  def create_rcp_xml(device, modules, opts = {})
    builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.RosettaProfile(:name => 'InfusionPump RCP Test') do
        _create_rcp_module_xml(modules, xml) unless opts[:expand] || opts[:inline]
        device.create_rcp_xml(modules, xml, opts)
      end
    end
    builder.to_xml
  end

  def _create_rcp_module_xml(modules, xml, opts = {})
    modules.each do |mod|
      mod.create_rcp_xml(modules, xml, opts)
    end
  end
  
  def create_modules(rows)
    groups = {}
    rows.each do |row|
      cell1 = row.first&.strip  
      if cell1 =~ dots_mdc_regex
        group = row[10]
        if group
          groups[group] ||= []
          groups[group] << [cell1, row[4], row[9]]
        end
      end
    end
    modules = []
    groups.each do |group, rows|
      mod = Module.new(group)
      modules << mod
      rows.each do |dotted_refid, units, enums|
        dots  = dotted_refid.slice(/^[\.\s]+/) || ''
        dots  = dots.delete(" \t")
        level = dots&.length.to_i
        refid = dotted_refid.delete('.').strip
        # Can we assume that all members are metrics??
        raise "Group member #{dotted_refid} is not a Metric or Facet" unless level == 3 || level == 4
        member = level == 3 ? Metric.new : member = Facet.new
        member.refids << refid
        if units
          member.units = units.split(/\s/)
          member.type = 'Numeric'
        end
        if enums
          member.enums = enums.split(/\s/)
          member.type = 'Enumeration'
        end
        mod.members << member
      end
    end
    modules
  end
  
  def create_device(rows, modules)
    stack = []
    current_level = 0
    mds    = nil
    vmd    = nil
    chan   = nil
    metric = nil
    current_obj  = nil
    current_cell1  = nil
    previous_cell1 = nil
    description = nil
    included_modules = []

    rows.each do |row|
      next unless row.first
      previous_cell1 = current_cell1
      # puts Rainbow(row.inspect).yellow
      cell1 = row.first&.strip
      current_cell1 = cell1
      
      if cell1.strip.slice(/^Includes:/)
        included_groups = cell1.slice(/(?<=Includes:).+/)&.strip&.split(',')&.map(&:strip) || []
        included_modules = included_groups.map { |ig| modules.find { |mod| mod.name == ig } }.compact
        description = previous_cell1
        # puts Rainbow("#{included_modules} -- #{description}").red
      end
      # puts "included_groups is #{included_groups.inspect}"
      # puts "included_modules is #{included_modules.inspect}"
      
      if cell1 =~ dots_mdc_regex
        group = row[10]
        next if group
        
        # puts cell1
        # puts Rainbow("#{included_modules} -- #{description}").green
        
        dots  = cell1.slice(/^[\.\s]+/) || ''
        dots  = dots.delete(" \t")
        level = dots&.length.to_i
        refid = cell1.delete('.').strip
    
        # puts Rainbow("#{level} -- #{cell1}").orange
        type =  case level
                when 0; MDS
                when 1; VMD
                when 2; Channel
                when 3; Metric
                when 4; Facet
                end
        
        if description
          current_obj = type.new
          current_obj.includes = included_modules if included_modules.any?
          current_obj.description = description
          description = nil
          # puts "Current obj is a #{current_obj.class}"
          # pp current_obj
        else
        end
        current_obj.refids << refid

        case current_obj
        when RCPParse::MDS
          mds = current_obj
        when RCPParse::VMD
          vmd = current_obj
          mds.vmds << vmd unless mds.vmds.include?(vmd)
        when RCPParse::Channel
          chan = current_obj
          vmd.channels << chan unless vmd.channels.include?(chan)
        when RCPParse::Metric
          metric = current_obj
          raise 'Oh shit'
        when RCPParse::Facet
        else
          puts "No type for #{type}"
        end
               
        # case type
        # when 0
        #
        #   'mds'
        # when 1
        #   if description
        #     vmd = VMD.new
        #   else
        #   end
        #   'vmd'
        # when 2
        #   'channel'
        # when 3
        #   units = row[4]
        #   enums = row[9]
        #   puts units if units
        #   puts enums if enums
        #   'metric'
        # when 4
        #   units = row[4]
        #   enums = row[9]
        #   'facet'
        # end
        # if level == current_level
        # elsif level < current_level
        # else # level > current_level
        # end
      end
    end
    # pp mds
    mds
  end
  
  def dots_mdc_regex
    /[\.\s]*MDCX?_([A-Z]+([a-z0-9]+)?_)*[A-Z]+([a-z0-9]+)?/
  end
end
