require 'docx'
module RTM
  def self._extract10101R
    # source = '../standards/11073-10101R-D9-r1-2019-04-21'
    source = '../standards/11073-10101-2019'
    docx_path = File.join(__dir__, source) + '.docx'
    html_path = File.join(__dir__, source) + '.html'
    # stdout, stderr, status = Open3.capture3("pandoc -s #{docx_path} -o #{html_path}")
    # puts stderr if stderr && !stderr =~ /WARNING/i
    docx = Docx::Document.open(docx_path)
    TermExtractor.new(docx, html_path)
  end
  
  def self.te
    @te ||= _extract10101R
  end
  
  module_function
  def go
    t = Time.now
    _go
    puts "That took #{(Time.now - t)/60} minutes."
  end
  
  def supergo
    part1
    part2
    # go_ex
  end
  
  def testgo
    part1
    part2
    # go_ex
  end
  
  def go_ex
    RTM.reset_log('Term_expansion_log_10101R_' + Time.now.strftime('%Y%m%d%H%M'))
    RTM.expand_discriminators
  end
  
  def _go
    part1
    # return
    part2
    end_report
    return
    
    RTM.reset_log('Term_expansion_log_10101R_' + Time.now.strftime('%Y%m%d%H%M'))
    RTM.te.expand_discriminators
    # check
  end
  
  def part1
    RTM.reset_log('Migration_log_10101R_' + Time.now.strftime('%Y%m%d%H%M'))
    RTM.te.pre10101_fix1
    ieee10101R
    RTM.te.load_tables
    # RTM.te.process_partitions if false # this should be right already
    actions = [
      :create_discriminator_sets,
      :parse_discriminators,
      :parse_oo,
      :parse_units,
      :parse_typical_tables,
      :parse_token_tables,
      :parse_group1,
      :parse_mnemonics,
      :parse103,
      :parse102,
      :parse_gas_msmt_sites,
      :parse148,
      :parse217,
      :parse219,
      :parse256,
      :parse_synonyms,
      :create_program_group_terms,
      :add_discriminators
    ]
    actions.each { |action| puts "#{action}"; RTM.te.send(action) }
    system 'cp ~/Prometheus/data/rtm_generated/SQL.db ~/Prometheus/data/rtm_generated/SQL.db.part1'
  end
  
  def part2
    RTM.te.load_tables
    RTM.te.deprecate_terms
    RTM.te.withdraw_terms
    fixes_before_discriminator_expansion
    system 'cp ~/Prometheus/data/rtm_generated/SQL.db ~/Prometheus/data/rtm_generated/SQL.db.part2'
  end
  
  def fixes_before_discriminator_expansion
    actions = [
      :fix_mdcx_spaces,
      :remove_extra_underscores_from_refids,
      :fix_lead_set,
      :fix_incorrect_term_codes,
      :fix3_80,
      :set_preferred_refids,
      :remove_bad_id_trig
    ]
    actions.each { |action| RTM.te.send(action) }
  end
  
  def check
    # TODO check to make sure that all synonyms have same discriminator
    # TODO consolidate Provisional / Proposed / Published / etc.
    # RTM.te.check_code_duplicates
    # RTM.te.check_refid_duplicates
    RTM.te.check_refid_homonyms
    RTM.te.check_code_homonyms
    RTM.te.check_definitions
  end
  
  def end_report
    exclude = (0..20).to_a
    exclude += [23, 24, 30, 34, 35, 59, 60, 68, 71, 73, 74, 75, 76, 78, 79, 80, 81, 83, 84, 85, 89, 90, 91, 96, 97, 102, 117, 119, 120, 121, 125, 144, 255, 257]
    exclude << 64
    rmn = RTM.te.tables.reject { |t| t[:extracted]  || exclude.include?(t[:counter])}
    puts "#{rmn.count} remaining tables:"
    rmn.each do |t|
      n = t[:table] + ' - ' + t[:title] if t
      puts "#{t[:counter]} #{t[:table]} - #{t[:title]}"
    end
    cs = RTM.deduce_code_synonyms
    # RTM.categorize_synonyms(cs, "TermCode")
    rs = RTM.deduce_refid_synonyms
    # RTM.categorize_synonyms(rs)
  end
  
  # def parse_oo; RTM.te.parse_oo; end
  # def extract_tables; RTM.te.extract_tables; end
  # def process_partitions; RTM.te.process_partitions; end
  

  def ieee10101R
    @ieee10101R ||= SDoc::Document.where(:title => '10101R').first
    if @ieee10101R.nil?
      ChangeTracker.start
      @ieee10101R = SDoc::Document.create(:title => '10101R')
      ChangeTracker.commit
    end
    @ieee10101R
  end
  
  def get_10101R_table(section, headers = [])
    doc   = ieee10101R
    table = doc.content.find { |c| c.title == section.strip}
    unless table
      ChangeTracker.start
      table = SDoc::Table.create(:title => section.strip)
      headers.each do |header|
        col = SDoc::Column.create(:header => header)
        table.add_column(col)
      end
      ieee10101R.add_content(table)
      ChangeTracker.commit
    end
    table
  end
  
  def add_to_10101R(term)
    return if term.sources.find {|ts| ts.id == ieee10101R.id }
    ChangeTracker.start
    term.add_source(ieee10101R)
    ChangeTracker.commit
  end
  
  def connect_term_to_table(term, table)
    ChangeTracker.start
    row = SDoc::Row.create
    row.add_term(term)
    table.add_row(row)
    ChangeTracker.commit
  end
  
  # def table_index_a
  #   [93] + (103..112..)
  # end
  
  def patch_partitions
    ChangeTracker.start
    RTM::TermCode.each do |tc|
      tc.block.partition.add_term_code(tc)
    end
    ChangeTracker.commit
  end

end
