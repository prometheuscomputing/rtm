# load File.join(__dir__, '../lib/rtm/model_extensions.rb')

path = (File.expand_path(File.join(__dir__, '../new_terms/Paul/10101b_group2_1.txt')))
file = File.open(path, 'r')
codes  = {}
refids = {}
file.each_line do |l|
  words = l.split(/\s+/)
  refid = words.find { |w| w =~ /[A-Z]([A-Z_])+[A-Z]/ }
  code  = words.find { |w| w =~ /\d+::\d+/ }
  puts "OH NO: #{l}" unless refid && code
  codes[code] = refid
  refids[refid] = code
end

problem_refids = []
problem_codes  = []
new_terms = []
conflict_terms = []
old_terms = []
refids.each do |r, c|
  term = RTM.find("#{r} #{c}")
  if term
    old_terms << term._stat
  else
    newr = RTM.new_refid_is_unique?(r)
    newc = RTM.new_code_is_unique?(c)
    if newr && newc
      new_terms << "#{r} #{c}"
      next
    end
    existing_code = RTM.find_code(c)
    cterms = existing_code ? existing_code.terms.map { |t| "#{t.refid_stat} #{t.code_stat}"}.join(', ') : []
    if !newc
      problem_codes << "Code #{c} given with #{r} is already used with #{cterms}"
    end
    if !newr
      if existing_code
        existing_refid = RTM.find_refid(r)
        rterms = existing_refid.terms.map { |t| "#{t.refid_stat} #{t.code_stat}"}.join(', ')
        problem_refids << "#{r} given with #{c} is already used with #{rterms} but code #{c} is used with #{cterms}"
      else
        new_terms << "#{r} #{c} (refid was proposed)"
      end
    end
  end
end
#
# allcodes = RTM::TermCode.all;
# orphancodes = allcodes.select { |c| c.terms_count < 1};

puts problem_refids
puts problem_codes
puts '************ Old Terms ***************'
puts old_terms
# puts '************ New Terms ***************'
# puts new_terms
