unless Gui_Builder_Profile::User.where(:login => 'admin').first
  ChangeTracker.start
  admin = Gui_Builder_Profile::User.new
  admin.login = 'admin'
  admin.save
  admin.change_password('admin')
  role = Gui_Builder_Profile::UserRole.where(:name => 'Administrator').first
  admin.add_role(role)
  ChangeTracker.commit
end
