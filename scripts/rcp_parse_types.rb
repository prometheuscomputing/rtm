module RCPParse
  class Node
    attr_accessor :parent, :refids, :includes, :description
    def initialize
      @refids   = []
      @includes = []
    end
    def create_rcp_xml(modules, xml, opts = {})
      xml.send(tag, xml_attrs) {
        includes.each { |mod|
          if opts[:inline]
            unless mod.used
              mod.create_rcp_xml(modules, xml, opts)
              mod.used = true
            end
          end
          if opts[:expand]
            mod.members.each { |mem| mem.create_rcp_xml(modules, xml, opts)}
          else
            xml.include(:module => mod.name)
          end
        }
        create_contained_xml(modules, xml, opts)
      }
    end
    
    def xml_attrs
      {:refid => refids.join(' ')}
    end
    
    def tag
      self.class.name.demodulize.downcase
    end
  end
  
  class MDS < Node
    attr_accessor :vmds, :metrics
    def initialize
      super
      @vmds     = []
      @metrics  = []
    end
    
    def create_contained_xml(modules, xml, opts = {})
      vmds.each do |node|
        node.create_rcp_xml(modules, xml, opts)
      end
      metrics.each do |node|
        node.create_rcp_xml(modules, xml, opts)
      end
    end
        
  end
  
  class VMD < Node   
    attr_accessor :channels, :metrics
    def initialize
      super
      @channels = []
      @metrics  = []
      @parent   = parent
    end
    
    def create_contained_xml(modules, xml, opts = {})
      channels.each do |node|
        node.create_rcp_xml(modules, xml, opts)
      end
      metrics.each do |node|
        node.create_rcp_xml(modules, xml, opts)
      end
    end
  end
  
  class Channel < Node
    attr_accessor :metrics
    def initialize
      super
      @metrics  = []
      @parent   = parent
    end
    
    def create_contained_xml(modules, xml, opts = {})
      metrics.each do |node|
        node.create_rcp_xml(modules, xml, opts)
      end
    end
  end
  
  class MetricNode < Node
    attr_accessor :parent, :type, :enums, :units
    
    def initialize(type = nil)
      super()
      @units  = []
      @enums  = []
      @parent = parent
      @type   = type
    end
    
    def xml_attrs
      xa = super
      xa[:e] = enums.join(' ') if enums.any?
      xa[:u] = units.join(' ') if units.any?
      xa
    end
    
    def tag
      if type =~ /Numeric/i || units.any?
        'numeric'
      elsif type =~ /Enumeration/i || enums.any?
        'enumeration'
      else
        super
      end
    end
  end
  
  class Metric < MetricNode
    attr_accessor :facets
    def initialize(parent = nil, type = nil)
      super()
      @facets = []
    end

    def create_contained_xml(modules, xml, opts = {})
      facets.each do |node|
        node.create_rcp_xml(modules, xml, opts)
      end
    end
  end
  
  class Facet < MetricNode
    def create_contained_xml(modules, xml, opts = {})
    end
  end
  
  class Module# < Node
    attr_accessor :name, :members, :includes, :used # submodules not yet supported TODO
    def initialize(name)
      @name     = name
      @includes = []
      @members  = []
    end
    
    def create_rcp_xml(modules, xml, opts = {}) 
      xml.module(xml_attrs) do
        members.each do |member|
          member.create_rcp_xml(modules, xml, opts)
        end
      end
    end
    
    def xml_attrs
      {:name => name}
    end
  end
  
end
