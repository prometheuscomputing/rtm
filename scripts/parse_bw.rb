load '/Users/mfaughn/projects/rtm/scripts/rcp_parse.rb'
load '/Users/mfaughn/projects/rtm/scripts/rcp_parse_types.rb'

path = File.expand_path('~/projects/rtm/test_data/bw_all_mf_edit.csv')
RCPParse.parse(path)
RCPParse.parse(path, {expand:true})
RCPParse.parse(path, {inline:true})
