def what
  zero = []
  one  = []
  more = []
  RTM::Definition.all.each do |d|
    tcc = d.term_codes_count
    if tcc < 1
      zero << d
    elsif tcc == 1
      one << d
    else
      more << d
    end
  end
  puts "#{zero.count} had no term code."
  puts "#{one.count} had one term code."
  puts "#{more.count} had multiple term codes."
end

def table_cols
  table_types = {}
  tt = RTM.te.tables
  tt.each do |t|
    next if done.include?(t[:counter])
    hh = t[:headers].map { |h| h.gsub(/<\/?strong>/, '').strip.downcase }
    table_types[hh] ||= []
    table_types[hh] << t[:counter]
    # puts i.to_s + '--' + hh.inspect if hh.first =~ /Systematic name/i
  end
  report = table_types.to_a.sort_by { |k,v| v.count }
  # report = report.select{ |_,i| ((19..92).to_a & i).any? }
  report = report.map do |headers, indices|
    if indices.count < 2
      table_title = "#{tt[indices.first][:table]} - #{tt[indices.first][:title]}"
      "#{1} --> #{headers} : #{indices.inspect} -- #{table_title}"
    else
      "#{indices.count} --> #{headers} : #{indices.inspect}"
    end
  end
  puts report
  # puts table_types.keys.count
  # hh = table_types.keys.flatten.uniq
  # puts hh
  # puts hh.count
end

# def done
#   [105, 106, 109, 113, 114, 116, 122, 123, 124, 126, 127, 129, 130, 131, 132, 133, 134, 135, 138, 139, 140, 141, 142, 145, 146, 147, 150, 151, 152, 153, 155, 156, 157, 158, 159, 160, 161, 162, 163, 165, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 212] + [1, 95, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209] + [238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251] + [221, 222, 223, 224, 225, 226, 228, 229, 230, 231, 232, 233, 234, 235] + [236, 237] + [227] + (0..94).to_a + [1, 95, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209] + [107, 108, 110, 111, 112, 128, 143, 149] + [166, 210, 215, 216] + [213, 214] + [218, 220]
# end

def table(val)
  [val].flatten.collect do |v|
    t = RTM.te.tables.find { |t| t[:counter] == v }
    if t
      t.select {|key, _| [:table, :title, :page, :counter, :headers, :index].include?(key) }
    end
  end
end

def table_check(tables = nil)
  tables ||= RTM.te.tables
  tables.each do |t|
    next if done.include?(t[:counter])
    # [:table, :title, :page, :headers, :rows, :docx, :index]
    puts "<#{t[:counter]}> #{t[:headers]} - #{t[:table]}-#{t[:title]} :: page:#{t[:page]}"
  end
  nil
end

def x_tables(regex)
  dt = []
  RTM.te.tables.each do |t|
    dt << t if t[:title] =~ regex
  end
  table_check(dt)
  puts dt.map{|t| t[:counter]}.inspect
end

def deprecated_tables
  x_tables(/eprecate/)
end

def withdrawn_tables
  x_tables(/withdrawn/i)
end

def synonym_tables
  x_tables(/synonym/i)
end

def units_tables
  x_tables(/unit/i)
end


    
