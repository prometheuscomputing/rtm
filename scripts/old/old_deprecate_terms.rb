# does refid deprecation imply synonymization?? TODO find out
class TermExtractor
  def deprecated_tables
    # covered - 86 100 101 107 111 112 114 123 124 127 128 132 135 143 149 161 162 191 196
    # skip 125
    [86, 100, 101, 107, 108, 111, 112, 114, 123, 124, 125, 126, 127, 128, 132, 135, 143, 149, 161, 162, 191, 196, 200]
  end
  # covers A.7.1.7.1
  #
  def deprecate_terms
    deprecate100
    deprecate101
    deprecate108
    deprecate114
    deprecate196
    deprecate111
  end
  
  def deprecate111
    proc111 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, note:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym, :status => :deprecate_both}
      term = process_term(args)
    end
    puts Rainbow("Begin Deprecated terms for ECG enumerations SPECIAL METHOD").cyan
    t = @tables.find { |t| t[:counter] == 111 }
    section = t[:table] + ' - ' + t[:title]
    puts Rainbow("111 Parsing '#{section}'.").orange
    headers = t[:headers]
    rows    = t[:rows]#[1..-1]
    puts Rainbow(section + ' has no data.').yellow if rows.count < 1
    if rows.count == 1 && rows[0].count == 1
      words = extract_text(rows[0][0])
      RTM.error("Problem parsing Deprecated terms for ECG enumerations table with words: " + words, :red)
    end
    table = RTM.get_10101R_table(section, t[:headers])
    rows.pop # the last row is a bogus error....
    rows.each { |cells| proc111.call(cells, table) }
    t[:extracted] = true
  end
  
  def deprecate101
    proc101 = proc do |cells, table|
      refid, ucode, note = cells.collect { |cell| extract_text(cell) }
      part_code = '4::' + ucode.to_s.strip
      args = {type:RTM::Unit, table:table, refid:refid, part_code:part_code, note:note}
      deprecate_refid(args)
    end
    _parse_tables('Table A.6.7.1—Deprecated RefIds for vital signs units of measurement', [101], proc101)
  end
  
  def fix_deprecation_problems(refid_obj)
    val = refid_obj.value
    substitute_val = case val
    # when 'MDC_DIM_DS_PER_M_SQ_PER_CM5'
    #   'MDC_DIM_DYNE_SEC_PER_M_SQ_PER_CM_5'
    when 'MDC_DIM_X_G_PER_MILLI_G'
      'MDC_DIM_X_G_PER_MG'
    # when 'MDC_DIM_MILLI_G_PER_MILLI_G'
    #   'MDC_DIM_MILLI_G_PER_MG'
    end
    if substitute_val
      puts "Subbing #{val} with #{substitute_val}"
      sub_refid_obj = RTM.find_or_create_refid(substitute_val)
      sub_defns = sub_refid_obj.definitions
      raise if sub_defns.count > 1
      return nil unless sub_defns.any?
      sub_defn = sub_defns.first
      extra_defns = refid_obj.definitions
      raise if extra_defns.count > 1 # we probably already checked this in the calling method...but anyway
      extra_defn = extra_defns.first
      merged = RTM.combine_definitions(sub_defn, extra_defn)
      # puts "Cool #{val}"
      [merged]
    end
  end
  
  def deprecate_refid(type:, table:, refid:, part_code:, note:nil)
    
  end
  
  
  def old_deprecate_refid(type:, table:, refid:, part_code:, note:nil)
    # refid_objs = RTM::RefID.where(value:refid).all
    # raise if refid_objs.count > 1
    refid_obj = RTM.find_refid(refid)
    unless refid_obj
      RTM.info("Deprecated RefID #{refid} does not exist.  Creating it now.", :yellow)
      ChangeTracker.start
      refid_obj = RTM.find_or_create_refid(refid)
      ChangeTracker.commit
    end
    code_obj = RTM.get_code_obj(part_code)
    raise "Can't find #{part_code}" unless code_obj
    
    refid_defns  = refid_obj.definitions
    # code_defns   = code_obj.definitions
    # common_defns = refid_defns & code_defns
    
    defns_with_code = refid_defns.select { |df| df.term_codes.include?(code_obj) }
    
    # Hackedy Hack
    # merged_definitions = fix_deprecation_problems(refid_obj)
    # defns_with_code = merged_definitions if merged_definitions
      
    if defns_with_code.any?
      defns_with_code.each do |df|
        RTM.deprecate_refid(df, refid_obj, note)
        # ["MDC_ECG_TIME_PD_PQ_SEG (Deprecated)"]
        puts df.refids_status.inspect
      end
    else
      code = RTM.get_code_obj(part_code)
      if code
        # we need to add the refid to the definition and then deprecate it.
        defns = code.definitions
        raise if defns.empty?
        if defns.count > 1
          RTM.error("Could not find a definition with refid #{refid}.  Found too many definitions for #{part_code}.  Not deprecating #{refid} #{part_code}. Fix manually", :red)
          return
        end
        df = defns.first
        ChangeTracker.start
        df.add_refid(refid_obj)
        ChangeTracker.commit
        RTM.deprecate_refid(df, refid_obj, note)
        puts df.refids_status.inspect
      else
        RTM.error("Could not find a definition with refid #{refid} or code #{part_code} for which #{refid} could be deprecated.", :red)
        return
      end
    end
  end
  
  def deprecate_term(type:, table:, refid:, bad_code:, good_code:, note:nil)
    good_terms = RTM.find_terms(refid, good_code)
    bad_terms  = RTM.find_terms(refid, bad_code)
    unless good_terms.any?
      RTM.warn("Good Term #{refid} #{good_code} is missing while deprecating #{refid} #{bad_code}.  Creating it now.", :blue)
      ChangeTracker.start
      good_term = RTM.create_term(refid, good_code)
      good_term.status = 'Published'
      good_term.save
      ChangeTracker.commit
    end
    RTM.warn("Multiple Bad Terms must be deprecated for #{refid} #{bad_code}", :yellow) if bad_terms.count > 1
    unless bad_terms.any?
      RTM.info("Bad Term not found for deprecation: #{refid} #{bad_code}")
      ChangeTracker.start
      bad_terms << RTM.create_term(refid, good_code)
      ChangeTracker.commit
    end
    bad_terms.each { |bt| RTM.deprecate_term(bt, note) }
  end
  
  def old_deprecate_term(type:, table:, refid:, bad_code:, good_code:, note:nil)
    refid_obj = RTM::RefID.where(value:refid).first
    unless refid_obj
      RTM.info("Deprecated RefID #{refid} does not exist.  Creating it now.", :yellow)
      ChangeTracker.start
      refid_obj = RTM.find_or_create_refid(refid)
      ChangeTracker.commit
    end
    bad_tc_obj  = RTM.get_code_objs(bad_code).first
    good_tc_obj = RTM.get_code_objs(good_code).first
    unless bad_tc_obj
      RTM.info("Deprecated TermCode #{bad_code} does not exist.  Creating it now.", :yellow)
      ChangeTracker.start
      bad_tc_obj = RTM.find_or_create_code_in_partition(bad_code)
      ChangeTracker.commit
    end
    unless good_tc_obj
      RTM.info("Replacement #{good_code} for deprecated TermCode #{bad_code} does not exist.  Creating it now.", :yellow)
      ChangeTracker.start
      good_tc_obj = RTM.find_or_create_code_in_partition(good_code)
      ChangeTracker.commit
    end
    refid_defs     = refid_obj.definitions
    bad_code_defs  = bad_tc_obj.definitions
    good_code_defs = good_tc_obj.definitions
    
    if refid_defs.empty? && good_code_defs.count == 1
      ChangeTracker.start
      good_code_defs.first.add_refid(refid_obj)
      ChangeTracker.commit
      refid_defs = good_code_defs
    end
    if bad_code_defs.empty? && good_code_defs.count == 1
      ChangeTracker.start
      good_code_defs.first.add_term_code(bad_tc_obj)
      ChangeTracker.commit
      bad_code_defs = good_code_defs
    end
    
    refid_bad_defns  = bad_code_defs  & refid_defs
    refid_good_defns = good_code_defs & refid_defs
    bad_good_defns   = good_code_defs & bad_code_defs
    common_defns     = bad_code_defs & good_code_defs & refid_defs
    
    if common_defns.any?
      common_defns.each do |dfn|
        RTM.deprecate_code(dfn, bad_tc_obj, note)
        RTM.deprecate_refid(dfn, refid_obj, note)
        RTM.info("Deprecated -> #{dfn.status&.value} - #{dfn.refids_status} #{dfn.part_codes_status}", :none)
      end
    elsif refid_good_defns.any?
      refid_good_defns.each do |dfn|
        # puts "Good + RefID #{dfn.part_codes_status} - #{dfn.refids_status}"
        if good_code_defs.count == 1 && bad_code_defs.count == 1
          merged = RTM.combine_definitions(good_code_defs.first, bad_code_defs.first, :force)
          common_defns = [merged]
          return deprecate_term(type:type, table:table, refid:refid, bad_code:bad_code, good_code:good_code, note:note) if merged
        end
        puts "Crap. Good + RefID #{dfn.part_codes_status} - #{dfn.refids_status}"
      end
    elsif refid_bad_defns.any?
      refid_bad_defns.each do |dfn|
        if good_code_defs.count == 1 && refid_defs.count == 1
          merged = RTM.combine_definitions(good_code_defs.first, refid_defs.first)
          common_defns = [merged]
          return deprecate_term(type:type, table:table, refid:refid, bad_code:bad_code, good_code:good_code, note:note) if merged
        end
        puts "Crap. Bad + RefID #{dfn.part_codes_status} - #{dfn.refids_status}"
      end    
    elsif bad_good_defns.any?
      bad_good_defns.each do |dfn|
        if good_code_defs.count == 1 && refid_defs.count == 1
          merged = RTM.combine_definitions(good_code_defs.first, refid_defs.first)
          common_defns = [merged]
          return deprecate_term(type:type, table:table, refid:refid, bad_code:bad_code, good_code:good_code, note:note) if merged
        end
        puts "Crap. Bad + Good #{dfn.part_codes_status} - #{dfn.refids_status}"
      end
    else
      msg = []
      msg << " Found #{refid_defs.count} defintions for RefID." if refid_defs.any?
      msg << " Found #{bad_code_defs.count} defintions for Bad Code." if bad_code_defs.any?
      msg << " Found #{good_code_defs.count} defintions for Good Code." if good_code_defs.any?
      puts "Ugly: #{refid} #{bad_code} #{good_code}." + msg.join
    end
    # # if defns_with_code.empty?
    #   merged_definitions = fix_deprecation_problems(refid_obj)
    #   defns_with_code = merged_definitions if merged_definitions
    # # end
    # if defns_with_code.any?
    #   defns_with_code.each do |df|
    #     RTM.deprecate_refid(df, refid_obj, note)
    #     puts df.refids_status.inspect
    #   end
    # else
    #   codes = RTM.get_code_objs(part_code)
    #   raise if codes.count > 1
    #   if codes.nil?
    #     RTM.error("Could not find a definition with refid #{refid} or code #{part_code} for which #{refid} could be deprecated.", :red)
    #     return
    #   else
    #     # we need to add the refid to the definition and then deprecate it.
    #     defns = codes.first.definitions
    #     raise if defns.empty?
    #     if defns.count > 1
    #       RTM.error("Could not find a definition with refid #{refid}.  Found too many definitions for #{part_code}.  Not deprecating #{refid} #{part_code}. Fix manually", :red)
    #       return
    #     end
    #     df = defns.first
    #     ChangeTracker.start
    #     df.add_refid(refid_obj)
    #     ChangeTracker.commit
    #     RTM.deprecate_refid(df, refid_obj, note)
    #     puts df.refids_status.inspect
    #   end
    # end
  end
  
  def create_missing_deprecation_table_terms
    process_term(:refid => 'MDC_ECG_SV_P_C_FREQ', :part_code => '2::17316', :acronym => 'FSPVC', :systematic_name => 'Pattern | Extrasystoles, Contraction, SupraVentricular, Premature, Frequent | ECG, Heart | CVS', :table => get_table_from_index(112))
  end
  
  def deprecate108 # and others
    create_missing_deprecation_table_terms
    dtables = [108, 112, 124, 127, 132, 135, 149, 162]
    proc108 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, note:description, refid:refid, part_code:part_code, type:RTM::Definition}
      next if refid == 'RefId'
      term = deprecate_refid(args)
    end
    _parse_tables('Deprecated RefIds', dtables, proc108)
  end
    
  def deprecate114 # both -- also others
    dtables = [107, 111, 114, 123, 126, 128, 143, 161, 191]
    proc114 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      description = description.sub('MDC_PRESS__BLD_NONINV_MEAN', 'MDC_PRESS_BLD_NONINV_MEAN')
      args = {table:table, description:description, note:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym, :status => :deprecate_both}
      term = process_term(args)
    end
    _parse_tables('Deprecated terms', dtables, proc114)
  end
  
  def deprecate196 # both
    dtables = [196, 200]
    proc114 = proc do |cells, table|
      sys_name, common_term, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      description = description.sub('MDC_PRESS__BLD_NONINV_MEAN', 'MDC_PRESS_BLD_NONINV_MEAN')
      args = {table:table, description:description, note:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, :status => :deprecate_both}
      term = process_term(args)
    end
    _parse_tables('Deprecated terms', dtables, proc114)
  end
  
  def deprecate100
    proc100 = proc do |cells, table, opts|
      refid, ucode, replacement_code, note = cells.collect { |cell| extract_text(cell) }
      bad_code  = '4::' + ucode.to_s.strip
      good_code = '4::' + replacement_code.to_s.strip
      args = {type:RTM::Unit, table:table, refid:refid, bad_code:bad_code, good_code:good_code, note:note}
      term = deprecate_term(args) unless refid == 'UOM_MDC'
    end
    _parse_tables('Table A.6.6.1—Deprecated terms for vital signs units of measurement', [100], proc100)
  end

  
  def synonymize_refids
    # [<preferred>, <synonym>]
    # FIXME if you need to...
    synonyms = [
      ['MDC_ECG_HEART_RATE', 'MDC_ECG_CARD_BEAT_RATE']
    ]
  end
end

