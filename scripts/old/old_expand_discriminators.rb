
  def oldish_expand_discriminators
    sets = RTM::DiscriminatorSet.all
    sets.each do |disc_set|
      next if disc_set.name == 'LEAD'
      puts Rainbow("Expanding #{disc_set.name}.").cyan
      discriminators = disc_set.discriminators
      base_discriminator = discriminators.find {|d| d.offset == 0 }
      base_terms = base_discriminator.terms
      base_code_ids = base_codes.map(&:id)
      def_assocs = disc_set.definitions_associations
      work = []
      def_assocs.each do |da|
        defn  = da[:to]
        infix = da[:through].depth
        # Do not expand withdrawn term codes?
        term_codes = defn.term_codes_associations.reject { |tca| tca[:through].status&.value =~ /Withdrawn/i }
          .collect { |tca| tca[:to] }
          .select { |tc| base_code_ids.include?(tc.id) }
        if term_codes.count > 1 # Check each occurrence manually then decide
          msg = "#{defn.inspect}\n#{defn.part_codes_status}" # "\n#{base_codes.map(&:part_code)}"
          if defn.part_codes_status == ["4::11936", "4::12000"]
            # FIXME make sure that the withdrawn term did not get expanded
            # process both
            # RTM.error("Fix the thing with MDC_DIM_X_M_PER_HR and MDC_DIM_X_ROTATIONS!", :red)
          elsif defn.part_codes_status == ["2::16144", "2::33280"]
            # FIXME make sure that each of these gets expanded properly, with the appropriate discriminator set
            # process both
            # RTM.error("Fix the thing with MDC_ECG_TIME_PD_PQ_GL and MDC_ECG_TIME_PD_PQ!", :red)
          elsif defn.part_codes_status == ["2::16148", "2::33536"]
            # FIXME make sure that each of these gets expanded properly, with the appropriate discriminator set
            # process both
            # RTM.error("Fix the thing with MDC_ECG_TIME_PD_PQ_SEG and MDC_ECG_TIME_PD_PQ_SEG_GL!", :red)
          elsif defn.part_codes_status == ["10::10352", "2::16397"]
            # process both
          else
            raise msg
          end
        end
        if term_codes.any?
          term_codes.each do |tcode|
            base_code_status = defn.term_codes_associations.find { |tca| tca[:to] == tcode }[:through].status
            work << { base_defn:defn, infix:infix, base_code:tcode.code, partition:tcode.partition, bc_status:base_code_status }
          end
        end
      end
      work.each do |w|
        discriminators.each do |disc|
          bc    = w[:base_code]
          ptn   = w[:partition]
          bd    = w[:base_defn]
          infix = w[:infix]
          bc_status = w[:bc_status] # FIXME assign tc_status to expanded term_codes if there is a status
          # next if disc.offset == 0 # Nah, let's check them anyway
          offset_code = disc.offset + w[:base_code]
          expanded_tcs = RTM::TermCode.where(code:offset_code, partition_id:ptn.id).all
          raise if expanded_tcs.count > 1
          expanded_tc = expanded_tcs.first
          base_definition_refids = bd.refids
          if base_definition_refids.count > 1 && disc.offset == 0
            # RTM.warn("#{ptn.number}::#{bc} #{disc_set.name} (0) has multiple RefIDs: #{bd.refids_status}.  Expanding both RefIDs.", :orange)
          end
        
          # if this is a UoM with an _X_ placeholder then 1) make sure the unity units refid also exists and is associated and 2) make sure that the unity units refid is not in the collection of refids to expand
          if (disc_set.name == 'UoM' || disc_set.name == 'UoM1') && disc.offset == 0
            xrefid = base_definition_refids.find { |r| r.value =~ /_X_/ }
            if xrefid
              unity_refid_val = xrefid.value.sub('_X_', '_')
              unity_refid = RTM.find_refid(unity_refid_val)
              if unity_refid && !base_definition_refids.map(&:id).include?(unity_refid.id)
                ChangeTracker.start
                bd.add_refid(unity_refid)
                bd.save
                ChangeTracker.commit                
              else
                ChangeTracker.start
                unity_refid = RTM.find_or_create_refid(unity_refid_val)
                bd.add_refid(unity_refid)
                bd.save
                ChangeTracker.commit
              end
              base_definition_refids = base_definition_refids - [unity_refid]
            end
          end
          
          base_definition_refids.each do |base_definition_refid|
            base_definition_refid_status = bd.refids_associations.find { |ra| ra[:to] == base_definition_refid }[:through].status
            base_refid_val = base_definition_refid.value
            expanded_refid_val = expand_refid_val(disc_set, disc, base_refid_val, infix)
            if expanded_tc # The expanded TermCode already exists and we don't need to create it.
              existing_defs_for_expanded_tc = expanded_tc.definitions
              # check that the status is the same as the base_code_status
              existing_defs_for_expanded_tc.each do |edef|
                assoc = edef.term_codes_associations.find { |tca| tca[:to] == expanded_tc }[:through]
                expanded_tc_status = assoc.status
                unless bc_status == expanded_tc_status
                  ChangeTracker.start
                  assoc.status = bc_status
                  assoc.save
                  ChangeTracker.commit
                end
              end
            
              existing_defs_with_set_for_expanded_tc = existing_defs_for_expanded_tc.select { |ed| ed.discriminator_sets.map(&:id).include?(disc_set.id) }
            
              ok_definitions = existing_defs_for_expanded_tc.select { |edfn| (edfn.sources.map(&:title) & ['10101a', 'Annex B']).any? || edfn.status&.value == 'Published' }
            
              # Yea, a hack to fix the slop from RTMMSv1 and 10101R....
              if [ '2::29301', '2::29302', '2::29303', '2::29305', '2::29306', '2::29307', '2::29309', '2::29310', '2::29311', '2::29313', '2::29314', '2::29315'].include?(expanded_tc.part_code)
                ok_definitions = ok_definitions + existing_defs_for_expanded_tc 
              end
              
            
              unless expanded_tc.discriminators.map(&:id).include?(disc.id)
                # Check the sources.  If it is from Annex B or 10101a then just add it.  Otherwise, get mad.
                if ok_definitions.any?
                  ChangeTracker.start
                  expanded_tc.add_discriminator(disc) unless expanded_tc.discriminators.include?(disc)
                  ChangeTracker.commit
                else
                  puts "#{expanded_tc.part_code} has #{expanded_tc.discriminators.map {|d| 'd.discriminator_set.name' + ' (' + d.offset.to_s + ')'}}"
                  puts "Sources: #{existing_defs_for_expanded_tc.map {|df| df.sources.map(&:title)}.flatten}"
                  RTM.error("Existing TermCode #{expanded_tc.part_code} is not associated to #{disc_set.name} (#{disc.offset})", :orange)
                end
              end
              # Are there any existing Definitions that already associated with this DiscriminatorSet?

              if existing_defs_with_set_for_expanded_tc.any?
                # Happy place so far.
                # Ensure that one of the existing definitions maps to the expanded refid
                existing_refids = existing_defs_with_set_for_expanded_tc.map { |ed| ed.refids_status }.flatten
                if existing_refids.include?(expanded_refid_val)
                  # make sure the existing refids have the same status as the base refid
                  existing_defs_with_set_for_expanded_tc.each do |edef|
                    assoc = edef.refids_associations.find { |ra| ra[:to].value == expanded_refid_val }
                    if assoc
                      expanded_refid_status = assoc[:through].status
                      unless expanded_refid_status == base_definition_refid_status
                        ChangeTracker.start
                        assoc[:through].status = base_definition_refid_status
                        assoc[:through].save
                        ChangeTracker.commit
                      end
                    end
                  end
                else
                  # puts existing_refids.inspect
                  # puts expanded_refid_val.inspect
                  # puts "#{existing_refids.include?(expanded_refid_val)}"
                  RTM.warn("#{expanded_tc.part_code} #{disc_set.name} (#{disc.offset}) does not have any definitions with expanded refID #{expanded_refid_val}.  The refids found were #{existing_refids}.  Make sure that #{expanded_refid_val} is a synonym for #{existing_refids}.", :none)
                  ChangeTracker.start
                  new_refid = RTM.find_or_create_refid(expanded_refid_val)
                  ChangeTracker.commit
                  existing_defs_with_set_for_expanded_tc.each do |edef|
                    ChangeTracker.start
                    edef.add_refid(new_refid)
                    ChangeTracker.commit
                    ChangeTracker.start
                    new_refid_assoc = edef.refids_associations.find { |ra| ra[:to] == new_refid }[:through]
                    new_refid_assoc.status = base_definition_refid_status
                    new_refid_assoc.save
                    ChangeTracker.commit
                  end
                end
              else
                if ok_definitions.any?
                  ok_definitions.each do |okdf|
                    ChangeTracker.start
                    okdf.add_discriminator_set(disc_set) 
                    ChangeTracker.commit
                  end
                  # Ensure that one of the existing definitions maps to the expanded refid
                  existing_refids = ok_definitions.map { |okdf| okdf.refids.map(&:value) }.flatten
                  unless existing_refids.include?(expanded_refid_val)
                    # if the base def has synonyms then perhaps we're alright here
                    if (base_definition_refids.count > 1) && (existing_refids.count == (base_definition_refids.count - 1))
                    else
                      puts existing_refids.inspect
                      puts expanded_refid_val.inspect
                      puts "#{existing_refids.include?(expanded_refid_val)}"
                      RTM.error("#{expanded_tc.part_code} #{disc_set.name} (#{disc.offset}) does not have any definitions with expanded refID #{expanded_refid_val}.  The refids found were #{existing_refids}.  Investigate (Y).", :red)
                    end
                  end
                else
                  sources = existing_defs_for_expanded_tc.collect { |edfn| edfn.sources.map(&:title) }.flatten
                  msg = existing_defs_for_expanded_tc.collect do |edfn|
                    st = edfn.status&.value
                    st = 'none' if st.nil? || st.strip.empty?
                    "  definition: #{edfn.inspect}\n    status: #{st}, refids: #{edfn.refids_status}, term_codes: #{edfn.part_codes_status}, sources: #{sources}" 
                  end.join("\n")
                  RTM.error("#{expanded_tc.part_code} #{disc_set.name} (#{disc.offset}) does not have any definitions that are associated to the #{disc_set.name} DiscriminatorSet.  Investigate (Z).\n#{msg}", :red)
                  # next
                end
              end
            else # The expanded TermCode does not yet exist
              type = ptn.number == 4 ? RTM::Unit : RTM::Definition
              # Create a term_code, create a definition, associate appropriately
              ChangeTracker.start
              new_refid = RTM.find_or_create_refid(expanded_refid_val)
              new_tc    = RTM.find_or_create_code_in_partition(offset_code, ptn)
              new_def   = type.create(auto_expanded:true, acronym:bd.acronym, systematic_name:bd.systematic_name, common_term:bd.common_term)
              if disc.description
                new_def.descriptions = bd.descriptions.collect do |bdd|
                  new_description_content = bdd.value_content.to_s.strip.sub(/^<p>/, '').sub(/<\/p>$/, '') + '; ' + disc.description
                  RTM.find_or_create_description(new_description_content, true)
                end 
              else
                new_def.descriptions = bd.descriptions
              end
              new_def.add_refid(new_refid)
              new_def.add_term_code(new_tc)
              new_def.add_discriminator_set(disc_set)
              new_tc.add_discriminator(disc)
              ChangeTracker.commit
              if bc_status
                ChangeTracker.start
                assoc = new_def.term_codes_associations.first[:through]
                assoc.status = bc_status
                assoc.save
                ChangeTracker.commit
              end
              if infix
                ChangeTracker.start
                assoc = new_def.discriminator_sets_associations.first[:through]
                assoc.depth = infix
                assoc.save
                ChangeTracker.commit
              end
            end
          end
        end
      end
    end
  end
