class TermExtractor
  # covered - 99 110 131 171 211
  # skip - 164 (already is fine)
  
  def withdrawn_tables
    [99, 110, 131, 164, 171, 211]
  end
  # covers A.7.1.7.1
  #
  def withdraw_terms
    fix_withdrawn_term_problems
    withdraw99
    withdraw110
    withdraw131
    withdraw171
    withdraw211
    fix164
  end
  
  def fix_withdrawn_term_problems
    ChangeTracker.start
    RTM.get_terms('MDC_DIM_STEP', '4::11520').first.add_term_code(RTM.get_code_obj('4::6656'))
    RTM.get_terms('MDC_DIM_FOOT_PER_MIN', '4::11552').first.add_term_code(RTM.get_code_obj('4::6688'))
    RTM.get_terms('MDC_DIM_INCH_PER_MIN', '4::11584').first.add_term_code(RTM.get_code_obj('4::6720'))
    RTM.get_terms('MDC_DIM_STEP_PER_MIN', '4::11616').first.add_term_code(RTM.get_code_obj('4::6752'))
    RTM.get_terms('MDC_DIM_TICK', '4::11648').first.add_term_code(RTM.get_code_obj('4::6848'))
    RTM.get_terms('MDC_RET_CODE_UNKNOWN', '255::9999').first.add_term_code(RTM.get_code_obj('255::1'))
    ChangeTracker.commit
  end
  
  def withdraw99
    proc99 = proc do |cells, table|
      refid, ucode, replacement_ucode, note = cells.collect { |cell| extract_text(cell) }
      part_code = '4::' + ucode.to_s.strip
      note = note.gsub('104', '11073:104')
      note = "Replaced with #{replacement_ucode}.  #{note}"
      args = {type:RTM::Unit, :status => :withdraw_both, table:table, refid:refid, part_code:part_code, note:note}
      term = process_term(args)
    end
    _parse_tables('Table A.6.5.1—Withdrawn terms for vital signs units of measurement', [99], proc99)
  end
  
  def withdraw110
    wtables = [110]
    proc110 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, note:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym, :status => :withdraw_code}
    end
    _parse_tables('Withdrawn Terms', wtables, proc110)
  end
  
  def withdraw131
    wtables = [131]
    proc131 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, note:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym, :status => :withdraw_both}
      term = process_term(args)
    end
    _parse_tables('A.7.5.7.1—Withdrawn terms for blood-gas, blood, urine, and other fluid chemistry measurements', wtables, proc131)
  end
  
  def withdraw171
    wtables = [171]
    proc171 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, note:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym, :status => :withdraw_code}
      term = process_term(args)
    end
    _parse_tables('Table A.11.5.4.1—Withdrawn terms for PHD Disease Management, SABTE sensors and settings', wtables, proc171)
  end
  
  def fix164
    table = tables.find { |tb| tb[:counter] == 164 }
    puts Rainbow("Begin #{table[:title]}").cyan
    # we are going to associate an incorrect term code with 
    r = RTM::RefID.where(:value => 'MDC_DEV_SPEC_PROFILE_GENERIC').all
    raise unless r.count == 1
    r = r.first
    d = r.definitions
    raise unless d.count == 1
    d = d.first
    t = RTM.get_code_obj('8::4097')
    ChangeTracker.start
    d.add_term_code(t)
    ChangeTracker.commit
    # r = RTM::RefID.where(:value => 'MDC_DEV_SPEC_PROFILE_GENERIC').first
    # d = r.definitions.first
    # puts d.term_codes.inspect
    RTM.withdraw_code(d, t, 'The code is changed to 4169 due to conflict with an existing code.  Term code was already changed in RTMMSv1.  It was (re)added to MDC_DEV_SPEC_PROFILE_GENERIC during parsing of 10101R and then marked as withdrawn.')
    table = @tables.find {|t| t[:counter] == 164}
    table[:extracted] = true
  end
  
  def withdraw211
    proc211 = proc do |cells, table|
      note, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {:status => :withdraw_code, table:table, refid:refid, part_code:part_code, note:note, description:note}
      term = process_term(args)
    end
    _parse_tables('Table A.14.4.1—Withdrawn nomenclature for error return codes', [211], proc211)
  end
    
end

