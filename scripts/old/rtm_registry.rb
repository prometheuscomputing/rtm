module RTM
  def self.registry(entry, partition_number = nil)
    return @registry[entry] if partition_number.nil?
    @partition_registry[partition_number] &.[](entry.to_i)
  end
  def self.setup_registry
    t = Time.now
    @registry = {}
    @partition_registry = {}
    all_defs = RTM::Definition.all
    puts "There are #{all_defs.count} definitions."
    all_defs.each do |d|
      tcs = d.term_codes
      refids = d.refids
      partition_numbers = tcs.map(&:partition).map(&:number).uniq
      if tcs.count > 0 && partition_numbers.count != 1
        puts partition_numbers.inspect
        puts d.term_codes.map(&:code).inspect
        puts d.refids.map(&:value).inspect
        puts d.inspect
        puts
        raise if partition_numbers.count > 1
      end
      ptn = partition_numbers.first&.to_i
      @partition_registry[ptn] ||= {}
      entry = { :refids => refids.map(&:value), :codes => d.term_codes.map(&:code), :definition => d }
      tcs.each do |tc|
        if @registry[tc.cfcode]
          @registry[tc.cfcode] << entry
        else
          @registry[tc.cfcode] = [entry]
        end
        if @partition_registry[ptn][tc.code]
          @partition_registry[ptn][tc.code] << entry
        else
          @partition_registry[ptn][tc.code] = [entry]
        end
      end
      refids.each do |refid|
        if @registry[refid.value.strip]
          @registry[refid.value.strip] << entry
        else
          @registry[refid.value.strip] = [entry]
        end
        if @partition_registry[ptn][refid.value.strip]
          @partition_registry[ptn][refid.value.strip] << entry
        else
          @partition_registry[ptn][refid.value.strip] = [entry]
        end
      end
    end
    puts "Registry setup took #{Time.now - t} seconds."
  end
end
