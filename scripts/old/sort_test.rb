x = ['Withdrawn', 'Published C', 'Deprecated', 'Preferred', 'Published', 'Published B', 'Unknown']






class Foo
  attr_accessor :bar
  def sort_val
    return 999 if bar == 'Withdrawn'
    return 888 if bar == 'Deprecated'
    return -999 if bar == 'Preferred'
    return -888 if bar == 'Published'
    return -777 if bar == 'Published B'
    return -666 if bar == 'Published C'
    return 0
  end
end

xx = x.map { |word| f = Foo.new; f.bar = word; f }


puts xx.sort_by { |i| i.sort_val }.map(&:bar)
  

# puts x.sort do |i, j|
#   puts "Famous"
#   if i == 'Withdrawn'
#     puts "#{i} 999"
#     999
#   else
#     if i =~ /Published/
#       if i.reverse == 'Preferred'.reverse
#         puts "#{i} -999"
#         -999
#       elsif i == 'Published B'
#         puts "#{i} -100"
#         -100
#       else
#         puts "#{i} 0"
#         0
#       end
#     elsif i == 'Deprecated'
#       puts "#{i} 100"
#       100
#     else
#       puts "#{i} 10"
#       10
#     end
#   end
# end
#
#
#