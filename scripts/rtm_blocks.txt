0 -- #<RTM::RefID:0x00007f9bf4605328> -- Unspecified
  UNKNOWN: 0 - 65536
1 -- #<RTM::RefID:0x00007f9bf40af518> -- Object-oriented
  MOC/BASE: 1 - 65536
  AL-STAT: 1281 - 65536
  PMS: 1537 - 65536
  ATTR/GROUP: 2049 - 65536
  ATTRs: 2305 - 65536
  ACT: 3073 - 65536
  NOTI: 3328 - 65536
  MD-Gen: 4096 - 65536
2 -- #<RTM::RefID:0x00007f9c6bdfbee8> -- Supervisory control and data acquisition (SCADA)
  ECG-LEADS: 0 - 255
  ECG-MEAS: 256 - 16383
  ECG-PATT: 16384 - 18431
  HEMO: 18432 - 20479
  RESP/VENT: 20480 - 22527
  NEURO: 22528 - 26623
  FLUID DELIV: 26624 - 26687
  BLD CHEM: 28676 - 53215
  Ventilation modes: 53280 - 57347
  Misc-msmt: 57348 - 61439
  Neurological patterns: 23560 - 26623
  Pump data: 26688 - 28675
  Infant incubator/warmer: 53216 - 53279
  Substance/type: 53396 - 53503
  Neurological stimulation: 53504 - 57347
  Private: 61440 - 65535
3 -- #<RTM::RefID:0x00007f9c6f0b4010> -- Events
  EVENTS/TECH: 0 - 65536
  EVENTS/MEDICAL: 3072 - 65536
  EVENTS/STATUS: 6144 - 65536
  EVENTS/ADVISORY: 6658 - 65536
4 -- #<RTM::RefID:0x00007f9c6bcb1b78> -- Dimensions (units of measurement)
  UNITS/BASE: 0 - 65536
  UNITS/APPL: 512 - 65536
5 -- #<RTM::RefID:0x00007f9c6bcc8648> -- Virtual attributes
  Default Block for Partition 5: 0 - 65536
6 -- #<RTM::RefID:0x00007f9c6bc75b78> -- Parameter groups
  Parameter groups: 513 - 65536
7 -- #<RTM::RefID:0x00007f9c6bd23430> -- [Body] Sites
  BODY SITES: 4 - 65536
  BODY SITES: 8193 - 65536
8 -- #<RTM::RefID:0x00007f9bf352e400> -- Infrastructure
  PROFsupp: 1 - 65536
  SYSspec component: 257 - 65536
  DIF: 513 - 65536
  MIBelem: 1025 - 65536
  MIBdata: 2049 - 65536
  DEVspec: 4097 - 65536
  OPT package id: 8194 - 65536
9 -- #<RTM::RefID:0x00007f9bf42fbf88> -- File Exchange Format
  Default Block for Partition 9: 0 - 65536
10 -- #<RTM::RefID:0x00007f9bf4308df0> -- ECG Extensions
  aECG: 0 - 65536
11 -- #<RTM::RefID:0x00007f9bf4282700> -- IDCO Extensions
  IDC Nomenclature: 0 - 65536
128 -- #<RTM::RefID:0x00007f9bf4d23f38> -- Personal health devices disease management
  PHD-DM Nomenclature: 0 - 65536
129 -- #<RTM::RefID:0x00007f9c6bfd1290> -- Personal health devices health and fitness
  PHD-HF Nomenclature: 0 - 65536
130 -- #<RTM::RefID:0x00007f9c6bfee458> -- Personal health devices aging independently
  PHD-AI Nomenclature: 0 - 65536
255 -- #<RTM::RefID:0x00007f9bf4d4fd18> -- Return codes
  Default Block for Partition 255: 0 - 65536
256 -- #<RTM::RefID:0x00007f9bf4d618b0> -- External nomenclature references
  External nomenclatures: 0 - 65536
258 -- #<RTM::RefID:0x00007f9bf4d872e0> -- _SETTING terms for SCADA
  SCADA _SETTING: 0 - 65536
1024 -- #<RTM::RefID:0x00007f9bf4da0da8> -- Private
  Default Block for Partition 1024: 0 - 65536