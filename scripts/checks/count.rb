dnum = RTM::Definition.count
unum = RTM::Unit.count
tnum = RTM::Token.count
total = dnum + unum + tnum
puts "There are #{total} terms"

pdnum = RTM::Definition.where(status_id:6).count
punum = RTM::Unit.where(status_id:6).count
ptnum = RTM::Token.where(status_id:6).count
ptotal = pdnum + punum + ptnum
puts "There are #{ptotal} published terms"

aednum = RTM::Definition.where(auto_expanded:true).count
aeunum = RTM::Unit.where(auto_expanded:true).count
aetnum = RTM::Token.where(auto_expanded:true).count
aetotal = aednum + aeunum + aetnum
puts "There are #{aetotal} auto-expanded terms"

rvals = refids.map(&:value)
non_mdc = rvals.reject { |v| v =~ /MDCX?_([A-Z]+([a-z0-9]+)?_)*[A-Z]+([a-z0-9]+)?/ }