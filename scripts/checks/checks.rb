module RTM
  module_function
  def check_definitions
    @double_synonym_defs = nil; @refid_synonym_defs = nil; @code_synonym_defs = nil; @non_ecg = nil; @unmarked_refids = nil
    @definitions ||= (RTM::Definition.all + RTM::Unit.all)
    @double_synonym_defs ||= []
    @refid_synonym_defs  ||= []
    @code_synonym_defs   ||= []
    if @refid_synonym_defs.empty?
      @definitions.each do |defn|
        if defn.refids_count > 1 && defn.term_codes_count > 1
          multiple_term_codes = RTM.get_code_objs(defn.part_codes.first).count > 1
          report = [defn.id, defn.refids_status, defn.part_codes_status]
          report << "MULTIPLE TERM CODES for #{defn.part_codes.first}!" if multiple_term_codes
          @double_synonym_defs << report
        elsif defn.refids_count > 1
          @refid_synonym_defs << [defn.id, defn.refids_status, defn.part_codes_status]
        elsif defn.term_codes_count > 1
          @code_synonym_defs << [defn.id, defn.refids_status, defn.part_codes_status]
        end
      end
    end
    puts Rainbow("There are #{@refid_synonym_defs.count} definitions with RefID synonyms in the system.").cyan
    @non_ecg ||= @refid_synonym_defs.reject {|s| s[1].first =~ /MDC_ECG_LEAD/ }
    @unmarked_refids ||= @non_ecg.reject {|s| s[1].join =~ /Deprecated|Withdrawn|Preferred/ }
    @non_ecg.each { |s| puts s.inspect }
    # @unmarked_refids.each { |s| puts s.inspect }
    
    puts Rainbow("There are #{@code_synonym_defs.count} definitions with TermCode synonyms in the system.").cyan
    @code_synonym_defs.each {|s| puts s.inspect}
    puts Rainbow("There are #{@double_synonym_defs.count} definitions in the system that have both TermCode synonyms and RefID synonyms.").cyan
    @double_synonym_defs.each {|s| puts s.inspect}
  end
  def check_term_stats
    hash = {}
    terms = RTM::Term.all
    terms.each do |t|
      next if t.is_a?(RTM::Token)
      key = "#{t.refid&.value} #{t.term_code&.part_code}"
      hash[key] ||= []
      hash[key] << t
    end
    extra = hash.select { |k,v| v.count > 1 }
    puts extra.count
    extra
  end
  
  def check_refid_homonyms
    refid_homonyms = []
    RTM::RefID.each do |refid|
      if refid.definitions_count > 1
        # refid_homonyms << refid.definitions.collect { |defn| [defn.part_codes_status, defn.inspect, defn.descriptions.map(&:value_content) ]}
        refid_homonyms << refid.definitions.collect do |defn|
          fudge = defn.term_codes.collect{|tc| tc.discriminators.map(&:offset)}
          [defn.status&.value, defn.refids_status, defn.part_codes_status, defn.discriminator_sets.map(&:name), fudge, defn.descriptions.map(&:value_content)]
        end
      end
    end
    puts Rainbow("There are #{refid_homonyms.count} RefID homonyms in the system.").cyan
    refid_homonyms.each { |s| pp s;puts }
  end
  
  def check_code_homonyms
    code_homonyms = []
    RTM::TermCode.each do |c|
      if c.definitions_count > 1
        code_homonyms << c.definitions.collect { |defn| [defn.refids_status, defn.part_codes_status, defn.discriminator_sets.map(&:name), defn.descriptions.map(&:value_content)] }
      end
    end
    puts Rainbow("There are #{code_homonyms.count} TermCode homonyms in the system.").cyan
    code_homonyms.each { |s| pp s;puts }
  end
  
  def check_code_duplicates
    # Check that there is only one instance of each code in each partition.
    codes = DB[:rtm_term_codes].all
    store = {}
    codes.each do |c|
      store[c[:partition_id]] ||= []
      store[c[:partition_id]] << c[:code] 
    end
    good = true
    store.each do |ptn, codes|
      dupes = codes.select{|c| codes.count(c) > 1}.uniq
      if dupes.any?
        good = false
        puts "Partition #{RTM::Partition[ptn].number} had multiple instances of the following codes: #{dupes.sort}"
      end
    end
    puts "All TermCodes are unique." if good
  end
  
  def check_refid_duplicates
    # Check that there is only one instance of each refid
    refids = DB[:rtm_ref_ids].all.collect {|r| r[:value]};
    dupes = [];
    refids.each{|r| (dupes << r) if (refids.count(r) > 1)};
    if dupes.any?
      puts "Multiple instances of the following RefIDs: #{dupes.sort}"
    else
      puts "All RefIDs are unique."
    end
  end
  
  def checkuom
    noxterms = RTM::DiscriminatorSet.where(name:'UoM').first.discriminators.select { |d| d.offset == 0 }.map(&:terms).flatten.uniq.reject { |t| t.refid.value =~ /_X_/ }.select { |t| t.status&.value == 'Published' }
    noxterms.each { |x| x.stat }
    puts noxterms.count
  end
  
  def check_extra_discriminators
    extra = []
    RTM::Term.all.each do |t|
      extra << t._stat + ' ' + t.discriminators.map(&:representation).join(', ') if t.discriminators_count > 1 && t._stat !~ /LEAD\(/
    end
    extra.each { |e| puts e }
    extra
  end
  
  def check_primary_refids(terms, include_expanded = false)
    multi_primaries = {}
    terms = terms.reject { |t| t.auto_expanded == true } unless include_expanded
    puts "There are #{terms.count}"
    terms.each do |term|
      pr = term.preferred_refids.sort.reject { |r| r =~ /MDC_ECG_LEAD_CONFIG_/ }
      if pr.count > 1
        next if pr.count == 2 && pr.map { |r| r.sub('_X_', '_') }.uniq.count == 1
        multi_primaries[pr] ||= []
        multi_primaries[pr] << "#{term.auto_expanded ? '*' : ''}#{term._stat}"
      end
    end
    multi_primaries.each {|k,v| puts k; v.each {|x| puts x}; puts};
    puts "There are #{multi_primaries.count} instances of multi_primaries."
    multi_primaries
  end
  
end


