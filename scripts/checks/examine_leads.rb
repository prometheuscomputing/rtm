lset  = RTM::DiscriminatorSet.where(name:"LEAD").first
l1set = RTM::DiscriminatorSet.where(name:"LEAD1").first
l2set = RTM::DiscriminatorSet.where(name:"LEAD2").first
dd  = lset.discriminators
dd1 = l1set.discriminators
dd2 = l2set.discriminators

diff12 = {}
diff1  = {}
diff2  = {}
(1..255).each do |i|
  d  = dd.find  {|x| x.offset == i}&.value
  d1 = dd1.find {|x| x.offset == i}&.value
  d2 = dd2.find {|x| x.offset == i}&.value
  
  diff12[i] = [d1, d2] unless d1 == d2
  diff1[i]  = [d, d1]  unless d == d1
  diff2[i]  = [d, d2]  unless d == d2
end
puts "Differences between LEAD1 and LEAD2"
pp diff12
puts
puts "Differences between LEAD and LEAD1"
pp diff1
puts
puts "Differences between LEAD and LEAD2"
pp diff2
puts
  

