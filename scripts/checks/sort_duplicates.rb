def sortem(refids)
  refids.each do |refid|
    next if refid.terms_count < 2
    refid.terms.each do |term|
      published, unpublished = refid.terms.partition { |t| t.status&.value =~ /Published/ }
      if published.count > 1
        pcodes = published.map(&:term_code)
        if pcodes.count == pcodes.uniq.count
          next if refid.value =~ /MDC_ECG/
          puts Rainbow("TermCode synonyms").bg(:green)
          published.each do |term|
            puts term.summary(false)
          end
        else
          # They had the same refid and same term_code
          puts Rainbow("Same RefID and TermCode for published terms").bg(:orange)
          published.each do |term|
            puts term.summary(false)
          end
        end
        puts '*'
      end
    end
  end
end
