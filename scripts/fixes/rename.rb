# require 'fileutils'
# def rename_expanded(folder_path)
    folder_path = File.expand_path "~/projects/rtm/csv"
    Dir[File.expand_path "#{folder_path}/*"].each do |f|
    # Dir.glob(folder_path + "*").sort.each do |f|
      filename = File.basename(f, File.extname(f))
      new_name = File.join(folder_path, filename + '_expanded' + File.extname(f))
      # puts new_name
      File.rename(f, new_name)
    end
  