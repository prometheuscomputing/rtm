RTM::Note.all.each do |note|
  ChangeTracker.start
  if note.value_content =~ /RefID status note:/
    note.value_content = note.value_content.sub('RefID status note:', 'Status note:')
    note.save
  end
  ChangeTracker.commit
end
nil