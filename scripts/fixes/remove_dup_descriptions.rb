# r = RTM.find_refid 'MDC_ECG_ELEC_POTL'
# dfns = r.terms.map(&:definition)
def all_defs
  RTM::Definition.all
end

def get_paragraph_dupes(dfns)
  dupes = []
  dfns.each do |dfn|
    descs = dfn.descriptions
    next if descs.count < 2
    dupes << dfn if descs.map(&:value_content).uniq.count < descs.count
  end
  puts dupes.count
  dupes
end

def kill_paragraph_dupes(dfns)
  dfns.each do |dfn|
    descs = dfn.descriptions
    next if descs.count < 2
    descs_content = descs.map { |d| [d, d.value_content] }
    para, nopara = descs_content.partition { |d,vc| vc =~ /^<p>.*<\/p>$/ }
    # puts "Para"
    # pp para.map(&:last)
    # puts "NoPara"
    # pp nopara.map(&:last)
    para_contents = para.map(&:last)
    nopara_contents.each do |d,vc|
      ChangeTracker.start
      with_para = '<p>' + vc + '</p>'
      if para_contents.include?(with_para)
        puts "Destroying #{vc}"
        d.destroy
      else
        puts Rainbow(with_para).orange if with_para =~ /<p>/
        puts "Changing to: #{with_para}"
        d.value = find_or_create_description(with_para, true)
        d.save
      end
      ChangeTracker.start
    end
  end
end
# kill_paragraph_dupes dfns
nil
