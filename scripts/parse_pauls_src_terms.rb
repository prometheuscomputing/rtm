load File.join(__dir__, '../lib/rtm/model_extensions.rb')

csv = CSV.read(File.expand_path(File.join(__dir__, '../data/New10101b.1e.x1a.Group1.2020-03-12T20.csv')))
csv.shift
codes  = []
refids = []
csv.each_with_index do |row, i|
  refid = row[0].strip
  puts Rainbow("#{i+2} #{refid}").cyan
  disc_name = row[1].strip
  ptn  = row[2].strip.to_i
  code = row[3].strip.to_i
  ptn_code = "#{ptn}::#{code}"
  next if refids.flatten.include?(refid) && codes.flatten.include?(ptn_code)
  vals = RTM.base_term_expansion_values(refid, ptn_code, disc_name)
  refids << vals.first
  codes  << vals.last
  break if refid == 'MDC_SAT_O2_REGIONAL_AREA_UNDER_CURVE_CUMULATIVE'
end
refids.flatten!
codes.flatten!
refid_dupes = refids.select { |e| refids.count(e) > 1 }.uniq
code_dupes  = codes.select  { |e| codes.count(e)  > 1 }.uniq
if refid_dupes.any?
  puts "The following refids appeared multiple times:"
  refid_dupes.each {|rd| puts "  #{rd}"}
end
if code_dupes.any?
  puts "The following codes appeared multiple times:"
  code_dupes.each {|cd| puts "  #{cd}"}
end
problem_refids = []
problem_codes  = []
refids.each do |r|
  problem_refids << r unless RTM.new_refid_is_unique?(r)
end
codes.each do |c|
  problem_codes << c unless RTM.new_code_is_unique?(c)
end
if problem_refids.any?
  puts "The following RefIDs already existed: #{problem_refids}"
end
if problem_codes.any?
  puts "The following codes already existed: #{problem_codes}"
end
