require 'sequel'
require 'sqlite3'
require 'db_io'

# source_db = File.expand_path("~/Prometheus/data/rtm_generated/bSQL.db")
source_db = File.expand_path("~/Prometheus/data/rtm_generated/bSQL.db")
dest_db   = File.expand_path("~/Prometheus/data/rtm_generated/SQL.db")
$source   = Sequel.sqlite(source_db)
$target   = Sequel.sqlite(dest_db)
tt = $target.tables;nil
st = $source.tables;nil
puts "source has #{$source.tables.count} tables"
puts "target has #{$target.tables.count} tables"
puts "While ignoring _deleted tables..."
puts "source tables not present in target tables:\n  #{(st - tt).select{|t| t.to_s !~ /_deleted$/}.sort.inspect}"
puts "target tables not present in source tables:\n  #{(tt - st).select{|t| t.to_s !~ /_deleted$/}.sort.inspect}"

# load File.expand_path("~/projects/db_io/lib/db_io/sync.rb")

# klasses = [DIM, Nomenclature, MyDevice, Gui_Builder_Profile, Gui_Builder_Profile, IEEE20101, MetaInfo].collect{|m| m.classes(:ignore_class_namespaces => true)}.flatten.uniq

options = {:sync_type => 'overwrite', :verbose => true}

# old_name => new_name
# options[:renamed_tables] = {
#   :abstract_clause_ieee_standard_contents => :clause_content_ieee_standard_contents
# }


# v1 = "6.7.1"
# v2 = "6.7.4"
# class2table_map = YAML::load(File.open(File.expand_path("~/projects/dim_reference/table_mapping_#{v1}_to_#{v2}.yaml")))
# old2new_table_map = {}
# class2table_map.each{|k,v|
#   if (v.first != v.last) && st.include?(v.first) && tt.include?(v.last)
#     old2new_table_map[v.first] = v.last
#   end
# }
# puts "\n\nchanged table map"
# pp old2new_table_map
# options[:renamed_tables] = old2new_table_map

# klasses = [DIM, Nomenclature, MyDevice, Gui_Builder_Profile, Gui_Builder_Profile, IEEE20101].collect{|m| m.classes(:ignore_class_namespaces => true)}.flatten.uniq
# tables = klasses.collect{|c| c.table_name if c.respond_to? :table_name}.compact
# tables = tables + options[:renamed_tables].keys
# options[:tables] = tables


puts "press <Enter>"; gets
#  ****************************************************************************
$target.sync($source, options)
#  ****************************************************************************



# $source[:enumeration_enumeration_item_allowed_enumerations].each do |row|
#   $target[:enumeration_item_md_enumeration_allowed_fors].insert({
#     :allowed_enumeration_id => row[:allowed_enumeration_id],
#     :allowed_enumeration_class => row[:allowed_enumeration_class],
#     :allowed_for_id => row[:allowed_for_id],
#     :allowed_for_class => 'DIM::Medical::Enumeration',
#     :updated_at => row[:updated_at],
#     :created_at => row[:created_at]
#   })
# end
#
# $source[:numeric_unit_item_allowed_units].each do |row|
#   $target[:md_numeric_unit_item_allowed_units].insert({
#     :allowed_unit_id => row[:allowed_unit_id],
#     :allowed_unit_class => row[:allowed_unit_class],
#     :allowed_for_id => row[:allowed_for_id],
#     :allowed_for_class => 'DIM::Medical::Numeric',
#     :updated_at => row[:updated_at],
#     :created_at => row[:created_at]
#   })
# end
#
# $source[:sample_array_unit_item_allowed_units].each do |row|
#   $target[:md_numeric_unit_item_allowed_units].insert({
#     :allowed_unit_id => row[:allowed_unit_id],
#     :allowed_unit_class => row[:allowed_unit_class],
#     :allowed_for_id => row[:allowed_for_sa_id],
#     :allowed_for_class => row[:allowed_for_sa_class],
#     :updated_at => row[:updated_at],
#     :created_at => row[:created_at]
#   })
# end
