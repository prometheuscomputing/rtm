module RTM
  module_function
  def all_terms(include_tokens = false)
    if include_tokens
      Term.all
    else
      Term.all.reject { |t| t.is_a?(Token) }
    end
  end
  
  def deduce_refid_synonyms(codes = nil)
    synonyms = _deduce_refid_synonyms(codes = nil)
    categorize_synonyms(synonyms)
    puts "There are #{synonyms.count} Codes with multiple terms (multiple RefIDs)."
    synonyms
  end
  
  def _deduce_refid_synonyms(codes = nil)
    codes ||= RTM::TermCode.all
    synonyms = codes.select { |c| c.terms_count > 1 }
    synonyms.reject! { |c| c.terms.map(&:refid).map(&:value).map { |v| v.sub('_X_', '_') }.uniq.count == 1 }
    synonyms.reject! { |c| c.terms.first.discriminators.any? { |d| d.offset > 65 } }
    synonyms
  end
  
  def deduce_code_synonyms(refids = nil)
    synonyms = _deduce_code_synonyms(refids)
    categorize_synonyms(synonyms, "TermCode")
    puts "There are #{synonyms.count} RefIDs with multiple terms (multiple TermCodes)."
    synonyms
  end
  
  def _deduce_code_synonyms(refids = nil)
    refids ||= RTM::RefID.all
    synonyms = refids.select { |r| r.terms_count > 1 }
    # remove LEAD terms
    synonyms.reject! do |r|
      reject = r.terms.all? { |t| t.discriminator_set_names.all? { |n| n =~ /LEAD/ } }
      next unless reject
      reject = reject && r.terms.count == 2
      next unless reject
      t1, t2 = r.terms
      reject = reject && (t1.discriminator_set_names + t2.discriminator_set_names).sort == ['LEAD1', 'LEAD2']
    end
    synonyms
  end
  
  def categorize_synonyms(syns, type = "RefID")
    withdrawn = []
    deprecated = []
    rtmv1 = []
    others = []
    syns.each do |syn|
      tt = syn.terms
      next if tt.reject { |t| t.term_code.nil? }.count < 2
      categorized = false
      if tt.any? { |t| t.status&.value == 'Withdrawn' }
        withdrawn  << syn
        categorized = true
      end
      if tt.any? { |t| t.status&.value == 'Deprecated' }
        deprecated  << syn
        categorized = true
      end
      if tt.any? { |t| t.status&.value =~ /RTMMSv1/ }
        rtmv1 << syn
        categorized = true
      end
      others << syn unless categorized
    end
    puts Rainbow("Withdrawn #{type} Synonyms:").orange
    withdrawn.each { |st| st.terms.each { |stt| puts "  " + stt._stat }; puts }
    puts Rainbow("Deprecated #{type} Synonyms:").orange
    deprecated.each { |st| st.terms.each { |stt| puts "  " + stt._stat }; puts }
    puts Rainbow("#{type} Synonyms from RTMMSv1:").orange
    rtmv1.each { |st| st.terms.each { |stt| puts "  " + stt._stat }; puts }
    puts Rainbow("#{type} Synonyms:").orange
    others.each { |st| st.terms.each { |stt| puts "  " + stt._stat }; puts }
  end
  
  def create_synonym_set_id_text(syn)
    terms  = syn.terms
    codes  = terms.map(&:term_code).compact.map(&:part_code).uniq
    refids = terms.map(&:refid).compact.map(&:value).uniq
    if codes.count == 1 && refids.count > 1
      return codes
    end
    if codes.count > 1 && refids.count == 1
      return refids
    end
    if codes.count > 1 && refids.count > 1
      return codes.sort + refids.sort
    end
  end
  
  def build_synonym_sets
    existing_sets = SynonymSet.all.map(&:id_text)
    rs = _deduce_refid_synonyms
    cs = _deduce_code_synonyms
    rs.each do |syn|
      id_text = create_synonym_set_id_text(syn)
      if existing_sets.include?(id_text) # skip if this set already exists. TODO maybe we should be checking primary keys for term identity rather than just refid values and part_codes??
        puts "#{id_text} already exists."
        next
      end
      terms = syn.terms
      next if terms.reject { |term| term.term_code.nil? }.count < 2
      ChangeTracker.start
      set = RTM::SynonymSet.new
      set.terms = terms
      set.save
      ChangeTracker.commit
    end
    cs.each do |syn|
      id_text = create_synonym_set_id_text(syn)
      if existing_sets.include?(id_text) # skip if this set already exists. TODO maybe we should be checking primary keys for term identity rather than just refid values and part_codes??
        puts "#{id_text} already exists."
        next
      end
      terms = syn.terms
      next if terms.reject { |term| term.term_code.nil? }.count < 2
      ChangeTracker.start
      if terms.first.refid.value =~ /MDC_HEAD_EAR_/
        set = RTM::HomonymSet.new
        set.term_code_homonym = true
      else
        set = RTM::SynonymSet.new
        set.term_code_synonym = true
      end
      set.terms = terms
      set.save
      ChangeTracker.commit
    end
  end
end
