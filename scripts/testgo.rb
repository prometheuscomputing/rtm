load File.join(__dir__, '../migrate/migrate.rb')
load File.join(__dir__, '../migrate/extract10101R.rb')
load File.join(__dir__, '../migrate/term_extractor.rb')
load File.join(__dir__, 'create_admin.rb')
load File.join(__dir__, 'help.rb')
load File.join(__dir__, 'rtm_tools.rb')
load File.join(__dir__, 'build_synonym_sets.rb')
t = Time.now
RTM.test_v1
# RTM.testgo
#
# terms = RTM::Term.all
# load File.join(__dir__, 'dump.rb')
# parts = dump(terms)
# dump_to_csv(parts)
# parts_ex = dump(terms, true)
# dump_to_csv(parts_ex, true)
# dump_to_views(parts_ex)
puts "That took #{(Time.now - t)/60} minutes."
