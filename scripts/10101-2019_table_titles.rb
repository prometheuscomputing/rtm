module RTM
  module_function; def table_titles
  [
"A.2.4.1.1—Partition 1—Object-oriented elements, Device nomenclature",
"A.2.4.2.1—Partition 2—Metrics (multipage table)",
"A.2.4.3.1—Partition 3—Alerts and events",
"A.2.4.4.1—Partition 4—Units of measurement (dimensions)",
"A.2.4.6.1—Partition 6—Program group",
"A.2.4.7.1—Partition 7—Body sites",
"A.2.4.8.1—Partition 8—Communication infrastructure",
"A.2.4.9.1—Partition 9—Reserved for file exchange format (FEF)",
"A.2.4.10.1—Partition 10—ECG additional nomenclature (annotated ECG)",
"A.2.4.11.1—Partition 11—IDCO nomenclature",
"A.2.4.12.1—Partition 128—PHD disease management",
"A.2.4.13.1—Partition 129—PHD health and fitness",
"A.2.4.14.1—Partition 130—PHD aging independently",
"A.2.4.15.1—Partition 255—Return codes",
"A.2.4.16.1—Partition 256—External nomenclatures",
"A.2.4.17.1—Partition 258—Settings",
"A.2.4.18.1—Partition 514—Predicted values",
"A.2.4.19.1—Partition 1024—Private",
"A.3.2.1.1—Object-oriented modeling elements—General—object class items",
"A.3.2.1.2—Object-oriented modeling elements—General—attributes",
"A.3.2.1.3—Object-oriented modeling elements—General—attribute groups",
"A.3.2.1.4—Object-oriented modeling elements—General—behavior",
"A.3.2.1.5—Object-oriented modeling elements—General—notifications",
"A.3.2.2.1—Object-oriented modeling elements—Medical Package—object class items",
"A.3.2.2.2—Object-oriented modeling elements—Medical Package—attributes (multipage table)",
"A.3.2.2.3—Object-oriented modeling elements—Medical Package—attribute groups",
"A.3.2.2.4—Object-oriented modeling elements—Medical Package—behavior",
"A.3.2.2.5—Object-oriented modeling elements—Medical Package—notifications",
"A.3.2.3.1—Object-oriented modeling elements—Alert Package—object class items",
"A.3.2.3.2—Object-oriented modeling elements—Alert Package—attributes",
"A.3.2.3.3—Object-oriented modeling elements—Alert Package—attribute groups",
"A.3.2.3.4—Object-oriented modeling elements—Alert Package—behavior",
"A.3.2.3.5—Object-oriented modeling elements—Alert Package—notifications",
"A.3.2.4.1—Object-oriented modeling elements—System Package—object class items",
"A.3.2.4.2—Object-oriented modeling elements—System Package—attributes (multipage table)",
"A.3.2.4.3—Object-oriented modeling elements—System Package—attribute groups",
"A.3.2.4.4—Object-oriented modeling elements—System Package—behavior",
"A.3.2.4.5—Object-oriented modeling elements—System Package—notifications",
"A.3.2.5.1—Object-oriented modeling elements—Control Package—object class items",
"A.3.2.5.2—Object-oriented modeling elements—Control Package—attributes (multipage table)",
"A.3.2.5.3—Object-oriented modeling elements—Control Package—attribute groups",
"A.3.2.5.4—Object-oriented modeling elements—Control Package—behavior",
"A.3.2.5.5—Object-oriented modeling elements—Control Package—notifications",
"A.3.2.6.1—Object-oriented modeling elements—Extended Services Package—object class  items",
"A.3.2.6.2—Object-oriented modeling elements—Extended Services Package—attributes",
"A.3.2.6.3—Object-oriented modeling elements— Extended Services Package—attribute groups",
"A.3.2.6.4—Object-oriented modeling elements—Extended Services Package—behavior",
"A.3.2.6.5—Object-oriented modeling elements— Extended Services Package—notifications",
"A.3.2.7.1—Object-oriented modeling elements—Communication Package— object class items",
"A.3.2.7.2—Object-oriented modeling elements—Communication Package—attributes (multipage table)",
"A.3.2.7.3—Object-oriented modeling elements— Communication Package—attribute groups",
"A.3.2.7.4—Object-oriented modeling elements—Communication Package—behavior",
"A.3.2.7.5—Object-oriented modeling elements—Communication Package—notifications",
"A.3.2.8.1—Object-oriented modeling elements—Archival Package—object class items",
"A.3.2.8.2—Object-oriented modeling elements—Archival Package—attributes  (multipage table)",
"A.3.2.8.3—Object-oriented modeling elements—Archival Package—attribute groups",
"A.3.2.8.4—Object-oriented modeling elements—Archival Package—behavior",
"A.3.2.8.5—Object-oriented modeling elements—Archival Package—notifications",
"A.3.2.9.1—Object-oriented modeling elements—Patient Package—object class items",
"A.3.2.9.2—Object-oriented modeling elements—Patient Package—attributes (multipage table)",
"A.3.2.9.3—Object-oriented modeling elements—Patient Package—attribute groups",
"A.3.2.9.4—Object-oriented modeling elements—Patient Package—behavior",
"A.3.2.9.5—Object-oriented modeling elements—Patient Package—notifications",
"A.3.2.10.1—Object-oriented modeling elements—PHD—object class items",
"A.3.2.10.2—Object-oriented modeling elements—PHD—attributes (multipage table)",
"A.3.2.10.3—Object-oriented modeling elements—PHD—attribute groups",
"A.3.2.10.4—Object-oriented modeling elements—PHD-behavior",
"A.3.2.10.5—Object-oriented modeling elements—PHD—notifications",
"A.3.2.11.1—Object-oriented modeling elements—WCM—object class items",
"A.3.2.11.2—Object-oriented modeling elements—WCM—attributes (multipage table)",
"A.3.2.11.3—Object-oriented modeling elements—WCM—attribute groups",
"A.3.2.11.4—Object-oriented modeling elements—WCM—behavior",
"A.3.2.11.5—Object-oriented modeling elements—WCM—notifications",
"A.3.2.12.1—Object-oriented modeling elements—MEM-LS—object class items",
"A.3.2.12.2—Object-oriented modeling elements—MEM-LS—attributes (multipage table)",
"A.3.2.12.3—Object-oriented modeling elements—MEM-LS—attribute groups",
"A.3.2.12.4—Object-oriented modeling elements—MEM-LS—behavior",
"A.3.2.12.5—Object-oriented modeling elements—MEM-LS—notifications",
"A.3.2.13.1—Object-oriented modeling elements—USI—object class items",
"A.3.2.13.2—Object-oriented modeling elements—USI—attributes",
"A.3.2.13.3—Object-oriented modeling elements—USI—attribute groups",
"A.3.2.13.4—Object-oriented modeling elements—USI—behavior",
"A.3.2.13.5—Object-oriented modeling elements—USI—notifications",
"A.3.2.14.1—Object-oriented modeling elements—Deprecated identifier terms",
"A.4.1—Communication infrastructure—object class items",
"A.4.2—Communication infrastructure—attributes (multipage table)",
"A.4.3—Communication infrastructure—attribute groups",
"A.4.4—Communication infrastructure—behavior",
"A.4.5—Communication infrastructure—notifications",
"A.4.6—Communication infrastructure—profile support attributes",
"A.4.7—Communication infrastructure—optional package identifiers",
"A.4.8—Communication infrastructure—system specification components",
"A.5.1—Nomenclature and codes for vital signs devices (multipage table)",
"A.6.2.1—Table of decimal factors",
"A.6.3.1—Non-SI units (multipage table)",
"A.6.4.1—Vital signs units of measurement (multipage table)",
"A.6.5.1—Withdrawn terms for vital signs units of measurement",
"A.6.6.1—Deprecated terms for vital signs units of measurement",
"A.6.7.1—Deprecated RefIds for vital signs units of measurement",
"A.7.1.3.1—List of standardized ECG <lead> descriptors from SCP‑ECG (multipage table)",
"A.7.1.3.2—Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10101-2004 with discriminator group [LEAD1] (multipage table)",
"A.7.1.3.3—Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10102  with discriminator group [LEAD2] (multipage table)",
"A.7.1.6.1—Nomenclature and codes for global lead ECG measurements (multipage table)",
"A.7.1.6.2—Nomenclature and codes for ECG measurements with lead origin (multipage table)",
"A.7.1.7.1—Deprecated terms for ECG measurements",
"A.7.1.8.1—Deprecated RefIds for ECG measurements defined in IEEE11073-10102",
"A.7.2.6.1—Nomenclature and codes for ECG enumerations (multipage table)",
"A.7.2.7.1—Withdrawn terms for ECG enumerations",
"A.7.2.8.1—Deprecated terms for ECG enumerations",
"A.7.2.9.1—Deprecated RefIds for ECG enumerations",
"A.7.3.6.1—Nomenclature and codes for haemodynamic monitoring measurements  (multipage table)",
"A.7.3.7.1—Deprecated terms for haemodynamic monitoring measurements",
"A.7.4.11.1—Ventilator modes bit string (multipage table)",
"A.7.4.11.2—Ventilator modes nomenclature and codes (multipage table)",
"A.7.4.16.1—Correction of gas measurements",
"A.7.4.17.1—Gas measurement sites",
"A.7.4.17.2—Default gas measurement sites",
"A.7.4.18.1—Inspiratory breath type classifications",
"A.7.4.19.1—Deployment of gas partial pressure and concentration and consumption information (informative)",
"A.7.4.19.2—Nomenclature and codes for respiratory, ventilator, and anesthesia measurements (multipage table)",
"A.7.4.20.1—Deprecated terms for respiratory measurements",
"A.7.4.21.1—Deprecated RefIds for respiratory measurements",
"A.7.4.22.1—Recommended mapping of deprecated to ‘unified’ gas RefId prefixes  (informative)",
"A.7.4.23.1—Deprecated terms for respiratory measurements (multipage table)",
"A.7.4.24.1—Deprecated RefIds for respiratory measurements (multipage table)",
"A.7.4.25.1—Deprecated nomenclature for undefined respiratory measurements from Annex B",
"A.7.4.26.1—Nomenclature and codes for nebulizers (multipage table)",
"A.7.5.6.1—Nomenclature and codes for common blood-gas, blood, urine, and other fluid  chemistry measurements (multipage table)",
"A.7.5.7.1—Withdrawn terms for blood-gas, blood, urine, and other fluid chemistry  measurements",
"A.7.5.8.1—Deprecated RefIds for blood-gas, blood, urine, and other fluid chemistry  measurements",
"A.7.6.6.1—Nomenclature and codes for fluid-output measurements (multipage table)",
"A.7.7.6.1—Nomenclature and codes for pump data (multipage table)",
"A.7.7.7.1—Deprecated RefIds for pump data",
"A.7.7.13.1—Pump modes bit string values (value to be communicated in  enumeration observation element Mode | | Device | Pump)",
"A.7.7.19.1—Pump states bit string values (value to be communicated in  EnumerationObservation with code: Status | Operational | Device | Pump)",
"A.7.8.6.1—Nomenclature and codes for neurological monitoring measurements  (multipage table)",
"A.7.9.6.1—Nomenclature and codes for neurophysiologic enumerations (multipage table)",
"A.7.10.6.1—Nomenclature and codes for neurophysiologic stimulation modes (multipage table)",
"A.7.11.6.1—Nomenclature and codes for miscellaneous measurements",
"A.7.11.6.2—Nomenclature and codes for temperature (multipage table)",
"A.7.11.7.1—Deprecated nomenclature for temperature",
"A.7.11.8.1—Body weight and surface area for pre-coordinated RefIds",
"A.7.11.9.1—Nomenclature and codes for body mass (weight) and estimates",
"A.7.12.1.1—Nomenclature and code extensions for infant incubator and warmer microenvironments (multipage table)",
"A.7.13.6.1—Nomenclature and codes for spirometry measurements (multipage table)",
"A.7.14.1.1—Nomenclature and code extensions for personal health devices (multipage table)",
"A.7.14.2.1—Deprecated RefIds for personal health devices",
"A.8.2.5.1—Nomenclature and codes for sites for neurophysiological signal monitoring:  locations near peripheral nerves (multipage table)",
"A.8.3.5.1—Nomenclature and codes for sites for neurophysiological signal monitoring:  locations near or in muscles (multipage table)",
"A.8.4.6.1—Nomenclature and codes for electrode sites for EEG according to the international  10–20 system (multipage table)",
"A.8.5.6.1—Nomenclature and codes for sites for EOG signal monitoring (multipage table)",
"A.8.6.5.1—Nomenclature and codes for general neurological sites for monitoring measurements  and drainage",
"A.8.7.5.1—Nomenclature and codes for body sites for cardiovascular measurements  (multipage table)",
"A.8.8.5.1—Nomenclature and codes for miscellaneous body sites used in vital signs monitoring  and measurement (multipage table)",
"A.8.9.5.1—Nomenclature and codes for external sites used in vital signs monitoring and measurement",
"A.8.10.6.1—Nomenclature and codes for qualifiers of body site locations (multipage table)",
"A.9.2.5.1—Nomenclature and codes for pattern events (multipage table)",
"A.9.3.5.1—Nomenclature and codes for device-related and environment-related events (multipage table)",
"A.9.3.6.1—Deprecated terms for device-related and environment-related events",
"A.9.3.7.1—Deprecated RefIds for device-related and environment-related events",
"A.10.2.3.1—Nomenclature and codes for infrastructure, device specialization (multipage table)",
"A.10.2.4.1—Withdrawn terms for device specialization",
"A.10.3.3.1—Nomenclature and codes for Infastructure, sub-specialization (multipage table)",
"A.10.4.3.1—Time synchronization profiles",
"A.11.2.3.1—Nomenclature and codes for PHD Disease Management, general",
"A.11.3.3.1—Nomenclature and codes for PHD Disease Management, Basic ECG sensors",
"A.11.4.3.1—Nomenclature and codes for PHD Disease Management, Basic ECG event context",
"A.11.5.3.1—Nomenclature and codes for PHD Disease Management, SABTE sensors and settings (multipage table)",
"A.11.5.4.1—Withdrawn terms for PHD Disease Management, SABTE sensors and settings (multipage table)",
"A.11.6.3.1—Nomenclature and codes for PHD Disease Management, SABTE mode settings (multipage table)",
"A.11.7.3.1—Nomenclature and codes for PHD Disease Management, Glucose carbohydrate  source",
"A.11.8.3.1—Nomenclature and codes for PHD Disease Management, Glucose carbohydrate  source",
"A.11.9.3.1—Nomenclature and codes for PHD Disease Management, Glucose moncarbohydrate sources",
"A.11.10.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  insulin type",
"A.11.11.3.1—Nomenclature and codes for PHD Disease Management, insulin types",
"A.11.12.3.1—Nomenclature and codes for PHD Disease Management, general health",
"A.11.13.3.1—Nomenclature and codes for PHD Disease Management, general health",
"A.11.14.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  sample location",
"A.11.15.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  sample locations",
"A.11.16.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  meal",
"A.11.17.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  meal types",
"A.11.18.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  tester",
"A.11.19.3.1—Nomenclature and codes for PHD Disease Management, Glucose Monitoring,  tester types",
"A.11.20.3.1—Nomenclature and codes for PHD Disease Management, INR status and context",
"A.11.21.3.1—Nomenclature and codes for PHD Disease Management, CGM sensors (multipage table)",
"A.11.22.3.1—Nomenclature and codes for PHD Disease Management, CGM sensors",
"A.11.23.3.1—Nomenclature and codes for PHD Disease Management, CGM device",
"A.11.24.3.1—Nomenclature and codes for PHD Disease Management, Insulin Pump (multipage table)",
"A.11.24.4.1—Deprecated term codes for PHD Disease Management, Insulin Pump",
"A.11.25.4.1—Nomenclature and codes for PHD Disease Management, PSM device",
"A.11.26.2.1—Nomenclature and codes for PHD Disease Management, PSM device",
"A.11.27.3.1—Nomenclature and codes for PHD Disease Management, PEF status",
"A.12.3.2.1—Nomenclature and codes for health and fitness sensors (multipage table)",
"A.12.3.3.1—Deprecated terms for PHD disease management, health and fitness  (multipage table)",
"A.12.4.2.1—Nomenclature and codes for health and fitness activity (multipage table)",
"A.12.5.2.1—Nomenclature and codes for health and fitness exercise",
"A.12.6.2.1—Nomenclature and codes for health and fitness specific exercise",
"A.12.6.3.1—Deprecated terms for health and fitness exercise",
"A.13.3.2.1—Nomenclature and codes for assisted living sensors (multipage table)",
"A.13.4.3.1—Nomenclature and codes AI locations, general",
"A.13.5.3.1—Nomenclature and codes AI locations, rooms (multipage table)",
"A.13.6.3.1—Nomenclature and codes AI locations, medical rooms (multipage table)",
"A.13.7.3.1—Nomenclature and codes AI locations, doors and windows",
"A.13.8.3.1—Nomenclature and codes for AI locations, furniture",
"A.13.9.3.1—Nomenclature and codes for AI locations, appliances (multipage table)",
"A.13.10.2.1—Nomenclature and codes for AI events (multipage table)",
"A.13.11.3.1—Nomenclature and codes AI medication dispenser",
"A.14.3.1—Nomenclature for error return codes (multipage table)",
"A.14.4.1—Withdrawn nomenclature for error return codes",
"A.15.7.1—Nomenclature and codes for external nomenclatures and messaging standards (multipage table)",
"A.16.1.1—IHE PCD Alert Communication Management attributes",
"A.16.2.1—IHE PCD Alert Communication Management Notifications",
"A.16.3.1—Continua Services Interface infrastructure attributes",
"A.16.4.1—Continua Services Interface information attributes",
"A.16.5.1—IHE PCD and Continua Services Interface timekeeping information attributes (multipage table)",
"A.16.6.1—Breathing circuit attributes",
"A.16.6.2—Calculation method attribute",
"A.16.7.1—ECG nomenclature attributes",
"C.3.1.1—Device Type [MVC] discriminator set",
"C.3.2.1—Statistical [MMM] discriminator set",
"C.3.3.1—Haemodynamic pressure measurements [SDM] discriminator set",
"C.3.4.1—Rates for countable events [RCE] discriminator set",
"C.3.5.1—Rates for countable neurological events [RCN] discriminator set",
"C.3.6.1—Body Site Orientation (laterally) [LAT] discriminator set",
"C.3.7.1—Units of Measure [UoM] discriminator set",
"C.3.8.1—Unit of Measure (singular) [UoM1] discriminator set",
"C.3.9.1—No Discriminator [1] discriminator set",
"C.3.10.1—Event Discriminator [EVT] discriminator set",
"C.3.11.1—Statistical profile [PN3] discriminator set",
"C.3.12.1—Location Discriminator [LOC] discriminator set",
"C.3.13.1—Version of External Nomenclature Discriminator [64]",
"C.3.14.1—ECG lead discriminators [LEAD1] from  ISO/IEEE 11073-10101:2004  (multipage table)",
"C.3.15.1—ECG lead discriminators [LEAD2] from  ISO/IEEE 11073-10102:2012  (multipage table)",
"C.3.16.1—Equivalent ECG lead discriminators [LEAD] in  ISO/IEEE 11073-10101:2004 and  ISO/IEEE 11073-10102:2012 (multipage table)",
"C.3.17.1—Comparison of ECG lead discriminators in  ISO/IEEE 11073-10101:2004 and  ISO/IEEE 11073-10102:2012 (multipage table)",
"C.4.1.1—Object-Oriented—Partition 1 (multipage table)",
"C.4.2.1—SCADA—Partition 2 (multipage table)",
"C.4.3.1—Events and Alerts—Partition 3 (multipage table)",
"C.4.4.1—Dimensions—Partition 4 (multipage table)",
"C.4.5.1—Parameter Groups—Partition 6",
"C.4.6.1—Body Sites—Partition 7 (multipage table)",
"C.4.7.1—Communication Infrastructure—Partition 8 (multipage table)",
"C.4.8.1—PHD Disease Management—Partition 128 (multipage table)",
"C.4.9.1—PHD Health Fitness—Partition 129 (multipage table)",
"C.4.10.1—PHD Aging Independently—Partition 130 (multipage table)",
"C.4.11.1—Return Codes—Partition 255",
"C.4.12.1—External Nomenclature—Partition 256 (multipage table)",
"C.4.13.1—Device Settings—Partition 258 (multipage table)",
"C.4.14.1—Predicted Values—Partition 514 (multipage table)",
"D.1.1.1—Term code synonyms",
"D.1.1.2—Term code synonyms",
"D.1.2.1—RefId synonyms",
"E.1—Inspiratory breath and inflation types and rates",
"G.1—Gas measurement sites and anesthesia breathing circuit components (normative)",
"J.1—Revision history"
    ]
  end
end

<<-EOF
0 -- 0 -- 1 Nomenclature attributes
1 -- 1 -- 2 Attribution example
2 -- 2 -- 3 Systematic name derivation—medical device type example
3 -- 4 -- A.2.4.1.1 Partition 1—Object-oriented elements, Device nomenclature
4 -- 5 -- A.2.4.2.1 Partition 2—Metrics
5 -- 6 -- A.2.4.3.1 Partition 3—Alerts and events
6 -- 7 -- A.2.4.4.1 Partition 4—Units of measurement (dimensions)
7 -- 8 -- A.2.4.6.1 Partition 6—Program group
8 -- 9 -- A.2.4.7.1 Partition 7—Body sites
9 -- 10 -- A.2.4.8.1 Partition 8—Communication infrastructure
10 -- 11 -- A.2.4.9.1 Partition 9—Reserved for file exchange format (FEF)
11 -- 12 -- A.2.4.10.1 Partition 10—ECG additional nomenclature (annotated ECG)
12 -- 13 -- A.2.4.11.1 Partition 11—IDCO nomenclature
13 -- 14 -- A.2.4.12.1 Partition 128—PHD disease management
14 -- 15 -- A.2.4.13.1 Partition 129—PHD health and fitness
15 -- 16 -- A.2.4.14.1 Partition 130—PHD aging independently
16 -- 17 -- A.2.4.15.1 Partition 255—Return codes
17 -- 18 -- A.2.4.16.1 Partition 256—External nomenclatures
18 -- 19 -- A.2.4.17.1 Partition 258—Settings
19 -- 20 -- A.2.4.18.1 Partition 514—Predicted values
20 -- 21 -- A.2.4.19.1 Partition 1024—Private
21 -- 24 -- A.3.2.1.1 Object-oriented modeling elements—General—object class items
22 -- 25 -- A.3.2.1.2 Object-oriented modeling elements—General—attributes
23 -- 26 -- A.3.2.1.3 Object-oriented modeling elements—General—attribute groups
24 -- 27 -- A.3.2.1.4 Object-oriented modeling elements—General—behavior
25 -- 28 -- A.3.2.1.5 Object-oriented modeling elements—General—notifications
26 -- 29 -- A.3.2.2.1 Object-oriented modeling elements—Medical Package—object class items
27 -- 30 -- A.3.2.2.2 Object-oriented modeling elements—Medical Package—attributes
28 -- 31 -- A.3.2.2.3 Object-oriented modeling elements—Medical Package—attribute groups
29 -- 32 -- A.3.2.2.4 Object-oriented modeling elements—Medical Package—behavior
30 -- 33 -- A.3.2.2.5 Object-oriented modeling elements—Medical Package—notifications
31 -- 34 -- A.3.2.3.1 Object-oriented modeling elements—Alert Package—object class items
32 -- 35 -- A.3.2.3.2 Object-oriented modeling elements—Alert Package—attributes
33 -- 36 -- A.3.2.3.3 Object-oriented modeling elements—Alert Package—attribute groups
34 -- 37 -- A.3.2.3.4 Object-oriented modeling elements—Alert Package—behavior
35 -- 38 -- A.3.2.3.5 Object-oriented modeling elements—Alert Package—notifications
36 -- 39 -- A.3.2.4.1 Object-oriented modeling elements—System Package—object class items
37 -- 40 -- A.3.2.4.2 Object-oriented modeling elements—System Package—attributes
38 -- 41 -- A.3.2.4.3 Object-oriented modeling elements—System Package—attribute groups
39 -- 42 -- A.3.2.4.4 Object-oriented modeling elements—System Package—behavior
40 -- 43 -- A.3.2.4.5 Object-oriented modeling elements—System Package—notifications
41 -- 44 -- A.3.2.5.1 Object-oriented modeling elements—Control Package—object class items
42 -- 45 -- A.3.2.5.2 Object-oriented modeling elements—Control Package—attributes
43 -- 46 -- A.3.2.5.3 Object-oriented modeling elements—Control Package—attribute groups
44 -- 47 -- A.3.2.5.4 Object-oriented modeling elements—Control Package—behavior
45 -- 48 -- A.3.2.5.5 Object-oriented modeling elements—Control Package—notifications
46 -- 49 -- A.3.2.6.1 Object-oriented modeling elements—Extended Services Package—object class items
47 -- 50 -- A.3.2.6.2 Object-oriented modeling elements—Extended Services Package—attributes
48 -- 51 -- A.3.2.6.3 Object-oriented modeling elements— Extended Services Package—attribute groups
49 -- 52 -- A.3.2.6.4 Object-oriented modeling elements—Extended Services Package—behavior
50 -- 53 -- A.3.2.6.5 Object-oriented modeling elements— Extended Services Package—notifications
51 -- 54 -- A.3.2.7.1 Object-oriented modeling elements—Communication Package— object class items
52 -- 55 -- A.3.2.7.2 Object-oriented modeling elements—Communication Package—attributes
53 -- 56 -- A.3.2.7.3 Object-oriented modeling elements— Communication Package—attribute groups
54 -- 57 -- A.3.2.7.4 Object-oriented modeling elements—Communication Package—behavior
55 -- 58 -- A.3.2.7.5 Object-oriented modeling elements—Communication Package—notifications
56 -- 59 -- A.3.2.8.1 Object-oriented modeling elements—Archival Package—object class items
57 -- 60 -- A.3.2.8.2 Object-oriented modeling elements—Archival Package—attributes
58 -- 61 -- A.3.2.8.3 Object-oriented modeling elements—Archival Package—attribute groups
59 -- 62 -- A.3.2.8.4 Object-oriented modeling elements—Archival Package—behavior
60 -- 63 -- A.3.2.8.5 Object-oriented modeling elements—Archival Package—notifications
61 -- 64 -- A.3.2.9.1 Object-oriented modeling elements—Patient Package—object class items
62 -- 65 -- A.3.2.9.2 Object-oriented modeling elements—Patient Package—attributes
63 -- 66 -- A.3.2.9.3 Object-oriented modeling elements—Patient Package—attribute groups
64 -- 67 -- A.3.2.9.4 Object-oriented modeling elements—Patient Package—behavior
65 -- 68 -- A.3.2.9.5 Object-oriented modeling elements—Patient Package—notifications
66 -- 69 -- A.3.2.10.1 Object-oriented modeling elements—PHD—object class items
67 -- 70 -- A.3.2.10.2 Object-oriented modeling elements—PHD—attributes
68 -- 71 -- A.3.2.10.3 Object-oriented modeling elements—PHD—attribute groups
69 -- 72 -- A.3.2.10.4 Object-oriented modeling elements—PHD-behavior
70 -- 73 -- A.3.2.10.5 Object-oriented modeling elements—PHD—notifications
71 -- 74 -- A.3.2.11.1 Object-oriented modeling elements—WCM—object class items
72 -- 75 -- A.3.2.11.2 Object-oriented modeling elements—WCM—attributes
73 -- 76 -- A.3.2.11.3 Object-oriented modeling elements—WCM—attribute groups
74 -- 77 -- A.3.2.11.4 Object-oriented modeling elements—WCM—behavior
75 -- 78 -- A.3.2.11.5 Object-oriented modeling elements—WCM—notifications
76 -- 79 -- A.3.2.12.1 Object-oriented modeling elements—MEM-LS—object class items
77 -- 80 -- A.3.2.12.2 Object-oriented modeling elements—MEM-LS—attributes
78 -- 81 -- A.3.2.12.3 Object-oriented modeling elements—MEM-LS—attribute groups
79 -- 82 -- A.3.2.12.4 Object-oriented modeling elements—MEM-LS—behavior
80 -- 83 -- A.3.2.12.5 Object-oriented modeling elements—MEM-LS—notifications
81 -- 84 -- A.3.2.13.1 Object-oriented modeling elements—USI—object class items
82 -- 85 -- A.3.2.13.2 Object-oriented modeling elements—USI—attributes
83 -- 86 -- A.3.2.13.3 Object-oriented modeling elements—USI—attribute groups
84 -- 87 -- A.3.2.13.4 Object-oriented modeling elements—USI—behavior
85 -- 88 -- A.3.2.13.5 Object-oriented modeling elements—USI—notifications
86 -- 89 -- A.3.2.14.1 Object-oriented modeling elements—Deprecated identifier terms
87 -- 90 -- A.4.1 Communication infrastructure—object class items
88 -- 91 -- A.4.2 Communication infrastructure—attributes
89 -- 92 -- A.4.3 Communication infrastructure—attribute groups
90 -- 93 -- A.4.4 Communication infrastructure—behavior
91 -- 94 -- A.4.5 Communication infrastructure—notifications
92 -- 95 -- A.4.6 Communication infrastructure—profile support attributes
93 -- 96 -- A.4.7 Communication infrastructure—optional package identifiers
94 -- 97 -- A.4.8 Communication infrastructure—system specification components
95 -- 98 -- A.5.1 Nomenclature and codes for vital signs devices
96 -- 99 -- A.6.2.1 Table of decimal factors
97 -- 100 -- A.6.3.1 Non-SI units
98 -- 101 -- A.6.4.1 Vital signs units of measurement
99 -- 102 -- A.6.5.1 Withdrawn terms for vital signs units of measurement
100 -- 103 -- A.6.6.1 Deprecated terms for vital signs units of measurement
101 -- 104 -- A.6.7.1 Deprecated RefIds for vital signs units of measurement
102 -- 105 -- A.7.1.3.1 List of standardized ECG <lead> descriptors from SCP‑ECG
103 -- 106 -- A.7.1.3.2 Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10101-2004 with discriminator group [LEAD1]
104 -- 107 -- A.7.1.3.3 Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10102 with discriminator group [LEAD2]
105 -- 108 -- A.7.1.6.1 Nomenclature and codes for global lead ECG measurements
106 -- 109 -- A.7.1.6.2 Nomenclature and codes for ECG measurements with lead origin
107 -- 110 -- A.7.1.7.1 Deprecated terms for ECG measurements
108 -- 111 -- A.7.1.8.1 Deprecated RefIds for ECG measurements defined in IEEE11073-10102
109 -- 112 -- A.7.2.6.1 Nomenclature and codes for ECG enumerations
110 -- 113 -- A.7.2.7.1 Withdrawn terms for ECG enumerations
111 -- 114 -- A.7.2.8.1 Deprecated terms for ECG enumerations
112 -- 115 -- A.7.2.9.1 Deprecated RefIds for ECG enumerations
113 -- 116 -- A.7.3.6.1 Nomenclature and codes for haemodynamic monitoring measurements
114 -- 117 -- A.7.3.7.1 Deprecated terms for haemodynamic monitoring measurements
115 -- 118 -- A.7.4.11.1 Ventilator modes bit string
116 -- 119 -- A.7.4.11.2 Ventilator modes nomenclature and codes
117 -- 120 -- A.7.4.16.1 Correction of gas measurements
118 -- 121 -- A.7.4.17.1 Gas measurement sites
119 -- 122 -- A.7.4.17.2 Default gas measurement sites
120 -- 123 -- A.7.4.18.1 Inspiratory breath type classifications
121 -- 124 -- A.7.4.19.1 Deployment of gas partial pressure and concentration and consumption information (informative)
122 -- 125 -- A.7.4.19.2 Nomenclature and codes for respiratory, ventilator, and anesthesia measurements
123 -- 126 -- A.7.4.20.1 Deprecated terms for respiratory measurements
124 -- 127 -- A.7.4.21.1 Deprecated RefIds for respiratory measurements
125 -- 128 -- A.7.4.22.1 Recommended mapping of deprecated to ‘unified’ gas RefId prefixes (informative)
126 -- 129 -- A.7.4.23.1 Deprecated terms for respiratory measurements
127 -- 130 -- A.7.4.24.1 Deprecated RefIds for respiratory measurements
128 -- 131 -- A.7.4.25.1 Deprecated nomenclature for undefined respiratory measurements from Annex B
129 -- 132 -- A.7.4.26.1 Nomenclature and codes for nebulizers
130 -- 133 -- A.7.5.6.1 Nomenclature and codes for common blood-gas, blood, urine, and other fluid chemistry measurements
131 -- 134 -- A.7.5.7.1 Withdrawn terms for blood-gas, blood, urine, and other fluid chemistry measurements
132 -- 135 -- A.7.5.8.1 Deprecated RefIds for blood-gas, blood, urine, and other fluid chemistry measurements
133 -- 136 -- A.7.6.6.1 Nomenclature and codes for fluid-output measurements
134 -- 137 -- A.7.7.6.1 Nomenclature and codes for pump data
135 -- 138 -- A.7.7.7.1 Deprecated RefIds for pump data
136 -- 139 -- A.7.7.13.1 Pump modes bit string values (value to be communicated in enumeration observation element Mode | | Device | Pump)
137 -- 140 -- A.7.7.19.1 Pump states bit string values (value to be communicated in EnumerationObservation with code: Status | Operational | Device | Pump)
138 -- 141 -- A.7.8.6.1 Nomenclature and codes for neurological monitoring measurements
139 -- 142 -- A.7.9.6.1 Nomenclature and codes for neurophysiologic enumerations
140 -- 143 -- A.7.10.6.1 Nomenclature and codes for neurophysiologic stimulation modes
141 -- 144 -- A.7.11.6.1 Nomenclature and codes for miscellaneous measurements
142 -- 145 -- A.7.11.6.2 Nomenclature and codes for temperature
143 -- 146 -- A.7.11.7.1 Deprecated nomenclature for temperature
144 -- 147 -- A.7.11.8.1 Body weight and surface area for pre-coordinated RefIds
145 -- 148 -- A.7.11.9.1 Nomenclature and codes for body mass (weight) and estimates
146 -- 149 -- A.7.12.1.1 Nomenclature and code extensions for infant incubator and warmer microenvironments
147 -- 150 -- A.7.13.6.1 Nomenclature and codes for spirometry measurements
148 -- 151 -- A.7.14.1.1 Nomenclature and code extensions for personal health devices
149 -- 152 -- A.7.14.2.1 Deprecated RefIds for personal health devices
150 -- 154 -- A.8.2.5.1 Nomenclature and codes for sites for neurophysiological signal monitoring: locations near peripheral nerves
151 -- 155 -- A.8.3.5.1 Nomenclature and codes for sites for neurophysiological signal monitoring: locations near or in muscles
152 -- 156 -- A.8.4.6.1 Nomenclature and codes for electrode sites for EEG according to the international 10–20 system
153 -- 157 -- A.8.5.6.1 Nomenclature and codes for sites for EOG signal monitoring
154 -- 158 -- A.8.6.5.1 Nomenclature and codes for general neurological sites for monitoring measurements and drainage
155 -- 159 -- A.8.7.5.1 Nomenclature and codes for body sites for cardiovascular measurements
156 -- 160 -- A.8.8.5.1 Nomenclature and codes for miscellaneous body sites used in vital signs monitoring and measurement
157 -- 161 -- A.8.9.5.1 Nomenclature and codes for external sites used in vital signs monitoring and measurement
158 -- 162 -- A.8.10.6.1 Nomenclature and codes for qualifiers of body site locations
159 -- 163 -- A.9.2.5.1 Nomenclature and codes for pattern events
160 -- 164 -- A.9.3.5.1 Nomenclature and codes for device-related and environment-related events
161 -- 165 -- A.9.3.6.1 Deprecated terms for device-related and environment-related events
162 -- 166 -- A.9.3.7.1 Deprecated RefIds for device-related and environment-related events
163 -- 167 -- A.10.2.3.1 Nomenclature and codes for infrastructure, device specialization
164 -- 168 -- A.10.2.4.1 Withdrawn terms for device specialization
165 -- 169 -- A.10.3.3.1 Nomenclature and codes for Infastructure, sub-specialization
166 -- 170 -- A.10.4.3.1 Time synchronization profiles
167 -- 171 -- A.11.2.3.1 Nomenclature and codes for PHD Disease Management, general
168 -- 172 -- A.11.3.3.1 Nomenclature and codes for PHD Disease Management, Basic ECG sensors
169 -- 173 -- A.11.4.3.1 Nomenclature and codes for PHD Disease Management, Basic ECG event context
170 -- 174 -- A.11.5.3.1 Nomenclature and codes for PHD Disease Management, SABTE sensors and settings
171 -- 175 -- A.11.5.4.1 Withdrawn terms for PHD Disease Management, SABTE sensors and settings
172 -- 176 -- A.11.6.3.1 Nomenclature and codes for PHD Disease Management, SABTE mode settings
173 -- 177 -- A.11.7.3.1 Nomenclature and codes for PHD Disease Management, Glucose carbohydrate source
174 -- 178 -- A.11.8.3.1 Nomenclature and codes for PHD Disease Management, Glucose carbohydrate source
175 -- 179 -- A.11.9.3.1 Nomenclature and codes for PHD Disease Management, Glucose moncarbohydrate sources
176 -- 180 -- A.11.10.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, insulin type
177 -- 181 -- A.11.11.3.1 Nomenclature and codes for PHD Disease Management, insulin types
178 -- 182 -- A.11.12.3.1 Nomenclature and codes for PHD Disease Management, general health
179 -- 183 -- A.11.13.3.1 Nomenclature and codes for PHD Disease Management, general health
180 -- 184 -- A.11.14.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, sample location
181 -- 185 -- A.11.15.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, sample locations
182 -- 186 -- A.11.16.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, meal
183 -- 187 -- A.11.17.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, meal types
184 -- 188 -- A.11.18.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, tester
185 -- 189 -- A.11.19.3.1 Nomenclature and codes for PHD Disease Management, Glucose Monitoring, tester types
186 -- 190 -- A.11.20.3.1 Nomenclature and codes for PHD Disease Management, INR status and context
187 -- 191 -- A.11.21.3.1 Nomenclature and codes for PHD Disease Management, CGM sensors
188 -- 192 -- A.11.22.3.1 Nomenclature and codes for PHD Disease Management, CGM sensors
189 -- 193 -- A.11.23.3.1 Nomenclature and codes for PHD Disease Management, CGM device
190 -- 194 -- A.11.24.3.1 Nomenclature and codes for PHD Disease Management, Insulin Pump
191 -- 195 -- A.11.24.4.1 Deprecated term codes for PHD Disease Management, Insulin Pump
192 -- 196 -- A.11.25.4.1 Nomenclature and codes for PHD Disease Management, PSM device
193 -- 197 -- A.11.26.2.1 Nomenclature and codes for PHD Disease Management, PSM device
194 -- 198 -- A.11.27.3.1 Nomenclature and codes for PHD Disease Management, PEF status
195 -- 199 -- A.12.3.2.1 Nomenclature and codes for health and fitness sensors
196 -- 200 -- A.12.3.3.1 Deprecated terms for PHD disease management, health and fitness
197 -- 201 -- A.12.4.2.1 Nomenclature and codes for health and fitness activity
198 -- 202 -- A.12.5.2.1 Nomenclature and codes for health and fitness exercise
199 -- 203 -- A.12.6.2.1 Nomenclature and codes for health and fitness specific exercise
200 -- 204 -- A.12.6.3.1 Deprecated terms for health and fitness exercise
201 -- 205 -- A.13.3.2.1 Nomenclature and codes for assisted living sensors
202 -- 206 -- A.13.4.3.1 Nomenclature and codes AI locations, general
203 -- 207 -- A.13.5.3.1 Nomenclature and codes AI locations, rooms
204 -- 208 -- A.13.6.3.1 Nomenclature and codes AI locations, medical rooms
205 -- 209 -- A.13.7.3.1 Nomenclature and codes AI locations, doors and windows
206 -- 210 -- A.13.8.3.1 Nomenclature and codes for AI locations, furniture
207 -- 211 -- A.13.9.3.1 Nomenclature and codes for AI locations, appliances
208 -- 212 -- A.13.10.2.1 Nomenclature and codes for AI events
209 -- 213 -- A.13.11.3.1 Nomenclature and codes AI medication dispenser
210 -- 214 -- A.14.3.1 Nomenclature for error return codes
211 -- 215 -- A.14.4.1 Withdrawn nomenclature for error return codes
212 -- 216 -- A.15.7.1 Nomenclature and codes for external nomenclatures and messaging standards
213 -- 217 -- A.16.1.1 IHE PCD Alert Communication Management attributes
214 -- 218 -- A.16.2.1 IHE PCD Alert Communication Management Notifications
215 -- 219 -- A.16.3.1 Continua Services Interface infrastructure attributes
216 -- 220 -- A.16.4.1 Continua Services Interface information attributes
217 -- 221 -- A.16.5.1 IHE PCD and Continua Services Interface timekeeping information attributes
218 -- 222 -- A.16.6.1 Breathing circuit attributes
219 -- 223 -- A.16.6.2 Calculation method attribute
220 -- 224 -- A.16.7.1 ECG nomenclature attributes
221 -- 225 -- C.3.1.1 Device Type [MVC] discriminator set
222 -- 226 -- C.3.2.1 Statistical [MMM] discriminator set
223 -- 227 -- C.3.3.1 Haemodynamic pressure measurements [SDM] discriminator set
224 -- 228 -- C.3.4.1 Rates for countable events [RCE] discriminator set
225 -- 229 -- C.3.5.1 Rates for countable neurological events [RCN] discriminator set
226 -- 230 -- C.3.6.1 Body Site Orientation (laterally) [LAT] discriminator set
227 -- 231 -- C.3.7.1 Units of Measure [UoM] discriminator set
228 -- 232 -- C.3.8.1 Unit of Measure (singular) [UoM1] discriminator set
229 -- 233 -- C.3.9.1 No Discriminator [1] discriminator set
230 -- 234 -- C.3.10.1 Event Discriminator [EVT] discriminator set
231 -- 235 -- C.3.11.1 Statistical profile [PN3] discriminator set
232 -- 236 -- C.3.12.1 Location Discriminator [LOC] discriminator set
233 -- 237 -- C.3.13.1 Version of External Nomenclature Discriminator [64]
234 -- 238 -- C.3.14.1 ECG lead discriminators [LEAD1] from  ISO/IEEE 11073-10101:2004
235 -- 239 -- C.3.15.1 ECG lead discriminators [LEAD2] from  ISO/IEEE 11073-10102:2012
236 -- 240 -- C.3.16.1 Equivalent ECG lead discriminators [LEAD] in  ISO/IEEE 11073-10101:2004 and ISO/IEEE 11073-10102:2012
237 -- 241 -- C.3.17.1 Comparison of ECG lead discriminators in  ISO/IEEE 11073-10101:2004 and ISO/IEEE 11073-10102:2012
238 -- 242 -- C.4.1.1 Object-Oriented—Partition 1
239 -- 243 -- C.4.2.1 SCADA—Partition 2
240 -- 244 -- C.4.3.1 Events and Alerts—Partition 3
241 -- 245 -- C.4.4.1 Dimensions—Partition 4
242 -- 246 -- C.4.5.1 Parameter Groups—Partition 6
243 -- 247 -- C.4.6.1 Body Sites—Partition 7
244 -- 248 -- C.4.7.1 Communication Infrastructure—Partition 8
245 -- 249 -- C.4.8.1 PHD Disease Management—Partition 128
246 -- 250 -- C.4.9.1 PHD Health Fitness—Partition 129
247 -- 251 -- C.4.10.1 PHD Aging Independently—Partition 130
248 -- 252 -- C.4.11.1 Return Codes—Partition 255
249 -- 253 -- C.4.12.1 External Nomenclature—Partition 256
250 -- 254 -- C.4.13.1 Device Settings—Partition 258
251 -- 255 -- C.4.14.1 Predicted Values—Partition 514
252 -- 256 -- D.1.1.1 Term code synonyms
253 -- 257 -- D.1.1.2 Term code synonyms
254 -- 258 -- D.1.2.1 RefId synonyms
255 -- 259 -- E.1 Inspiratory breath and inflation types and rates
256 -- 260 -- G.1 Gas measurement sites and anesthesia breathing circuit components (normative)
EOF
