class TermExtractor
  # Only adding discriminator group and offset when parsing these
  # alphabeticals: ["refid", "disc", "part:code", "cf_code10"]
  def discriminator_tables
    [238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251]
  end
  
  def fix_ver
    ver = RTM::DiscriminatorSet.where(:name => '64').first
    if ver
      ChangeTracker.start
      ver.name = 'VER'
      ver.save
      ChangeTracker.commit
    end
  end
  
  def parse104
    proc104 = proc do |cells, table|
      offset, suffix, description, polarity, pos_ref, neg_ref = cells.collect { |cell| extract_text(cell) }
      next if description == 'Description'
      next if offset == '200-255'
      next if cells.first =~ /^SCP/ # someone put a bunch of notes in a cell at the bottom...not IEEE approved way of adding table notes FWIW...
      offset = offset.to_i
      set  = RTM::DiscriminatorSet.where(:name => 'LEAD2').first
      disc = set.discriminators.find { |d| d.offset == offset }
      raise offset.inspect unless disc
      RTM.error("LEAD2 (#{offset}) suffix mismatch.  Annex C has #{disc.offset}.  Annex A has #{offset}", :red) unless disc.offset == offset
      if description.to_s != disc.description
        description = disc.description + " | " + description.to_s
      end
      description = description  + " | " + "Bipolar-Unipolar: #{polarity}" if polarity.to_s[0]
      description = description  + " | " + "Positive reference: #{pos_ref}" if pos_ref.to_s[0]
      description = description  + " | " + "Negative reference: #{neg_ref}" if neg_ref.to_s[0]
      ChangeTracker.start
      disc.description = description
      disc.save
      ChangeTracker.commit
    end
    _parse_tables('Table A.7.1.3.3—Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10102 with discriminator group [LEAD2]', [104], proc104)
  end
  
  def add_discriminators
    fix_ver
    proc_discriminator_adder = proc do |cells, table|
      refid, disc, part_code, cf_code = cells.collect { |cell| extract_text(cell) }
      disc = 'LOC (0)' if disc == 'LOC(0)'
      if refid == 'MDC_DIM_X_JOULES_PER_L_PER_KG' || refid == 'MDC_DIM_X_G_FORCE_M_PER_MIN_PER_M2'
        disc = 'UoM (0)'
      end  
      args = {table:table, refid_value:refid, disc:disc, cf_code:cf_code, part_code:part_code}
      next if disc == 'Disc'
      add_discriminator(args)
    end
    _parse_tables('Discriminator base values', discriminator_tables, proc_discriminator_adder)
    parse104
  end
    
  # This is also a check to make sure codes and refids aren't duplicated
  def add_discriminator(table:nil, refid_value:, disc:, cf_code:nil, part_code:)
    terms = RTM.find_terms(refid_value, part_code)
    RTM.error("Multiple terms with the same RefID and TermCode: #{terms.map(&:stat)}", :red) if terms.count > 1
    term = terms.first
    unless term
      refid = RTM.find_refid(refid_value)
      unless refid
        RTM.info("No RefID: #{refid_value} (#{part_code}) while adding discriminator #{disc}. Creating it now.", :none)
        ChangeTracker.start
        refid = RTM.find_or_create_refid(refid_value)
        ChangeTracker.commit
      end
      code = RTM.find_code(part_code)
      unless code
        RTM.info("No TermCode: #{refid_value} (#{part_code}) while adding discriminator #{disc}. Creating it now.", :none)
        ChangeTracker.start
        code = RTM.find_or_create_code_in_partition(part_code)
        ChangeTracker.commit
      end
      type = part_code =~ /4::/ ? RTM::Unit : RTM::Term
      ChangeTracker.start
      term = type.create
      # term.status = 'Published (10101:2019 Annex C)'
      term.save
      term.term_code = code
      term.refid = refid
      ChangeTracker.commit
    end
    
    set_name, offset = disc.split(/\s/)
    offset = offset.delete('(),').to_i
    set = RTM::DiscriminatorSet.where(:name => set_name).first
    raise "No DiscriminatorSet for #{set_name}" unless set
    discriminator = set.discriminators.find { |d| d.offset == offset }
    raise "No Discriminator with offset #{offset} in #{set_name}" unless discriminator

    if term.refid.terms.reject { |tm| tm.status&.value =~ /Provisional|Proposed/}.find { |t| t.term_code.nil? } # another oddball situation....
      term.refid.terms.each {|tm| puts tm.summary(false) }
      puts term.statplus; puts '***'
      # FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME 
      # RTM.error("Fix the problem with #{refid_value} and discriminator assignment of #{set.name}(#{disc.value})", :red)
      raise 'WTF'
      main_refid = RTM.find_or_create_refid('MDC_DIM_BREATHS_PER_MIN_PER_X_L')
      main_def = main_refid.definitions.first
      main_def = RTM.combine_definitions(main_def, refid.definitions.first)
      return add_discriminator(table:table, refid_value:refid_value, disc:disc, cf_code:cf_code, part_code:part_code)
    end

    # Get the discriminator and offset objects
    # make sure it is in an expected format
    infix_skip = nil
    unless disc =~ /^(MVC|SDM|RCE|RCN|LAT|UoM|UoM1|1|MMM|LEAD|EVT|PN3|LOC|VER|LEAD1|LEAD2) \(\d+\)$/
      if disc =~ /^(MVC|SDM|RCE|RCN|LAT|UoM|UoM1|1|MMM|LEAD|EVT|PN3|LOC|VER|LEAD1|LEAD2) \(\d+\),+$/
        infix_skip = disc.count(',')
      else
        raise "Special disc '#{disc}'"
      end
    end
    
    ChangeTracker.start
    if term.discriminators_count > 0
      unless term.discriminator_set_names.all? { |n| n =~ /LEAD[1|2]/ }
        if term.discriminators != [discriminator]
          RTM.warn("Term #{term.refid_stat} #{term.code_stat} #{term.discriminators_display} does not match 10101R discriminator #{discriminator.representation}.  Removing existing discriminators.", :orange)
          term.remove_all_discriminators
        end
      end
    end
    term.add_discriminator(discriminator) unless term.discriminators.include?(discriminator)
    term.status = 'Published (10101:2019 Annex C)' unless term.published2019?
    set_infix_skip(term, set_name, infix_skip) if infix_skip
    ChangeTracker.commit
  end
  
  def set_infix_skip(term, set_name, infix_skip)
    return if infix_skip == 0
    term.infix_depths = (term.infix_depths.to_s.split(/\s+/) << "#{set_name}:#{infix_skip}").join(' ')
    term.save
  end
  
end