class TermExtractor
  attr_accessor :processed_tables
   
  def typical_tables
    # ["systematic name", "common term", "acronym", "description/definition", "refid", "part::code"]
    [105, 106, 109, 113, 114, 116, 122, 123, 124, 126, 127, 129, 130, 131, 132, 133, 134, 135, 138, 139, 140, 141, 142, 145, 146, 147, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 165, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 212]
  end
  def processtt1
    proc do |cells, table|
      if cells.count == 1 # because somebody thought it would be a great idea to put a note in a table cell...sheesh...
        puts Rainbow("Skipping row with one cell.  Text = #{extract_text(cells.first).inspect}").orange
        next
      end
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      refid = 'MDC_PRESS_BLD_VENT_SYS_END' if refid =~ /MDC_PRESS_BLD_VENT_/ && part_code == '2::1905'
      if refid == '' && part_code == ''
        ChangeTracker.start
        @current_tag = RTM.find_or_create_tag('Term Group: ' + sys_name)
        ChangeTracker.commit
        next
      end
      args = {table:table, description:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym}
      begin
        term = process_term(args)
      rescue
        puts "args was #{args.pretty_inspect}"
        raise
      end
    end
  end
  
  def typical_no_acronym_tables
    #["systematic name", "common term", "description/definition", "refid", "part::code"]
    [95, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209]
  end
  def processtt2
    proc do |cells, table|
      sys_name, common_term, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      if refid == '' && part_code == ''
        ChangeTracker.start
        @current_tag = RTM.find_or_create_tag('Term Group: ' + sys_name)
        ChangeTracker.commit
        next
      end
      args = {table:table, description:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term}
      term = process_term(args)
    end
  end
  
  def parse_typical_tables
    _parse_tables('Typical', typical_tables, processtt1)
    _parse_tables('Typical no acronym', typical_no_acronym_tables, processtt2)
  end
  
=begin  
  # def parse_typicals
  #   puts "Begin Typical Deprecated"
  #   typical_deprecated_tables.each { |tables_index| parse_typical_table(index, :deprecated) }
  #   puts "Begin Typical Withdrawn"
  #   typical_withdrawn_tables.each { |tables_index| parse_typical_table(index, :withdrawn) }
  # end
  #
  # def parse_typical_table(index, status = nil)
  #   t = @tables[index]
  #   section = t[:table] + ' - ' + t[:title]
  #   # number, title = t[:title].split(/ [-–] /)
  #   headers = t[:docx_rows][0].collect { |cell| extract_text(cell) }
  #   rows    = t[:docx_rows][1..-1]
  #   if rows.count == 1 && rows[0].count == 1
  #     words = extract_text(rows[0][0])
  #     next if words =~ /This table has no content/
  #     RTM.error('Problem parsing Typical table with words: ' + words, :red)
  #     next
  #   end
  #   table = RTM.get_10101R_table(section, t[:headers])
  #   # puts Rainbow("#{title} -- #{section}").orange
  #   # FIXME add all section references to term in database
  #   rows.each do |cells|
  #     term = nil
  #     if cells.count == 5
  #       # ["systematic name", "common term", "description/definition", "refid", "part::code"]
  #       sys_name, common_term, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
  #       args = {table:table, description:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term}
  #     elsif cells.count == 6
  #       # ["systematic name", "common term", "acronym", "description/definition", "refid", "part::code"]
  #       sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
  #       args = {table:table, description:description, refid:refid, part_code:part_code, sys_name:sys_name, common_term:common_term, acronym:acronym}
  #     else
  #       raise
  #     end
  #     term[:status] = status
  #     term = process_term(args)
  #   end
  # end
=end  
  
end