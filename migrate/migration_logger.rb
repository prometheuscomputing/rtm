require 'logger'
module RTM  
  
  def self.log_path(name = nil)
    name ||= 'Migration_log'
    File.expand_path(File.join(__dir__, '../migrate/logs', name))
  end
  
  def self.log_file(filename = nil)
    @log_file ||= File.open(log_path(filename), File::WRONLY | File::APPEND | File::CREAT)
  end
  
  def self.reset_log(filename = nil)
    @log_file.close if @log_file
    @log_file = nil
    if File.exist?(log_file(filename))
      f = File.open(log_path(filename), File::TRUNC) {}
    end
    @logger = Logger.new(log_file(filename), :level => :info)
    @log_file.sync = true
    @logger.formatter = proc do |severity, datetime, progname, msg|
      sev = (severity == 'INFO') ? ('   ') : (severity[0] + ', ')
      "#{sev}#{msg}\n"
    end
  end
  
  def self.logger
    @logger
  end
  
  def self.info(msg, color = nil)
    # puts (color ? Rainbow(msg).send(color) : msg) unless color == :none
    logger.info(msg)
  end
  
  def self.warn(msg, color = nil)
    puts (color ? Rainbow(msg).send(color) : msg) unless color == :none
    logger.warn(msg)
  end
  
  def self.error(msg, color = nil)
    puts (color ? Rainbow(msg).send(color) : msg)
    logger.error(msg)
  end
end
