# There is probably going to be some orphaned junk to clean up at the end (Definitions mostly, maybe other stuff)
require_relative 'remove_v1_unit_duplicates'
class TermExtractor
  
  def set_preferred_refids
    terms = RTM.find_terms('MDC_VOL_AWAY_TIDAL_EXP_BTSD_PSAZC_PER_IBW', '2::21596') + RTM.find_terms('MDC_VOL_MINUTE_AWAY_EXP_BTSD_PSAZC_PER_IBW', '2::21616') + RTM.find_terms('MDC_ECG_LEAD', '2::0')
    ChangeTracker.start
    terms.each { |t| t.refid_preferred = true; t.save }
    ChangeTracker.commit
  end
  
  def connect_odd_synonyms
    'MDC_EVT_CUFF_INFLAT_OVER 3::232 and MDC_EVT_PRESS_CUFF_OVER 3::378 are not simply RefID synonyms.  Skipping them for now!'
    t1 = RTM
  end
  
  def pre10101_fix1
    remove_v1_unit_duplicates
    ['MDC_DIM_KILO_PASCAL_PER_L', 'MDC_DIM_MILLI_L_PER_HPA'].each do |refid|
      refidobj = RTM.find_refid(refid)
      terms = refidobj.terms
      terms.each do |term|
        next if term.status&.value =~ /Published/
        puts "Destroying:"
        puts term.stat
        ChangeTracker.start
        term.destroy
        ChangeTracker.commit
      end
    end
    
    ChangeTracker.start
    term = RTM.get_terms('MDC_ID_TRIG', '2::53253').first
    term.destroy if term
    ChangeTracker.commit

    ChangeTracker.start
    notch = RTM.find_or_create_refid('MDC_ECG_CTL_VBL_ATTR_FILTER_NOTCH_DESCRIPTION')
    tc = RTM.find_code_in_partition('10::11410')
    tc.terms.first.refid = notch
    ChangeTracker.commit

    # tc = RTM.find_code_in_partition('2::16004')
    # terms = tc.terms
    # terms.each do |term|
    #   if term.refid.value == 'MDC_ECG_TIME_PD_QTU'
    #
    #
    #
    #   trefid = term.refid
    #   if drefids.find {|rf| rf.value == 'MDC_ECG_TIME_PD_QTU'}
    #     ChangeTracker.start
    #     df.refids = df.refids.reject { |rf| rf.value == 'MDC_ECG_TIME_PD_QTU'}
    #     ChangeTracker.commit
    #   end
    # end
    
    tc1 = RTM.find_code_in_partition('2::17016')
    tc1.terms.each do |t|
      RTM.deprecate_term(t)
    end
    
    # rf = RTM.find_refid('MDC_ECG_BEAT_COUNT')
    # defs = rf.definitions
    # good_def = defs.find {|d| d.systematic_name }
    # bad_def  = defs.find {|d| d.systematic_name.nil? }
    # RTM.combine_definitions(good_def, bad_def)
    #
    # rf = RTM.find_refid('MDC_ECG_RHY_IRREG')
    # defs = rf.definitions
    # RTM.combine_definitions(defs.first, defs.last)
    
    # fix_term_code_homonyms
  end
  
  def fix_mdcx_spaces
    problems = RTM::RefID.where(Sequel.ilike(:value, 'MDCX_ %')).all
    ChangeTracker.start
    problems.each do |refid|
      refid.value = refid.value.gsub(/\s+/, '')
      refid.save
    end
    ChangeTracker.commit
  end
  
  # FIXME from b4 refactor
  def fix_term_code_homonyms
    # code_homonyms = []
    RTM::TermCode.each do |c|
      if c.terms_count > 1
        if c.terms_count == 2
          RTM.combine_definitions(c.definitions.first, c.definitions.last)
        else
          # code_homonyms << c.definitions.collect { |defn| [defn.refids_status, defn.part_codes_status, defn.discriminator_sets.map(&:name), defn.descriptions.map(&:value_content)] }
        end
      end
    end
    # puts Rainbow("There are still #{code_homonyms.count} TermCode homonyms in the system.").cyan
    # code_homonyms.each { |s| pp s;puts }
  end
  
  def remove_extra_underscores_from_refids
    r = RTM.find_refid('MDC__CONC_NO2_INSP')
    ChangeTracker.start
    r.destroy if r
    ChangeTracker.commit
    RTM::RefID.all.each do |r| 
      if r.value =~ /__/
        if r.terms.empty?
          ChangeTracker.start
          r.destroy
          ChangeTracker.commit
        else
          correct_value = r.value.gsub('__', '_')
          correct_refid = RTM::RefID.where(value:correct_value).first
          if correct_refid
            ChangeTracker.start
            correct_refid.terms = correct_refid.terms + r.terms
            correct_refid.save
            ChangeTracker.commit
            ChangeTracker.start
            r.destroy
            ChangeTracker.commit            
          else
            ChangeTracker.start
            r.value = correct_value
            r.save
            ChangeTracker.commit
          end
        end
      end
    end
  end
  
  def fix_lead_set
    lead  = RTM::DiscriminatorSet.where(:name => 'LEAD').first
    lead1 = RTM::DiscriminatorSet.where(:name => 'LEAD1').first
    lead2 = RTM::DiscriminatorSet.where(:name => 'LEAD2').first
    lead.discriminators.each do |disc|
      disc1 = lead1.discriminators.find { |l1d| l1d.offset == disc.offset }
      disc2 = lead2.discriminators.find { |l2d| l2d.offset == disc.offset }
      disc.terms.each do |term|
        rv = term.refid.value
        # puts "#{rv} -#{disc.offset}- #{disc1.value}:#{!!(rv =~ /#{disc1.value}$/)} -- #{disc2.value}:#{!!(rv =~ /#{disc2.value}$/)}"
        ChangeTracker.start
        term.remove_discriminator(disc)
        term.add_discriminator(disc1) if !!(rv =~ /#{disc1.value}$/)
        term.add_discriminator(disc2) if !!(rv =~ /#{disc2.value}$/)
        term.save
        ChangeTracker.commit
      end
    end
  end

  def fix_for_table_A_6_5_1
    return # appears to no longer be needed
    base_tc = RTM.get_code_obj('4::4704')
    bd = base_tc.terms.first
    tc20 = RTM.find_code('4::4724')
    tc24 = RTM.find_code('4::4728')
    dset = RTM::DiscriminatorSet.where(name:'UoM').first
    d20 = dset.discriminators.find { |d| d.offset == 20 }
    d24 = dset.discriminators.find { |d| d.offset == 24 }
    ChangeTracker.start
    tc20.add_discriminator(d20)
    tc24.add_discriminator(d24)
    new_def20 = RTM::Unit.create(auto_expanded:true, acronym:bd.acronym, systematic_name:bd.systematic_name, common_term:bd.common_term)
    new_def24 = RTM::Unit.create(auto_expanded:true, acronym:bd.acronym, systematic_name:bd.systematic_name, common_term:bd.common_term)
    new_def20.descriptions = bd.descriptions
    new_def24.descriptions = bd.descriptions
    new_def20.add_discriminator_set(dset)
    new_def24.add_discriminator_set(dset)
    new_def20.add_term_code(tc20)
    new_def24.add_term_code(tc24)
    ChangeTracker.commit
  end
  
  def fix3_80
    ChangeTracker.start
    terms = RTM.find_terms('MDC_EVT_OBSTRUC', '3::80') + RTM.find_terms('MDC_EVT_OCCL', '3::80')
    terms.each { |t| t.status = 'Published'; t.save }
    ChangeTracker.commit
  end
  
  def fix_for_table_A_7_2_7_1
    return # appears to no longer be needed
    bad_terms = RTM.find_terms('MDC_ECG_ARTIFACT_ANNOT', '2::16455')
    base_tc_brady = RTM.get_code_obj('2::16448')
    bd_brady = base_tc_brady.definitions.first # there is only one in this case
    tc_conflicting = RTM.get_code_obj('2::16455')
    def1 = tc_conflicting.definitions.first
    refid_annot = RTM.find_refid('MDC_ECG_ARTIFACT_ANNOT')
    RTM.withdraw_code(def1, tc_conflicting, 'Withdrawn as clashes with MDC_ECG_BRADY_ANNOT [RCE(7)] 2::16455')
    # RTM.withdraw_refid(def1, refid_annot, 'Withdrawn as clashes with MDC_ECG_BRADY_ANNOT [RCE(7)] 2::16455')

    ChangeTracker.start
    tc_artifact_annot = RTM.find_or_create_code_in_partition('2::16495')
    def1.add_term_code(tc_artifact_annot)
    def1.systematic_name = 'Pattern | Artifact, Annotation | ECG, Heart | CVS'
    def1.common_term = 'Artifact'
    def1.descriptions = [RTM.find_or_create_description('Artifact; annotation', true)]
    def1.save
    
    brady_def = RTM::Definition.create(auto_expanded:true, acronym:bd_brady.acronym, systematic_name:bd_brady.systematic_name, common_term:bd_brady.common_term)
    brady_descriptions = bd_brady.descriptions.collect do |bdd|
      brady_description_content = bdd.value_content.to_s.strip.sub(/^<p>/, '').sub(/<\/p>$/, '') + '; ' + '; annotation'
      RTM.find_or_create_description(brady_description_content, true)
    end
    brady_def.descriptions = brady_descriptions
    brady_def.add_term_code(tc_conflicting)
    r = RTM.find_or_create_refid('MDC_ECG_BRADY_ANNOT')
    brady_def.add_refid(r)
    brady_def.save
    ChangeTracker.commit
  end
  
  # Per discussions with Paul Schluter
  def fix_incorrect_term_codes
    # MDC_DIM_X_M_PER_HR as 4::11936 incorrect
    terms = RTM.find_terms('MDC_DIM_X_M_PER_HR', '4::11936')
    terms.each { |t| RTM.withdraw_term(t, 'Per review with Paul Schluter') }
    
    terms = RTM.find_terms('MDC_ECG_TIME_PD_PQ', '2::16144')
    terms.each { |t| RTM.deprecate_term(t, 'Per review with Paul Schluter') }
    
    terms = RTM.find_terms('MDC_ECG_TIME_PD_PQ_SEG', '2::16148')
    terms.each { |t| RTM.deprecate_term(t, 'Per review with Paul Schluter') }
  end
  
  def remove_bad_id_trig
    terms = RTM.find_terms('MDC_ID_TRIG', '2::53253')
    ChangeTracker.start
    puts "Destroying #{terms.count} terms for MDC_ID_TRIG 2::53253"
    terms.each { |t| t.destroy }
    ChangeTracker.commit
  end
  
  # def fix_MDC_PRESS_BLD_VENT_SYS_END
  #   terms = RTM.find_terms('MDC_PRESS_BLD_VENT_SYS', '2::19025')
  #   MDC_PRESS_BLD_VENT_SYS
  #   MDC_PRESS_BLD_VENT_SYS_END
  #   _END
    
    
  
end
