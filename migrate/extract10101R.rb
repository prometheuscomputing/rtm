#  NOTES #
=begin
  Currents skipping all tokens!  Need to modify model such that tokens are scoped.
=end
require 'rainbow'
require 'docx'
require 'open3'
# require_relative 'table_metadata'
load File.join(__dir__, 'fixes.rb')
load File.join(__dir__, 'migration_logger.rb')
load File.join(__dir__, 'term_extractor.rb')
load File.join(__dir__, '../scripts/rtm_tools.rb')
# load File.join(__dir__, 'rtm_registry.rb')
# load File.join(__dir__, '10101-2019_table_titles.rb')
load File.join(__dir__, 'table_metadata.rb')
load File.join(__dir__, 'extract_partitions.rb')
load File.join(__dir__, 'extract_discriminator_sets.rb')
load File.join(__dir__, 'extract_oo.rb')
load File.join(__dir__, 'extract_units.rb')
load File.join(__dir__, 'extract_typical_terms.rb')
load File.join(__dir__, 'extract_some_terms.rb')
load File.join(__dir__, 'extract_tokens.rb')
load File.join(__dir__, 'deprecate_terms.rb')
load File.join(__dir__, 'withdraw_terms.rb')
load File.join(__dir__, 'add_discriminators.rb')
load File.join(__dir__, '../lib/rtm/utils/expand_discriminators.rb')
load File.join(__dir__, 'synonyms.rb')
load File.join(__dir__, '../scripts/checks/checks.rb')

def extract_text(node)
  return node if node.is_a?(String)
  begin
    runs = node.xpath('.//w:r').reject do |run|
      superscripter = run.xpath('.//w:position').any? || run.xpath('.//w:vertAlign').any?
    end
    texts = runs.collect{ |run| run.xpath('.//w:t')}.flatten
    # puts texts;puts
    texts.map { |t| t.content }.join.strip
  rescue
    puts Rainbow(node.to_s).cyan
    raise
  end
end