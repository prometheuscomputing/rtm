  
  def table_by_index(index)
    @tables.find { |t| t[:index] == index }
  end
  
  def tables_report
    @tables.each do |t|
      puts "#{t[:title]} -- #{t[:headers]}"
    end
  end
  
  # def extract_docx_tables # from docx
  #   excluded_table_indices = [3,22,23,153]
  #   folder = File.join(__dir__, 'tables')
  #   node_names = []
  #   @tables = []
  #   table_counter = 238
  #   @docx.tables.each_with_index do |tbl, i|
  #     next if i < 242 # FIXME remove this after testing
  #     next if excluded_table_indices.include?(i)
  #     stuff = RTM.table_metadata[table_counter]
  #     entry = stuff ? {}.merge(stuff) : {}
  #     entry[:counter] = table_counter
  #     column_count = tbl.columns.count
  #     header_row = tbl.rows.find { |r| r.cells.count == column_count } # because some of the tables have a row at the top with the table caption in it.  this is such a PITA...
  #     entry[:headers] = header_row.cells.map do |cell|
  #       extract_text(cell.node)
  #     end
  #     puts Rainbow(i.to_s.rjust(3,'0') + ' ' + entry[:headers].inspect).cyan
  #     entry[:rows] = tbl.rows[1..].map do |row|
  #       row.cells.map do |cell|
  #         txt = extract_text(cell.node)  # problem --> no preservation of formatting! FIXME must use Pandoc to produce HTML version of this table (??) or maybe just the cell content
  #         # puts "  Cell: #{txt}"
  #         txt
  #       end
  #     end
  #     entry[:docx] = tbl.node
  #     File.open("#{folder}/table#{i.to_s.rjust(3,'0')}", 'w+') { |f| f.puts JSON.pretty_generate(entry) }
  #     @tables << entry
  #     table_counter += 1
  #   end
  #   @tables
  # end
  #

  def list_tables
    @tables[147..160].each_with_index do |t,i|
      puts "#{t[:index]} - #{t[:counter]}\n#{t[:title]}\n#{t[:headers]}\n\n"
    end
  end
  
  # FIXME goodness knows that synonym means more than one thing.  deal with it...
  # <252> ["Preferred RefId", "Part:Code", "RefId", "Part:Code"] - D.1.1.1-Term code synonyms :: page:1042
  # <253> ["Anatomic RefId", "Part:Code", "EEG auricular RefId", "Part:Code"] - D.1.1.2-Term code synonyms :: page:1042
  # <254> ["Preferred RefId", "Part:Code", "RefId", "Part:Code"] - D.1.2.1-RefId synonyms :: page:1043
  def synonym_tables
    [252, 253, 254]
  end
  
  # <99> ["UOM_MDC", "Deprecated UCODE10", "New UCODE10", "Note"] - A.6.5.1-Withdrawn terms for vital signs units of measurement :: page:122
  # <110> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.2.7.1-Withdrawn terms for ECG enumerations :: page:171
  # <164> ["Systematic name", "Common term", "Acronym", "Description", "RefId", "Part::Code"] - A.10.2.4.1-Withdrawn terms for device specialization :: page:522
  # <211> ["Description/Definition/Use", "RefId", "Part::Code"] - A.14.4.1-Withdrawn nomenclature for error return codes :: page:596
  
  def todo_tables
    # UNITS
    [98] + # A.6.4.1 - Vital signs units of measurement
    
    [103] + # A.7.1.3.2 - Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10101-2004 with discriminator group [LEAD1]
    [118] + # A.7.4.17.1 - Gas measurement sites
    [148] + # A.7.14.1.1 - Nomenclature and code extensions for personal health devices
    [154] + # A.8.5.6.1 - Nomenclature and codes for sites for EOG signal monitoring
    [164] + # A.10.2.3.1 - Nomenclature and codes for infrastructure, device specialization
    [211] + # A.14.3.1 - Nomenclature for error return codes
    [217] + # A.16.4.1 - Continua Services Interface information attributes
    [219] + # A.16.6.1 - Breathing circuit attributes
    [252] + # D.1.1.1 - Term code synonyms
    [253] + # D.1.1.2 - Term code synonyms
    [254] + # D.1.2.1 - RefId synonyms
    [256] # G.1 - Gas measurement sites and anesthesia breathing circuit components (normative)
  end
  

  
  
  
  # Look at preserving row order from standard. A.7.4.19.2 FIXME
  # preserve groupings
  def review_tables # REVIEW
    [104] + # A.7.1.3.3 - Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10102 with discriminator group [LEAD2] -- this is auxillary info for the stuff in C.3.15.1 -- add in JSON attr
    # FIXME
    [115] + # A.7.4.11.1 - Ventilator modes bit string
    
    [136] + # A.7.7.13.1-Pump modes bit string values (value to be communicated in enumeration observation element Mode | | Device | Pump)
    [137]  # A.7.7.19.1-Pump states bit string values (value to be communicated in EnumerationObservation with code: Status | Operational | Device | Pump)
  end
    
    
  # FIXME these may not all be the same kind of "deprecated".  Check and make sure.
  # <100> ["UOM_MDC", "Deprecated UCODE10", "New UCODE10", "Note"] - A.6.6.1-Deprecated terms for vital signs units of measurement :: page:123
  # <101> ["UOM_MDC", "UCODE10", "Note"] - A.6.7.1-Deprecated RefIds for vital signs units of measurement :: page:124
  # <107> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.1.7.1-Deprecated terms for ECG measurements :: page:156
  # <108> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.1.8.1-Deprecated RefIds for ECG measurements defined in IEEE11073-10102 :: page:156
  # <111> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.2.8.1-Deprecated terms for ECG enumerations :: page:171
  # <112> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.2.9.1-Deprecated RefIds for ECG enumerations :: page:172
  # <125> ["Interpretation", "Deprecated gas RefId prefix", "‘Unified’ gas RefId prefix"] - A.7.4.22.1-Recommended mapping of deprecated to ‘unified’ gas RefId prefixes (informative) :: page:259
  # <128> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.4.25.1-Deprecated nomenclature for undefined respiratory measurements from Annex B :: page:269
  # <143> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.11.7.1-Deprecated nomenclature for temperature :: page:357
  # <149> ["Systematic name", "Common term", "Acronym", "Description/Definition/Use", "RefId", "Part::Code"] - A.7.14.2.1-Deprecated RefIds for personal health devices :: page:376

  
=begin NOTES
  
  # FIXME so this means that there are synonyms!
  The ‘_X_’ may be omitted or included in the RefId if conveyed as a text string in a message according to guidelines of the protocol, e.g., as in OBX-6 of an HL7 V2 message.
  
  * Why are certain terms expanded?? e.g. pg. 966 MDC_DIM_BREATHS_PER_MIN_PER_MILLI_L | UoM (18)
  
  * Does the existence of a base term with a discriminator imply that all expansions are valid terms?

  
  Tables 3-20 are for partitions
  Tables 21-94 are for DIM
  Tables 
  
  
  typical: ["systematic name", "common term", "acronym", "description/definition", "refid", "part::code"] : [105, 106, 109, 113, 114, 116, 122, 123, 124, 126, 127, 129, 130, 131, 132, 133, 134, 135, 138, 139, 140, 141, 142, 145, 146, 147, 150, 151, 152, 153, 155, 156, 157, 158, 159, 160, 161, 162, 163, 165, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 212]
  
  typical w/o acronym: ["systematic name", "common term", "description/definition", "refid", "part::code"] : [1, 95, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209]
  

=end
  
=begin
# This covers 263 html tables -- the same number of tables that are reported from @docx.tables.count
# def extract_tables
#   # body = @html.at('body')
#   node_names = []
#   @tables = []
#   table_counter = 0
#   # body.children.each do |node|
#   @html.css('body table').each do |node|
#     if node.name != 'table'
#       puts "Skipping a #{node.name}:"# " #{extract_text(node)}"
#       puts node;puts
#       next
#     end
#     docx_table = @docx_tables[table_counter]
#     # puts docx_table.node.previous.name
#     table_counter += 1
#     title_node = node.previous
#     title_node = title_node.previous if title_node.name == 'text'
#     tt = nil
#     if title_node.name == 'table' # seriously gross and ugly way to do this.  If the thing above the current table is also a table then we're going to look inside of the current table for the title.  Pandoc just does not work all that well at all.  That is why this is so nasty.  Extracting all this from the raw docx maybe better.
#       tt = node.css('thead tr th').first.text
#     end
#     if ['p', 'h3'].include?(title_node.name)
#       tt = title_node.text
#     end
#     if tt
#       tt = tt.gsub(/(multipage table)/, '').strip
#       # next unless tt =~ /^Table/
#         # puts Rainbow(tt).orange
#       section = tt.slice(/(?<=Table).+(?=—)/)
#       title = tt.slice(/(?<=—).+/)
#       # elsif tt =~ /Table/
#       #   # puts Rainbow(tt).yellow
#       #   next # there is only one of these and it is an Example table
#       # else
#       #   # puts Rainbow(tt).red
#       #   next # there is only one of these and it is an Example table
#       # end
#       thead = node.at('thead')
#       headers = thead.css('tr > th').to_a.collect { |n| table_cell_text(n) } if thead
#       # HACK Well, this is unfortunate...  Pandoc's conversion of tables to docx tables is, for some reason, totally inconsistent in the resulting structure.  So, we're hacking our way out of it -- hopefully without botching things
#       if headers.nil?
#         headers = node.at('tbody').css('tr').to_a.first.css('td').collect { |cell| table_cell_text(cell) }
#       end
#       headers = headers.map { |h| h.gsub(/\n/, ' ') }
#       # html_rows = node.at('tbody').css('tr').to_a.collect do |row|
#       #   row.css('td').collect { |cell| table_cell_text(cell) }
#       # end
#       html_rows = node.at('tbody').css('tr').to_a.collect do |row|
#         row.css('td').collect { |td| td.inner_html }
#       end
#
#       docx_rows = docx_table.rows.map do |row|
#         row.cells.map { |c| c.node }
#       end
#
#       @tables << {:raw_title => tt, :extracted_title => title, :extracted_section => section, :headers => headers, :html_rows => html_rows, :xml => docx_table.node, :docx_rows => docx_rows, :html => node}
#     else
#       puts "Skipping because title node name is #{title_node.name}:"
#       puts title_node.to_xml;puts
#       next
#       # There are only 5 tables here and none of them are of consequence
#     end
#   end
#   # @tables.each { |entry| puts Rainbow(entry[:title]).green; entry[:docx_rows].each {|r| puts r.inspect} }
#   # @tables.each do |table|
#   #   table[:docx_rows].each do |row|
#   #     row.each { |cell| puts '  * ' + extract_text(cell) }
#   #     puts '___________'
#   #   end
#   # end
#   # puts @tables.count
#   # puts table_counter
#   tmd = table_metadata
#   @tables.each_with_index do |table, i|
#     table.merge!(tmd[i])
#   end
#   @tables
# end
=end