class TermExtractor
   
  def disc_tables_typical
    # ["doffset", "dsuffix", "ddescription"]
    [221, 222, 223, 224, 225, 226, 228, 229, 230, 231, 232, 233, 234, 235]
  end

  # NOTE this are mapping tables b/w different discriminator sets...we're not doing that right now, OK
  def disc_tables_leads
    # ecg leads: ["doffset", "[lead1] dsuffix", "[lead2] dsuffix"]
    # [236, 237]
  end

  def disc_tables_infix
    # ["doffset", "dinfix", "ddescription"]
    [227]
  end

  def parse_discriminators
    proc_discriminators = proc do |cells, table|
      offset, xfix, desc = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:desc, offset:offset.to_i, xfix:xfix}
      discrim = process_discriminator(args)
    end
    _parse_tables('Discriminators', disc_tables_typical, proc_discriminators)
    _parse_tables('Discriminators infix', disc_tables_infix, proc_discriminators)
  end
  
  def process_discriminator(description:, table:, offset:, xfix:)
    set_name = table.title.slice(/(?<=\[).+(?=\])/)
    set = RTM::DiscriminatorSet.where(:name => set_name).first
    raise unless set
    discriminator = set.discriminators.find { |d| d.offset == offset }
    unless discriminator
      ChangeTracker.start
      discriminator = RTM::Discriminator.create(:offset => offset)
      set.add_discriminator(discriminator)
      ChangeTracker.commit
    end
    ChangeTracker.start
    discriminator.value = xfix.to_s.gsub("\u00A0", "").strip
    discriminator.description = description.strip if description
    discriminator.save
    ChangeTracker.commit
  end
  
  def create_discriminator_sets
    # first, fix the name of 'mmm' and delete the one that already is named MMM
    mmm = RTM::DiscriminatorSet.where(:name => 'mmm').first
    ChangeTracker.start
    RTM::DiscriminatorSet.where(:name => 'MMM').first&.destroy
    if mmm
      mmm.name = 'MMM'
      mmm.save
    end
    ChangeTracker.commit
    # create DiscriminatorSets
    discriminator_set_info.each do |index, data|
      ChangeTracker.start
      set = RTM::DiscriminatorSet.where(:name => data[:name]).first
      if set
        # puts "Found #{set.name}"
      else
        set = RTM::DiscriminatorSet.create(:name => data[:name])
        # puts "Created #{set.name}"
      end
      set.allocation = data[:bits].to_i
      set.description = data[:description]
      set.type = (index == 231) ? 'infix' : 'suffix'
      set.save
      ChangeTracker.commit
    end
  end
    
  
  def discriminator_set_info
    {
     225=>{:name=>"MVC", :description=>"Device Type Discriminators", :bits=>"2"},
     226=>{:name=>"MMM", :description=>"Statistical Discriminators", :bits=>"2"},
     227=>
      {:name=>"SDM",
       :description=>"Haemodynamic pressure measurements",
       :bits=>"2"},
     228=>{:name=>"RCE", :description=>"Rates for countable events", :bits=>"3"},
     229=>
      {:name=>"RCN", :description=>"Rates for countable neuro events", :bits=>"3"},
     230=>
      {:name=>"LAT",
       :description=>"Body Site Orientation (laterality)",
       :bits=>"2"},
     231=>{:name=>"UoM", :description=>"Units of Measure", :bits=>"5"},
     232=>{:name=>"UoM1", :description=>"Unit of Measure (singular)", :bits=>"5"},
     233=>{:name=>"1", :description=>"No discriminator", :bits=>"0"},
     234=>{:name=>"EVT", :description=>"No discriminator", :bits=>nil},
     235=>
      {:name=>"PN3",
       :description=>"Statistical profile discriminator",
       :bits=>"4"},
     236=>{:name=>"LOC", :description=>"Location discriminator", :bits=>"5"},
     237=>{:name=>"64", :description=>"Version discriminator", :bits=>"6"},
     238=>
      {:name=>"LEAD1",
       :description=>"ECG lead discriminators from ISO/IEEE 11073-10101:2004",
       :bits=>"8"},
     239=>
      {:name=>"LEAD2",
       :description=>"ECG lead discriminators from ISO/IEEE 11073-10102:2012",
       :bits=>"8"}
    }
  end
  
  
  def extract_docx_discriminator_tables # from docx
    folder = __dir__
    table_counter = 238
    hash = {}
    @docx.tables.each_with_index do |tbl, i|
      next if i < 225 || i > 239
      stuff = RTM.table_metadata[table_counter]
      entry = stuff ? {}.merge(stuff) : {}
      title_row = tbl.rows.first
      title = extract_text(title_row.cells.first.node)
      unless title =~ /^\[/
        title_row = tbl.rows[1]
        title = extract_text(title_row.cells.first.node)
      end
      name = title.slice(/(?<=\[).+(?=\])/)
      bits = title.slice(/\d(?= bits)/)
      description = title.slice(/(?<=\]).*(?=\{)/).strip
      hash[i] = {:name => name, :description => description, :bits => bits}
      # File.open("#{folder}/discriminator_titles", 'w+') { |f| f.puts JSON.pretty_generate(entry) }
    end
    pp hash
  end
  
  
  
end