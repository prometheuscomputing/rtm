class TermExtractor
  # covered - 99 110 131 171 211
  # skip - 164 (already is fine)
  
  def withdrawn_tables
    [99, 110, 131, 164, 171, 211]
  end
  # covers A.7.1.7.1
  #
  def withdraw_terms
    fix_withdrawn_term_problems
    withdraw99
    withdraw110
    withdraw131
    withdraw171
    withdraw211
    withdraw164
  end
  
  def fix_withdrawn_term_problems
    return # FIXME, why is this here? Do I still need it?
    ChangeTracker.start
    RTM.get_terms('MDC_DIM_STEP', '4::11520').first.add_term_code(RTM.get_code_obj('4::6656'))
    RTM.get_terms('MDC_DIM_FOOT_PER_MIN', '4::11552').first.add_term_code(RTM.get_code_obj('4::6688'))
    RTM.get_terms('MDC_DIM_INCH_PER_MIN', '4::11584').first.add_term_code(RTM.get_code_obj('4::6720'))
    RTM.get_terms('MDC_DIM_STEP_PER_MIN', '4::11616').first.add_term_code(RTM.get_code_obj('4::6752'))
    RTM.get_terms('MDC_DIM_TICK', '4::11648').first.add_term_code(RTM.get_code_obj('4::6848'))
    RTM.get_terms('MDC_RET_CODE_UNKNOWN', '255::9999').first.add_term_code(RTM.get_code_obj('255::1'))
    ChangeTracker.commit
  end
  
  def withdraw99
    proc99 = proc do |cells, table|
      refid, ucode, replacement_ucode, note = cells.collect { |cell| extract_text(cell) }
      bad_code = '4::' + ucode.to_s.strip
      good_code = '4::' + replacement_ucode.to_s.strip
      note = note.gsub('104', '11073:104')
      note = "Replaced with #{replacement_ucode}.  #{note}"
      args = {type:RTM::Unit, table:table, refid:refid, bad_code:bad_code, good_code:good_code, note:note}
      withdraw_term(args)
    end
    _parse_tables('Table A.6.5.1—Withdrawn terms for vital signs units of measurement', [99], proc99)
  end
  
  def withdraw_term(type:nil, table:, refid:, bad_code:, good_code:nil, note:nil, good_refid:nil)
    if good_code
      good_terms = RTM.find_terms(good_refid || refid, good_code)
      unless good_terms.any?
        RTM.warn("Good Term #{good_refid || refid} #{good_code} is missing while withdrawing #{refid} #{bad_code}.  Creating it now.", :blue)
        ChangeTracker.start
        good_term = RTM.create_term(good_refid || refid, good_code)
        good_term.status = 'Published'
        good_term.save
        ChangeTracker.commit
      end
    end
    bad_terms  = RTM.find_terms(refid, bad_code)
    RTM.warn("Multiple Bad Terms must be withdrawn for #{refid} #{bad_code}", :yellow) if bad_terms.count > 1
    if bad_terms.empty?
      RTM.info("Bad Term not found for withdrawn: #{refid} #{bad_code}")
      ChangeTracker.start
      bad_terms << RTM.create_term(refid, bad_code)
      ChangeTracker.commit
    end
    bad_terms.each { |bt| RTM.withdraw_term(bt, note) }
  end
  
  def withdraw110
    wtables = [110]
    proc110 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, note:description, refid:refid, bad_code:part_code}
      withdraw_term(args)
    end
    _parse_tables('Withdrawn Terms', wtables, proc110)
  end
  
  def withdraw131
    wtables = [131]
    proc131 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, note:description, refid:refid, bad_code:part_code}
      term = withdraw_term(args)
    end
    _parse_tables('A.7.5.7.1—Withdrawn terms for blood-gas, blood, urine, and other fluid chemistry measurements', wtables, proc131)
  end
  
  def withdraw171
    wtables = [171]
    proc171 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      good_code = description.slice(/\d+::\d+/)
      good_refid = description.slice(/MDC_[A-Z0-9_]+/)
      args = {table:table, note:description, refid:refid, bad_code:part_code, good_refid:good_refid, good_code:good_code}
      term = withdraw_term(args)
    end
    _parse_tables('Table A.11.5.4.1—Withdrawn terms for PHD Disease Management, SABTE sensors and settings', wtables, proc171)
  end
  
  def withdraw164
    wtables = [164]
    proc171 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      good_code = '8::4169'
      args = {table:table, note:description, refid:refid, bad_code:part_code, good_code:good_code}
      term = withdraw_term(args)
    end
    _parse_tables('Table A.10.2.4.1—Withdrawn terms for device specialization', wtables, proc171)
  end
  
  def withdraw211
    proc211 = proc do |cells, table|
      note, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, refid:refid, bad_code:part_code, note:note, good_code:'255::9999'}
      term = withdraw_term(args)
    end
    _parse_tables('Table A.14.4.1—Withdrawn nomenclature for error return codes', [211], proc211)
  end
    
end

