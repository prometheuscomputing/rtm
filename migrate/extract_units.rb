class TermExtractor
  # ["Dimension(not normative)", "Dimensionality(not normative)", "Unit of measurement (not normative)", "Symbol(not normative)", "UoM UCUM(not normative)", "RefId", "Part::Code"]
  def parse_units
    process = proc do |cells, table|
      dimension, dimensionality, uom, symbol, ucum, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, dimension:dimension, dimensionality:dimensionality, symbol:symbol, uom:uom, ucum:ucum, refid:refid, part_code:part_code, type:RTM::Unit}
      unit = process_term(args)
    end
    _parse_tables('Units', [98], process)
  end
end
