require 'json'
require_relative 'term_migrator'

class TermExtractor
  include TermMigrator
  def html
    @html
  end
  def docx
    @docx
  end
  def docx_tables
    @docx_tables
  end
  def tables
    @tables
  end
  def initialize(docx, html)
    @docx = docx
    @docx_tables = docx.tables
    @html = File.open(html) { |f| Nokogiri::XML(f) }
  end
  
  def get_table_from_index(index)
    found_tables = @tables.select { |t| t[:counter] == index }
    if found_tables.count > 1
      found_tables.each { |tbl| puts "Found table w/ index #{tbl[:index]}"}
      raise "Found too many tables where :counter was #{index}"
    end
    raise "No table where :counter was #{tables_index}" if found_tables.count < 1
    t = found_tables.first
    section = t[:table] + ' - ' + t[:title]
    headers = t[:headers]
    table = RTM.get_10101R_table(section, t[:headers])
  end
  
  def _parse_tables(label, table_indicies, inner_proc)
    puts Rainbow("Begin #{label} Tables: #{table_indicies}").cyan
    table_indicies.each do |tables_index|
      found_tables = @tables.select { |t| t[:counter] == tables_index }
      if found_tables.count > 1
        found_tables.each { |tbl| puts "Found table w/ index #{tbl[:index]}"}
        raise "Found too many tables where :counter was #{tables_index}"
      end
      raise "No table where :counter was #{tables_index}" if found_tables.count < 1
      t = found_tables.first
      section = t[:table] + ' - ' + t[:title]
      puts Rainbow("#{tables_index.to_s.rjust(3,'0')} Parsing '#{section}'.").blue
      # headers = t[:rows][0].collect { |cell| extract_text(cell) }
      headers = t[:headers]
      # puts headers.inspect
      rows    = t[:rows]#[1..-1]
      if rows.count < 1
        puts Rainbow(section + ' has no data.').yellow
        next
      end
      if rows.count == 1 && rows[0].count == 1
        words = extract_text(rows[0][0])
        next if words =~ /This table has no content/
        RTM.error("Problem parsing #{label} table with words: " + words, :red)
        next
      end
      # if rows.count == 1
      #   puts Rainbow(rows.first.inspect).magenta
      # end
      table = RTM.get_10101R_table(section, t[:headers])
      @current_tag = nil
      # puts Rainbow("#{title} -- #{section}").orange
      rows.each { |cells| inner_proc.call(cells, table) }
      t[:extracted] = true
    end      
  end
  
  # these were changed in v1 so we have to go back and create refID objects to associate to the definition so we can then create a record of the fact that there is, in fact, a deprecated refID.
  def deprecated_terms_resolved_in_v1
    ['MDC_DIM_KILO_CAL', 'MDC_DIM_PERTHOUSAND', 'MDC_DIM_RPM', 'MDC_DIM_X_YARD', 'MDC_DIM_X_FOOT', 'MDC_DIM_X_INCH', 'MDC_DIM_SQ_X_INCH', 'MDC_DIM_X_LB', 'MDC_DIM_X_OZ', 'MDC_PER_GRAM', 'MDC_DIM_HZ', 'MDC_ECG_TIME_PD_P_GL', 'MDC_ECG_TIME_PD_P', 'MDC_ECG_TIME_PD_PP', 'MDC_ECG_TIME_PD_QRS', 'MDC_ECG_TIME_PD_QT', 'MDC_VENT_CONC_CO2', 'MDC_VENT_CONC_CO2_EXP', 'MDC_VENT_CONC_CO2_INSP', 'MDC_VENT_CONC_O2', 'MDC_VENT_CONC_O2_EXP', 'MDC_VENT_CONC_O2_INSP', 'MDC_ID_TRIG', 'MDC_DIM_STEP', 'MDC_DIM_FOOT_PER_MIN', 'MDC_DIM_INCH_PER_MIN', 'MDC_DIM_STEP_PER_MIN', 'MDC_DIM_TICK']
  end
    
  def table_cell_text(node)
    i = ''
    node.children.each do |cnode|
      puts Rainbow(cnode.name).red unless ['p', 'text', 'blockquote', 'strong', 'em', 'span', 'br'].include?(cnode.name)
      if cnode.name == 'blockquote' && cnode.children.any?
        cnode.css('p').each { |pnode| i << _inner_html_or_text(pnode) }
      else
        i << _inner_html_or_text(cnode)
      end
    end
    i
  end
  
  def _inner_html_or_text(node)
    node_name = node.name
    ret = node.inner_html
    ret = node.text if ret.empty?
    puts Rainbow(node_name) + ': ' + ret.inspect unless ['p', 'text', 'strong', 'em', 'span', 'br'].include?(node_name)
    ret
  end
  
  def load_tables
    return @tables if @tables&.any?
    @tables = []
    folder = File.join(__dir__, 'tables')
    (0..261).each do |index|
      # next if excluded_table_indices.include?(index)
      file = File.join(folder, "table#{index.to_s.rjust(3,'0')}")
      next unless File.exists?(file)
      tbl = JSON.parse(File.read(file), :symbolize_names => true)
      # next unless RTM.table_metadata[index]
      # tbl[:title] = RTM.table_metadata[index][:title]
      # tbl[:page]  = RTM.table_metadata[index][:page]
      # tbl[:table] = RTM.table_metadata[index][:table]
      tbl[:index] = index
      @tables << tbl
    end
    @tables.reject! { |t| skip_tables.include?(t[:counter]) }
    @tables
  end
  
  def skip_tables
    [96] + # A.6.2.1 - Table of decimal factors -- # These are duplicated in discriminator tables....
    [97] + # A.6.3.1 - Non-SI units
    [102] + # A.7.1.3.1 - List of standardized ECG <lead> descriptors from SCP‑ECG
    [117] + # A.7.4.16.1 - Correction of gas measurements
    [119] + # A.7.4.17.2 - Default gas measurement sites
    [120] + # A.7.4.18.1 - Inspiratory breath type classifications
    [121] + # A.7.4.19.1 - Deployment of gas partial pressure and concentration and consumption information (informative)
    [125] + # A.7.4.22.1 - Recommended mapping of deprecated to ‘unified’ gas RefId prefixes (informative)
    [144] + # A.7.11.8.1 - Body weight and surface area for pre-coordinated RefIds
    [255] # E.1 - Inspiratory breath and inflation types and rates
    [257] # J.1-Revision history
  end
end
