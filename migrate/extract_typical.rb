class TermExtractor

  # # ["systematic name", "common term", "acronym", "description/definition", "reference id", "part::code"]
  # def parse_typical_tables1
  #   process = proc do |cells, table|
  #     sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
  #     term = process_term(table:table, sys_name:sys_name, common_term:common_term, acronym:acronym, description:description, refid:refid, part_code:part_code)
  #   end
  #   table_indices = [103, 104, 107, 111, 112, 114, 120, 121, 122, 124, 125, 127, 128, 129, 130, 131, 132, 133, 136, 137, 138, 139, 140, 143, 144, 145, 148, 149, 150, 151, 153, 154, 155, 156, 157, 158, 159, 160, 161, 163, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 210]
  #   _parse_tables('Typical 1', table_indices, process)
  # end
  #
  # # ["systematic name", "common term", "description/definition", "reference id", "part::code"]
  # def parse_typical_tables2
  #   process = proc do |cells, table|
  #     sys_name, common_term, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
  #     term = process_term(table:table, sys_name:sys_name, common_term:common_term, description:description, refid:refid, part_code:part_code)
  #   end
  #   table_indices = [93, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207]
  #   _parse_tables('Typical 2', table_indices, process)
  # end
  #
  # # ["systematic name", "common term", "acronym", "description/definition/use", "reference id", "part::code"]
  # # This is the same as typical 1 except the description header is slightly different
  # def parse_typical_tables3
  #   process = proc do |cells, table|
  #     sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
  #     term = process_term(table:table, sys_name:sys_name, common_term:common_term, acronym:acronym, description:description, refid:refid, part_code:part_code)
  #   end
  #   table_indices = [105, 106, 108, 109, 110, 126, 141, 147]
  #   _parse_tables('Typical 3', table_indices, process)
  # end
  #
  # def parse_typical
  #   parse_typical_tables1
  #   parse_typical_tables2
  #   parse_typical_tables3
  # end
  
end