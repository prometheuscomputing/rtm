class TermExtractor
  attr_accessor :processed_tables
   
  def tables_group1
    # ["description/definition", "refid", "part::code"]
    [166, 210, 215, 216]
  end
  
  def tables_with_mnemonic
    # ["mnemonic", "description/definition", "refid", "part::code"]
    [213, 214]
  end

  def parse_group1
    process = proc do |cells, table|
      description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, refid:refid, part_code:part_code}
      term = process_term(args)
    end
    _parse_tables('Group 1', tables_group1, process)
  end
  
  def parse_mnemonics
    process = proc do |cells, table|
      mnemonic, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      aux = {:mnemonic => mnemonic}.to_json
      args = {table:table, description:description, refid:refid, part_code:part_code, aux:aux}
      term = process_term(args)
    end
    _parse_tables('Mnemonics', tables_with_mnemonic, process)
  end
  
  def create_program_group_terms
    table = RTM.get_10101R_table('A.2.4.6.1—Partition 6—Program group', ['RefId', 'Part::Code'])
    pg_terms = [[ 'MDC_PGRP_HEMO', 513], [ 'MDC_PGRP_ECG', 514], [ 'MDC_PGRP_RESP', 515], [ 'MDC_PGRP_VENT', 516], [ 'MDC_PGRP_NEURO', 517], [ 'MDC_PGRP_DRUG', 518], [ 'MDC_PGRP_FLUID', 519], [ 'MDC_PGRP_BLOOD_CHEM', 520], [ 'MDC_PGRP_MISC', 521]]
    pg_terms.each do |pg_refid, pg_tc|
      pc = '6::' + pg_tc.to_s
      process_term(refid:pg_refid, part_code:pc, table:table)
    end
  end
  
  def parse103
    process = proc do |cells, table|
      common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, refid:refid, part_code:part_code, acronym:acronym, aux:{lead:1}.to_json, common_term:common_term}
      term = process_term(args)
    end
    _parse_tables('ECG LEAD1 Terms', [103], process)
  end
  
  def parse_gas_msmt_sites
    process = proc do |cells, table|
      sys_name, key, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      args = {sys_name:sys_name, table:table, description:description, refid:refid, part_code:part_code, acronym:acronym, aux:{key:key}.to_json}
      term = process_term(args)
    end
    _parse_tables('Table A.7.4.17.1—Gas measurement sites', [118], process)
  end
  
  def parse148
    process = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code, source = cells.collect { |cell| extract_text(cell) }
      source = '11073:' + source unless source.to_s.strip.empty?
      args = {table:table, sys_name:sys_name, common_term:common_term, acronym:acronym, description:description, refid:refid, part_code:part_code, aux:{source:source}.to_json}
      term = process_term(args)
    end
    _parse_tables('Table A.7.14.1.1—Nomenclature and code extensions for personal health devices', [148], process)
  end
  
  def parse217
    process = proc do |cells, table|
      attr_name, description, refid, part_code, source = cells.collect { |cell| extract_text(cell) }
      args = {table:table, description:description, refid:refid, part_code:part_code, aux:{attribute_name:attr_name, source:source}.to_json}
      term = process_term(args)
    end
    _parse_tables('Table A.16.5.1—IHE PCD and Continua Services Interface timekeeping information attributes', [217], process)
  end
  
  def parse219
    process = proc do |cells, table|
      attr_name, refid, attr_type, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, refid:refid, part_code:part_code, aux:{attribute_name:attr_name, attribute_type:attr_type}.to_json}
      term = process_term(args)
    end
    _parse_tables('Table A.16.6.2—Calculation method attribute', [219], process)
  end
  
  def parse256
    process = proc do |cells, table|
      refid, key1, key2, description, part_code = cells.collect { |cell| extract_text(cell) }
      fks = [key1, key2].compact.join(', ')
      args = {
        table:table,
        description:description,
        refid:refid,
        part_code:part_code,
        aux:({figure_keys:fks}.to_json)
      }
      next unless part_code
      term = process_term(args)
    end
    _parse_tables('Table G.1—Gas measurement sites and anesthesia breathing circuit components', [256], process)
  end
  
  def parse102
    process = proc do |cells, table|
      lead, refid, scp_code = cells.collect { |cell| extract_text(cell) }
      aux = {:lead => lead, :scp_code => scp_code}
      args = {refid:refid, aux:aux}
      add_ecg_lead_descriptors(args)
    end
    _parse_tables('List of standardized ECG <lead> descriptors from SCP-ECG', [102], process)
  end
  
  def add_ecg_lead_descriptors(refid:, aux:)
    return if refid == 'RefId' || refid.empty?
    r = RTM.find_refid(refid.strip)
    raise "Can't find #{refid.inspect}" unless r
    terms = r.terms
    terms.each do |term|
      ChangeTracker.start
      unless term.definition
        term.definition = RTM::Definition.create
      end
      # puts term.stat unless term.definition
      term.definition.update_aux(aux)
      ChangeTracker.commit
    end
  end
end