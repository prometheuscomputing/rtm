# does refid deprecation imply synonymization?? TODO find out
class TermExtractor
  def deprecated_tables
    # covered - 86 100 101 107 111 112 114 123 124 127 128 132 135 143 149 161 162 191 196
    # skip 125
    [86, 100, 101, 107, 108, 111, 112, 114, 123, 124, 125, 126, 127, 128, 132, 135, 143, 149, 161, 162, 191, 196, 200]
  end
  # covers A.7.1.7.1
  #
  def deprecate_terms
    deprecate86
    deprecate100
    deprecate101
    deprecate108
    deprecate114
    deprecate196
    # deprecate111 # handled in a different way now
  end

  def deprecate86
    proc86 = proc do |cells, table|
      description, refid, note, part_code = cells.collect { |cell| extract_text(cell) }
      args = {table:table, note:note, bad_refid:refid, part_code:part_code}
      args[:alt_code] = '1::2540' if part_code.strip == '1::2451'
      args[:alt_code] = '1::2457' if part_code.strip == '1::2456'
      args[:alt_code] = '1::2458' if part_code.strip == '1::2459'
      deprecate_refid(args)
    end
    _parse_tables('Table A.3.2.14.1—Object-oriented modeling elements—Deprecated identifier terms', [86], proc86)
  end
  
  def deprecate_refid(table:, bad_refid:, part_code:, alt_code:nil, note:nil)
    deprecate_bad_terms(bad_refid, part_code, note)
  
    good_refids = note.to_s.scan(/MDC_[A-Z0-9_]+/).reject { |str| str == bad_refid }
    alt_code ||= note.to_s.scan(/\(\d?:?:?\d+\)/).first&.delete('()') || note.to_s.scan(/21052\)|21108\)/).first&.delete('()') || part_code
    alt_code = part_code.gsub(/::\d+/, "::#{alt_code}") unless alt_code =~ /::/
    # good_refids.each { |gr|  puts "Use #{gr} #{alt_code} instead of #{bad_refid} #{part_code}" }
    good_refids.each do |good_refid|
      publish_missing_good_terms(good_refid, bad_refid, alt_code, part_code)
    end
  end

  def deprecate_10101R_term(table:, refid:, bad_code:, good_code:, good_refid:nil, note:nil)
    deprecate_bad_terms(refid, bad_code, note)
    publish_missing_good_terms(good_refid || refid, refid, good_code, bad_code)
  end

  def publish_missing_good_terms(good_refid, bad_refid, good_code, bad_code)
    terms = RTM.find_terms(good_refid, good_code)
    unless terms.any?
      RTM.warn("Good Term #{good_refid} #{good_code} is missing while deprecating #{bad_refid} #{bad_code}.  Creating it now.", :yellow)
      ChangeTracker.start
      term = RTM.create_term(good_refid, good_code)
      term.status = 'Published'
      term.save
      ChangeTracker.commit
    end
  end

  def deprecate_bad_terms(refid, code, note = nil)
    code = '2::53252' if refid == 'MDC_ID_TRIG' # fixing error in 10101R
    terms = RTM.find_terms(refid, code)
    RTM.warn("Multiple Bad Terms must be deprecated for #{refid} #{code}", :yellow) if terms.count > 1
    unless terms.any?
      term = get_existing_term(refid:refid, part_code:code)
      if term
        terms = [term]
      else
        RTM.info("Bad Term not found for deprecation: #{refid} #{code}")
        ChangeTracker.start
        terms << RTM.create_term(refid, code)
        ChangeTracker.commit
      end
    end
    terms.each { |t| RTM.deprecate_term(t, note) }
  end
  
  def deprecate101
    proc101 = proc do |cells, table|
      refid, ucode, note = cells.collect { |cell| extract_text(cell) }
      part_code = '4::' + ucode.to_s.strip
      note.gsub!('MDC_PER', 'MDC_DIM_PER')
      args = {table:table, bad_refid:refid, part_code:part_code, note:note}
      deprecate_refid(args)
    end
    _parse_tables('Table A.6.7.1—Deprecated RefIds for vital signs units of measurement', [101], proc101)
  end
  
  def fix_deprecation_problems(refid_obj)
    val = refid_obj.value
    substitute_val = case val
    # when 'MDC_DIM_DS_PER_M_SQ_PER_CM5'
    #   'MDC_DIM_DYNE_SEC_PER_M_SQ_PER_CM_5'
    when 'MDC_DIM_X_G_PER_MILLI_G'
      'MDC_DIM_X_G_PER_MG'
    # when 'MDC_DIM_MILLI_G_PER_MILLI_G'
    #   'MDC_DIM_MILLI_G_PER_MG'
    end
    if substitute_val
      puts "Subbing #{val} with #{substitute_val}"
      sub_refid_obj = RTM.find_or_create_refid(substitute_val)
      sub_defns = sub_refid_obj.definitions
      raise if sub_defns.count > 1
      return nil unless sub_defns.any?
      sub_defn = sub_defns.first
      extra_defns = refid_obj.definitions
      raise if extra_defns.count > 1 # we probably already checked this in the calling method...but anyway
      extra_defn = extra_defns.first
      merged = RTM.combine_definitions(sub_defn, extra_defn)
      # puts "Cool #{val}"
      [merged]
    end
  end
  
  def create_missing_deprecation_table_terms
    process_term(:refid => 'MDC_ECG_SV_P_C_FREQ', :part_code => '2::17316', :acronym => 'FSPVC', :systematic_name => 'Pattern | Extrasystoles, Contraction, SupraVentricular, Premature, Frequent | ECG, Heart | CVS', :table => get_table_from_index(112))
  end
  
  def deprecate108 # and others
    # create_missing_deprecation_table_terms
    dtables = [108, 112, 124, 127, 132, 135, 149, 162]
    proc108 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      part_code = '2::17136' if part_code.strip == '2::17316'
      args = {table:table, note:description, bad_refid:refid, part_code:part_code}
      next if refid == 'RefId'
      term = deprecate_refid(args)
    end
    _parse_tables('Deprecated RefIds', dtables, proc108)
  end
    
  def deprecate114 # both -- also others
    dtables = [107, 111, 114, 123, 126, 128, 143, 161, 191]
    proc114 = proc do |cells, table|
      sys_name, common_term, acronym, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      description = description.sub('MDC_PRESS__BLD_NONINV_MEAN', 'MDC_PRESS_BLD_NONINV_MEAN')
      args = {table:table, note:description, bad_refid:refid, part_code:part_code}
      deprecate_refid(args)
    end
    _parse_tables('Deprecated terms', dtables, proc114)
  end
  
  def deprecate196 # both
    dtables = [196, 200]
    proc114 = proc do |cells, table|
      sys_name, common_term, description, refid, part_code = cells.collect { |cell| extract_text(cell) }
      next if refid == 'RefId'
      description = description.sub('MDC_PRESS__BLD_NONINV_MEAN', 'MDC_PRESS_BLD_NONINV_MEAN')
      good_code   = description.scan(/\d+::?\d+/).map { |c| c.gsub(/:+/, '::')}.reject { |c| c == part_code }.first
      good_code ||= description.scan(/\(\d+\)/).map { |c| '129::' + c.delete('()') }.first
      good_refid  = description.scan(/MDC_[A-Z_]+/).first
      args = {table:table, note:description, refid:refid, bad_code:part_code, good_code:good_code, good_refid:good_refid}
      term = deprecate_10101R_term(args)
    end
    _parse_tables('Deprecated terms', dtables, proc114)
  end
  
  def table100_good_replacement_codes
    {
      'MDC_DIM_KG_PER_M_SQ' => 'MDC_DIM_KILO_G_PER_M_SQ',
      'MDC_DIM_X_DYNE_PER_SEC_PER_CM5' => 'MDC_DIM_DYNE_SEC_PER_CM_5',
      'MDC_DIM_PERTHOUSAND' => 'MDC_DIM_PARTS_PER_10_TO_3',
      'MDC_DIM_RPM' => 'MDC_DIM_ROTATIONS_PER_MIN'
    }
  end
  
  # Not needed but keeping it around for now
  def table101_good_replacement_codes
    {
      'MDC_DIM_PARTS_PER_10_TO_MINUS_3' => 'MDC_DIM_PARTS_PER_10_TO_3',
      'MDC_DIM_PARTS_PER_10_TO_MINUS_6' => 'MDC_DIM_PARTS_PER_10_TO_6',
      'MDC_DIM_PARTS_PER_10_TO_MINUS_9' => 'MDC_DIM_PARTS_PER_10_TO_9',
      'MDC_DIM_PARTS_PER_10_TO_MINUS_12' => 'MDC_DIM_PARTS_PER_10_TO_12',
      'MDC_DIM_PARTS_PER_10_TO_MINUS_18' => 'MDC_DIM_PARTS_PER_10_TO_18',
      'MDC_DIM_X_YARD' => 'MDC_DIM_YARD',
      'MDC_DIM_X_FOOT'=> 'MDC_DIM_FOOT',
      'MDC_DIM_X_INCH' => 'MDC_DIM_INCH',
      'MDC_DIM_SQ_X_INCH' => 'MDC_DIM_SQ_INCH',
      'MDC_DIM_X_LB' => 'MDC_DIM_LB',
      'MDC_DIM_X_OZ' => 'MDC_DIM_OZ',
      'MDC_PER_GRAM' => 'MDC_PER_G',
      'MDC_DIM_HZ' => 'MDC_DIM_X_HZ',
      'MDC_DIM_DS_PER_M_SQ_PER_CM5' => 'MDC_DIM_DYNE_SEC_PER_M_SQ_PER_CM_5',
      'MDC_DIM_X_G_PER_MILLI_G' => 'MDC_DIM_X_G_PER_MG',
      'MDC_DIM_MILLI_G_PER_MILLI_G' => 'MDC_DIM_MILLI_G_PER_MG'
    }
  end
  
  def deprecate100
    proc100 = proc do |cells, table, opts|
      refid, ucode, replacement_code, note = cells.collect { |cell| extract_text(cell) }
      bad_code  = '4::' + ucode.to_s.strip
      good_code = '4::' + replacement_code.to_s.strip
      good_refid =  table100_good_replacement_codes[refid] || refid
      args = {table:table, refid:refid, bad_code:bad_code, good_code:good_code, note:note}
      args[:good_refid] = good_refid if good_refid
      term = deprecate_10101R_term(args) unless refid == 'UOM_MDC'
    end
    _parse_tables('Table A.6.6.1—Deprecated terms for vital signs units of measurement', [100], proc100)
  end

  
  # def synonymize_refids
  #   # [<preferred>, <synonym>]
  #   # FIXME if you need to...
  #   synonyms = [
  #     ['MDC_ECG_HEART_RATE', 'MDC_ECG_CARD_BEAT_RATE']
  #   ]
  # end
end

