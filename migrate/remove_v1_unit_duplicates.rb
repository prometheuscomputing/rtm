class TermExtractor
  def remove_v1_unit_duplicates
    refid_vals = ['MDC_DIM_KILO_PASCAL_PER_L', 'MDC_DIM_MILLI_L_PER_HPA']
    refid_vals.each do |refid_val|
      refid = RTM.find_refid(refid_val)
      refid.terms.each do |term|
        published, unpublished = refid.terms.partition { |t| t.status&.value =~ /Published/ }
        if published.count > 1
          pcodes = published.map(&:term_code)
          if pcodes == pcodes.uniq
            # These are term code synonyms
          else
            published.each do |term|
              if term.is_a?(RTM::Unit) && term.status&.value =~ /from RTMMSv1 coded term table/
                if term.ucum_units.empty?
                  puts term.summary(false)
                  if term.terms.count == 0 && term.unit_groups.count == 0
                    puts "DESTROYING extra #{term._stat}"
                    ChangeTracker.start
                    term.destroy
                    ChangeTracker.commit
                  else
                    puts Rainbow("NOT DESTROYED").red
                  end
                end
                next
              end
              puts term.summary(false)
            end
            puts '*'
          end
        end
      end
    end
  end
end
