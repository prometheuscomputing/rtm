class TermExtractor

  # ["systematic name", "common term", "description/definition", "bit string"]
  def parse_token_tables
    process = proc do |cells, table|
      sys_name, common_term, description, token = cells.collect { |cell| extract_text(cell) }
      term = process_token(table:table, sys_name:sys_name, common_term:common_term,  description:description, value:token)
    end
    table_indices = [136, 137]
    _parse_tables('Tokens', table_indices, process)
    
    process = proc do |cells, table|
      sys_name, common_term, acronym, description, token = cells.collect { |cell| extract_text(cell) }
      term = process_token(table:table, sys_name:sys_name, common_term:common_term,  description:description, value:token, acronym:acronym)
    end
    table_indices = [115]
    _parse_tables('Ventilator modes bit string', [115], process)
  end
end
