def term_code_matches?(existing_def, code, refid)
  existing_codes = existing_def.term_codes.map(&:code)
  if existing_codes.include?(code.to_i)
    true
  else
    RTM.error("Existing definition with database id #{existing_def.id}, refids '#{existing_def.refids.map(&:value)}' and term codes: #{existing_def.term_codes.map(&:code)} has a different term code than 10101R term with refid:'#{refid}' and term_code: #{code}!", :red)
    false
  end
end
def partition_matches?(existing_def, ptnum, refid, part_code = nil)
  existing_ptnum = existing_def.term_codes.map(&:partition).map(&:number).uniq
  if existing_ptnum.count > 1
    msg = "Existing definition with id #{existing_def.id} and refid '#{existing_def.refids.map(&:value)}' maps to multiple paritions!"
    msg << " part_code was #{part_code}." if part_code
    RTM.error(msg, :red)
    return false
  end
  if existing_ptnum.first.to_i == ptnum.to_i
    true
  else
    msg = "Existing definition with id #{existing_def.id} and refid '#{existing_def.refids.map(&:value)}' in partition #{existing_ptnum.first} maps to a different partition than 10101R term #{refid} which is in parition #{ptnum}!"
    msg << " part_code was #{part_code}." if part_code
    RTM.error(msg, :red)
    false
  end
end

def fix_rtm_blocks
  ChangeTracker.start
  b = RTM::Block[53]; b.upper = 1280;b.save
  b = RTM::Block[54]; b.upper = 1536;b.save
  b = RTM::Block[55]; b.upper = 2048;b.save
  b = RTM::Block[56]; b.upper = 2304;b.save
  b = RTM::Block[57]; b.upper = 3073;b.save
  b = RTM::Block[58]; b.upper = 3327;b.save
  b = RTM::Block[59]; b.upper = 4095;b.save
  b = RTM::Block[60]; b.upper = 61439;b.save
  ChangeTracker.commit
  parts = RTM::Partition.all
  parts.each do |part|
    puts Rainbow("#{part.number} -- #{part.name.value} -- #{part.description}").orange
    part.blocks.each do |block|
      puts "  #{block.id} --> #{block.name}: #{block.lower} - #{block.upper} -- #{block.description}"
    end
  end
end
