# does refid deprecation imply synonymization?? TODO find out
class TermExtractor
  
  def parse_synonyms
    parse252
    parse253
    parse254
  end
  
  # this is for a single refid that has two term codes.  really, we're manually fixing things here
  def parse252
    mdc_resp_rate = RTM.find_refid('MDC_RESP_RATE')
    term = RTM.find_terms('MDC_RESP_RATE','2::20482').first
    unless term
      ChangeTracker.start
      term = RTM::Term.create
      term.refid = mdc_resp_rate
      term.term_code = RTM.find_or_create_code_in_partition('2::20482')
      ChangeTracker.commit
    end
    proc252 = proc do |cells, table|      
      refid1, part_code1, refid2, part_code2 = cells.collect { |cell| extract_text(cell) }
      preferred_terms = RTM.find_terms(refid1, part_code1)
      raise if preferred_terms.empty?
      raise if preferred_terms.count > 1
      non_preferred_terms = RTM.find_terms(refid2, part_code2)
      raise if non_preferred_terms.empty?
      raise if non_preferred_terms.count > 1
      pt  = preferred_terms.first
      npt = non_preferred_terms.first
      ChangeTracker.start
      pt.code_preferred = true; pt.status = 'Published'
      pt.save
      puts Rainbow("#{npt.part_code} should not be preferred.").yellow if npt.code_preferred
      ChangeTracker.commit
      puts '  ' + pt._stat + Rainbow(' CODE preferred over ').yellow + npt._stat
    end
    _parse_tables('Term code synonyms', [252], proc252)
  end
  
  def parse253
    proc253 = proc do |cells, table|
      # next# Nothing to do here.  These aren't synonyms, they are homonyms.
      refid1, part_code1, refid2, part_code2 = cells.collect { |cell| extract_text(cell) }
      preferred_terms = RTM.find_terms(refid1, part_code1)
      raise if preferred_terms.empty?
      raise if preferred_terms.count > 1
      non_preferred_terms = RTM.find_terms(refid2, part_code2)
      raise if non_preferred_terms.empty?
      raise if non_preferred_terms.count > 1
      pt  = preferred_terms.first
      npt = non_preferred_terms.first
      next # neither is actually preferred over the other.  This is just telling us that they are synonyms.  TODO probably add the notes in at some point
      # ChangeTracker.start
      # preferred_terms.each { |t| t.code_status = 'Preferred'; t.status = 'Published' }
      # non_preferred_terms.each {|t| puts Rainbow(t.code_status).yellow unless t.code_status.nil? }
      # ChangeTracker.commit
      # puts "#{preferred_terms.map {|t| t.refid_stat + ' ' + t.code_stat}} is preferred over #{non_preferred_terms.map {|t| t.refid_stat + ' ' + t.code_stat}}"
    end
    _parse_tables('Term code homonyms', [253], proc253)
    puts "These are simply synonyms.  One is not preferred over the other."
  end
  
  def parse254
    proc254 = proc do |cells, table|
      refid1, part_code1, refid2, part_code2 = cells.collect { |cell| extract_text(cell) }
      refid2 = refid2.sub('(n.b. these are synonymous)', '').strip
      
      if part_code1 != part_code2
        RTM.error("#{refid1} #{part_code1} and #{refid2} #{part_code2} are not simply RefID synonyms.  Skipping them for now!", :red)
        next
      end
      
      preferred_terms = RTM.find_terms(refid1, part_code1)
      raise if preferred_terms.empty?
      raise if preferred_terms.count > 1
      non_preferred_terms = RTM.find_terms(refid2, part_code2)
      raise if non_preferred_terms.empty?
      raise if non_preferred_terms.count > 1
      pt  = preferred_terms.first
      npt = non_preferred_terms.first
      ChangeTracker.start
      pt.refid_preferred = true; pt.status = 'Published'
      pt.save
      if npt.refid_preferred
        puts Rainbow("#{npt.refid.value} should not be preferred.").yellow
        npt.refid_preferred = nil
        npt.save
      end
      ChangeTracker.commit
      puts '  ' + pt._stat + Rainbow(' REFID preferred over ').yellow + npt._stat
    end
    
    _parse_tables('RefID synonyms', [254], proc254)
  end
end
