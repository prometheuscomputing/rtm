module RTM
  module_function
  def table_metadata
    [
      {:table => '1', :title => 'Nomenclature attributes', :page => 26},
      {:table => '2', :title => 'Attribution example', :page => 26},
      {:table => '3', :title => 'Systematic name derivation—medical device type example', :page => 27},
      {:table => 'A.2.4.1.1', :title => 'Partition 1—Object-oriented elements, Device nomenclature', :page => 37},
      {:table => 'A.2.4.2.1', :title => 'Partition 2—Metrics', :page => 37},
      {:table => 'A.2.4.3.1', :title => 'Partition 3—Alerts and events', :page => 39},
      {:table => 'A.2.4.4.1', :title => 'Partition 4—Units of measurement (dimensions)', :page => 39},
      {:table => 'A.2.4.6.1', :title => 'Partition 6—Program group', :page => 40},
      {:table => 'A.2.4.7.1', :title => 'Partition 7—Body sites', :page => 40},
      {:table => 'A.2.4.8.1', :title => 'Partition 8—Communication infrastructure', :page => 41},
      {:table => 'A.2.4.9.1', :title => 'Partition 9—Reserved for file exchange format (FEF)', :page => 41},
      {:table => 'A.2.4.10.1', :title => 'Partition 10—ECG additional nomenclature (annotated ECG)', :page => 41},
      {:table => 'A.2.4.11.1', :title => 'Partition 11—IDCO nomenclature', :page => 41},
      {:table => 'A.2.4.12.1', :title => 'Partition 128—PHD disease management', :page => 42},
      {:table => 'A.2.4.13.1', :title => 'Partition 129—PHD health and fitness', :page => 42},
      {:table => 'A.2.4.14.1', :title => 'Partition 130—PHD aging independently', :page => 43},
      {:table => 'A.2.4.15.1', :title => 'Partition 255—Return codes', :page => 43},
      {:table => 'A.2.4.16.1', :title => 'Partition 256—External nomenclatures', :page => 43},
      {:table => 'A.2.4.17.1', :title => 'Partition 258—Settings', :page => 44},
      {:table => 'A.2.4.18.1', :title => 'Partition 514—Predicted values', :page => 44},
      {:table => 'A.2.4.19.1', :title => 'Partition 1024—Private', :page => 44},
      {:table => 'A.3.2.1.1', :title => 'Object-oriented modeling elements—General—object class items', :page => 46},
      {:table => 'A.3.2.1.2', :title => 'Object-oriented modeling elements—General—attributes', :page => 47},
      {:table => 'A.3.2.1.3', :title => 'Object-oriented modeling elements—General—attribute groups', :page => 47},
      {:table => 'A.3.2.1.4', :title => 'Object-oriented modeling elements—General—behavior', :page => 47},
      {:table => 'A.3.2.1.5', :title => 'Object-oriented modeling elements—General—notifications', :page => 47},
      {:table => 'A.3.2.2.1', :title => 'Object-oriented modeling elements—Medical Package—object class items', :page => 47},
      {:table => 'A.3.2.2.2', :title => 'Object-oriented modeling elements—Medical Package—attributes', :page => 48},
      {:table => 'A.3.2.2.3', :title => 'Object-oriented modeling elements—Medical Package—attribute groups', :page => 52},
      {:table => 'A.3.2.2.4', :title => 'Object-oriented modeling elements—Medical Package—behavior', :page => 52},
      {:table => 'A.3.2.2.5', :title => 'Object-oriented modeling elements—Medical Package—notifications', :page => 52},
      {:table => 'A.3.2.3.1', :title => 'Object-oriented modeling elements—Alert Package—object class items', :page => 53},
      {:table => 'A.3.2.3.2', :title => 'Object-oriented modeling elements—Alert Package—attributes', :page => 53},
      {:table => 'A.3.2.3.3', :title => 'Object-oriented modeling elements—Alert Package—attribute groups', :page => 53},
      {:table => 'A.3.2.3.4', :title => 'Object-oriented modeling elements—Alert Package—behavior', :page => 53},
      {:table => 'A.3.2.3.5', :title => 'Object-oriented modeling elements—Alert Package—notifications', :page => 54},
      {:table => 'A.3.2.4.1', :title => 'Object-oriented modeling elements—System Package—object class items', :page => 54},
      {:table => 'A.3.2.4.2', :title => 'Object-oriented modeling elements—System Package—attributes', :page => 54},
      {:table => 'A.3.2.4.3', :title => 'Object-oriented modeling elements—System Package—attribute groups', :page => 56},
      {:table => 'A.3.2.4.4', :title => 'Object-oriented modeling elements—System Package—behavior', :page => 57},
      {:table => 'A.3.2.4.5', :title => 'Object-oriented modeling elements—System Package—notifications', :page => 57},
      {:table => 'A.3.2.5.1', :title => 'Object-oriented modeling elements—Control Package—object class items', :page => 57},
      {:table => 'A.3.2.5.2', :title => 'Object-oriented modeling elements—Control Package—attributes', :page => 57},
      {:table => 'A.3.2.5.3', :title => 'Object-oriented modeling elements—Control Package—attribute groups', :page => 59},
      {:table => 'A.3.2.5.4', :title => 'Object-oriented modeling elements—Control Package—behavior', :page => 59},
      {:table => 'A.3.2.5.5', :title => 'Object-oriented modeling elements—Control Package—notifications', :page => 59},
      {:table => 'A.3.2.6.1', :title => 'Object-oriented modeling elements—Extended Services Package—object class items', :page => 59},
      {:table => 'A.3.2.6.2', :title => 'Object-oriented modeling elements—Extended Services Package—attributes', :page => 60},
      {:table => 'A.3.2.6.3', :title => 'Object-oriented modeling elements— Extended Services Package—attribute groups', :page => 60},
      {:table => 'A.3.2.6.4', :title => 'Object-oriented modeling elements—Extended Services Package—behavior', :page => 61},
      {:table => 'A.3.2.6.5', :title => 'Object-oriented modeling elements— Extended Services Package—notifications', :page => 61},
      {:table => 'A.3.2.7.1', :title => 'Object-oriented modeling elements—Communication Package— object class items', :page => 61},
      {:table => 'A.3.2.7.2', :title => 'Object-oriented modeling elements—Communication Package—attributes', :page => 62},
      {:table => 'A.3.2.7.3', :title => 'Object-oriented modeling elements— Communication Package—attribute groups', :page => 63},
      {:table => 'A.3.2.7.4', :title => 'Object-oriented modeling elements—Communication Package—behavior', :page => 63},
      {:table => 'A.3.2.7.5', :title => 'Object-oriented modeling elements—Communication Package—notifications', :page => 63},
      {:table => 'A.3.2.8.1', :title => 'Object-oriented modeling elements—Archival Package—object class items', :page => 63},
      {:table => 'A.3.2.8.2', :title => 'Object-oriented modeling elements—Archival Package—attributes', :page => 64},
      {:table => 'A.3.2.8.3', :title => 'Object-oriented modeling elements—Archival Package—attribute groups', :page => 65},
      {:table => 'A.3.2.8.4', :title => 'Object-oriented modeling elements—Archival Package—behavior', :page => 65},
      {:table => 'A.3.2.8.5', :title => 'Object-oriented modeling elements—Archival Package—notifications', :page => 65},
      {:table => 'A.3.2.9.1', :title => 'Object-oriented modeling elements—Patient Package—object class items', :page => 65},
      {:table => 'A.3.2.9.2', :title => 'Object-oriented modeling elements—Patient Package—attributes', :page => 66},
      {:table => 'A.3.2.9.3', :title => 'Object-oriented modeling elements—Patient Package—attribute groups', :page => 67},
      {:table => 'A.3.2.9.4', :title => 'Object-oriented modeling elements—Patient Package—behavior', :page => 67},
      {:table => 'A.3.2.9.5', :title => 'Object-oriented modeling elements—Patient Package—notifications', :page => 67},
      {:table => 'A.3.2.10.1', :title => 'Object-oriented modeling elements—PHD—object class items', :page => 68},
      {:table => 'A.3.2.10.2', :title => 'Object-oriented modeling elements—PHD—attributes', :page => 68},
      {:table => 'A.3.2.10.3', :title => 'Object-oriented modeling elements—PHD—attribute groups', :page => 70},
      {:table => 'A.3.2.10.4', :title => 'Object-oriented modeling elements—PHD-behavior', :page => 71},
      {:table => 'A.3.2.10.5', :title => 'Object-oriented modeling elements—PHD—notifications', :page => 71},
      {:table => 'A.3.2.11.1', :title => 'Object-oriented modeling elements—WCM—object class items', :page => 72},
      {:table => 'A.3.2.11.2', :title => 'Object-oriented modeling elements—WCM—attributes', :page => 72},
      {:table => 'A.3.2.11.3', :title => 'Object-oriented modeling elements—WCM—attribute groups', :page => 74},
      {:table => 'A.3.2.11.4', :title => 'Object-oriented modeling elements—WCM—behavior', :page => 74},
      {:table => 'A.3.2.11.5', :title => 'Object-oriented modeling elements—WCM—notifications', :page => 74},
      {:table => 'A.3.2.12.1', :title => 'Object-oriented modeling elements—MEM-LS—object class items', :page => 74},
      {:table => 'A.3.2.12.2', :title => 'Object-oriented modeling elements—MEM-LS—attributes', :page => 75},
      {:table => 'A.3.2.12.3', :title => 'Object-oriented modeling elements—MEM-LS—attribute groups', :page => 77},
      {:table => 'A.3.2.12.4', :title => 'Object-oriented modeling elements—MEM-LS—behavior', :page => 77},
      {:table => 'A.3.2.12.5', :title => 'Object-oriented modeling elements—MEM-LS—notifications', :page => 77},
      {:table => 'A.3.2.13.1', :title => 'Object-oriented modeling elements—USI—object class items', :page => 77},
      {:table => 'A.3.2.13.2', :title => 'Object-oriented modeling elements—USI—attributes', :page => 77},
      {:table => 'A.3.2.13.3', :title => 'Object-oriented modeling elements—USI—attribute groups', :page => 77},
      {:table => 'A.3.2.13.4', :title => 'Object-oriented modeling elements—USI—behavior', :page => 78},
      {:table => 'A.3.2.13.5', :title => 'Object-oriented modeling elements—USI—notifications', :page => 78},
      {:table => 'A.3.2.14.1', :title => 'Object-oriented modeling elements—Deprecated identifier terms', :page => 78},
      {:table => 'A.4.1', :title => 'Communication infrastructure—object class items', :page => 79},
      {:table => 'A.4.2', :title => 'Communication infrastructure—attributes', :page => 80},
      {:table => 'A.4.3', :title => 'Communication infrastructure—attribute groups', :page => 82},
      {:table => 'A.4.4', :title => 'Communication infrastructure—behavior', :page => 82},
      {:table => 'A.4.5', :title => 'Communication infrastructure—notifications', :page => 83},
      {:table => 'A.4.6', :title => 'Communication infrastructure—profile support attributes', :page => 83},
      {:table => 'A.4.7', :title => 'Communication infrastructure—optional package identifiers', :page => 83},
      {:table => 'A.4.8', :title => 'Communication infrastructure—system specification components', :page => 83},
      {:table => 'A.5.1', :title => 'Nomenclature and codes for vital signs devices', :page => 88},
      {:table => 'A.6.2.1', :title => 'Table of decimal factors', :page => 99},
      {:table => 'A.6.3.1', :title => 'Non-SI units', :page => 99},
      {:table => 'A.6.4.1', :title => 'Vital signs units of measurement', :page => 101},
      {:table => 'A.6.5.1', :title => 'Withdrawn terms for vital signs units of measurement', :page => 122},
      {:table => 'A.6.6.1', :title => 'Deprecated terms for vital signs units of measurement', :page => 123},
      {:table => 'A.6.7.1', :title => 'Deprecated RefIds for vital signs units of measurement', :page => 124},
      {:table => 'A.7.1.3.1', :title => 'List of standardized ECG <lead> descriptors from SCP‑ECG', :page => 130},
      {:table => 'A.7.1.3.2', :title => 'Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10101-2004 with discriminator group [LEAD1]', :page => 132},
      {:table => 'A.7.1.3.3', :title => 'Nomenclature and codes for ECG lead descriptors from IEEE Std 11073-10102 with discriminator group [LEAD2]', :page => 135},
      {:table => 'A.7.1.6.1', :title => 'Nomenclature and codes for global lead ECG measurements', :page => 144},
      {:table => 'A.7.1.6.2', :title => 'Nomenclature and codes for ECG measurements with lead origin', :page => 151},
      {:table => 'A.7.1.7.1', :title => 'Deprecated terms for ECG measurements', :page => 156},
      {:table => 'A.7.1.8.1', :title => 'Deprecated RefIds for ECG measurements defined in IEEE11073-10102', :page => 156},
      {:table => 'A.7.2.6.1', :title => 'Nomenclature and codes for ECG enumerations', :page => 160},
      {:table => 'A.7.2.7.1', :title => 'Withdrawn terms for ECG enumerations', :page => 171},
      {:table => 'A.7.2.8.1', :title => 'Deprecated terms for ECG enumerations', :page => 171},
      {:table => 'A.7.2.9.1', :title => 'Deprecated RefIds for ECG enumerations', :page => 172},
      {:table => 'A.7.3.6.1', :title => 'Nomenclature and codes for haemodynamic monitoring measurements', :page => 176},
      {:table => 'A.7.3.7.1', :title => 'Deprecated terms for haemodynamic monitoring measurements', :page => 191},
      {:table => 'A.7.4.11.1', :title => 'Ventilator modes bit string', :page => 200},
      {:table => 'A.7.4.11.2', :title => 'Ventilator modes nomenclature and codes', :page => 202},
      {:table => 'A.7.4.16.1', :title => 'Correction of gas measurements', :page => 206},
      {:table => 'A.7.4.17.1', :title => 'Gas measurement sites', :page => 207},
      {:table => 'A.7.4.17.2', :title => 'Default gas measurement sites', :page => 207},
      {:table => 'A.7.4.18.1', :title => 'Inspiratory breath type classifications', :page => 209},
      {:table => 'A.7.4.19.1', :title => 'Deployment of gas partial pressure and concentration and consumption information (informative)', :page => 210},
      {:table => 'A.7.4.19.2', :title => 'Nomenclature and codes for respiratory, ventilator, and anesthesia measurements', :page => 211},
      {:table => 'A.7.4.20.1', :title => 'Deprecated terms for respiratory measurements', :page => 258},
      {:table => 'A.7.4.21.1', :title => 'Deprecated RefIds for respiratory measurements', :page => 258},
      {:table => 'A.7.4.22.1', :title => 'Recommended mapping of deprecated to ‘unified’ gas RefId prefixes (informative)', :page => 259},
      {:table => 'A.7.4.23.1', :title => 'Deprecated terms for respiratory measurements', :page => 260},
      {:table => 'A.7.4.24.1', :title => 'Deprecated RefIds for respiratory measurements', :page => 265},
      {:table => 'A.7.4.25.1', :title => 'Deprecated nomenclature for undefined respiratory measurements from Annex B', :page => 269},
      {:table => 'A.7.4.26.1', :title => 'Nomenclature and codes for nebulizers', :page => 270},
      {:table => 'A.7.5.6.1', :title => 'Nomenclature and codes for common blood-gas, blood, urine, and other fluid chemistry measurements', :page => 276},
      {:table => 'A.7.5.7.1', :title => 'Withdrawn terms for blood-gas, blood, urine, and other fluid chemistry measurements', :page => 287},
      {:table => 'A.7.5.8.1', :title => 'Deprecated RefIds for blood-gas, blood, urine, and other fluid chemistry measurements', :page => 287},
      {:table => 'A.7.6.6.1', :title => 'Nomenclature and codes for fluid-output measurements', :page => 290},
      {:table => 'A.7.7.6.1', :title => 'Nomenclature and codes for pump data', :page => 297},
      {:table => 'A.7.7.7.1', :title => 'Deprecated RefIds for pump data', :page => 302},
      {:table => 'A.7.7.13.1', :title => 'Pump modes bit string values (value to be communicated in enumeration observation element Mode | | Device | Pump)', :page => 305},
      {:table => 'A.7.7.19.1', :title => 'Pump states bit string values (value to be communicated in EnumerationObservation with code: Status | Operational | Device | Pump)', :page => 308},
      {:table => 'A.7.8.6.1', :title => 'Nomenclature and codes for neurological monitoring measurements', :page => 313},
      {:table => 'A.7.9.6.1', :title => 'Nomenclature and codes for neurophysiologic enumerations', :page => 330},
      {:table => 'A.7.10.6.1', :title => 'Nomenclature and codes for neurophysiologic stimulation modes', :page => 349},
      {:table => 'A.7.11.6.1', :title => 'Nomenclature and codes for miscellaneous measurements', :page => 355},
      {:table => 'A.7.11.6.2', :title => 'Nomenclature and codes for temperature', :page => 356},
      {:table => 'A.7.11.7.1', :title => 'Deprecated nomenclature for temperature', :page => 357},
      {:table => 'A.7.11.8.1', :title => 'Body weight and surface area for pre-coordinated RefIds', :page => 358},
      {:table => 'A.7.11.9.1', :title => 'Nomenclature and codes for body mass (weight) and estimates', :page => 359},
      {:table => 'A.7.12.1.1', :title => 'Nomenclature and code extensions for infant incubator and warmer microenvironments', :page => 360},
      {:table => 'A.7.13.6.1', :title => 'Nomenclature and codes for spirometry measurements', :page => 364},
      {:table => 'A.7.14.1.1', :title => 'Nomenclature and code extensions for personal health devices', :page => 372},
      {:table => 'A.7.14.2.1', :title => 'Deprecated RefIds for personal health devices', :page => 376},
      {:table => 'A.8.2.5.1', :title => 'Nomenclature and codes for sites for neurophysiological signal monitoring: locations near peripheral nerves', :page => 381},
      {:table => 'A.8.3.5.1', :title => 'Nomenclature and codes for sites for neurophysiological signal monitoring: locations near or in muscles', :page => 398},
      {:table => 'A.8.4.6.1', :title => 'Nomenclature and codes for electrode sites for EEG according to the international 10–20 system', :page => 433},
      {:table => 'A.8.5.6.1', :title => 'Nomenclature and codes for sites for EOG signal monitoring', :page => 441},
      {:table => 'A.8.6.5.1', :title => 'Nomenclature and codes for general neurological sites for monitoring measurements and drainage', :page => 444},
      {:table => 'A.8.7.5.1', :title => 'Nomenclature and codes for body sites for cardiovascular measurements', :page => 447},
      {:table => 'A.8.8.5.1', :title => 'Nomenclature and codes for miscellaneous body sites used in vital signs monitoring and measurement', :page => 454},
      {:table => 'A.8.9.5.1', :title => 'Nomenclature and codes for external sites used in vital signs monitoring and measurement', :page => 467},
      {:table => 'A.8.10.6.1', :title => 'Nomenclature and codes for qualifiers of body site locations', :page => 470},
      {:table => 'A.9.2.5.1', :title => 'Nomenclature and codes for pattern events', :page => 475},
      {:table => 'A.9.3.5.1', :title => 'Nomenclature and codes for device-related and environment-related events', :page => 491},
      {:table => 'A.9.3.6.1', :title => 'Deprecated terms for device-related and environment-related events', :page => 518},
      {:table => 'A.9.3.7.1', :title => 'Deprecated RefIds for device-related and environment-related events', :page => 518},
      {:table => 'A.10.2.3.1', :title => 'Nomenclature and codes for infrastructure, device specialization', :page => 520},
      {:table => 'A.10.2.4.1', :title => 'Withdrawn terms for device specialization', :page => 522},
      {:table => 'A.10.3.3.1', :title => 'Nomenclature and codes for Infastructure, sub-specialization', :page => 523},
      {:table => 'A.10.4.3.1', :title => 'Time synchronization profiles', :page => 526},
      {:table => 'A.11.2.3.1', :title => 'Nomenclature and codes for PHD Disease Management, general', :page => 527},
      {:table => 'A.11.3.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Basic ECG sensors', :page => 528},
      {:table => 'A.11.4.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Basic ECG event context', :page => 529},
      {:table => 'A.11.5.3.1', :title => 'Nomenclature and codes for PHD Disease Management, SABTE sensors and settings', :page => 530},
      {:table => 'A.11.5.4.1', :title => 'Withdrawn terms for PHD Disease Management, SABTE sensors and settings', :page => 536},
      {:table => 'A.11.6.3.1', :title => 'Nomenclature and codes for PHD Disease Management, SABTE mode settings', :page => 540},
      {:table => 'A.11.7.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose carbohydrate source', :page => 542},
      {:table => 'A.11.8.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose carbohydrate source', :page => 543},
      {:table => 'A.11.9.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose moncarbohydrate sources', :page => 544},
      {:table => 'A.11.10.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, insulin type', :page => 545},
      {:table => 'A.11.11.3.1', :title => 'Nomenclature and codes for PHD Disease Management, insulin types', :page => 546},
      {:table => 'A.11.12.3.1', :title => 'Nomenclature and codes for PHD Disease Management, general health', :page => 547},
      {:table => 'A.11.13.3.1', :title => 'Nomenclature and codes for PHD Disease Management, general health', :page => 548},
      {:table => 'A.11.14.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, sample location', :page => 549},
      {:table => 'A.11.15.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, sample locations', :page => 550},
      {:table => 'A.11.16.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, meal', :page => 551},
      {:table => 'A.11.17.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, meal types', :page => 552},
      {:table => 'A.11.18.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, tester', :page => 553},
      {:table => 'A.11.19.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Glucose Monitoring, tester types', :page => 554},
      {:table => 'A.11.20.3.1', :title => 'Nomenclature and codes for PHD Disease Management, INR status and context', :page => 555},
      {:table => 'A.11.21.3.1', :title => 'Nomenclature and codes for PHD Disease Management, CGM sensors', :page => 556},
      {:table => 'A.11.22.3.1', :title => 'Nomenclature and codes for PHD Disease Management, CGM sensors', :page => 558},
      {:table => 'A.11.23.3.1', :title => 'Nomenclature and codes for PHD Disease Management, CGM device', :page => 559},
      {:table => 'A.11.24.3.1', :title => 'Nomenclature and codes for PHD Disease Management, Insulin Pump', :page => 560},
      {:table => 'A.11.24.4.1', :title => 'Deprecated term codes for PHD Disease Management, Insulin Pump', :page => 562},
      {:table => 'A.11.25.4.1', :title => 'Nomenclature and codes for PHD Disease Management, PSM device', :page => 563},
      {:table => 'A.11.26.2.1', :title => 'Nomenclature and codes for PHD Disease Management, PSM device', :page => 564},
      {:table => 'A.11.27.3.1', :title => 'Nomenclature and codes for PHD Disease Management, PEF status', :page => 565},
      {:table => 'A.12.3.2.1', :title => 'Nomenclature and codes for health and fitness sensors', :page => 567},
      {:table => 'A.12.3.3.1', :title => 'Deprecated terms for PHD disease management, health and fitness', :page => 569},
      {:table => 'A.12.4.2.1', :title => 'Nomenclature and codes for health and fitness activity', :page => 570},
      {:table => 'A.12.5.2.1', :title => 'Nomenclature and codes for health and fitness exercise', :page => 572},
      {:table => 'A.12.6.2.1', :title => 'Nomenclature and codes for health and fitness specific exercise', :page => 573},
      {:table => 'A.12.6.3.1', :title => 'Deprecated terms for health and fitness exercise', :page => 574},
      {:table => 'A.13.3.2.1', :title => 'Nomenclature and codes for assisted living sensors', :page => 577},
      {:table => 'A.13.4.3.1', :title => 'Nomenclature and codes AI locations, general', :page => 580},
      {:table => 'A.13.5.3.1', :title => 'Nomenclature and codes AI locations, rooms', :page => 581},
      {:table => 'A.13.6.3.1', :title => 'Nomenclature and codes AI locations, medical rooms', :page => 585},
      {:table => 'A.13.7.3.1', :title => 'Nomenclature and codes AI locations, doors and windows', :page => 587},
      {:table => 'A.13.8.3.1', :title => 'Nomenclature and codes for AI locations, furniture', :page => 588},
      {:table => 'A.13.9.3.1', :title => 'Nomenclature and codes for AI locations, appliances', :page => 589},
      {:table => 'A.13.10.2.1', :title => 'Nomenclature and codes for AI events', :page => 591},
      {:table => 'A.13.11.3.1', :title => 'Nomenclature and codes AI medication dispenser', :page => 594},
      {:table => 'A.14.3.1', :title => 'Nomenclature for error return codes', :page => 595},
      {:table => 'A.14.4.1', :title => 'Withdrawn nomenclature for error return codes', :page => 596},
      {:table => 'A.15.7.1', :title => 'Nomenclature and codes for external nomenclatures and messaging standards', :page => 599},
      {:table => 'A.16.1.1', :title => 'IHE PCD Alert Communication Management attributes', :page => 602},
      {:table => 'A.16.2.1', :title => 'IHE PCD Alert Communication Management Notifications', :page => 602},
      {:table => 'A.16.3.1', :title => 'Continua Services Interface infrastructure attributes', :page => 603},
      {:table => 'A.16.4.1', :title => 'Continua Services Interface information attributes', :page => 604},
      {:table => 'A.16.5.1', :title => 'IHE PCD and Continua Services Interface timekeeping information attributes', :page => 605},
      {:table => 'A.16.6.1', :title => 'Breathing circuit attributes', :page => 607},
      {:table => 'A.16.6.2', :title => 'Calculation method attribute', :page => 607},
      {:table => 'A.16.7.1', :title => 'ECG nomenclature attributes', :page => 607},
      {:table => 'C.3.1.1', :title => 'Device Type [MVC] discriminator set', :page => 844},
      {:table => 'C.3.2.1', :title => 'Statistical [MMM] discriminator set', :page => 844},
      {:table => 'C.3.3.1', :title => 'Haemodynamic pressure measurements [SDM] discriminator set', :page => 844},
      {:table => 'C.3.4.1', :title => 'Rates for countable events [RCE] discriminator set', :page => 845},
      {:table => 'C.3.5.1', :title => 'Rates for countable neurological events [RCN] discriminator set', :page => 845},
      {:table => 'C.3.6.1', :title => 'Body Site Orientation (laterally) [LAT] discriminator set', :page => 845},
      {:table => 'C.3.7.1', :title => 'Units of Measure [UoM] discriminator set', :page => 846},
      {:table => 'C.3.8.1', :title => 'Unit of Measure (singular) [UoM1] discriminator set', :page => 846},
      {:table => 'C.3.9.1', :title => 'No Discriminator [1] discriminator set', :page => 847},
      {:table => 'C.3.10.1', :title => 'Event Discriminator [EVT] discriminator set', :page => 847},
      {:table => 'C.3.11.1', :title => 'Statistical profile [PN3] discriminator set', :page => 847},
      {:table => 'C.3.12.1', :title => 'Location Discriminator [LOC] discriminator set', :page => 848},
      {:table => 'C.3.13.1', :title => 'Version of External Nomenclature Discriminator [64]', :page => 849},
      {:table => 'C.3.14.1', :title => 'ECG lead discriminators [LEAD1] from  ISO/IEEE 11073-10101:2004', :page => 849},
      {:table => 'C.3.15.1', :title => 'ECG lead discriminators [LEAD2] from  ISO/IEEE 11073-10102:2012', :page => 852},
      {:table => 'C.3.16.1', :title => 'Equivalent ECG lead discriminators [LEAD] in  ISO/IEEE 11073-10101:2004 and ISO/IEEE 11073-10102:2012', :page => 857},
      {:table => 'C.3.17.1', :title => 'Comparison of ECG lead discriminators in  ISO/IEEE 11073-10101:2004 and ISO/IEEE 11073-10102:2012', :page => 859},
      {:table => 'C.4.1.1', :title => 'Object-Oriented—Partition 1', :page => 862},
      {:table => 'C.4.2.1', :title => 'SCADA—Partition 2', :page => 895},
      {:table => 'C.4.3.1', :title => 'Events and Alerts—Partition 3', :page => 950},
      {:table => 'C.4.4.1', :title => 'Dimensions—Partition 4', :page => 965},
      {:table => 'C.4.5.1', :title => 'Parameter Groups—Partition 6', :page => 981},
      {:table => 'C.4.6.1', :title => 'Body Sites—Partition 7', :page => 981},
      {:table => 'C.4.7.1', :title => 'Communication Infrastructure—Partition 8', :page => 1014},
      {:table => 'C.4.8.1', :title => 'PHD Disease Management—Partition 128', :page => 1019},
      {:table => 'C.4.9.1', :title => 'PHD Health Fitness—Partition 129', :page => 1025},
      {:table => 'C.4.10.1', :title => 'PHD Aging Independently—Partition 130', :page => 1028},
      {:table => 'C.4.11.1', :title => 'Return Codes—Partition 255', :page => 1036},
      {:table => 'C.4.12.1', :title => 'External Nomenclature—Partition 256', :page => 1036},
      {:table => 'C.4.13.1', :title => 'Device Settings—Partition 258', :page => 1037},
      {:table => 'C.4.14.1', :title => 'Predicted Values—Partition 514', :page => 1040},
      {:table => 'D.1.1.1', :title => 'Term code synonyms', :page => 1042},
      {:table => 'D.1.1.2', :title => 'Term code synonyms', :page => 1042},
      {:table => 'D.1.2.1', :title => 'RefId synonyms', :page => 1043},
      {:table => 'E.1', :title => 'Inspiratory breath and inflation types and rates', :page => 1045},
      {:table => 'G.1', :title => 'Gas measurement sites and anesthesia breathing circuit components (normative)', :page => 1049},
      {:table => 'J.1', :title => 'Revision history', :page => 1059}
    ]
  end
end
