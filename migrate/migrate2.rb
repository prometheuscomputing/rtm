require 'csv'

module RTM
  module_function

  def all_csv_files
    here = Dir.pwd
    Dir.chdir(csv_dir)
    files = Dir.glob('**/*.csv').collect! { |f| File.join(csv_dir, f) }
    Dir.chdir(here)
    files
  end
  
  def structs
    @structs;
  end
  
  def structs_arrays
    @structs_arrays;
  end
  
  def create_structs(files = nil)
    # return if @structs # suppresses warnings
    # puts files.inspect
    @structs = {}
    @structs_arrays = {}
    files ||= all_csv_files
    # files.select! { |f| f =~ /coded_term/}
    files.each do |path|
      name = path.split('/').last.split('.').first.split('_').collect(&:capitalize).join
      begin
        raw = File.read(path)#.encode!('UTF-8', 'UTF-8', :invalid => :replace)
        raw = raw.gsub("\r", "\n").gsub(/\n+/, "\n")
        raw = raw.gsub(',""', ',"').gsub('"",', '",')
        raw = raw.gsub(/(?<=[^,"])"(?=[^,"\n])/, '""')
        rows = CSV.new(raw, liberal_parsing: true).read
      rescue Exception => e
        puts "Path is #{path}"
        m = /in line (\d+)/.match(e.message)
        # puts m[1].inspect if m
        # puts raw.encoding
        problem_line1 = raw.split("\n")[m[1].to_i - 1]
        problem_line2 = raw.split("\n")[m[1].to_i]
        puts Rainbow(problem_line1).cyan
        puts problem_line1.inspect
        puts Rainbow(problem_line2).yellow
        puts problem_line2.inspect
        puts Rainbow(e.message).orange + ' ' + e.class.name
        puts
        next
      end
      vars = rows.first.dup
      vars[0] = 'id'
      vars << 'obj' # add a var to the struct that will hold a ref to the Sequel::Model instance
      vars = vars.collect(&:to_sym)
      # Object.const_set(name.to_sym, Struct.new(name, *vars)) unless Object.const_defined?(name)
      # struct = name.to_const
      if Struct.constants.include?(name.to_sym)
        struct = Struct.const_get(name.to_sym)
        # puts "Struct #{struct} was already defined."
      else
        struct = Struct.const_set(name.to_sym, Struct.new(*vars))
        # puts "Struct is #{struct}"
      end
      # puts Rainbow("#{struct} is a #{struct.class}").orange
      @structs[struct] = {}
      @structs_arrays[struct] = []
      rows[1..-1].each do |row|
        row = row.collect do |c|
          if c == 'NULL'
            nil
          elsif c.is_a?(String)
            c.strip
          else
            c
          end
        end
        # use integers for keys if possible
        begin
          # key = row[0][0] =~ /\d/ ? row[0].to_i : row[0]
          key = row[0]
        rescue
          puts path
          puts Rainbow(row.inspect).red
          raise
        end
        if row.size != vars.size - 1
          puts path
          # puts "vars: #{vars}"
          pp vars.zip row
          raise "Arguments don't match correctly for #{struct.name}"
        end
        begin
          s = struct.new(*row)
        rescue
          puts path
          puts "vars: #{vars}"
          pp row
          raise
        end
        @structs[struct][key.to_s.strip] = s 
        @structs_arrays[struct] << s
      end
    end
  end  
  
  def setup_sources
    ChangeTracker.start
    @ieee10101  = SDoc::Document.create(:title => '10101:2004')
    @annexA     = SDoc::Annex.create(:title => 'Annex A')
    @annexB     = SDoc::Annex.create(:title => 'Annex B')
    @ieee10101.add_content(@annexA)
    @ieee10101.add_content(@annexB)
    @ieee10101a = SDoc::Document.create(:title => '10101a')
    @annexAa    = SDoc::Annex.create(:title => 'Annex A')
    @annexBa    = SDoc::Annex.create(:title => 'Annex B')
    @ieee10101a.add_content(@annexAa)
    @ieee10101a.add_content(@annexBa)
    @idc        = SDoc::Document.create(:title => 'IDC Nomenclature')
    @phd        = SDoc::Document.create(:title => 'PHD Nomenclature')
    @other      = SDoc::Document.create(:title => 'Other / Unknown')
    ChangeTracker.commit
  end
  
  def setup_commentors
    ChangeTracker.start
    u = Gui_Builder_Profile::User
    @todd = u.create(:first_name => 'Todd', :last_name => 'Cooper', :login => 'ToddCooper')
    @nico = u.create(:first_name => 'Nicolas', :last_name => 'Crozier', :login => 'nicosdo')
    @jr   = u.create(:first_name => 'John', :last_name => 'Rhoads', :login => 'jrphilips')
    @cmi  = u.create(:login => 'Rtayrien')
    ChangeTracker.commit
  end
  
  def csv_dir
    # File.expand_path('~/projects/rtm_ref/rtmv1csv')
    File.expand_path('~/projects/rtm_ref/rtmv1csv_14042020')
  end
  
  def csv_path(name)
    File.join(csv_dir, "#{name}.csv")
  end
  
  def migrate_partitions
    structs[Struct::Partition].each_value do |s|
      ChangeTracker.start
      ptn = find_or_create_partition(s.id)
      ptn.description = s.description
      ptn.name = s.name
      private_block = RTM::Block.create(:name => 'Private')
      range = RTM::Range.create(:lower => 61440, :upper => 65535)
      range.block = private_block
      ptn.add_block(private_block)
      ChangeTracker.commit
      s.obj = ptn
      # structs[Struct::Partition][s.id] = ptn
    end
  end
  
  def migrate_blocks
    # Note that RTMMS v1 has the concept of block and partition mixed up
    # partition_id,block_id,lower_range,upper_range,partition_name,partition_description,dc
    structs[Struct::Block].each_value do |s|
      if s.lower.to_i == 0 && s.upper.to_i == 0 
        next # These are just there to serve as joins
        puts "migrate_blocks says WHAT?"
        pp s
        puts
      end
      ChangeTracker.start
      partition = find_or_create_partition(s.partition_id)
      lower = s.lower
      if lower.nil?
        lower = 0 
      else
        lower = lower.to_i
      end
      upper = s.upper
      if upper.nil?
        upper = 2**16 
      else
        upper = upper.to_i
      end
      block = find_or_create_block(s.name)
      block.save
      block.partition = partition
      range = find_range_in_block(lower, upper, block)
      range ||= create_range_in_block(lower, upper, block)
      ChangeTracker.commit
      s.obj = block
    end
  end
  
  def import_partitions_and_blocks
    migrate_partitions
    migrate_blocks
    partitions = {}
    RTM::Partition.each do |ptn|
      if ptn.blocks.count == 1
        ChangeTracker.start
        block = find_or_create_block("Default Block for Partition #{ptn.number}")
        range = create_range_in_block(0, 61439, block)
        ptn.add_block(block)
        ChangeTracker.commit
      end
      partitions[ptn.number] = ptn.blocks
    end
    
    # partitions.each do |ptn, blocks|
    #   puts ptn
    #   blocks.each { |block| puts "#{block.name} -- #{block.lower} -> #{block.upper}"}
    #   puts
    # end
  end
  
  def connect_sources
    structs_arrays[Struct::TermSourceRelation].each do |s|
      source = case s.source_id.to_i
               when 1
                 @annexA
               when 2
                 @annexB
               when 3
                 @idc
               when 4
                 @phd
               when 5
                 @other
               end
      term_struct = get_struct(Struct::CodedTerm, s.id)
      unless source && term_struct
        pp s;puts
      end
      term = term_struct.obj
      ChangeTracker.start
      term.add_source(source)
      ChangeTracker.commit
    end
  end
  
  def migrate_discriminator_sets
    structs[Struct::DiscriminatorSet].each_value do |s|
      ChangeTracker.start
      v1name = s.discriminator_set_name + '-RTMMSv1'
      v1ds = RTM::DiscriminatorSet.where(:name => v1name, :allocation => s.discriminator_set_allocation.to_i, :description => s.discriminator_set_description).first 
      v1ds ||= RTM::DiscriminatorSet.new(:name => v1name, :allocation => s.discriminator_set_allocation.to_i, :description => s.discriminator_set_description)
      v1ds.type = s.type
      v1ds.save
      
      ds = RTM::DiscriminatorSet.where(:name => s.discriminator_set_name, :allocation => s.discriminator_set_allocation.to_i, :description => s.discriminator_set_description).first 
      ds ||= RTM::DiscriminatorSet.new(:name => s.discriminator_set_name, :allocation => s.discriminator_set_allocation.to_i, :description => s.discriminator_set_description)
      ds.type = s.type
      ds.save
      ChangeTracker.commit
      s.obj = v1ds
    end
  end
  
  def migrate_discriminators
    structs[Struct::Discriminator].each_value do |s|
      ChangeTracker.start
      suffix = s.suffix
      suffix = nil if suffix.nil?
      v1ds = get_obj(Struct::DiscriminatorSet, s.discriminator_set_id)
      v1disc = v1ds.discriminators.find { |d| d.offset == s.offset.to_i && d.value == suffix }
      v1disc ||= RTM::Discriminator.new(:offset => s.offset.to_i, :value => suffix, :description => s.description)
      v1disc.save
      v1ds.add_discriminator(v1disc)
      v1ds.save
      
      # For 10101R DiscriminatorSets
      ds = RTM::DiscriminatorSet.where(:name => v1ds.name.sub('-RTMMSv1', '')).first
      raise unless ds
      disc = ds.discriminators.find { |d| d.offset == s.offset.to_i && d.value == suffix }
      disc ||= RTM::Discriminator.new(:offset => s.offset.to_i, :value => suffix, :description => s.description)
      disc.save
      ds.add_discriminator(disc)
      ds.save
      ChangeTracker.commit
      s.obj = v1disc
    end
  end
  
  def migrate_vendors
    structs[Struct::Vendor].each_value do |s|
      ChangeTracker.start
      suffix = s.suffix&.strip
      suffix = nil if suffix&.empty?
      v = RTM::Vendor.where(:name => s.vendor_name.strip, :suffix => suffix).first || RTM::Vendor.new(:name => s.vendor_name.strip, :suffix => suffix)
      v.save
      ChangeTracker.commit
      s.obj = v
    end
  end
  
  def migrate_tags
    structs[Struct::Tag].each_value do |s|
      ChangeTracker.start
      n = s.tag_name.to_s.strip
      n = '<tag has no value>' if n.empty?
      t = RTM::Tag.where(:name => n).first || RTM::Tag.new(:name => t)
      t.save
      ChangeTracker.commit
      s.obj = t
    end
  end
  
  def migrate_discussions
    structs[Struct::Discussion].each_value do |s|
      ChangeTracker.start
      d = RTM::Discussion.create
      ChangeTracker.commit
      s.obj = d
    end
  end
  
  # ignoring comment date...
  def migrate_discussions_and_comments
    setup_commentors unless @todd
    migrate_discussions
    structs[Struct::Comment].each_value do |s|
      ct = s.comment_text.strip
      next if ct == 'dsfsdf' # obviously garbage in record 4546
      ChangeTracker.start
      c = RTM::Comment.where(:comment => ct).first || RTM::Comment.new(:comment => ct)
      case s.user_name
      when 'ToddCooper'; c.user = @todd
      when 'nicosdo';    c.user = @nico
      when 'jrphilips';  c.user = @jr
      when 'Rtayrien';   c.user = @cmi
      end
      disc = get_struct(Struct::Discussion, s.discussion_id)&.obj
      c.discussion = disc if disc
      c.save
      ChangeTracker.commit
      s.obj = c
    end
  end
  
  def preprocess_units
    @units = {}
    @new_units = {}
    structs[Struct::Unit].each_value do |s|
      @units[s.term_id] = s.id if s.term_id
      @new_units[s.new_reference_id] = s.id if s.new_reference_id
      if s.dimension
        dim = RTM::Dimension.where(:value => s.dimension.strip).first
        unless dim
          ChangeTracker.start
          RTM::Dimension.create(:value => s.dimension.strip)
          ChangeTracker.commit
        end
      end
    end
  end
  
  # As good a time as any to go ahead and make Tokens...
  # TODO make sure discussions are connected to Tokens
  def preprocess_enums
    @enums = {}
    @new_enums = {}
    @tokens = {}
    structs[Struct::Enumeration].each_value do |s|
      @enums[s.term_id] = s.id if s.term_id
      @new_enums[s.new_reference_id] = s.id if s.new_reference_id
      if s.new_reference_id.nil? && s.term_id.nil?
        v = s.enum_value_code.strip
        if v && !v.empty?
          v = v.gsub(/\(\d+\)\s*$/, '')
          has_bits = !!(v =~ /<0/)
          v = v.gsub(/^<.*>\^/, '')
          desc = s.vendor_enum_description&.strip
          ChangeTracker.start
          token = RTM::Token.where(:value => v).first || RTM::Token.create(:value => v)
          if desc && !desc.empty?
            # add a definition and then a description to that
            defn = create_definition
            defn.add_description(find_or_create_description(desc, :p))
            token.definition = defn
          end
          token.asn1_bit = s.num_value.to_i if s.num_value && !(s.num_value.nil?)
          if has_bits
            token.bit_field = true
            token.valid0 = true
            token.valid1 = true
            token.valid_unset = false
          end
          token.save
          ChangeTracker.commit
          s.obj = token
          @tokens[s.id] = token
        end
      end
    end
  end
  
  def tokens
    @tokens
  end
  
  def migrate_enum_groups
    structs[Struct::EnumGroup].each_value do |s|
      ChangeTracker.start
      desc = s.vendor_description
      desc = "No description for #{s._enum_group.strip}" if desc.nil? || desc.strip.empty?
      eg = RTM::EnumerationGroup.where(:description => desc).first || RTM::EnumerationGroup.new(:description => desc)
      eg.refid = find_or_create_refid(s._enum_group)
      eg.save
      ChangeTracker.commit
      s.obj = eg
    end
  end  
  
  def migrate_unit_groups
    structs[Struct::UnitGroup].each_value do |s|
      ChangeTracker.start
      n = 'RefID: ' + s._uom_group
      ug = RTM::UnitGroup.where(:name => n).first || RTM::UnitGroup.new(:name => n)
      ug.refid = find_or_create_refid(s._uom_group)
      ug.save
      ChangeTracker.commit
      s.obj = ug
    end
  end
  
  def migrate_parameter_groups
    structs[Struct::ParamGroup].each_value do |s|
      ChangeTracker.start
      next if s.description =~ /Ignore me!/
      pg = nil
      if (s.signal_device || s.description) || s.system
        pg = RTM::ParameterGroup.where(:signal_device => s.signal_device, :description => s.description).first || RTM::ParameterGroup.create(:signal_device => s.signal_device, :description => s.description)
      end
      if s.system
        sys = RTM::System.where(:name => s.system).first || RTM::System.create(:name => s.system)
        pg.system = sys if pg
      end
      ChangeTracker.commit
      s.obj = pg
    end
  end
  
  def connect_unit_groups
    structs_arrays[Struct::RosettaUnitGroup].each do |s|
      r  = get_struct(Struct::Rosetta, s.id)
      ug = get_obj(Struct::UnitGroup, s.unit_group_id)
      ChangeTracker.start
      if r.term_id
        term = get_obj(Struct::CodedTerm, r.term_id)
        term.add_unit(ug) unless term.units.include?(ug)
      # elsif r.newReference_id
      #   term = get_obj(Struct::NewReference,r.newReference_id)
      end
      r.obj.add_unit(ug)
      ChangeTracker.commit
    end
  end
  
  def connect_enum_groups
    structs_arrays[Struct::RosettaEnumGroup].each do |s|
      r  = get_struct(Struct::Rosetta, s.id)
      eg = get_obj(Struct::EnumGroup, s.enum_group_id)
      ChangeTracker.start
      if r.term_id
        term = get_obj(Struct::CodedTerm,r.term_id)
        term.add_enum(eg) unless term.enums.include?(eg)
      # elsif r.newReference_id
      #   term = get_obj(Struct::NewReference,r.newReference_id)
      end
      r.obj.add_enum(eg)
      ChangeTracker.commit
    end
    structs_arrays[Struct::RosettaExternalSiteGroup].each do |s|
      r  = get_struct(Struct::Rosetta, s.id)
      esg = get_obj(Struct::EnumGroup, s.external_site_group_id)
      ChangeTracker.start
      if r.term_id
        term = get_obj(Struct::CodedTerm,r.term_id)
        # if s.external_site_group_id =~ /80/
        #   tstruct = get_struct(Struct::CodedTerm,r.term_id)
        #   # pp s
        #   # pp r.term_id
        #   pp tstruct
        #   puts term.statplus
        # end
        term.add_body_site(esg) unless term.enums.include?(esg)
        # puts "Added #{esg.description} to #{term.stat}"
      # elsif r.newReference_id
      #   term = get_obj(Struct::NewReference,r.newReference_id)
      end
      r.obj.add_enum(esg)
      ChangeTracker.commit
    end
  end

  def populate_unit_groups
    structs_arrays[Struct::UnitUnitGroup].each do |s|
      ug = get_obj(Struct::UnitGroup, s.id)
      u  = get_struct(Struct::Unit, s.unit_id)
      if u.term_id
        uterm = get_obj(Struct::CodedTerm,u.term_id)
      elsif u.new_reference_id
        uterm = get_obj(Struct::NewReference,u.new_reference_id)
      end
      ChangeTracker.start
      ug.add_member(uterm)
      ChangeTracker.commit
    end
  end
  
  def populate_enum_groups
    structs_arrays[Struct::EnumEnumGroup].each do |s|
      eg = get_obj(Struct::EnumGroup, s.enum_group_id)
      e  = get_struct(Struct::Enumeration, s.id)
      if e.term_id
        eterm = get_obj(Struct::CodedTerm,e.term_id)
      elsif e.new_reference_id
        eterm = get_obj(Struct::NewReference,e.new_reference_id)
      end
      ChangeTracker.start
      eg.add_member(eterm)
      ChangeTracker.commit
    end
  end
  
  def connect_units
    structs_arrays[Struct::RosettaUnit].each do |s|
      r = get_struct(Struct::Rosetta, s.id)
      u = get_struct(Struct::Unit, s.unit_id)
      ChangeTracker.start
      if u.term_id
        uterm = get_obj(Struct::CodedTerm,u.term_id)
      elsif u.new_reference_id
        uterm = get_obj(Struct::NewReference,u.new_reference_id)
      end
      if r.term_id
        term = get_obj(Struct::CodedTerm,r.term_id)
        if term.units.include?(uterm)
          # puts "#{term.refids.first.value} already includes #{uterm.refids.first.value}"
        else
          begin
            term.add_unit(uterm)
          rescue
            pp s
            puts
            pp r
            puts
            pp u
            puts
            if u.term_id
              us = get_struct(Struct::CodedTerm,u.term_id)
            elsif u.new_reference_id
              us = get_struct(Struct::NewReference,u.new_reference_id)
            end
            pp us
            puts
            pp uterm
            pp uterm.definition
            pp uterm.definition&.descriptions
            puts
            raise
          end
        end
      # elsif r.newReference_id
      #   term = get_obj(Struct::NewReference,r.newReference_id)
      end
      r.obj.add_unit(uterm)
      ChangeTracker.commit
    end
  end

  def connect_enums
    structs_arrays[Struct::RosettaEnum].each do |s|
      r = get_struct(Struct::Rosetta, s.id)
      e = get_struct(Struct::Enumeration, s.enum_id)
      if r.term_id
        term = get_obj(Struct::CodedTerm,r.term_id)
      elsif r.newReference_id
        term = get_obj(Struct::NewReference,r.newReference_id)
      end
      if e.obj
        eterm = e.obj.term
      elsif e.term_id
        puts Rainbow('BAD1').red
        eterm = get_obj(Struct::CodedTerm,e.term_id)
      elsif e.new_reference_id
        puts Rainbow('BAD2').red
        eterm = get_obj(Struct::NewReference,e.new_reference_id).term
      end
      ChangeTracker.start
      r.obj.add_enum(eterm)
      if r.term_id
        if term.enums.include?(eterm)
          if eterm.is_a?(RTM::Token)
            puts "#{term.refids.first.value} already includes #{eterm.value}"
          else
            # puts "#{term.refids.first.value} already includes #{eterm.refids.first.value}"
          end
        else
          term.add_enum(eterm)
        end
      end
      ChangeTracker.commit
    end
  end
  
  def connect_ucums
    structs_arrays[Struct::UnitUcumUnit].each do |s|
      uu = get_obj(Struct::UcumUnit, s.ucum_unit_id)
      u  = get_struct(Struct::Unit, s.id)
      if u.term_id
        uterm = get_obj(Struct::CodedTerm,u.term_id)
      elsif u.new_reference_id
        newref = get_struct(Struct::NewReference,u.new_reference_id)
        uterm = newref.obj
        unless uterm
          # We are looking for a term that replaced the MDCX term
          refid = newref.reference_id.gsub('MDCX_', 'MDC_')
          refid_obj = RTM::RefID.where(:value => refid).first
          if refid_obj
            if refid_obj.terms_count == 1
              uterm = refid_obj.terms.first
            else
              raise
            end
          else
            raise
          end
        end
      end
      unless uterm
        pp s
        p uu
        p u
        p get_struct(Struct::NewReference, u.new_reference_id)
      end
      ChangeTracker.start
      uterm.add_ucum_unit(uu)
      ChangeTracker.commit
    end
  end
  
  def migrate_groups
    structs[Struct::GroupId].each_value do |s|
      ChangeTracker.start
      g  = RTM::Group.where(:name => s.group_name).first || RTM::Group.create(:name => s.group_name)
      if s.param_group_id
        pg = get_struct(Struct::ParamGroup, s.param_group_id)
        unless pg
          pp s; puts
        end
        pg.obj.group = g if pg&.obj
      end
      ChangeTracker.commit
      s.obj = g
    end
  end
  
  def migrate_loinc
    structs[Struct::Loinc].each_value do |s|
      ChangeTracker.start
      args = {:number => s.loinc_num, :version => s.version, :common_name => s.loinc_common_name, :description => s.description}
      l = RTM::Loinc.where(args).first || RTM::Loinc.create(args)
      # ucums = s.ucum.to_s.split(/\s+/).collect { |x| RTM::UCUM_Unit.where(:value => x).first }.compact
      # l.ucum_units = ucums if ucums.any?
      refid = RTM::RefID.where(:value => s.refid.strip).first
      if refid
        refid.terms.each { |t| t.add_loinc(l) }
        # if refid.definitions_count > 1
        #   puts
        #   p s
        #   puts Rainbow("Multiple definitions for LOINC #{s.loinc_num} with refid: #{s.refid}").orange
        #   puts refid.definitions.collect { |d| d.refids.collect { |r| r.value }}.flatten.uniq.inspect
        #   p refid.definitions
        # else
        #   term = refid.definitions.first
        #   if term.loincs
        #     # puts Rainbow("Definition with refid: #{s.refid} already has a LOINC term!").yellow
        #   else
        #     # term.loinc = l
        #   end
        # end
      else
        puts Rainbow("No Term for LOINC #{s.loinc_num} with refid: #{s.refid}").red
      end
      ChangeTracker.commit
      s.obj = l
    end
  end
  
  def migrate_ucums
    structs[Struct::UcumUnit].each_value do |s|
      ChangeTracker.start
      u = RTM::UCUM_Unit.where(:value => s.uom_ucum).first || RTM::UCUM_Unit.create(:value => s.uom_ucum)
      ChangeTracker.commit
      s.obj = u
    end
  end
  
  def connect_tags
    puts Rainbow('Connect Tags').cyan
    structs_arrays[Struct::RosettaTag].each do |s|
      rs = get_struct(Struct::Rosetta, s.id)
      r = rs.obj
      t = get_obj(Struct::Tag, s.tag_id)
      term = r.term if r
      if term && t
        ChangeTracker.start
        term.add_tag(t)
        ChangeTracker.commit
      else
        puts Rainbow("(Rosetta) No match for tag entry #{s}").orange
        p rs; p r; p t; p term
      end
    end
    structs_arrays[Struct::EnumTag].each do |s|
      es = get_struct(Struct::Enumeration, s.id)
      e = es.obj
      t = get_obj(Struct::Tag, s.tag_id)
      term = e.term if e
      if term && t
        ChangeTracker.start
        term.add_tag(t)
        ChangeTracker.commit
      else
        puts Rainbow("(Enum) No match for tag entry #{s}").orange
        pp es; p t; p term
      end
    end
    structs_arrays[Struct::UnitTag].each do |s|
      us = get_struct(Struct::Unit, s.id)
      u = us.obj
      t = get_obj(Struct::Tag, s.tag_id)
      term = u.term if u
      if term && t
        ChangeTracker.start
        term.add_tag(t)
        ChangeTracker.commit
      else
        puts Rainbow("(Unit) No match for tag entry #{s}").orange
        p us; p u; p t; p term
      end
    end
  end
  
  def migrate_synonyms
    puts 'Migrating Synonyms'
    structs[Struct::Synonyms].each_value do |s|
      r1 = RTM.find_refid(s.preferred.strip)
      r2 = RTM.find_refid(s.acceptable.strip)
      r1terms = r1.terms
      unless r2
        term1 = r1terms.first if r1terms.count == 1
        ChangeTracker.start
        term2 = term1.class.new
        r2 = RTM.find_or_create_refid(s.acceptable.strip)
        term2.refid = r2
        term2.term_code = term1.term_code
        term2.save
        ChangeTracker.commit
      end
      r2terms = r2.terms
      
      if r1 && r2
        if r1terms.count == 1 && r2terms.count == 1
          term1 = r1terms.first
          term2 = r2terms.first
          ChangeTracker.start
          term1.refid_preferred = true
          term2.refid_preferred = nil
          term1.save
          term2.save
          ChangeTracker.commit
        else
          RTM.error("#{r1.value} has #{r1.terms_count} and #{r2.value} has #{r2.terms_count}. Fix this manually.", :red)
        end
      else
        if r1
          RTM.error("Could not locate both refIDs for v1 synonym #{s.id} - #{s.preferred} - #{s.acceptable} <- Missing.", :orange)
        else
          RTM.error("Could not locate both refIDs for v1 synonym #{s.id} - Missing -> #{s.preferred} - #{s.acceptable} <- Missing.", :orange)
        end
      end
    end
  end
    
  def clear
    clear_tables
    @structs = nil
  end
  
  def clear_tables
    RTM.classes(:no_imports => true).each do |c|
      next unless c < Sequel::Model
      next if c.enumeration?
      if DB.tables.include?(c.table_name)
        c.delete
      end
    end
    nil
  end
  
end

=begin
def create_structs(files = nil)
  # return if @structs # suppresses warnings
  # puts files.inspect
  @structs = {}
  @structs_arrays = {}
  files ||= all_csv_files
  files.select! { |f| f =~ /coded_term/}
  files.each do |path|
    puts Rainbow(path).red
    name = path.split('/').last.split('.').first.split('_').collect(&:capitalize).join
    begin
      # raw = File.read(path).gsub('\"', '"').gsub('"', '\u0022')#.encode!('UTF-8', 'UTF-8', :invalid => :replace)
      raw = File.read(path).gsub('\"', '"').gsub('"', '&quot;')#.encode!('UTF-8', 'UTF-8', :invalid => :replace)
      # raw.each_line do |l|
      #   if l =~ /&quot;/
      #     puts Rainbow(l).yellow
      #   end
      # end
      # raw.gsub!('\"', '"')
      # puts raw
      # raw = raw.gsub("\r", "\n").gsub(/\n+/, "\n")
      # raw = raw.gsub(/(?<=[^,"])"(?=[^,"\n])/, '""')
      rows = CSV.new(raw).read
    rescue Exception => e
      # puts "Path is #{path}"
      m = /in line (\d+)/.match(e.message)
      # puts m[1].inspect if m
      # puts raw.encoding
      problem_line = raw.split("\n")[m[1].to_i - 1]
      puts problem_line
      puts problem_line.inspect
      puts e.message
      puts
      next
    end
    # rows.each { |r| puts Rainbow(r).magenta }
    vars = rows.first.dup
    vars[0] = 'id'
    vars << 'obj' # add a var to the struct that will hold a ref to the Sequel::Model instance
    vars = vars.collect(&:to_sym)
    Object.const_set(name.to_sym, Struct.new(name, *vars)) unless Object.const_defined?(name)
    struct = name.to_const
    @structs[struct] = {}
    @structs_arrays[struct] = []
    rows[1..-1].each do |row|
      # debug = row.find { |c| c == 'NULL' }
      # puts Rainbow(row.inspect).green# if debug
      row = row.collect{ |c| c == 'NULL' ? nil : c }
      # use integers for keys if possible
      begin
        # key = row[0][0] =~ /\d/ ? row[0].to_i : row[0]
        key = row[0]
      rescue
        puts path
        puts Rainbow(row.inspect).red
        raise
      end
      begin
        s = struct.new(*row)
      rescue
        pp row
        raise
      end
      # pp s if debug
      # puts if debug
      @structs[struct][key.to_s.strip] = s 
      @structs_arrays[struct] << s
    end
    # return
  end
end  

=end