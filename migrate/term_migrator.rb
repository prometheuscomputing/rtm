require 'json'

module TermMigrator
  extend self
  
  def process_multiple_terms(opts)
    refid = opts[:refid]
    part_code = opts[:part_code]

    # some rows have multiple refids and codes (grrrrr)
    refids = refid.split(/\s/)
    if refids.count > 1
      puts Rainbow("Processing multiple term row with #{refids}").orange
      part_codes = part_code.split(/\s/)
      if part_codes.count == refids.count
        refids.each_with_index do |r, i|
          process_term(opts.merge({:refid => r, :part_code => part_codes[i]}))
        end
      elsif part_codes.count == 1
        refids.each do |r|
          process_term(opts.merge({:refid => r, :part_code => part_code}))
        end
      else
        RTM.error("#{refids} did not match #{part_codes}", :red)
      end
      return true
    else
      return false
    end  
  end

  def get_existing_term(opts)
    refid     = opts[:refid].strip
    part_code = opts[:part_code].strip
    terms = RTM.find_terms(refid, part_code)
    if terms.count > 1
      RTM.error("NOT PROCESSING. #{refid} #{part_code} because there are already multiple existing terms. Review manually.")
      return :error
    end
    term = terms.first
    return term if term      
    terms = RTM.find_codeless_terms(refid)
    if terms.count > 1
      RTM.error("NOT PROCESSING. #{refid} #{part_code} because there are already multiple codeless terms with RefID #{refid}. Review manually.")
      return :error
    end
    term = terms.first
    if term
      puts Rainbow("Found codeless #{term.class.to_s.split('::').last} ").orange + term._stat
      code_obj = RTM.find_code(part_code)
      unless code_obj
        RTM.info("Creating #{part_code} for provisional / proposed term #{refid}.", :green)
        ChangeTracker.start
        code_obj = RTM.find_or_create_code_in_partition(part_code)
        ChangeTracker.commit
      end
      ChangeTracker.start
      term.term_code = code_obj
      ChangeTracker.commit
      return term
    else
      # puts Rainbow("Not found #{refid} #{part_code}").bg(:red)
      return nil
    end
  end
  
  def process_term(opts = {})
    opts[:refid] = fix_refid(opts[:refid])
    return if process_multiple_terms(opts)
    
    # name         = opts[:name]
    refid        = opts[:refid]
    part_code    = opts[:part_code]
    type         = opts[:type] || (part_code =~ /4::/ ? RTM::Unit : RTM::Term)
    # belongs_to   = opts[:belongs_to]
    # derived_from = opts[:derived_from]
    table        = opts[:table]
    note         = opts[:note]
    status       = opts[:status]

    return if part_code =~ /\(\d+::\d+\)/ # parenthesis here in 10101R indicate it was already defined
    
    unless refid && !refid.empty?
      pp opts
      raise "refid is required"
    end
    unless part_code && !part_code.empty?
      pp opts
      raise "part_code is required"
    end
    raise "table is required" unless table || status == 'Provisional'
    
    return if refid.strip =~ /^RefId$/i # Why????
    
    term = get_existing_term(opts)
    return if term == :error # FIXME ? More?  might be adequate at this point<tt>.</tt>
              
    # TODO :belongs_to and :derived_from map to @used_by and @generalization, respectively.  Probably going to need to have a lookup for all of the possible values and have already created terms for them in order to do the association. 
    # What is name? --> it is "DIM Name" and there is nowhere in the model for it.  Using description since these terms don't have a description column in 10101R table
    # TODO check uom and ucum if they are included.  Do you still need to do this here?

    # This is a new term
    if term.nil?
      ChangeTracker.start
      term = RTM.create_term(refid, part_code)
      # TODO do :belongs_to and :derived_from and handle "DIM Name" or whatever else is in column 1
      ChangeTracker.commit
    end
    term.update_definition(opts, true)
    
    term.set_unit_properties(opts) if term.is_a?(RTM::Unit)

    if status == 'Provisional'
      RTM.set_term_status(term, 'Provisional')
    else
      # WARNING this is only OK if it is in 10101R
      RTM.set_term_status(term, 'Published')
    end
    
    RTM.connect_term_to_table(term, table) if table # partition 6 doesn't have an SDoc::Table because of the one-off method to deal with it
    RTM.add_to_10101R(term) unless status == 'Provisional'
    
    if @current_tag
      ChangeTracker.start
      term.add_tag(@current_tag)
      ChangeTracker.commit
    end
    
    raise if status && status != 'Provisional' # Why???
    if note&.strip&.[](0) && status.nil?
      ChangeTracker.start
      nt = RTM.find_or_create_note(note)
      ChangeTracker.commit
      unless term.notes.find { |old_nt| old_nt.id == nt.id }
        ChangeTracker.start
        term.add_note(nt)
        ChangeTracker.commit
      end
    end
    term
  end
  
  def process_token(opts)
    value = opts[:value].to_s.strip
    raise 'empty token' if value.empty?
    ChangeTracker.start
    token = RTM.find_or_create_token(value)
    ChangeTracker.commit
    token.update_definition(opts, true)
  
    RTM.connect_term_to_table(token, opts[:table]) if opts[:table]
    RTM.add_to_10101R(token) unless opts[:status] == 'Provisional'
    RTM.set_term_status(token, opts[:status] || 'Published')
    token
  end

  def fix_refid(refid)
    if refid == 'MDC_ATTR_SEG_DATA_GENMDC_ATTR_SEG_DATA_NU_OPTMDC_ATTR_SEG_DATA_RTSA_OPT'
      refid = 'MDC_ATTR_SEG_DATA_GEN MDC_ATTR_SEG_DATA_NU_OPT MDC_ATTR_SEG_DATA_RTSA_OPT'
    end
    refid
  end
  
  # not very DRY w/ add_discriminator
  def add_discriminator_to_term(term, set_name, offset, opts = {})
    set = RTM::DiscriminatorSet.where(:name => set_name).first
    raise "No DiscriminatorSet for #{set_name}" unless set
    discriminator = set.discriminators.find { |d| d.offset == offset }
    raise "No Discriminator with offset #{offset} in #{set_name}" unless discriminator

    # Get the discriminator and offset objects
    # make sure it is in an expected format
    # FIXME this whole method will fall apart if there is a coma in there.  Fix this when you actually have to deal with infix offsets...
    infix_skip = nil
    # unless set_name =~ /^(MVC|SDM|RCE|RCN|LAT|UoM|UoM1|1|MMM|LEAD|EVT|PN3|LOC|VER|LEAD1|LEAD2|EVT) \(\d+\)$/
    #   if set_name =~ /^(MVC|SDM|RCE|RCN|LAT|UoM|UoM1|1|MMM|LEAD|EVT|PN3|LOC|VER|LEAD1|LEAD2|EVT) \(\d+\),+$/
    #     infix_skip = disc.count(',')
    #   else
    #     raise "Special disc '#{set_name}'"
    #   end
    # end
    
    ChangeTracker.start
    if term.discriminators_count > 0
      unless term.discriminator_set_names.all? { |n| n =~ /LEAD[1|2]/ }
        if term.discriminators != [discriminator]
          RTM.warn("Term #{term.refid_stat} #{term.code_stat} #{term.discriminators_display} does not match 10101R discriminator #{discriminator.representation}.  Removing existing discriminators.", :orange)
          term.remove_all_discriminators
        end
      end
    end
    term.add_discriminator(discriminator) unless term.discriminators.include?(discriminator)
    term.status = 'Published (10101:2019 Annex C)' unless term.published2019? || opts[:provisional]
    set_infix_skip(term, set_name, infix_skip) if infix_skip
    ChangeTracker.commit
    set
  end
end
