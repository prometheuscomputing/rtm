module RTM
  module_function
  def test_v1
    create_structs
    preprocess_enums; preprocess_units
    # migrate_coded_terms
    # puts 'Copy the DB now'
    # return

    structs[Struct::NewReference].each_value do |s|
      # skip this entry if it is an MDCX term and there is already a corresponding MDC term
      new_refid = s.reference_id.strip
      if new_refid =~ /MDCX_/
        mdc_refid = new_refid.gsub('MDCX_', 'MDC_').strip
        if all_refids.include?(mdc_refid)
          mdc_source_struct   = structs[Struct::CodedTerm].find {|_,v| v.reference_id == mdc_refid}
          mdc_source_struct ||= structs[Struct::NewReference].find {|_,v| v.reference_id == mdc_refid}
          if mdc_source_struct
            obj = mdc_source_struct.last.obj
            if obj
              s.obj = obj
            # else
            #   puts Rainbow("No obj for #{new_refid} because NewReference with #{mdc_refid} hasn't been processed yet.").yellow
            end
          else
            puts new_refid
            puts mdc_refid
            pp mdc_coded_term
            puts
          end
          next
        end
      end
      if (rtm_refid = RTM::RefID.where(:value => new_refid).first) # because there are refids in the new_reference table that are also in the coded_term table...sigh...
        s.obj = rtm_refid.terms.first
        next
      end
      
      ChangeTracker.start      
      unit_id = @new_units[s.id]
      if unit_id
        u = get_struct(Struct::Unit, unit_id)
        # puts "New Unit: #{new_refid}"
        term = RTM::Unit.create(:dim => u.dim, :dim_c => u.dim_c, :symbol => u.symbol)
        if u.dimension
          dimension = RTM::Dimension.where(:value => u.dimension.strip).first
          term.add_dimension(dimension) if dimension
        end
      elsif s.partition_id =~ /^(23|24)$/
        term = RTM::Unit.create
      else
        term = RTM::Term.create
      end
      term.status = s.term_code ? 'Provisional' : 'Proposed'
      
      _add_description(s, term)
            
      refid = find_or_create_refid(new_refid)
      term.refid = refid
      # The new_reference table doesn't tell us what the status of the term is
      
      block = nil
      if s.partition_id
        # get block
        block_id = s.partition_id # this is really the id of the block. Original RTM had blocks & partitions mixed up
        block = get_struct(Struct::Block, block_id)
      end
      if block
        if s.term_code 
          code = s.term_code
          ptn  = block.partition_id
          tc   = find_or_create_code_in_partition("#{ptn}::#{code}")
          term.term_code = tc
        else
          puts Rainbow('Rosetta term with block and no term code! ' + new_refid).red
        end
      end

      ChangeTracker.commit
      if RTM.find_code('3::698')
        pp s
        pp block
        puts term.inspect
        raise "BANG"
      end
      s.obj = term
    end
    
  end  
end
