require 'csv'
load File.join(__dir__, 'migrate2.rb')
load File.join(__dir__, 'migration_logger.rb')
load File.join(__dir__, 'test.rb')

module RTM
  module_function
  
  def x73_refids
    @x73_refids ||= structs[Struct::CodedTerm].each_value.collect do |s|
      s.reference_id.strip
    end
  end
  
  def new_refids
    @new_refids ||= structs[Struct::NewReference].each_value.collect do |s|
      s.reference_id.strip
    end
  end  
  
  def all_refids
    @all_refids ||= x73_refids + new_refids
  end
  
  def migrate_rtm_v1
    time = Time.now
    RTM.reset_log('Migration_log_RTMMSv1')
    create_structs
    setup_sources

    import_partitions_and_blocks

    migrate_discriminator_sets; migrate_discriminators; migrate_vendors; migrate_tags
    migrate_discussions_and_comments
    migrate_enum_groups; preprocess_enums; migrate_unit_groups; preprocess_units
    migrate_ucums
    migrate_parameter_groups
    migrate_groups

    migrate_coded_terms
    migrate_new_terms

    migrate_rosetta
    migrate_enumerations
    migrate_units
    connect_units
    connect_enums
    connect_unit_groups

    connect_enum_groups
    # return
    populate_unit_groups
    populate_enum_groups
    connect_ucums
    migrate_loinc
    connect_sources
    connect_tags
    migrate_synonyms
    v1_end_report;
    system 'cp ~/Prometheus/data/rtm_generated/SQL.db ~/Prometheus/data/rtm_generated/SQL.db.v1'
    puts "That took #{(Time.now - time)/60} minutes." 
  end
  
  def v1_end_report
    puts "Finished at #{Time.now.strftime('%T%P %D')}"
  end

  def migrate_coded_terms    
    # term_id
    # standard_table_id - TODO
    # partition_id
    # term_code
    # reference_id
    # systematic_name
    # common_term
    # acronym
    # term_description
    # standard - TODO sortof
    # type - TODO (what is this?)
    # discriminator
    # discriminator_offset
    # attr_name - TODO (what is this?)
    # attr_type - TODO (what is this?)
    # dimension - TODO
    # unit - TODO
    # symbol - TODO (what is this?)
    # ref - TODO (what is this?)
    # mnemonic - TODO (what is this?)
    # update_date - TODO is it alright to not preserve this????
    extra_discriminators = {}
    standards = []
    types     = []
    structs[Struct::CodedTerm].each_value do |s|
      standards << s.standard if s.standard
      types << s.type if s.type
      
      ChangeTracker.start
      unit_id = @units[s.id]
      if unit_id
        u = get_struct(Struct::Unit, unit_id)
        # puts "New Unit: #{new_refid}"
        term = RTM::Unit.create(:dim => u.dim, :dim_c => u.dim_c, :symbol => u.symbol)
        if u.dimension
          dimension = RTM::Dimension.where(:value => u.dimension.strip).first
          term.add_dimension(dimension) if dimension # TODO assuming we've already parsed all the dimensions....FIXME if not
        end
      elsif s.partition_id =~ /^(23|24)$/ # this is a unit if it is in parition 4, which includes blocks with id 23 and 24.  Same true for new_reference table.  It isn't associated with any values for the units table so we don't have any params for it
        term = RTM::Unit.create
      else
        term = RTM::Term.create
      end
      
      refid = find_or_create_refid(s.reference_id.strip)
      term.refid = refid
      term.status = 'Published from RTMMSv1 coded term table'
      
      # get block
      block_id = s.partition_id # this is really the id of the block. Original RTM had blocks & partitions mixed up
      block = get_struct(Struct::Block, block_id)
      
      # do term code and discriminator
      # if block
      code = s.term_code
      ptn  = block.partition_id
      tc   = find_or_create_code_in_partition("#{ptn}::#{code}")
      if s.discriminator && s.discriminator[0]
        dset = RTM::DiscriminatorSet.where(:name => s.discriminator).first
        if dset
          discriminator = dset.discriminators.find { |d| d.offset == s.discriminator_offset.to_i }
          term.add_discriminator(discriminator) if discriminator
        else # FIXME argghhhhTMMS
          extra_discriminators[s.discriminator] ||= []
          extra_discriminators[s.discriminator] << s.discriminator_offset.to_i unless extra_discriminators[s.discriminator].include?(s.discriminator_offset.to_i)
        end
      end
      term.term_code = tc
      # else
      #   puts "NO BLOCK for #{s.reference_id}"
      #   puts Rainbow("#{s.partition_id}: #{s.term_code}\n").red
      # end

      if s.standard || s.standard_table_id # if it has a value, then it is 10101a TODO does it matter if it was the discriminator extension??
        term.add_source(@ieee10101a)
        # FIXME later....
        # if s.standard_table_id
        #   ChangeTracker.start
        #   loc = defn.sources_associations.find { |assoc| assoc[:to].name == '10101a' }[:through]
        #   loc_struct = get_struct(Struct::StandardTable, s.standard_table_id)
        #   pp s unless loc_struct
        #   val = "Table #{loc_struct.table_id}"
        #   val << " - #{loc_struct.table_desc}" if loc_struct.table_desc
        #   val << " (refid MDC: #{loc_struct.refid_mdc})" if loc_struct.refid_mdc
        #   loc.table = val
        #   loc.save
        #   ChangeTracker.commit
        # end
      end
      # FIXME there are terms with broken partition stuff.  Just bypassing that for now but that really won't fly.
      
      _add_description(s, term)
      ChangeTracker.commit
      s.obj = term
    end
    unless extra_discriminators.empty?
      puts Rainbow('Extra discriminators').orange
      pp extra_discriminators;puts
    end
    # puts Rainbow('Standards').orange
    # pp standards.uniq.sort;puts
    # puts Rainbow('Types').orange
    # pp types.uniq.sort;puts
  end
  
  def _appropriate_definition(s)
    definition_args = {:systematic_name => s.systematic_name, :acronym => s.acronym, :common_term => s.common_term}
    # Don't reuse empty definitions
    defn = definition_args.values.compact.any? ? find_or_create_definition(definition_args) : RTM::Definition.create
  end
  
  def _add_description(s, term)
    if s.term_description        
      defn = _appropriate_definition(s)
      desc = find_or_create_description(s.term_description, :p)
      defn.add_description(desc) unless defn.descriptions.include?(desc)
      term.definition = defn
    end
  end
  
  # standard is always null
  # none have a discriminator (other than 1)
  # TODO map to standard table (i.e. table in a standard from whence this came) later
  def migrate_new_terms
    # id
    # standard_table_id - TODO
    # partition_id -- really the block
    # block_id -- really the partition
    # term_code
    # reference_id
    # systematic_name
    # common_term
    # acronym
    # term_description
    # standard - all NULL
    # discriminator - can ignore - all are either NULL or 1 (where 1 means no discriminator)
    # discriminator_offset - can ignore
    # update_date - TODO is it alright to not preserve this????
    structs[Struct::NewReference].each_value do |s|
      # skip this entry if it is an MDCX term and there is already a corresponding MDC term
      new_refid = s.reference_id.strip
      if new_refid =~ /MDCX_/
        mdc_refid = new_refid.gsub('MDCX_', 'MDC_').strip
        if all_refids.include?(mdc_refid)
          mdc_source_struct   = structs[Struct::CodedTerm].find {|_,v| v.reference_id == mdc_refid}
          mdc_source_struct ||= structs[Struct::NewReference].find {|_,v| v.reference_id == mdc_refid}
          if mdc_source_struct
            obj = mdc_source_struct.last.obj
            if obj
              s.obj = obj
            # else
            #   puts Rainbow("No obj for #{new_refid} because NewReference with #{mdc_refid} hasn't been processed yet.").yellow
            end
          else
            puts new_refid
            puts mdc_refid
            pp mdc_coded_term
            puts
          end
          next
        end
      end
      if (rtm_refid = RTM::RefID.where(:value => new_refid).first) # because there are refids in the new_reference table that are also in the coded_term table...sigh...
        s.obj = rtm_refid.terms.first
        next
      end
      
      ChangeTracker.start      
      unit_id = @new_units[s.id]
      if unit_id
        u = get_struct(Struct::Unit, unit_id)
        # puts "New Unit: #{new_refid}"
        term = RTM::Unit.create(:dim => u.dim, :dim_c => u.dim_c, :symbol => u.symbol)
        if u.dimension
          dimension = RTM::Dimension.where(:value => u.dimension.strip).first
          term.add_dimension(dimension) if dimension
        end
      elsif s.partition_id =~ /^(23|24)$/
        term = RTM::Unit.create
      else
        term = RTM::Term.create
      end
      term.status = s.term_code ? 'Provisional' : 'Proposed'
      
      _add_description(s, term)
            
      refid = find_or_create_refid(new_refid)
      term.refid = refid
      # The new_reference table doesn't tell us what the status of the term is
      
      block = nil
      if s.partition_id
        # get block
        block_id = s.partition_id # this is really the id of the block. Original RTM had blocks & partitions mixed up
        block = get_struct(Struct::Block, block_id)
      end
      if block
        if s.term_code 
          code = s.term_code
          ptn  = block.partition_id
          tc   = find_or_create_code_in_partition("#{ptn}::#{code}")
          term.term_code = tc
        else
          puts Rainbow('Rosetta term with block and no term code! ' + new_refid).red
        end
      end

      ChangeTracker.commit
      s.obj = term
    end
  end
  
  def migrate_rosetta
    structs[Struct::Rosetta].each_value do |s|
      # TODO change attr to :desription (singular) ?
      args = {:dname => s.vendor_dname, :vendor_uom_legacy => s.vendor_uom, :vendor_vmd_legacy => s.vendor_vmd }
      ChangeTracker.start
      r = RTM::RosettaEntry.new(args)
      if s.vendor_description
        desc = find_or_create_description(s.vendor_description, :p)
        r.description = desc
      end
      if s.term_id
        # It should already be marked as 'Published' 
        term = get_term(s.term_id)
        # term.status = 'Published'
        
        # one off because there is only one
        if s.term_id == '476'
          term.status = 'Deprecated'
          term.save
        end
      elsif s.newReference_id
        term = get_new_term(s.newReference_id)
        pp s unless term
        puts unless term
        # one off because there is only one
        if s.newReference_id.to_i == 2640
          term.status = 'Rejected'
          term.save
        end
      end
      r.harmonized = true if s.approved&.strip == '1'
      puts term.inspect if term.is_a?(String)
      r.term = term
      r.vendor = get_obj(Struct::Vendor, s.vendor_id) if s.vendor_id
      r.status = s.vendor_status.strip if s.vendor_status
      r.sort = s.vendor_sort.to_i if s.vendor_sort
      r.general_discussion = get_obj(Struct::Discussion, s.general_discussion_id) if s.general_discussion_id
      r.vendor_discussion  = get_obj(Struct::Discussion, s.vendor_discussion_id)  if s.vendor_discussion_id
      r.comment = s.vendor_comment.strip if s.vendor_comment
      ChangeTracker.commit
      s.obj = r
    end
  end
  
  def migrate_enumerations
    structs[Struct::Enumeration].each_value do |s|
      args = {}
      ChangeTracker.start
      r = RTM::RosettaEntry.new(args)
      if s.vendor_enum_description
        desc = find_or_create_description(s.vendor_enum_description, :p)
        r.description = desc
      end
      # the struct already has an associated obj if it is for a Token.  Shift the token to being the definition for the Rosetta obj.
      term = s.obj
      if s.term_id
        # It should already be marked as 'Published'
        term = get_term(s.term_id)
        # term.status = 'Published'
        # term.save
      elsif s.new_reference_id
        nr = get_struct(Struct::NewReference, s.new_reference_id)
        term = nr.obj
        unless term
          refid_obj = RTM::RefID.where(:value => s.new_reference_id.sub('MDCX_', 'MDC_').strip).first
          if refid_obj && refid_obj.terms_count == 1
            term = refid_obj.terms.first
          end
        end
        unless term
          mdc_struct = structs[Struct::NewReference].find { |_,x| x.reference_id == s.new_reference_id.sub('MDCX_', 'MDC_').strip}
          if mdc_struct
            term = mdc_struct.obj
          end
        end
        
      end
      RTM.error("No term for #{s}", :red) unless term
      r.harmonized = true if s.approved&.strip == '1'
      r.term = term
      r.general_discussion = get_obj(Struct::Discussion, s.discussion_id) if s.discussion_id
      ChangeTracker.commit
      s.obj = r
    end
  end
  
  def migrate_units
    structs[Struct::Unit].each_value do |s|
      args = {}
      ChangeTracker.start
      r = RTM::RosettaEntry.new(args)
      if s.description
        desc = find_or_create_description(s.description, :p)
        r.description = desc
      end
      if s.term_id
        # It should already be marked as 'Published'
        term = get_term(s.term_id)
        # term.status = 'Published'
        # term.save
      elsif s.new_reference_id
        nr = get_struct(Struct::NewReference, s.new_reference_id)
        term = nr.obj
        unless term
          refid_obj = RTM::RefID.where(:value => nr.reference_id.sub('MDCX_', 'MDC_').strip).first
          if refid_obj && refid_obj.terms_count == 1
            term = refid_obj.terms.first
          end
        end
        unless term
          mdc_struct = structs[Struct::NewReference].find { |_,x| x.reference_id == nr.reference_id.sub('MDCX_', 'MDC_').strip}
          if mdc_struct
            puts Rainbow(mdc_struct.inspect).purple
            term = mdc_struct.last.obj
          end
        end
        if s.deprecated.to_i == 1
          term.status = 'Rejected'
          term.save
        end
      end
      r.harmonized = true if s.approved&.strip == '1'
      puts Rainbow(term.inspect).red if term.is_a?(String)
      r.term = term
      ChangeTracker.commit
      s.obj = r
    end
  end
  
  def get_term(id)
    get_obj(Struct::CodedTerm, id)
  end

  def get_new_term(id)
    return nil unless id
    struct = get_struct(Struct::NewReference, id)
    obj = struct.obj
    return obj if obj
    mdc_refid = struct.reference_id.gsub("MDCX_", "MDC_").strip
    mdc_source_struct   = structs[Struct::CodedTerm].find {|_,v| v.reference_id == mdc_refid}
    mdc_source_struct ||= structs[Struct::NewReference].find {|_,v| v.reference_id == mdc_refid}
    obj = mdc_source_struct&.last.obj
    return obj if obj
    puts Rainbow("No obj for #{struct.reference_id}!").red
  end
  
  def get_struct(type, id)
    return nil unless id
    ret = structs[type][id.to_s.strip]
    puts Rainbow("#{type}[#{id.inspect}] not found.  Called from #{caller[0..2]}").magenta unless ret
    ret
  end
  
  def get_obj(type, id)
    # puts Rainbow(type.name + ' ' + id.to_s.inspect).green
    struct = get_struct(type, id)
    unless struct
      puts Rainbow("No struct for #{type} with ID #{id.inspect} -- #{caller.first}").cyan
      return nil
    end
    ret = struct.obj
    unless ret
      puts Rainbow("No obj for #{type} with ID #{id.inspect} -- #{caller.first}").cyan
      pp struct
      return nil
    end
    ret
  end

end
