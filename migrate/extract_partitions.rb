class TermExtractor
  def parse_partitions
    partitions = {}
    @tables[3..20].each_with_index do |t,idx|
      next if idx == 2 # messed up copy of the Metrics partition table
      next if idx == 5 # this table contains terms, not blocks....sigh...
      number, title = t[:title].split(/ [-–] /)
      number = number.sub('Partition ', '').strip.to_i
      partition = RTM::Partition.where(:number => number).first
      partitions[number] = {:name => title}
      rowz = t[:docx_rows][1..-1]
      bloks = []
      rowz.each_with_index do |row, ridx|
        raise unless row.count == 2
        name, range = row
        name = extract_text(name)
        range = extract_text(range)
        lower, upper = range.strip.split(/\s*[–-]\s*/)
        # get rid of leading zeros
        lower = lower.strip.gsub(/^0+(?=\d)/, '') if lower
        upper = upper.strip.gsub(/^0+(?=\d)/, '') if upper
        bloks << {:name => name, :lower => lower, :upper => upper} unless lower.nil?
      end
      partitions[number][:blocks] = bloks
    end
    partitions
  end
  
  # pasted in results from method defined above #parse_partitions
  # things commented out manually
  # partition 514 added manually, other fixes after manual review
  # comments added at problem spots
  def parsed_partitions
    {1=>
      {:name=>"Object-oriented elements, Device nomenclature",
       :blocks=>
        [#{:name=>"Object-Oriented", :lower=>"0", :upper=>"3999"},
         {:name=>"Classes", :lower=>"1", :upper=>"1280"},
         {:name=>"Attribute groups", :lower=>"2049", :upper=>"2304"},
         {:name=>"Attributes", :lower=>"2305", :upper=>"3073"},
         {:name=>"Actions", :lower=>"3074", :upper=>"3327"},
         {:name=>"Notifications", :lower=>"3328", :upper=>"3999"},
         {:name=>"Devices", :lower=>"4000", :upper=>"5999"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     2=>
      {:name=>"Metrics",
       :blocks=>
        [{:name=>"ECG leads", :lower=>"0", :upper=>"255"},
         {:name=>"ECG per lead measurements", :lower=>"256", :upper=>"15359"},
         {:name=>"ECG measurements reserved", :lower=>"15360", :upper=>"15879"},
         {:name=>"ECG global measurements", :lower=>"15880", :upper=>"16383"},
         {:name=>"ECG diagnostic patterns", :lower=>"16384", :upper=>"16447"},
         {:name=>"ECG diagnostic events", :lower=>"16448", :upper=>"17999"},
         {:name=>"ECG diagnostic statistics", :lower=>"18000", :upper=>"18431"},
         {:name=>"ECG per lead measurements", :lower=>"34048", :upper=>"35363"},
         {:name=>"Haemodynamic events", :lower=>"18432", :upper=>"18687"},
         {:name=>"Haemodynamic indices", :lower=>"18688", :upper=>"18943"},
         {:name=>"Haemodynamic measures", :lower=>"18944", :upper=>"20479"},
         {:name=>"Respiratory events", :lower=>"20480", :upper=>"20607"},
         {:name=>"Respiratory measures", :lower=>"20608", :upper=>"21407"},
         {:name=>"Respiratory rates", :lower=>"21408", :upper=>"21523"},
         {:name=>"Nebulizer measures", :lower=>"21524", :upper=>"22531"},
         {:name=>"Ventilation modes", :lower=>"53280", :upper=>"53281"},
         {:name=>"Blood gas, urine, etc.", :lower=>"28676", :upper=>"29999"},
         {:name=>"Fluid output", :lower=>"26624", :upper=>"26684"},
         {:name=>"Pump data", :lower=>"26688", :upper=>"26876"},
         {:name=>"Substance/Type", :lower=>"53396", :upper=>"53408"},
         {:name=>"Neurological haemodynamics", :lower=>"22532", :upper=>"22563"},
         {:name=>"Neurological", :lower=>"22564", :upper=>"23559"},
         {:name=>"Neurological patterns", :lower=>"23560", :upper=>"25179"},
         {:name=>"Neurological stimulation", :lower=>"53504", :upper=>"53553"},
         {:name=>"Miscellaneous", :lower=>"57348", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     3=>
      {:name=>"Alerts and events",
       :blocks=>
        [#{:name=>"Alerts/Events", :lower=>"0", :upper=>"6600"},
         {:name=>"Device events", :lower=>"0", :upper=>"999"},
         {:name=>"Pattern events", :lower=>"3072", :upper=>"3309"},
         {:name=>"Status events", :lower=>"6144", :upper=>"6733"},
         {:name=>"Location services events", :lower=>"7168", :upper=>"7191"},
         {:name=>"Ventilator events", :lower=>"20576", :upper=>"20581"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     4=>
      {:name=>"Units of measurement (dimensions)",
       :blocks=>
        [{:name=>"Units of measurement", :lower=>"0", :upper=>"6047"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     6=>
      {:name=>"Program group",
       :blocks=>
        [{:name=>"Program group codes", :lower=>"513", :upper=>"521"}]},
     7=>
      {:name=>"Body sites",
       :blocks=>
        [
         # {:name=>"Body sites", :lower=>"0", :upper=>"1810"},
         {:name=>"Neuro nerves", :lower=>"0", :upper=>"246"},
         {:name=>"Neuro muscles", :lower=>"248", :upper=>"994"},
         {:name=>"Electroencephalogram (EEG)", :lower=>"996", :upper=>"1318"},
         {:name=>"Electrooculogram (EOG)", :lower=>"1320", :upper=>"1402"},
         {:name=>"Neuro monitoring", :lower=>"1404", :upper=>"1422"},
         {:name=>"Cardio", :lower=>"1424", :upper=>"1506"},
         {:name=>"Coronary arteries", :lower=>"1812", :upper=>"1840"},
         {:name=>"Miscellaneous sites", :lower=>"1508", :upper=>"1810"},
         # {:name=>"Head region", :lower=>"1844", :upper=>"1662"}, # FIXME
         {:name=>"Gas measurement sites", :lower=>"2048", :upper=>"2097"},
         {:name=>"Body site qualifier", :lower=>"8193", :upper=>"8271"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     8=>
      {:name=>"Communication infrastructure",
       :blocks=>
        [{:name=>"Infrastructure", :lower=>"0", :upper=>"8197"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     9=>
      {:name=>"Reserved for file exchange format (FEF)",
       :blocks=>
        [{:name=>"FEF", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     10=>
      {:name=>"ECG additional nomenclature (annotated ECG)",
       :blocks=>
        [{:name=>"Annotated ECG", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     11=>
      {:name=>"IDCO nomenclature",
       :blocks=>
        [{:name=>"ICDO", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     128=>
      {:name=>"PHD disease management",
       :blocks=>
        [
         # {:name=>"PHD DM", :lower=>"0", :upper=>"61439"},
         {:name=>"Basic ECG", :lower=>"21976", :upper=>"21999"},
         {:name=>"SABTE", :lower=>"22100", :upper=>"22571"},
         {:name=>"Glucose meter", :lower=>"29144", :upper=>"29288"},
         {:name=>"INR", :lower=>"29300", :upper=>"29319"},
         {:name=>"Insulin pump", :lower=>"29400", :upper=>"29414"},
         {:name=>"Continuous glucose meter", :lower=>"29428", :upper=>"29836"},
         {:name=>"Battery status", :lower=>"29904", :upper=>"30032"},
         {:name=>"Peak Expiratory Flow Meter", :lower=>"30720", :upper=>"30720"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     129=>
      {:name=>"PHD health and fitness",
       :blocks=>
        [{:name=>"PHD HF", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     130=>
      {:name=>"PHD aging independently",
       :blocks=>
        [
         # {:name=>"PHD AI", :lower=>"0", :upper=>"61439"},
         {:name=>"Sensors", :lower=>"1", :upper=>"20"},
         {:name=>"Measurements", :lower=>"16", :upper=>"127"},
         {:name=>"Location", :lower=>"1024", :upper=>"4543"},
         {:name=>"Location", :lower=>"9216", :upper=>"11743"},
         {:name=>"Location", :lower=>"20000", :upper=>"22335"},
         {:name=>"Appliance", :lower=>"7168", :upper=>"8672"},
         {:name=>"Medical location", :lower=>"35000", :upper=>"35491"},
         {:name=>"Events", :lower=>"55000", :upper=>"55141"},
         {:name=>"Medication monitor", :lower=>"13312", :upper=>"13320"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     255=>
      {:name=>"Return codes",
       :blocks=>
        [{:name=>"Return codes", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     256=>
      {:name=>"External nomenclatures",
       :blocks=>
        [{:name=>"External nomenclatures", :lower=>"0", :upper=>"1600"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     258=>
      {:name=>"Settings",
       :blocks=>
        [{:name=>"Settings", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     514=>
      {:name=>"Predicted values",
       :blocks=>
         [{:name=>"Predicted values", :lower=>"0", :upper=>"61439"},
          {:name=>"Private", :lower=>"61440", :upper=>"65535"}]},
     1024=>
      {:name=>"Private",
       :blocks=>
        [{:name=>"Private", :lower=>"0", :upper=>"61439"},
         {:name=>"Private", :lower=>"61440", :upper=>"65535"}]
       }
     }
  end
  
  def process_partitions
    parsed_partitions.each do |number, parsed_partition|
      partition = RTM.find_or_create_partition(number.to_s)
      ChangeTracker.start
      partition.name = parsed_partition[:name]
      partition.save
      ChangeTracker.commit
      # puts Rainbow("#{number} -- #{parsed_partition[:name]}").green
      # puts Rainbow("#{number} -- #{partition.name.value} -- #{partition.description}").orange
      existing_blocks = partition.blocks
      # existing_blocks.each do |block|
      #   puts Rainbow("#{block.name} -- #{block.lower} - #{block.upper} -- #{block.description}").yellow
      # end
      parsed_blocks = parsed_partition[:blocks]
      puts Rainbow("Different number of blocks for #{number} - #{parsed_partition[:name]}!").orange unless parsed_blocks.count == existing_blocks.count + 1
      parsed_blocks.each_with_index do |pb, ridx|
        existing_block = existing_blocks[ridx]
        unless existing_block
          puts Rainbow("No block matching #{pb[:name]} -- #{pb[:lower]} - #{pb[:upper]}").magenta
          next
        end
        if (pb[:upper] != existing_block.upper) || (pb[:lower] != existing_block.lower) || (pb[:name] != existing_block.name) && !pb[:name] == 'Private'
          puts Rainbow("#{pb[:name]} -- #{pb[:lower]} - #{pb[:upper]}").cyan
          puts Rainbow("#{existing_block.name} -- #{existing_block.lower} - #{existing_block.upper} -- #{existing_block.description}").yellow
        end
      end
    end
  end  
end