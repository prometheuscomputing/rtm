class TermExtractor

  # ["dim name", "reference id", "derived from", "part::code"]
  def parse_object_tables
    process = proc do |cells, table|
      dim_name, refid, derived_from, part_code = cells.collect { |cell| extract_text(cell) }
      description = 'DIM Name: ' + dim_name
      term = process_term(table:table, description:description, refid:refid, derived_from:derived_from, part_code:part_code)
    end
    object_tables = [21, 26, 31, 36, 41, 46, 51, 56, 61, 66, 70, 71, 76, 81, 87, 218, 220]
    _parse_tables('Object', object_tables, process)
  end
  
  # ["dim name", "reference id", "belongs to object", "part::code"]
  def parse_attribute_tables
    process = proc do |cells, table|
      dim_name, refid, belongs, part_code = cells.collect { |cell| extract_text(cell) }
      description = 'DIM Name: ' + dim_name
      term = process_term(table:table, description:description, refid:refid, belongs_to:belongs, part_code:part_code)
    end
    attribute_tables = [22, 23, 24, 25, 27, 28, 29, 30, 32, 33, 34, 35, 37, 38, 39, 40, 42, 43, 44, 45, 47, 48, 49, 50, 52, 53, 54, 55, 57, 58, 59, 60, 62, 63, 64, 65, 67, 68, 69, 73, 74, 75, 78, 79, 80, 83, 84, 85, 88, 89, 90, 91, 92, 93, 94]
    _parse_tables('Attribute', attribute_tables, process)
  end
  
  # ["description", "reference id", "belongs to object", "part::code"]
  # NOTE: belongs_to column is empty for all rows in 10101R
  def parse_usi_tables
    process = proc do |cells, table|
      description, refid, belongs, part_code = cells.collect { |cell| extract_text(cell) }
      term = process_term(table:table, description:description, refid:refid, part_code:part_code)
    end
    _parse_tables('USI', [82], process)
  end
  
  # ["description", "unit of measurement", "uom ucum", "reference id", "part::code"]
  # FIXME we are not dealing with uom and ucum in #process_term for these things right now
  def parse_wcm_mem_ls_tables
    process = proc do |cells, table|
      description, uom, ucum, refid, part_code = cells.collect { |cell| extract_text(cell) }
      term = process_term(table:table, description:description, uom:uom, ucum:ucum, refid:refid, part_code:part_code)
    end
    _parse_tables('IHE', [72, 77], process)
  end
  
  # ["dim name", "reference id", "note", "part::code"]
  # OK...the rows with part codes 1::2582, 1::2398, and 1::2592 are only deprecated refids.  The rest are deprecated refids and term codes.  fun.  FIXME
  # def parse_deprecated_oo_tables
  #   process = proc do |cells, table|
  #     dim_name, refid, note, part_code = cells.collect { |cell| extract_text(cell) }
  #     description = 'DIM Name: ' + dim_name if dim_name && dim_name[0]
  #     term = process_term(:status => :deprecate_refid, table:table, description:description, refid:refid, note:note, part_code:part_code)
  #   end
  #   _parse_tables('Deprecated OO', [86], process)
  # end
  
  def parse_oo
    parse_object_tables
    parse_attribute_tables
    parse_usi_tables
    parse_wcm_mem_ls_tables
  end

end