def annex_b514
  [
    {:block => 'SPIROMETRY  ', :partition => 514 , :description => 'Predicted Spirometry parameters   '},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_PEAK_PRED', :code => 21512},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_1S_PRED', :code => 21514},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_6S_PRED', :code => 21515},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_CAPACITY_PRED', :code => 57856},
    {:refid => 'MDC_VOL_AWAY_SLOW_CAPACITY_PRED', :code => 57860},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_FEV1_FEV6_PRED', :code => 57864},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_0_5S_PRED', :code => 57868},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_0_75S_PRED', :code => 57872},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_1S_FVC_PRED', :code => 57876},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_0_5S_FVC_PRED', :code => 57880},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_0_75S_FVC_PRED', :code => 57884},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_25_75_FVC_PRED', :code => 57888},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_25_FVC_PRED', :code => 57892},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_50_FVC_PRED', :code => 57896},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_75_FVC_PRED', :code => 57900},
    {:refid => 'MDC_FLOW_AWAY_INSP_FORCED_PEAK_PRED', :code => 57904},
    {:refid => 'MDC_VOL_AWAY_INSP_FORCED_CAPACITY_PRED', :code => 57908},
    {:refid => 'MDC_VOL_AWAY_INSP_FORCED_1S_PRED', :code => 57912},
    {:refid => 'MDC_FLOW_AWAY_INSP_FORCED_25_PRED', :code => 57916},
    {:refid => 'MDC_FLOW_AWAY_INSP_FORCED_50_PRED', :code => 57920},
    {:refid => 'MDC_FLOW_AWAY_INSP_FORCED_75_PRED', :code => 57924},
    {:refid => 'MDC_VOL_AWAY_INSP_CAPACITY_PRED', :code => 57928},
    {:refid => 'MDC_VOL_AWAY_EXP_RESERVE_PRED', :code => 57932},
    {:refid => 'MDC_VOL_AWAY_INSP_RESERVE_PRED', :code => 57936},
    {:refid => 'MDC_VOL_AWAY_INSP_SLOW_CAPACITY_PRED', :code => 57940},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_TIME_PRED', :code => 57944},
    {:refid => 'MDC_VOL_AWAY_EXTRAP_PRED', :code => 57948},
    {:refid => 'MDC_VOL_AWAY_EXP_SLOW_CAPACITY_PRED', :code => 57964},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_2S_PRED', :code => 57968},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_3S_PRED', :code => 57972},
    {:refid => 'MDC_VOL_AWAY_EXP_FORCED_5S_PRED', :code => 57976},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_2S_FVC_PRED', :code => 57980},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_3S_FVC_PRED', :code => 57984},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_5S_FVC_PRED', :code => 57988},
    {:refid => 'MDC_RATIO_AWAY_EXP_FORCED_6S_FVC_PRED', :code => 57992},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_MAX_PRED', :code => 57996},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_25_50_PRED', :code => 58000},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_75_85_PRED', :code => 58004},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_0_2L_1_2L_PRED', :code => 58008},
    {:refid => 'MDC_FLOW_AWAY_EXP_FORCED_85_PRED', :code => 58012},
    {:refid => 'MDC_VOL_AWAY_EXP_TIDAL_TIME_PRED', :code => 58016},
    {:refid => 'MDC_VOL_AWAY_INSP_TIDAL_TIME_PRED', :code => 58020},
    {:refid => 'MDC_RATIO_AWAY_TIN_TEX_PRED', :code => 58024},
    {:refid => 'MDC_FLOW_AWAY_INSP_FORCED_25_50_PRED', :code => 58028},
    {:refid => 'MDC_FLOW_AWAY_INSP_FORCED_25_75_PRED', :code => 58032},
    {:refid => 'MDC_RATIO_AWAY_INSP_FORCED_1S_FIVC_PRED', :code => 58036},
    {:refid => 'MDC_VOL_AWAY_CAPACITY_VOLUNTARY_MAX_12S_PRED', :code => 58040},
    {:refid => 'MDC_VOL_AWAY_CAPACITY_VOLUNTARY_MAX_15S_PRED', :code => 58044},
    {:refid => 'MDC_VOL_AWAY_EXP_25_75_TIME_PRED', :code => 58048},
    {:refid => 'MDC_FLOW_AWAY_EXP_PEAK_TIME_PRED', :code => 58052},
    {:refid => 'MDC_FLOW_AWAY_EXP_TIDAL_MEAN_PRED', :code => 58056}
  ]
end
