[
  {:block => 'GROUP DEFINITIONS  ', :partition => 6, :description => 'Group Definition'},
  {:refid => 'MDC_PGRP_HEMO', :code => 513},
  {:refid => 'MDC_PGRP_ECG', :code => 514},
  {:refid => 'MDC_PGRP_RESP', :code => 515},
  {:refid => 'MDC_PGRP_VENT', :code => 516},
  {:refid => 'MDC_PGRP_NEURO', :code => 517},
  {:refid => 'MDC_PGRP_DRUG', :code => 518},
  {:refid => 'MDC_PGRP_FLUID', :code => 519},
  {:refid => 'MDC_PGRP_BLOOD_CHEM', :code => 520},
  {:refid => 'MDC_PGRP_MISC', :code => 521}
]
