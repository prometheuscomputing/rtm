def annex_b9
  [{:block => 'FEF  ', :partition => 9, :description => 'File Exchange Format This is an externally defined nomenclature   '}]
end

def annex_b10
  [{:block => 'ECGext  ', :partition => 10, :description => 'ECG Extension This nomenclature is defined in IEEE 11073-10102   '}]
end

def annex_b11
  [{:block => 'ICDOext  ', :partition => 11, :description => 'ICDO Extension This nomenclature is defined in IEEE 11073-10103   '}]
end
