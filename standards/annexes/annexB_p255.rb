def annex_b255
  [
    {:block => 'MDC_PART_RET_CODE/CMIP  ', :partition => 255, :description => 'Error Return Codes for CMIP actions '},
    {:refid => 'MDC_RET_CODE_NOOSUCHOBJECTCLASS', :code => 0},
    {:refid => 'MDC_RET_CODE_NOSUCHOBJECTINSTANCE', :code => 1},
    {:refid => 'MDC_RET_CODE_ACCESSDENIED', :code => 2},
    {:refid => 'MDC_RET_CODE_NOSUCHATTRIBUTE', :code => 5},
    {:refid => 'MDC_RET_CODE_INVALIDATTRIBUTEVALUE', :code => 6},
    {:refid => 'MDC_RET_CODE_GETLISTERROR', :code => 7},
    {:refid => 'MDC_RET_CODE_SETLISTERROR', :code => 8},
    {:refid => 'MDC_RET_CODE_NOSUCHACTION', :code => 9},
    {:refid => 'MDC_RET_CODE_PROCESSINGFAILURE', :code => 10},
    {:refid => 'MDC_RET_CODE_DUPLICATEMANAGEDOBJECTINSTANCE', :code => 11},
    {:refid => 'MDC_RET_CODE_NOSUCHEVENTTYPE', :code => 13},
    {:refid => 'MDC_RET_CODE_NOSUCHARGUMENT', :code => 14},
    {:refid => 'MDC_RET_CODE_INVALIDARGUMENTVALUE', :code => 15},
    {:refid => 'MDC_RET_CODE_INVALIDSCOPE', :code => 16},
    {:refid => 'MDC_RET_CODE_INVALIDOBJECTINSTANCE', :code => 17},
    {:refid => 'MDC_RET_CODE_MISSINGATTRIBUTEVALUE', :code => 18},
    {:refid => 'MDC_RET_CODE_CLASSINSTANCECONFLICT', :code => 19},
    {:refid => 'MDC_RET_CODE_MISTYPEDOPERATION', :code => 21},
    {:refid => 'MDC_RET_CODE_NOSUCHINVOKEID', :code => 22},

    {:block => 'MDC_PART_RET_CODE/OBJ', :partition => 255, :description => ' Object errors'},
    {:refid => 'MDC_RET_CODE_OBJ_BUSY', :code => 1000, :comment => ' Object is busy so cannot handle the request '},

    {:block => 'MDC_PART_RETURN_CODES', :partition => 255, :description => ' Storage errors'},
    {:refid => 'MDC_RET_CODE_STORE_EXH', :code => 2000, :comment => ' Storage such as disk is full '},
    {:refid => 'MDC_RET_CODE_STORE_OFFLN', :code => 2001, :comment => ' Storage such as  disk is offline '},

    {:block => 'MDC_PART_RETURN_CODES/Unknown', :partition => 255},
    {:refid => 'MDC_RET_CODE_UNKNOWN', :code => 9999, :comment => ' Generic error code '}
  ]
end
